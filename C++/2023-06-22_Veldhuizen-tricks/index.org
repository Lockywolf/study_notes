# -*- mode: org; eval: (visual-line-mode) -*-
# Time-stamp: <2023-06-24 19:54:02 lockywolf>
#+title: 
#+author: 
#+date: 
#+created: <2023-06-22 Thu 23:40>
#+refiled:
#+language: 
#+category: 
#+tags: 
#+creator: Emacs-30050/org-mode-961


Okay, what does Todd Veldhuizen say about c++ tricks in August 2000?


* Introduction

Do not be misled, even though it is the "Introduction", it is also the only one
chapter.

** Static Polymorphism

*** Solution B: the Barton and Nackman Trick

#+begin_src c++
  // Base class takes a template parameter. This parameter
  // is the type of the class which derives from it.
  template<class T_leaftype>
  class Matrix {
   public:
    T_leaftype& asLeaf()
    { return static_cast<T_leaftype&>(*this); }
    double operator()(int i, int j)
    { return asLeaf()(i,j); }
    // delegate to leaf
  };
  class SymmetricMatrix : public Matrix<SymmetricMatrix> {
    ...
  };
  class UpperTriMatrix : public Matrix<UpperTriMatrix> {
    ...
  };
  // Example routine which takes any matrix structure
  template<class T_leaftype>
  double sum(Matrix<T_leaftype>& A);
  // Example usage
  SymmetricMatrix A;
  sum(A);
#+end_src

** Callback inlining techniques
*** Callbacks: the typical C++ approach

1. Veldhuizen is saying that the typical way of passing a function to a, say,
   differentiator, is by inheritance.
2. He is also saying that "expression templates" do not make much sense.
3. STL-style function objects

#+begin_src c++
  class MyFunction {
   public:
    double operator()(double x)
    { return 1.0 / (1.0 + x); }
  };
  template<class T_function>
  double integrate(double a, double b, int numSamplePoints,
                   T_function f)
  {
    double delta = (b-a) / (numSamplePoints-1);
    double sum = 0.;
    for (int i=0; i < numSamplePoints; ++i)
      sum += f(a + i*delta);
    return sum * (b-a) / numSamplePoints;
  }
  // Example use
  integrate(0.3, 0.5, 30, MyFunction());
#+end_src

So, we are _parameterising_ the algorithm with compile-time defined functor object.
Why he insists on overloading operators is still a puzzle for me.

4. Pointer-to-function as a template parameter

#+begin_src c++
  double function1(double x)
  {
    return 1.0 / (1.0 + x);
  }
  template<double T_function(double)>
  double integrate(double a, double b, int numSamplePoints)
  {
    double delta = (b-a) / (numSamplePoints - 1);
    double sum = 0.0;
    for (int i=0; i < numSamplePoints; ++i)
      sum += T_function(a + i*delta);
    return sum * (b-a) / numSamplePoints;
  }
  // ...
  integrate<function1>(1.0, 2.0, 100);
#+end_src

I didn't know that I could write a function type in the template parameter.

** Managing code bloat
*** Put function bodies outside template classes

Template classes can inherit non-template classes.

*** Inlining levels
Lol, he uses #defined to determine inlining levels.
** Containers
This section is very simple, and I am not deleting this section.
*** STL-style containers
*** Data/View containers
** Aliasing and restrict

~In Fortran 77/90, the compiler is able to assume that there is never any aliasing
between arrays. This can make a di erence of 20-50% on performance for in-cache
arrays; it depends on the particular loop/architecture.~


** Traits
*** An example: average()

Okay, Todd explains traits quite well!

*** Type promotion example

Long and annoying example of automatics type promotion for vectors.

** Expression templates
*** Performance implications of pairwise evaluation

Typically scienti c codes are limited by memory bandwidth (rather than
ops), so this really hurts.

*** Recursive templates
Interesting
*** Expression templates: building parse trees
Almost like Scheme.
*** A minimal implementation
A nice demo.
*** Refinements

*** Pointers to more information
*** References
** Template metaprograms

Bizarre fun shit.

*** Template metaprograms: some history
*** The need for specialized algorithms
*** Using template metaprograms to specialize algorithms
** Comma overloading

Bizarre not fun shit.

*** An example

** Overloading on return type

I have needed this for too long.

** Dynamic scoping in C++
*** Some caution
** Interfacing with Fortran codes
** Some thoughts on performance tuning
*** General suggestions
*** Know what your compiler can do
*** Data structures and algorithms
*** Effcient use of the memory hierarchy

