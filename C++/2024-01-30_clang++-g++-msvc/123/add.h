#ifndef ADD_H
#define ADD_H

template<typename T>
class Add
{
public:
	Add(T a, T b);
	T run();

	T& get_a()
	{
		return a;
	}

	T& get_b()
	{
		return b;
	}

	T*& get_c_ptr()
	{
		return c;
	}

private:
	T a;
	T b;
	T* c;
};


template class Add<int>;


#endif
