#include <iostream>
#include "add.h"

//extern template class Add<int>;
//template class Add<int>;

int main(int argc, char* argv[])
{
	Add add(1, 2);

	std::cout << "add.run(): " << add.run() << std::endl;

	// Add1<int> add1(2, 3);

	// //Add1<int> add1 = add;

	// //add1.get_a() = 4;

	// int tmp = add1.get_a();
	// tmp = 5;

	// std::cout << "add1.run1(): " << add1.run1() << std::endl;

	// int c = 5;
	// add.get_c_ptr() = &c;



	return 0;
}
