#include "gtest/gtest.h"
#include <stdexcept>

class MyTest : public testing::Test {
protected:
};

TEST_F(MyTest, first)
{
  throw std::runtime_error("Hello, world:" __FILE__);
  EXPECT_TRUE(true);
}

TEST_F(MyTest, second)
{
  std::cout << 1/0 << std::endl;
  EXPECT_TRUE(true);
}
