#include <iostream>
#if !defined(BOOST_STACKTRACE_ADDR2LINE_LOCATION)
#error BOOST_STACKTRACE_ADDR2LINE_LOCATION not defined
#endif

#define STRINGIFY2(X) #X
#define STRINGIFY(X) STRINGIFY2(X)


int main()
{
  std::cerr << "addr2line=" STRINGIFY(BOOST_STACKTRACE_ADDR2LINE_LOCATION) << std::endl;
  std::cout << "Hello, world" << std::endl;
  int a = 1 / 0 ;
  // std::cerr << a << std::endl;
  throw "hello, world";
  return 0;
}
