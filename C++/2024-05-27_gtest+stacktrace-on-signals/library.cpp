#include <csignal>
#include <iostream>
#include <boost/filesystem.hpp>
#include <boost/stacktrace.hpp>

__attribute__((unused)) static void debug_print_stack_trace()
{
    boost::stacktrace::safe_dump_to("./backtrace.dump");
    std::ifstream ifs("./backtrace.dump");
    boost::stacktrace::stacktrace st = boost::stacktrace::stacktrace::from_dump(ifs);
    std::ofstream ofs("./backtrace.human-readable.txt");
    ofs << "This run crashed:\n" << st << std::endl;
    // cleaning up
    ifs.close();
    ofs.close();
    std::cerr << st << std::endl;
    std::cerr.flush();
}

__attribute__((unused)) static void my_signal_handler(int signum)
{
    ::signal(signum, SIG_DFL);
    debug_print_stack_trace();
    ::raise(signum);
}

__attribute__((constructor)) void init_errors()
{
  //    std::cerr << "installing stack trace handler" << std::endl;
  ::signal(SIGSEGV, &my_signal_handler);
  ::signal(SIGABRT, &my_signal_handler);
  ::signal(SIGFPE, &my_signal_handler);
}
