#include <cmath>
#include <cstdio>
#include <vector>
#include <iostream>
#include <algorithm>
//#include <queue>
#include <cassert>
#include <deque>
using namespace std;

class Graph 
{
    private:
        std::vector< std::vector< int > > connectivity;
    public:
        Graph(int n) 
        {
            //connectivity = std::vector< std::vector< int > >(n);
            for( int i=0; i<n; i++)
            {
                connectivity.push_back( std::vector< int >( n,-1));
            }
            for( int i=0; i<n; i++)
            {
                //connectivity[i][i] = 0;
            }
            
            
        }
    
        void add_edge(int u, int v) 
        {
            connectivity[u][v] = 6;
            connectivity[v][u] = 6;
        }
    
        vector<int> shortest_reach(int start)
        {
            int n = connectivity.size();
            std::vector<int> paths(n, -1);
            
            //std::cout << "debug: graph's size=" << connectivity.size() << std::endl;
            
            //paths[start] = 0;
            
            std::deque< std::pair<int, int> > pq;
            
            pq.push_back( std::make_pair( start, 0) );//one element
            
            //std::cout << "debug: start=" << start << std::endl;
            
            //int step = 1;
            //int cur;
            while( true )
            {
                if( pq.size() == 0)
                    break;
                auto cur = pq.front();
                pq.pop_front();
                
                for( int i=0; i<n; ++i)//перебираем детей
                {
                    if( connectivity[cur.first][i] == -1)
                        continue; //the route cur->i doesn't exist
                    assert( connectivity[cur.first][i] = 6 );
                    if( paths[i] > 0) //the route s->i already found
                        continue;
                    //std::cout << "debug: paths[i]="  << paths[i] << std::endl;
                    assert( paths[i] == -1 );
                    paths[i] = cur.second + 6; //MAGIC hard-coded weight=6
                    pq.push_back(std::make_pair( i, cur.second+6));//детей всех предыдущих вершин рассмотрим раньше
                }
                //++step;
                
            }
            //paths.erase( paths.begin()+start );
            return paths;
        }
    
};

int main() {
    int queries;
    cin >> queries;
        
    for (int t = 0; t < queries; t++) {
      
      int n, m;
        cin >> n;
        // Create a graph of size n where each edge weight is 6: 
        Graph graph(n);
        cin >> m;
        // read and set edges
        for (int i = 0; i < m; i++) {
            int u, v;
            cin >> u >> v;
            u--, v--;
            // add each edge to the graph
            graph.add_edge(u, v);
        }
      int startId;
        cin >> startId;
        startId--;
        // Find shortest reach from node s
        vector<int> distances = graph.shortest_reach(startId);

        for (int i = 0; i < distances.size(); i++) {
            if (i != startId) {
                cout << distances[i] << " ";
            }
        }
        cout << endl;
    }
    
    return 0;
}
