#include <cmath>
#include <cstdio>
#include <vector>
#include <iostream>
#include <algorithm>
#include <regex>
#include <string>
#include <memory>
#include <map>
//using namespace std;

class Node
{
    private:
        std::map< std::string, std::unique_ptr<Node> > children;
        std::map< std::string, std::string> attributes;
        
     
    
    public:
        Node( std::string source_text, std::string attributes_string = "" )
        {
            //parse my own attributes
            //add my attributes to the attributes map
            //split source into tags
            //for each tag, extract: tagname, attribute string, content string
            //for each tag, make new Node, calling the constructor with attribute string and content string
            //and add this Node to the children map
            
            parse_attributes( attributes_string );
            
            //std::cout << "debug::Node()::" << "source_text = " << source_text << std::endl;// << "value" << value << std::endl;
                
            
            while( source_text.size() !=0 )
            {
                //взять первого
                
                int taglen = source_text.find( ' ' );
                int toklen = source_text.find( '>' );
                
                std::string tagname = source_text.substr( 1, std::min(taglen,toklen)-1);
                //std::cout << "debug::Node()::" << "tagname=" << tagname << std::endl;
                
                std::string ch_attrib;
                if( toklen > taglen)//attributes exist
                {
                    ch_attrib = source_text.substr( taglen+1, toklen-(taglen+1));
                }
                else
                {
                    ch_attrib = "";
                }
                
                //std::cout << "debug::Node()::" << "ch_attrib=" << ch_attrib << ", toklen=" << toklen << std::endl;
                std::string searchtoken = "</" + tagname + ">";
                int closing_tag = source_text.find( searchtoken );
                //std::cout << "debug::closing_tag=" << closing_tag << " searchtoken="<< searchtoken <<std::endl;
                //std::string contents = source_text.substr( toklen+1, closing_tag - (toklen+1));
                
                std::string contents = source_text.substr( 0, closing_tag);
                contents.erase(0, toklen+1);
                
                
                //std::cout << std::endl;
                children.emplace( tagname, std::unique_ptr<Node>( new Node( contents, ch_attrib)));
                
                int end_index = closing_tag + 2 + tagname.length() + 1;
                source_text.erase( 0, end_index);
                
                //<tag *> *** </tag>
                    
            }
        }
    
    
        std::string query_tree( std::string request)
        {
            /* 
                case1: ~name
                case2: tag~name
                case2: tag1.tag2~name
            
            */
            //std::cout << "debug::query_tree::request=" << request << std::endl;
            if( request.at(0) == '~')
            {
                //looking for an attribute
                request.erase(0, 1);
                if( attributes.count(request) == 1 )
                {
                    return attributes.at(request);
                }
                else
                {
                    return "Not Found!";
                }
            }
            
            if( request.find('.') == std::string::npos )//dot not found, second case
            {
                int tilde_index = request.find( '~' );
                if( tilde_index == std::string::npos )//no tilde in the string???
                {
                    return "Not Found!";
                }
                std::string tagname = request.substr( 0, tilde_index );
                if( children.count(tagname) == 0)
                {
                    return "Not Found!";
                }
                else
                {
                    request.erase( 0, tilde_index );
                    //auto tag_child = children.at( tagname );
                    //tag_child->query_tree( request ); 
                    return children.at( tagname )->query_tree( request );
                }
            }
            //std::cout << "debug::query_tree::third_case, request=" << request << std::endl;
            if( request.find('.') != std::string::npos )
            {
                int dot_index = request.find('.');
                std::string tagname = request.substr( 0, dot_index );
                //std::cout << "debug::query_tree::third_case, tagname=" << tagname << std::endl;
                request.erase( 0, dot_index+1);
                if( children.count(tagname) == 0)
                {
                    return "Not Found!";
                }
                else
                {
                    //Node* tag_child = children.at(tagname);
                    //tag_child->query_tree( request ); 
                    return children.at(tagname)->query_tree( request );
                }
            }
            return "Not Found!";
        }
    private:
    
        void parse_attributes( std::string tagline )
        {
            std::vector<std::string> strings;
            std::istringstream f( tagline );
            std::string s;
            //std::cout << "debug::parse_attributes:: tagline=" << tagline << std::endl;
            while (std::getline(f, s, ' ')) 
            {
                strings.push_back(s);
                //std::cout << "debug::parse_attributes::" << "name=" << name << "value=" << value << std::endl;
                //attributes.insert( name, value );
            }
            for( int i = 0; i < strings.size(); i++)
            {
                //std::cout << "debug::parse_attributes::strings[i]=" << strings[i] <<std::endl;
                if( strings[i] == "=")
                {
                    std::string attr_name = strings[i-1];
                    std::string attr_value = strings[i+1].substr( 1, strings[i+1].size()-2);
                    
                    attributes.insert( std::make_pair(attr_name, attr_value));
                    //std::cout << "debug::parse_attributes::name-value=(" << attr_name << "," << attr_value << ")" <<std::endl;
                }
            }
        }
};


int main() {
    /* Enter your code here. Read input from STDIN. Print output to STDOUT */   
    std::string my_input;
    int nlines;
    int nqueries;
    
    std::string empty;
    
    std::cin >> nlines >> nqueries;
    std::getline( std::cin, empty);
    for( int i=0; i<nlines; ++i)
    {
        std::string buffer;
        //std::cin >> buffer;
        std::getline( std::cin, buffer);
        
        //std::cout << buffer << std::endl;
        
        my_input += buffer;
    }
    
    //std::cout << "debug::main::" << "HRML ended "<< std::endl;
                
    
    Node my_tree( my_input );
    
    for( int i=0; i<nqueries; ++i)
    {
        std::string buffer;
        std::cin >> buffer;
        
        std::string result = my_tree.query_tree( buffer );
        
        std::cout << result << std::endl;
    }
    
    
    return 0;
}
