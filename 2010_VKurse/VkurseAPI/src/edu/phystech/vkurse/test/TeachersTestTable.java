/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package edu.phystech.vkurse.test;
import edu.phystech.vkurse.model.*;
import java.util.*;

/**
 *
 * @author Дима
 */
public class TeachersTestTable implements TeachersTable
{
    ArrayList<Teacher> data = new ArrayList<Teacher>();
    public TeachersTestTable()
    {
        data.add(new Teacher(0, "Юрий Аммосов", ""));
        data.add(new Teacher(1, "Сергей Коновалов", ""));
        data.add(new Teacher(2, "Юрий Белоусов", ""));
    }

    public boolean insert(Teacher item) throws TableException
    {
        data.add(item);
        return true;
    }

    public boolean update(Teacher item) throws TableException
    {
        int i;
        for (i=0; i<data.size(); ++i)
        {
            if (data.get(i).getID() == item.getID()) data.set(i, item);
        }
        return true;
    }

    public Teacher get(int ID) throws TableException
    {
        int i;
        for (i=0; i<data.size(); ++i)
        {
            if (data.get(i).getID() == ID) return data.get(i);
        }
        return null;
    }

    public boolean remove(int ID) throws TableException
    {
        int i;
        int r = -1;
        for (i=0; i<data.size(); ++i)
        {
            if (data.get(i).getID() == ID) r = i;
        }
        if (r>=0)
        {
            data.remove(r);
            return true;
        }
        else
        {
            return false;
        }
    }

    public java.util.Vector getAll() throws TableException
    {
        Vector r = new Vector();
        int i;
        for (i=0; i<data.size(); ++i)
        {
            r.add(data.get(i));
        }
        return r;
    }
}
