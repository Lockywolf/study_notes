/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package edu.phystech.vkurse.postgresql;

/**
 *
 * @author Дима
 */
public class PgSqlSettings
{
    private static String jdbcDriverClass = "org.postgresql.Driver";
    private static String url = "jdbc:postgresql://localhost/inno2010_vkurse";
    private static String dbName = "inno2010_vkurse";
    private static String username = "innocourse-dtb";
    private static String password = "innocourse-dtb";

    //  For local debugging on ApX's server
    //private static String jdbcDriverClass = "com.mysql.jdbc.Driver";
    //private static String url = "jdbc:mysql://localhost:3306/inno2010_vkurse";
    //private static String dbName = "inno2010_vkurse";
    //private static String username = "root";
    //private static String password = "mysql";

    private PgSqlSettings(){}

    public static String getJdbcDriverClass()
    {
        return jdbcDriverClass;
    }

    public static String getUrl()
    {
        return url;
    }

    public static String getDbName()
    {
        return dbName;
    }

    public static String getUsername()
    {
        return username;
    }

    public static String getPassword()
    {
        return password;
    }

    public static void setJdbcDriverClass(String jdbcDriverClass)
    {
        PgSqlSettings.jdbcDriverClass = jdbcDriverClass;
    }

    public static void setUrl(String url)
    {
        PgSqlSettings.url = url;
    }

    public static void setDbName(String dbName)
    {
        PgSqlSettings.dbName = dbName;
    }

    public static void setUsername(String username)
    {
        PgSqlSettings.username = username;
    }

    public static void setPassword(String password)
    {
        PgSqlSettings.password = password;
    }
}
