#!/usr/bin/env awk -M

function adequate_math_precision(n)
{
  return (1 != (1+(1/(2^(n-1)))))
}

BEGIN {
#  print "PROCINFO[prec_max]=" PROCINFO["prec_max"]
  fpbits = 123
  PREC = fpbits
  if (! adequate_math_precision(fpbits)) {
    print("Error: insufficient computation precision available.\n" \
          "Try again with the -M argument?") > "/dev/stderr"
    # Note: you may need to set a flag here to bail out of END rules
    exit 1
  }

}
{if ($1 == "total") {printf 2^64 ; exit 0}
  if ( ($1 < 1) || ($1 > 64))
  {
    print "error" ; exit 1;
  }
  print 2^$1
}
