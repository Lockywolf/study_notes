BEGIN {
  c["black"]=0
  c["brown"]=1
  c["red"]=2
  c["orange"]=3
  c["yellow"]=4
  c["green"]=5
  c["blue"]=6
  c["violet"]=7
  c["grey"]=8
  c["white"]=9
  FS=" "
}
{#print "input=" $0
  flag=0
  for( v in c ) { if ( v == $1 ) flag=1 }
  if (flag != 1) {print "invalid color" ; exit 1 }
 print c[$1]*10 + c[$2]}
