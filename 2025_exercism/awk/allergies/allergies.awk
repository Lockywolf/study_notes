  function d2b(d,  b) {
    while(d) {
      b=d%2b
      d=int(d/2)
    }
    return(b?b:0)
  }
  function rev( s,  x, i ) {
    for(i=length(s);i!=0;i--) x=x substr(s,i,1);
  return(x)
  }
BEGIN {
  FS=","
  a[1]="eggs"
  a[2]="peanuts"
  a[3]="shellfish"
  a[4]="strawberries"
  a[5]="tomatoes"
  a[6]="chocolate"
  a[7]="pollen"
  a[8]="cats"

  b["eggs"]=1
  b["peanuts"]=2
  b["shellfish"]=3
  b["strawberries"]=4
  b["tomatoes"]=5
  b["chocolate"]=6
  b["pollen"]=7
  b["cats"]=8

}
{
  map = rev(d2b($1))
  bin = d2b($1)
  reversed = rev(bin)
  rec = substr(reversed,1,8)
#  print "line=" $0 " NF=" NF " score=" $1 " binary:" bin " reversed=" reversed " rec:" rec  " command=" $2 " argument=" $3
  if ( $2 == "allergic_to")
  {
    idx = b[$3];
    if( substr(rec,idx,1) == "1" ) print "true"
    else print "false"
    exit 0
  }
  for( i=1; i<=length(rec); i++)
  {
    if( substr(rec, i, 1) == "1" ) {
      #printf("%s (length(rec)=%s)", a[i], length(rec));
      idx++
      output[idx]=a[i]
      }
    }
  
  for( i=1 ; i<= length(output) ; i++)
  {
    printf( "%s", output[i])
    if( i < length(output) )
      printf( ",")

  }

  }
