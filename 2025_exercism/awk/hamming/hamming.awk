BEGIN {
  getline line1
  getline line2
  if (length(line1) != length(line2)) { print "strands must be of equal length" ; exit 1}
  len = length(line1)
  dist = 0
  for( i=1;i<=len;i++)
  {
    if ( substr(line1,i,1) != substr(line2,i,1) ) dist++
  }
  print dist
}

