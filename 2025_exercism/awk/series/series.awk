# These variables are initialized on the command line (using '-v'):
# - len

BEGIN {
#  print "len=" len
  getline line
#  print "line=" line
  if ( line == "" ) { printf "series cannot be empty" ; exit 1}
  if( len > length(line) ) { printf "invalid length" ; exit 1}
  if( len == 0 ) { printf "invalid length" ; exit 1}
  if ( len < 0 ) { printf "invalid length" ; exit 1}
  for (i=1; i+len-1 <= length(line) ; i++) {
    printf( "%s", substr(line,i,len))
    if ( i+len-1 < length(line) ) printf (" ")
  }
}


