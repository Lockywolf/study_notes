//#include <stdio.h>
#include <mpi.h>
#include <io.h>
#include <windows.h>
#include <CL/cl.hpp>

#include <cstdio>
#include <cstdlib>
#include <fstream>
#include <iostream>
#include <string>
#include <iterator>


inline void checkErr(cl_int err, const char * name)
{
    if (err != CL_SUCCESS) {
        std::cerr << "ERROR: " << name << " (" << err << ")" << std::endl;
        exit(EXIT_FAILURE);
    }
}

typedef enum MSGTYPES
{
	HOSTNAME,
	CPUS,
	GPUS
} MSGTYPES;

typedef struct clientinfo
{
	char processor_name[MPI_MAX_PROCESSOR_NAME];
	int cpus;
	int gpus;
} clientinfo;

int main(int argc, char *argv[]) {
  int numprocs, rank, namelen;
  char processor_name[MPI_MAX_PROCESSOR_NAME];
  int numcpus = 0;
  SYSTEM_INFO sysinfo;
  
  //std::vector<cl::Platform> platforms;
  //cl::Platform::get(&platforms);

  MPI_Init(&argc, &argv);
  MPI_Comm_size(MPI_COMM_WORLD, &numprocs);
  MPI_Comm_rank(MPI_COMM_WORLD, &rank);
  MPI_Get_processor_name(processor_name, &namelen);
  
  
  GetSystemInfo(&sysinfo);
  numcpus = sysinfo.dwNumberOfProcessors;


  //printf("Process %d on %s out of %d\n", rank, processor_name, numprocs);
	if( rank == 0 )
	{
		///printf("I am the master\n");
		clientinfo* recd_data = (clientinfo*) malloc( numprocs * sizeof(clientinfo) );
		MPI_Status* statuses = (MPI_Status*) malloc( numprocs * sizeof(MPI_Status) );
		//MPI_Status statuses[numprocs];
		int i = 0;
		for( i = 1; i < numprocs; i++ )
		{	
			int mycount = MPI_MAX_PROCESSOR_NAME;
			int res;
			//recd_data[i] = (char*) malloc( mycount * sizeof(char) );
			//int getttt;

			res = MPI_Recv( recd_data[i].processor_name, mycount, MPI_CHAR, i, HOSTNAME, MPI_COMM_WORLD, &(statuses[i]) );
			res = MPI_Recv( &recd_data[i].cpus, sizeof(int) , MPI_INT, i, CPUS, MPI_COMM_WORLD, &(statuses[i]) );
			//res = MPI_Recv( &recd_data[i].cpus, 1, MPI_CHAR, i, CPUS, MPI_COMM_WORLD, &(statuses[i]) );
		}
		MPI_Barrier( MPI_COMM_WORLD );
		for( i = 1; i < numprocs; i++ )
		{
			printf( "Received hostname from slave %d:%s, cpus:%d\n", i, recd_data[i].processor_name, recd_data[i].cpus );
		}
		
	}
	else
	{
		//printf( "I am the slave number %d of %d\n ", rank, numprocs);
		int result =  MPI_Send( processor_name , MPI_MAX_PROCESSOR_NAME, MPI_CHAR, 0, HOSTNAME, MPI_COMM_WORLD);
		result =  MPI_Send( &numcpus , sizeof(int), MPI_INT, 0, CPUS, MPI_COMM_WORLD);
		MPI_Barrier( MPI_COMM_WORLD );
	}
  
  MPI_Finalize();
}
