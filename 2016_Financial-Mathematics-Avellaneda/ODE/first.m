function first()
xdot = inline('t-x^2', 't', 'x');
ydot = @(t,y)t-y^2;
[t, x] = ode45( xdot, [0, 5], 0);
[t2, x2] = ode45( ydot, [0, 5], 0);

nu = 1;


[x3, y3] = ode45( @(x, y) mybessel1(x, y, nu) , [0.5, 20], [3;1] );
% hold on;
% plot( x3, y3, '-o');
% plot( x3, y3(:,1), '-o', x3, y3(:,2), '-.');
[X, Y, Z] = besselfield();
scatter3(X, Y, Z );
% ylim( [-5 5] );

end

function dydx = mybessel1( x, y, nu)

% 	nu = 1;
	dydx = [y(2); -(1/x)*y(2) - (1- (nu^2/x^2))*y(1)];
end

function [ X, Y, Z] = besselfield()
	nu = [-1:0.1:1];
% 	nu = 1;
	X = [];
	Y = [];
	Z = [];
	for p = 1:numel(nu)
	    [x3, y3] = ode45( @(x, y) mybessel1(x, y, nu(p)) , [0.5, 20], [3;1] );
 		X = [X, x3'];
		Y = [Y, ones( 1, numel(x3))*nu(p)];
		Z = [Z, y3(:,1)'];
% 		Z(p,:) = y3(:,1);
% 		numel(x3)

	end
% 	Y = repmat( nu', size( x3, 2) );
	
end
