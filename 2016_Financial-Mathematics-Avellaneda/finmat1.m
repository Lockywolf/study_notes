%% Financial Functions Example
% Trying to imitate stuff from "Financial Calculus". I will try to imitate
% all the examples from the fook, by chapters. Mainly: forward( future )
% and call option. For every chapter I will try to make one pricing
% function myself and one pricing function I will take from the Matlab
% toolbox.

%% Section 1: Obtaining Data
% I use the Datafeed toolbox to get data from Yahoo Finance.
c = yahoo;
sec = 'IBM';
d = fetch(c,sec); %getting real-world data

field = 'Close'; % retrieve closing price data
fromdate = '01/01/2002'; % beginning of date range for historical data 
todate = '06/30/2012'; % ending of date range for historical data 

d = fetch(c,sec,field,fromdate,todate);

close(c);
plot( d(:,2));


%% Section 2 Title
% Description of second code block
a=2;
