function [ activations_vector ] = my_softmax( probabilities_vector )
%MY_SOFTMAX Manual implementation of softmax renormalization
%   I implement it here because I want my MLP implementation to be as much
%   independent as possible from matlab.

my_enumerator = exp( probabilities_vector );
activations_vector = my_enumerator / sum(my_enumerator);


end

