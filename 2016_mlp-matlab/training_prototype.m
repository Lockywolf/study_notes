function [ my_model, my_quality, total_loss ] = training_prototype( my_model, dataset_to_be_classified, labels )
%TRAINING_PROTOTYPE attempts to train a multi(2)layer perceptron

%Have a look at the 'evaluate_model_once'. Since most of the operations are
%either linear or vectorized inside matlab, we can compute a C-by-N matrix
%of predictions for all the dataset, where C is the number of classes and Z
%is the size of the dataset

%Have a loot at the compute_total_loss(). This function tries to implement
%basic model fitting for a two-layer perceptron

%2017.04.30 - I started writing this function and wrote the forward pass.
%2017.05.01 - I try to write a simple gradient descent backward pass with full batch

learning_rate = 3;
number_of_epochs = 300;

for k=1:number_of_epochs
	%layer 0
	%the next line seems to be just Ax+b. However, in fact it is computing a
	%linear part of activations of every neuron. N neurons * M input dendrites
	%* Z where Z is the size of the dataset
	linear_activations_0 = my_model.layer0_weights*dataset_to_be_classified + my_model.layer0_bias;
	%linear_activations_0 is a matrix of activations (C by Z). We should apply
	%nonlinearity elementwise
	%as linearity I take a hyperbolic tangent

	full_activations_0 = tanh( linear_activations_0 );

	%layer 1
	%same as layer0, but the non-linearity here is renormalizing
	linear_activations_1 = my_model.layer1_weights*full_activations_0 + my_model.layer1_bias;
	full_activations_1 = my_softmax_vectorized( linear_activations_1);
	classes_probabilities = full_activations_1;
	%classes_probabilities should be C-by-Z. 
	[~, predicted_class] = max( classes_probabilities );
	%predicted confidence  and predicted class should be 1-by-Z

	%In fact, there should be a double sum here, (but!) since our correct
	%distribution is degenerate, that is, it has 1 (one) at correct labels and
	%0 elsewhere, we can sum over only nonzero classes.
	correct_distribution = zeros( size( classes_probabilities ) );
	indices = sub2ind(size(correct_distribution), labels+1, 1:numel(labels));
	total_loss = -1*sum(log(classes_probabilities( indices )))/numel(labels);
	correct_distribution( indices ) = 1.0;

	my_quality = sum( predicted_class == labels+1 )/60000.0; % measuring average quality

	fprintf( 'Iteration = %d, quality is %f \n', k, my_quality );
	% total_loss_2 = -1*sum( sum (correct_distribution .* log(classes_probabilities))) / size(correct_distribution, 2);

	%2017.05.01 - learning with vanilla gradient descent

	delta = classes_probabilities - correct_distribution;

	gradients_of_layer1_bias = sum(delta, 2)/size(delta, 2);
	gradients_of_layer1_weights_fullbatch(:,:) = zeros( size(delta,1), size(full_activations_0,1));
	for i=1:size(full_activations_0,2)
		gradients_of_layer1_weights_fullbatch(:,:) = gradients_of_layer1_weights_fullbatch(:,:) + delta(:,i)*full_activations_0(:,i)';
	end


	gradients_of_layer1_weights(:,:) = gradients_of_layer1_weights_fullbatch/size(full_activations_0,2);

	%gradients_for_layer0

	delta2 = (ones( size( full_activations_0)) - full_activations_0.^2).*(my_model.layer1_weights'*delta(:,:));

	gradients_of_layer0_bias = sum(delta2, 2)/size(delta2, 2);

	gradients_of_layer0_weights_fullbatch(:,:) = zeros( size(my_model.layer0_weights ) );
	for i=1:size(dataset_to_be_classified,2)
	% 	fprintf( 'Epoch NULL, w0, iteration %d\n', i);
		gradients_of_layer0_weights_fullbatch(:,:) = gradients_of_layer0_weights_fullbatch(:,:) + delta2(:,i)*dataset_to_be_classified(:,i)';
	end

	gradients_of_layer0_weights = gradients_of_layer0_weights_fullbatch(:,:)/size(dataset_to_be_classified,2);

	% fprintf( '\nSanity check: total_loss_optimized=%f, total_loss_brute=%f \n', total_loss, total_loss_2);
	
	%final truck: adding the gradients
	
	my_model.layer0_bias = my_model.layer0_bias - learning_rate*gradients_of_layer0_bias;
	my_model.layer0_weights = my_model.layer0_weights - learning_rate*gradients_of_layer0_weights;
	my_model.layer1_bias = my_model.layer1_bias - learning_rate*gradients_of_layer1_bias;
	my_model.layer1_weights = my_model.layer1_weights - learning_rate*gradients_of_layer1_weights;
	
	
end

%We are done, right? now we just return the model?


end
