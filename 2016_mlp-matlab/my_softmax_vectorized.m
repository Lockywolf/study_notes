function [ activations_vector ] = my_softmax_vectorized( probabilities_matrix )
%MY_SOFTMAX Manual implementation of softmax renormalization
%   I implement it here because I want my MLP implementation to be as much
%   independent as possible from matlab.

my_enumerator = exp( probabilities_matrix );
activations_vector = my_enumerator ./ sum(my_enumerator);


end

