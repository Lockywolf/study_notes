function [ prediction_confidence, predicted_class, classes_probabilities ] = evaluate_model_once( my_model, image_to_be_classified )
%EVALUATE_MODEL_ONCE predicts the class of one example
%   Written as an MLP exersise, this function takes a model, which holds
%   weights and biases for two layers (one hidden layer) and computes the 

%manually compute classification

%layer 0
%the next line seems to be just Ax+b. However, in fact it is computing a
%linear part of activations of every neuron. N neurons * M input dendrites
linear_activations_0 = my_model.layer0_weights*image_to_be_classified + my_model.layer0_bias;
%linear_activations_0 is a list of activations. We should apply
%nonlinearity elementwise
%as linearity I take a hyperbolic tangent

full_activations_0 = tanh( linear_activations_0 );

%layer 1
%same as layer0, but the non-linearity here is renormalizing
linear_activations_1 = my_model.layer1_weights*full_activations_0 + my_model.layer1_bias;
full_activations_1 = my_softmax_vectorized( linear_activations_1);
classes_probabilities = full_activations_1;
[prediction_confidence, predicted_class] = max( classes_probabilities );


end

