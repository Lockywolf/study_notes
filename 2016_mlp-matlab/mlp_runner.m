%MLP_RUNNER A script to organize my training exercise with MLP, LSTM and a
%CNN
%   This is the root file of the exersise arrangement.

%gist of the idea
%weights = my_train();

%save( weights, 'weights_file.mat');

%learning_rate = 0.1

%first attemps
load train_data.mat
feature_vector_length = size( images, 1 )*size(images, 2);
test_set_length = size(images, 3);

flattened_images = double(reshape( images,...
					feature_vector_length,...
					test_set_length ));


normalized_flattened_images = double(flattened_images)/double(max(max(flattened_images)));


%normalized_flattened_images = double(flattened_images) ./ ;
% normalized_flattened_images = normalized_flattened_images - mean( mean( normalized_flattened_images ));

number_of_hidden_units = 30;%how many do I need?
number_of_classes = 10;

rng( 129);

my_model = struct( 'layer0_weights', rand(number_of_hidden_units, feature_vector_length )-0.5,...
				'layer0_bias', rand(number_of_hidden_units,1)-0.5,...
				'layer1_weights', rand(number_of_classes, number_of_hidden_units )-0.5,...
				'layer1_bias', rand(number_of_classes,1)-0.5...
			);

%how to make a random prediction?

%just for now we define an image to make our code valid. 
%in the future this should be replaced with a real test set
%and the code should be moved to a separate evaluation function

image_to_be_classified = normalized_flattened_images(:,1);

[ prediction_confidence, predicted_class, classes_probabilities ] = evaluate_model_once( my_model, image_to_be_classified );

%fprintf( 'Predicted class is %d\n', predicted_class );
%classes_probabilities

%now we will try to compute total loss on all the training dataset

[ prediction_confidence_training, predicted_class_training, classes_probabilities_training, total_loss ] = compute_total_loss( my_model, normalized_flattened_images, labels );

% int64( total_loss )

fprintf( '\nInitial total loss is %d\n', total_loss);

%now we are trying to do the actual training
%the API is very poor for now
trained_model = training_prototype(	my_model, normalized_flattened_images, labels);

