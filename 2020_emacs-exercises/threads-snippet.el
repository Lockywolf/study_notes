
;; Time-stamp: <2023-12-07 13:18:45 lockywolf>
;;#+created: <2023-12-07 Thu>

(defvar lwf-exit-thread nil)
(defvar lwf-thread-id nil)
(defun lwf-thread-demo ()
  (let ((inhibit-quit t))
        (message "lwf-entered-thread")
        (while (not lwf-exit-thread)
          (sleep-for 5)
          (message "lwf-slept-in-thread")
          (thread-yield))
        (message "lwf-ended-thread")))

(setf lwf-thread-id (make-thread #'lwf-thread-demo "lwf-name"))

(provide 'threads-snippet)

