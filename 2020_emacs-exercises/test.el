(let ((my_memory (current-buffer)))
  (set-buffer "*scratch*")
  (goto-char (point-min))
  (set-buffer my_memory))
(cl-assert
 (equalp
  (point-min)
  (with-current-buffer "*scratch*"
    (point))))
(save-current-buffer
  (set-buffer "*scratch*")
  (let
      ((inhibit-read-only t))
    (insert (make-string 100 ?s))
    (redisplay))) ; insert left the point after the inserted piece of text
(let ((my_memory (current-buffer)))
  (set-buffer "*scratch*")
  (cl-assert
   (not (equalp
	 (point-min)
	 (point))))
  (set-buffer my_memory))
