#include <vector>
#include <list>
#include <map>
#include <set>
#include <deque>
#include <queue>
#include <stack>
#include <bitset>
#include <algorithm>
#include <functional>
#include <numeric>
#include <utility>
#include <sstream>
#include <iostream>
#include <iomanip>
#include <cstdio>
#include <cmath>
#include <cstdlib>
#include <ctime>
#include <math.h>
#include <limits.h>
#include <hash_map>
 
using namespace std;
 
// Datatypes
typedef vector<int> vi; 
typedef vector<string> vs;
typedef vector<vi> vvi; 
typedef pair<int,int> ii; 
typedef long long ll;
typedef long double ld;
 
// Define
#define sz(a)             int((a).size()) 
#define pb                push_back 
#define all(c)            (c).begin(),(c).end() 
#define present(c,x)      ((c).find(x) != (c).end()) 
#define cpresent(c,x)     (find(all(c),x) != (c).end()) 
#define FOR(_i,_n)        for(int (_i) = 0; (_i) < (_n); (_i)++)
#define mp                make_pair
 
// Constants
const double eps = 1e-8; 
const double PI = 3.1415926535897932384626433832795;
 
#define WHITE 0
#define GREY  1
#define BLACK 2
 
vector< vector<int> > g;
vector< int > color;
vector< int > order;
 
int main(void)
{
    int N, M;
 
    cin >> N >> M;
    g.resize(N);
    color.resize(N);
 
    for(int i = 0; i < M; i++)
    {
        int u,v;
        cin >> u >> v;
        g[u-1].pb(v-1);
    }
 
    for(int i = 0; i < N; i++)
    {
        int u;
        cin >> u;
        order.pb(u-1);
    }
 
    reverse(order.begin(), order.end());
 
    for(int i = 0; i < N; i++)
    {
        int u = order[i];
 
        for(int i = 0; i < sz(g[u]); i++)
        {
            if(color[g[u][i]] != BLACK )
            {
                cout << "NO";
                exit(0);
            } 
        } 
 
        color[u] = BLACK;
    }
 
    cout << "YES";
 
    return 0;
}
