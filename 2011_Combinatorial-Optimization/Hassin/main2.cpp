#define _USE_MATH_DEFINES
#include <vector>
#include <iostream>
#include <algorithm>
#include <math.h>
#include<stdio.h>
#include<map>
#include<utility>
using namespace std;
struct point
{
	double x;
	double y;
};
struct edge
{
	int num_vertex_from;
	int num_vertex_to;
	int capacity;
	//vector facets;
};



int N;//кол-во станций// число вершин
int M;//кол-во нефтепроводов
vector <point> coord_vect;
vector <edge> edges_vect;
vector <vector<int> > g; // граф
//vector < vector<int> > facets;
class dupoint
{
	public:
		vector<int> facet;
		vector<int> connects;
};
vector <dupoint> dugraph;
map< pair< int, int>, vector<int> > edges2;
	
class cmp_ang {
	int center;
public:
	cmp_ang (int center1) : center(center1)
	{ }
	bool operator() (int a, int b) const {
		//double ang1 = atan2(coord_vect[a].y - coord_vect[center].y, coord_vect[a].x - coord_vect[center].x) + M_PI ;
		//double ang2 = atan2(coord_vect[b].y - coord_vect[center].y, coord_vect[b].x - coord_vect[center].x) + M_PI ;
		
		double ang1 = atan2(coord_vect[a].x - coord_vect[center].x, coord_vect[a].y - coord_vect[center].y)  ;
		double ang2 = atan2(coord_vect[b].x - coord_vect[center].x, coord_vect[b].y - coord_vect[center].y)  ;
		//... должна возвращать true, если точка a имеет
		//меньший чем b полярный угол относительно center ...
		bool result;
		if(ang1 < ang2)
		{
			result=true;
		}
		else
		{
			result=false;
		}
		//cout << result;
		return result;
	}
};

void init()
{
	scanf("%d", &N);
	for(int i = 1; i <= N; i ++)
	{
		int x,y;
		scanf("%d%d", &x, &y);
		point newpoint;
		newpoint.x = x;
		newpoint.y = y;
		coord_vect.push_back(newpoint);
		vector<int> tt;
		g.push_back(tt);
	}
	//S = 1;
	//T = N;
	scanf("%d", &M);
	for(int i = 0; i < M; i ++)
	{
		int a, b, w;
		scanf("%d%d%d", &a, &b, &w);
		edge newedge;
		newedge.num_vertex_from = a-1;
		newedge.num_vertex_to = b-1;
		newedge.capacity = w;
		g[a-1].push_back(b-1);
		g[b-1].push_back(a-1);
		//add(a, b, w);
		//add(b, a, w);
	}

}

void build()
{
 
	int N1 = g.size();//всего вершин
	vector < vector<bool> > used (N1);//Использованные
	for (int i=0; i < N1; ++i)//Забиваем массив пустотой
	{
		//used[i].resize (g[i].size());
		for(int j=0; j < g[i].size(); j++)
		{
			used[i].push_back(false);
		}
	}
	for (int i=0; i<N1; ++i)//перебираем вершины. все
	{
	//printf("\nworking on vertex %d, face:", i ); 
		for (size_t j=0; j<g[i].size(); ++j)//Проходим по коннектам _от_ этой вершины
			if (!used[i][j]) //Если по этому коннекту еще не гуляли
			{
 				used[i][j] = true;//Гуляем
 				int v = g[i][j],  pv = i;//pv - parent vertex, v - vertex типа, из pv пришли, v - куда пойдем
 				vector<int> facet;//набор вершин в грани
 				for (;;) 
				{
 					facet.push_back (v);//Хмм.... проталкиваем v? а почему не сначала 
					sort ( g[v].begin(), g[v].end(), cmp_ang(v));
					vector<int>::iterator it = g[v].begin();
					for (; it != g[v].end();it++)
					{
						if (*it == pv) 
						{
							break;
						}
					}
 					//vector<int>::iterator it = lower_bound (g[v].begin(), g[v].end(), pv, cmp_ang(v));
					if (++it == g[v].end())  it = g[v].begin();
 					if (used[v][it-g[v].begin()])  break;
					if (used[v][it-g[v].begin()])  break;
 					used[v][it-g[v].begin()] = true;
 					pv = v,  v = *it;
 				}
				for(vector<int>::iterator it = facet.begin();it!=facet.end(); it++)
				{
					cout <<  *it << " ";
				}
				cout << endl;
				//... вывод facet - текущей грани ...
				dupoint point;
				point.facet = facet;
				dugraph.push_back( point );
				for( int i=0; i< facet.size()-1; i++)
				{
					if( facet[i] <= facet[i+1] )
					{
						edges2.insert( make_pair(make_pair( facet[i] , facet[i+1]), dugraph.size()-1 ) );
					}
					else
					{
						edges2.insert( make_pair(make_pair( facet[i+1] , facet[i]), dugraph.size()-1) );
					}
				}
				if( facet[facet.size()-1] <= facet[0] )
				{
					edges2.insert( make_pair(make_pair( facet.size()-1 , facet[0]), dugraph.size()-1 ));
				}
				else
				{
					edges2.insert( make_pair(make_pair( facet[0] , facet[facet.size()-1]), dugraph.size()-1 ));
				}
				
				
 					
			}
	}
}

int main()
{
	init();
	build();
	int h;
	cin >> h;
	return 0;
}
