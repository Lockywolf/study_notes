#include <cstdio>
#include <cstring>
#define maxn 101
using namespace std;
bool g[maxn][maxn];
int tar[maxn];
bool act=1;
int n,res;
bool bo[maxn];
int bel[maxn],resp[maxn];
void dfs(int s,int p)
{
bo[s]=1;
for(int i=1;i<=n;i++)
if(g[s][i]&&!bo[i])
{
g[p][i]=1;
dfs(i,p);
}
}
bool find(int p)
{
bo[p]=1;
for(int i=1;i<=n;i++)
if(g[p][i])
if(!bel[i]||(!bo[bel[i]]&&bel[i]!=p&&find(bel[i])))
{
tar[p]=i;
bel[i]=p;
return 1;
}
return 0;
}
inline void search(int p)
{
while(p!=0&&!bo[p])
{
bo[p]=1;
p=tar[p];
}
}
inline int getfather(int p)
{
while(p!=tar[p])
p=tar[p];
return p;
}
int main()
{
#ifndef ONLINE_JUDGE
freopen("input.in","r",stdin);
freopen("output.out","w",stdout);
#endif
scanf("%d\n",&n);
int i,j;
for(i=1;i<=n;i++)
for(j=1;j<=n;j++)
scanf("%d",&g[i][j]);
for(i=1;i<=n;i++)
{
memset(bo,0,sizeof(bo));
dfs(i,i);
}
for(i=1;i<=n;i++)
{
memset(bo,0,sizeof(bo));
if(!find(i))
{
resp[++res]=i,tar[i]=i;
// printf("%d ",i);
}
}
// printf("\n");

act=1;
while(act)
{
memset(bo,0,sizeof(bo));
act=0;
for(int i=1;i<=n;i++)
bo[getfather(i)]=1;
for(int i=1;i<=n;i++)
if(bo[i])
for(int j=1;j<=n;j++)
if(g[i][j]&&bo[j])
{
tar[bel[j]]=bel[j];
tar[j]=0;
bo[j]=0;
act=1;
}
}
memset(bo,0,sizeof(bo));
for(int i=1;i<=n;i++)
bo[getfather(i)]=1;
int temp=0;

printf("%d\n",res);
for(int i=1;i<=n;i++)
if(bo[i])
{
printf("%d",i);
++temp;
if(temp==res)
printf("\n");
else
printf(" ");
}

/*
if(temp!=res)
printf("Wrong!!!\n");*/
return 0;
}