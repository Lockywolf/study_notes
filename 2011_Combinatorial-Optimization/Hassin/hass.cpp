#include<stdio.h>
#include<vector>
#include<math.h>
#include <iostream>
#include <algorithm>
using namespace std;
int n; // число вершин
int m; //число труб
vector < vector<int> > g; // граф
vector < vector<char> > used (n);

//extern class LWEdge{};
/*class LWConn
{
	public:
		void* tover;
		int capacity;
};*/

class LWVertex
{
	public:
		int x;
		int y;
		int number;
		vector< LWVertex*>* graph; 
	LWVertex( int l_x, int l_y, int l_number, vector< LWVertex*>* papa )
	{
		graph = papa;
		x = l_x;
		y = l_y;
		number = l_number;
	}
	//vector <LWVertex*> connections;
	//vector <void*> near_edges;
	class LWConn
	{	
		public:
			int tover;
			int capacity;
			int used;
		LWConn( int to, int cap)
		{
			used = 0;
			capacity = cap;
			tover = to;
		}
	};
	class cmp_ang
	{
		public:
			int x;
			int y;
			vector< LWVertex*>* graph; 
		cmp_ang(int xa, int ya, vector< LWVertex*>* papa)
		{
			x = xa;
			y = ya;
			graph = papa;
		}
		bool operator() (const LWConn* a, const LWConn* b) 
		{
			int x_a = (*graph)[ a->tover ]->x - x;
			int y_a = (*graph)[ a->tover ]->y - y;
			int x_b = (*graph)[ b->tover ]->x - x;
			int y_b = (*graph)[ b->tover ]->y - y;
			double angle_a = atan2( x_a, y_a  );
			double angle_b = atan2( x_b, y_b);
			if( angle_a <= angle_b ) return true;
			return false;
		}
	};
		
	
	vector <LWConn*> conns;
	int sort_con()
	{
		sort( conns.begin(), conns.end(), cmp_ang(x, y, graph) );
	}
};



vector <LWVertex*> vertices;
/*
class LWEdge
{
	public:
		int capacity;
		LWVertex* a;
		LWVertex* b;
		int num_a;
		int num_b;
	LWEdge( int left, int right, int objom )
	{
		a = vertices[left];
		b = vertices[right];
		num_a = left;
		num_b = right;
		capacity = objom;
	}
};

vector <LWEdge*> edges;
*/

class LWFace
{
	public:
		vector <LWVertex*> is_of;
		
};


void init()
{
	if( scanf("%d", &n ) != 1)
        {
		printf("wrong number string\n");
		
	}
	for(int i = 1; i <= n; i ++)
        {
		int x,y;
		scanf("%d%d", &x, &y);
		vertices.push_back ( new LWVertex( x, y, i, &vertices) );
	}	
	scanf("%d", &m); //Трубы
	{
		int a,b,w;
		scanf("%d%d%d", &a, &b, &w);
		a--;
		b--;
//		edges.push_back( new LWEdge( a, b, w ));
		(vertices[a]->conns).push_back( new LWVertex::LWConn( b, w) );
		(vertices[b]->conns).push_back( new LWVertex::LWConn( a, w) );
	}
	for( int i =  0; i < vertices.size() ; i++ )
	{
		vertices[i]->sort_con();
	
	}
}

vector< LWFace > faces; 
LWFace* curface = NULL;
bool working = false;



void eat( int ver)
{
	if( ver == curface->is_of[0]->number )
	{
		return;
	}
	int prev = curface->is_of[ curface->is_of.size() - 1 ]->number;
	//int nn = myfind( vertices[ver]->conns, prev );//beware!
	int nn = -1;
	for( int i=0; i< vertices[ver]->conns.size(); i++)
	{
		if( ( vertices[ver] -> conns[ i ] -> tover) == prev )
		{
			nn = i;
			break;
		}
	}
	
	//int nn = lower_bound( vertices[ver]->conns.begin(), vertices[ver]->conns.end(),  
	if( nn == 0 )
	{
		vertices[ver]->conns[vertices[ver]->conns.size()-1]->used = true;
		eat( vertices[ver]->conns[vertices[ver]->conns.size()-1]->tover );
	}
	else
	{
		vertices[ver]->conns[ nn-1 ]->used = true;
		eat( vertices[ver]->conns[ nn-1 ]->tover );
	}
}

void create_faces()
{
	int curver = 0;
	for( curver = 0; curver< n; curver++ )
	{
		for( int i = 0; i< vertices[curver]->conns.size(); i++ )
		{
			if( vertices[ curver ]->conns[ i ]->used != true )
			{
				//значит, у вершины остались чистые коннекты. можно юзать
				curface = new LWFace();
				curface->is_of.push_back( vertices[curver] );
				vertices[ curver ]->conns[ i ]->used = true;
				eat(vertices[ curver ]->conns[ i ]->tover);
				faces.push_back( *curface );
				delete curface;
			}
		}
	}
}

int main()
{
	init();
	create_faces();
	//printf( "hello, world\n");
	return(0);
}
