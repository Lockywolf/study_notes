#define _USE_MATH_DEFINES
#include <vector>
#include <iostream>
#include <algorithm>
#include <math.h>
#include<stdio.h>
using namespace std;
struct point
{
	double x;
	double y;
};
struct edge
{
	int num_vertex_from;
	int num_vertex_to;
	int capacity;
};

int N;//���-�� �������// ����� ������
int M;//���-�� �������������
vector <point> coord_vect;
vector <edge> edges_vect;
vector <vector<int> > g; // ����
class cmp_ang {
	int center;
public:
	cmp_ang (int center1) : center(center1)
	{ }
	bool operator() (int a, int b) const {
		double ang1 = atan2(coord_vect[a].y - coord_vect[center].y, coord_vect[a].x - coord_vect[center].x) + M_PI ;
		double ang2 = atan2(coord_vect[b].y - coord_vect[center].y, coord_vect[b].x - coord_vect[center].x) + M_PI ;
		//... ������ ���������� true, ���� ����� a �����
		//������� ��� b �������� ���� ������������ center ...
		bool result;
		if(ang1 < ang2)
		{
			result=true;
		}
		else
		{
			result=false;
		}
		//cout << result;
		return result;
	}
};

void init()
{
	scanf("%d", &N);
	for(int i = 1; i <= N; i ++)
	{
		int x,y;
		scanf("%d%d", &x, &y);
		point newpoint;
		newpoint.x = x;
		newpoint.y = y;
		coord_vect.push_back(newpoint);
		vector<int> tt;
		g.push_back(tt);
	}
	//S = 1;
	//T = N;
	scanf("%d", &M);
	for(int i = 0; i < M; i ++)
	{
		int a, b, w;
		scanf("%d%d%d", &a, &b, &w);
		edge newedge;
		newedge.num_vertex_from = a-1;
		newedge.num_vertex_to = b-1;
		newedge.capacity = w;
		g[a-1].push_back(b-1);
		g[b-1].push_back(a-1);
		//add(a, b, w);
		//add(b, a, w);
	}

}

void build()
{
 
	int N1 = g.size();
	vector < vector<bool> > used (N1);
	for (int i=0; i < N1; ++i)
	{
		//used[i].resize (g[i].size());
		for(int j=0; j < g[i].size(); j++)
		{
			used[i].push_back(false);
		}
	}
	for (int i=0; i<N1; ++i)
		for (size_t j=0; j<g[i].size(); ++j)
			if (!used[i][j]) 
			{
 				used[i][j] = true;
 				int v = g[i][j],  pv = i;
 				vector<int> facet;
 				for (;;) 
				{
 					facet.push_back (v);
					sort ( g[v].begin(), g[v].end(), cmp_ang(v));
					vector<int>::iterator it = g[v].begin();
					for (; it != g[v].end();it++)
					{
						if (*it == pv) 
						{
							break;
						}
					}
 					//vector<int>::iterator it = lower_bound (g[v].begin(), g[v].end(), pv, cmp_ang(v));
					if (++it == g[v].end())  it = g[v].begin();
 					if (used[v][it-g[v].begin()])  break;
					if (used[v][it-g[v].begin()])  break;
 					used[v][it-g[v].begin()] = true;
 					pv = v,  v = *it;
 				}
				for(vector<int>::iterator it = facet.begin();it!=facet.end(); it++)
				{
					cout <<  *it << " ";
				}
				cout << endl;
 					//... ����� facet - ������� ����� ...
			}
}

int main()
{
	init();
	build();
	int h;
	cin >> h;
	return 0;
}
