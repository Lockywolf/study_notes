#pragma comment(linker, "/STACK:64000000")
#define _USE_MATH_DEFINES
#define _CRT_SECURE_NO_DEPRECATE
#include <iostream>
#include <sstream>
#include <cstdio>
#include <cmath>
#include <algorithm>
#include <set>
#include <map>
#include <queue>
#include <stack>
#include <deque>
#include <ctime>
#include <cstdlib>
#include <cassert>
using namespace std;

#define forn(i, n) for(int i = 0; i < int(n); i++)
#define for1(i, n) for(int i = 1; i <= int(n); i++)
#define forv(i, v) forn(i, v.size())
#define all(v) v.begin(), v.end()
#define mp make_pair
#define pb push_back

#define CIN_FILE "input.txt"
#define COUT_FILE "output.txt"


typedef long long ll;
const double EPS = 1e-9;

#define NMAX 505
#define INF 100000000

int n, m;
int s, f;
int c[NMAX][NMAX];
int d[NMAX];

int calc(int v)
{
    if (v == f) return 0;
    if (d[v] != -1) return d[v];
    d[v] = -INF;
    forn(i, n)
    {
        if (c[v][i])
        {
            if (calc(i) >= 0)
            {
                d[v] = max(d[v], c[v][i] + calc(i));
            }
        }
    }
    return d[v];
}

int main()
{
    cin >> n >> m;
    forn(i, m)
    {
        int u, v;
        scanf("%d %d", &u, &v);
        --u;
        --v;
        scanf("%d", &c[u][v]);
    }

    cin >> s >> f;
    --s;
    --f;

    forn(i, n) d[i] = -1;

    int ans = calc(s);

    if (ans < 0)
    {
        printf("No solution\n");
    }
    else
    {
        printf("%d\n", ans);
    }
    return 0;
}