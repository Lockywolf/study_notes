#pragma comment(linker, "/STACK:64000000")
#define _CRT_SECURE_NO_DEPRECATE
#include <iostream>
#include <vector>
#include <algorithm>
#include <set>
#include <map>
#include <cmath>
#include <cstdio>
#include <cassert>
#include <string>
#include <queue>
#include <stack>
#include <deque>
#include <numeric>
#include <sstream>

using namespace std;

#define CIN_FILE "input.txt"
#define COUT_FILE "output.txt"

#define forn(i, n) for(int i = 0; i < int(n); i++)
#define for1(i, n) for(int i = 1; i <= int(n); i++)
#define forv(i, v) forn(i, v.size())

#define pb push_back
#define mp make_pair
#define all(v) v.begin(), v.end()

typedef long long ll;

#define NMAX 205

int n, s, t;
int f[NMAX][NMAX], c[NMAX][NMAX];
int flow[NMAX], e[NMAX], h[NMAX];

int main()
{
    cin >> n;
    s = 2 * n;
    t = 2 * n + 1;
    int s1 = 0, s2 = 0;
    forn(i, n)
    {
        int a;
        cin >> a;
        c[s][i] = a;
        s1 += a;
    }
    forn(i, n)
    {
        int a;
        cin >> a;
        c[i + n][t] = a;
        s2 += a;
    }
    if (s1 != s2)
    {
        printf("NO\n");
        return 0;
    }

    forn(i, n)
    {
        forn(j, n)
        {
            c[i][j + n] = 100;
        }
    }

    int N = 2 * n + 2;

    forn(i, N)
    {
        e[i] = c[s][i];
        f[s][i] = e[i];
        f[i][s] = -e[i];
    }

    h[s] = N;
    while (1)
    {
        int p = - 1;
        forn(i, N)
        {
            if (e[i] && i != s && i != t)
            {
                p = i;
                break;
            }
        }
        if (p < 0) break;
        while (e[p])
        {
            forn(i, N)
            {
                if (c[p][i] > f[p][i] && h[p] > h[i])
                {
                    int a = min(e[p], c[p][i] - f[p][i]);
                    f[p][i] += a;
                    f[i][p] -= a;
                    e[p] -= a;
                    e[i] += a;
                }
            }
            if (e[p]) h[p]++;
        }
    }

    if (s1 != e[t])
    {
        printf("NO\n");
        return 0;
    }

    printf("YES\n");
    forn(i, n)
    {
        forn(j, n)
        {
            if (j) printf(" ");
            printf("%d", f[i][j + n]);
        }
        printf("\n");
    }

    return 0;
}