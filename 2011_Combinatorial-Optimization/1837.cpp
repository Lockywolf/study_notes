#include <iostream>
#include <algorithm>
#include <cstdlib>
#include <cstdio>
#include <cstring>
#include <string>
#include <map>
using namespace std;
const int maxn=1305;
bool vis[maxn];
int w[maxn][maxn], dis[maxn];
map<string, int> mp;
map<string, int>::iterator it;

void Dijstra(int N, int srt)
{
    int i, k;

    dis[srt]=0;
    while (1)
    {
        k=-1;
        int Min=10000;
        for (i=1; i<=N; i++) if (!vis[i] && dis[i]>=0)
            if (dis[i]<Min) Min=dis[k=i];

        if (k<0) break;
        vis[k]=1;
        for (i=1; i<=N; i++) if (!vis[i] && w[k][i]>=0)
            if (dis[i]<0 || dis[k]+w[k][i]<dis[i]) dis[i] = dis[k]+w[k][i];
    }
}
void add(int i, int j)
{
    w[i][j]=w[j][i]=1;
}
int main()
{

    int n, i, j, k, la, lb, lc;
    string a, b, c;

    int T=0;

    mp.clear();
    memset (w, -1, sizeof(w));
    scanf ("%d", &n);
    for (i=0; i<n; i++)
    {
        cin >> a >> b >> c;
        it = mp.find(a);
        if (it==mp.end())
        {
            mp[a]=++T;
            la = T;
        }
        else{
            la = it->second;
        }
        it = mp.find(b);
        if (it==mp.end())
        {
            mp[b]=++T;
            lb = T;
        }
        else{
            lb = it->second;
        }
        it = mp.find(c);
        if (it==mp.end())
        {
            mp[c]=++T;
            lc = T;
        }
        else{
            lc = it->second;
        }
        add(la, lb);
        add(lb, lc);
        add(lc, la);
    }
    string tmp="Isenbaev";
    it = mp.find(tmp);
    memset (dis, -1, sizeof(dis));
    memset (vis, 0, sizeof(vis));
    if (it!=mp.end())
        Dijstra(T, mp.find(tmp)->second);
    for (it=mp.begin(); it!=mp.end(); it++)
    {
        cout << it->first;
        i = it->second;
        if (dis[i]<0)
            printf (" undefined\n");
        else printf (" %d\n", dis[i]);
    }

    return 0;
}