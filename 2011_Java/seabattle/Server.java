package net.lockywolf.games.seabattle;

import java.util.HashMap;

public class Server
{
	public enum field_type { EMPTY, HIT, SHIP, SHIPHIT };
	
	public enum order {BLACK, WHITE};
	
	HashMap<order, field_type[][]> fields = new HashMap<order, field_type[][]>();
	{
		fields.put(order.BLACK, new field_type[10][10]);
		fields.put(order.WHITE, new field_type[10][10]);
		
	}
	
	
	private order turn=order.WHITE;
	
	public void receive_click( int xcoor, int ycoor )
	{
		field_type[][] field = fields.get(turn);
		switch(field[xcoor][ycoor])
		{
			case EMPTY:
				field[xcoor][ycoor]=field_type.SHIP;
				break;
			case SHIP:
				field[xcoor][ycoor]=field_type.EMPTY;
				break;
		}
		//check_validity();
		//if valid - send activate button;
	}
	
}
