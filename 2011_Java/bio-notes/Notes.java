import java.awt.*;
import java.awt.event.*;



public class Notes
{
	public static void main( String args[] )
	{
		new Notes();
		
	}
	public class MyListener extends WindowAdapter
	{
		public void windowClosing(WindowEvent e) 
		{
			//System.out.println("windowClosing called");
			Window ToKill = e.getWindow();
			ToKill.dispose();
		}
	}
	
	public class NotesMenuActionListener implements ActionListener
	{
		Object FileOpen;
		Object FileSave;
		Object FileExit;
		public void actionPerformed(ActionEvent e)
		{
			if( e.getSource() == FileOpen )
			{
				System.out.println("File_open_pressed.");
			}
			if( e.getSource() == FileSave )
			{
				System.out.println("File_save_pressed.");
			}
			if( e.getSource() == FileExit )
			{
				System.out.println("File_exit_pressed.");
				Runtime ru = Runtime.getRuntime();
				ru.exit(1);
			}
			
		}
		public void tune_action( int obj, Object arg)
		{
			if( obj == 1 )
			{
				FileOpen = arg;
			}
			if( obj == 2 )
			{
				FileSave = arg;
			}
			if( obj == 3 )
			{
				FileExit = arg;
			}
		}
		
	}
	
	
	
	public class BirdsField extends Canvas 
		{
			
			
			BirdsField()
			{
				this.setSize( 600, 400 );
				this.setBackground( new Color(200, 200, 200) );
				//this.setColor( new Color( 0, 0, 0) );
				//this.fillRect( 0 , 0, 100, 100 );
				this.setVisible(true);
			}
			
			
		}
	
	
	Notes()
	{
		Frame w = new Frame("Birds Notes");
		w.setLocation( 10 , 10 );
		w.setSize( 640, 480 );
		MyListener MyListen = new MyListener();
		w.addWindowStateListener( MyListen );
		w.addWindowFocusListener( MyListen );
		w.addWindowListener( MyListen );
		//w.enableEvents();
		w.setVisible(true);
		w.validate();
		
		w.setLayout(new GridBagLayout());
		/*
		Label lb2;
		lb2 = new Label("Label2");
		lb2.setBounds(0, 0, 100, 20 );
		w.add( lb2 );
		w.validate();
		
		Label lb1;
		lb1 = new Label("Label1");
		lb1.setBounds( 100, 20, 30, 30 ); 
		w.add( lb1 );
		w.validate();*/
		/* Creating menu. Maybe it should be forwarded to other class.*/
		MenuBar notes_menu = new MenuBar();
		Menu notes_menu_file = new Menu( "File" );
			MenuItem notes_menu_file_open = new MenuItem( "Open..." );
			MenuItem notes_menu_file_save = new MenuItem( "Save..." );
			MenuItem notes_menu_file_exit = new MenuItem( "Exit..." );
			
			NotesMenuActionListener menu_processor = new NotesMenuActionListener();
			menu_processor.tune_action( 1, notes_menu_file_open );
			menu_processor.tune_action( 2, notes_menu_file_save );
			menu_processor.tune_action( 3, notes_menu_file_exit );
			
			notes_menu_file_open.addActionListener( menu_processor );
			notes_menu_file_save.addActionListener( menu_processor );
			notes_menu_file_exit.addActionListener( menu_processor );
			
			
			notes_menu_file.add( notes_menu_file_open );
			notes_menu_file.add( notes_menu_file_save );
			notes_menu_file.add( notes_menu_file_exit );
		Menu notes_menu_edit = new Menu( "Edit" );
		Menu notes_menu_view = new Menu( "View" );
		Menu notes_menu_help = new Menu( "Help" );
		notes_menu.add( notes_menu_file );
		notes_menu.add( notes_menu_edit );
		notes_menu.add( notes_menu_view );
		notes_menu.setHelpMenu( notes_menu_help );
		w.setMenuBar( notes_menu );
		
		BirdsField field = new BirdsField();
		w.add( field );
		
		w.validate();
	}
}

