import java.util.Scanner;
import java.util.Vector;

public class Monsters
{
	int totaldamage;
	Vector<Integer> monsters = new Vector<Integer>();
	public Monsters()
	{
		totaldamage=0;
		Scanner in = new Scanner(System.in);
		int size = in.nextInt();
		for( int i=0; i<size; i++ )
		{
			monsters.insertElementAt(in.nextInt(), i);
		}
		while(calculate_damage())
		{
			//System.out.println( totaldamage );
			;
		}
		System.out.println( totaldamage );
	}

	public boolean calculate_damage()
	{
		int maxdam=0;;
		int dam;
		int inddam=0;
		for( int i=0; i<monsters.size(); i++)
		{
			if( i==0 )
			{
				dam=monsters.get(monsters.size()-1)+monsters.get(i)+monsters.get(i+1);
			}
			else if( i == (monsters.size()-1) )
			{
				dam=monsters.get(i-1)+monsters.get(i)+monsters.get(0);
			}
			else
			{
				dam=monsters.get(i-1)+monsters.get(i)+monsters.get(i+1);
			}
			if( dam>=maxdam)
			{
				maxdam=dam;
				inddam = i;
			}
		}
		//System.out.println( inddam );
		if(maxdam == 0)
		{
			return(false);
		}
		else
		{
			if(inddam == 0)
			{
				monsters.set(0, 0);
				monsters.set(1, 0);
				monsters.set(monsters.size()-1, 0);
			}
			else if( inddam == monsters.size()-1 )
			{
				monsters.set(0, 0);
				monsters.set(monsters.size()-2, 0);
				monsters.set(monsters.size()-1, 0);
			}
			else
			{
				monsters.set(inddam-1, 0);
				monsters.set(inddam, 0);
				monsters.set(inddam+1, 0);
			}
			for(Integer t : monsters)
			{
				totaldamage += t;
			}
			return(true);
		}
		
	}
	
	public static void main(String[] args)
	{
		new Monsters();

	}

}
