import java.util.*;


public class Flagsy
{
	class Flag
	{
		public Vector<Integer> colours;
		int equals(Flag comp)
		{
			if( colours.size() < comp.colours.size())
				return -1;
			else if( colours.size() > comp.colours.size() )
				return 1;
			else
			{
				int flag=0;
				for( int i=0; i<colours.size(); i++ )
				{
					if( colours.get(i) != comp.colours.get(i) )
						flag=1;
				}
				return(flag);
			}
		}
	}
	
	HashSet<Flag> myflags;
	
	public static void main(String[] args) throws NoSuchMethodError
	{
		new Flagsy();
		//return(0);
	}
	
	Flagsy()
	{
		Scanner in = new Scanner(System.in);
		int input = in.nextInt();
		int result = calculate_result( input );
		System.out.println(result);
	}	
	
	int calculate_result( int num_cols )
	{
		if( num_cols == 1 )
		{
			Flag a = new Flag();
			a.colours.insertElementAt(0,0);
			Flag b = new Flag();
			b.colours.insertElementAt(2,0);
			myflags.add(a);
			myflags.add(b);
			
			return(2);
		}
		else
		{
			calculate_result( num_cols-1 );
			HashSet<Flag> localflags = myflags;
			myflags=new HashSet();
			for( Flag prev : localflags )
			{
				//сгенерить флаг и вставить его в myflags
				for( int i=0; i<prev.colours.size(); i++ )
				{
					if( i==0)
					{
						Vector<Integer> colours = (Vector<Integer>) prev.colours.clone();
						if( colours.get( i ) == 0 )
						{
							colours.insertElementAt( 2, i );
						}
						else
						{
							colours.insertElementAt( 0, i );
						}
						Flag toins = new Flag();
						toins.colours = colours;
						myflags.add(toins);
					}
					if ( i == (prev.colours.size() - 1)  )
					{
						Vector<Integer> colours = (Vector<Integer>) prev.colours.clone();
						if( colours.get( i ) == 0 )
						{
							colours.insertElementAt( 2, i+1 );
						}
						else
						{
							colours.insertElementAt( 0, i+1 );
						}
						Flag toins = new Flag();
						toins.colours = colours;
						myflags.add(toins);
					}
					else
					{
						Vector<Integer> colours = (Vector<Integer>) prev.colours.clone();
						if( (colours.get( i ) == 0 && colours.get( i ) == 2)  || (colours.get( i ) == 2 && colours.get( i ) == 0)  )
						{
							colours.insertElementAt( 1, i+1);
							Flag toins = new Flag();
							toins.colours = colours;
							myflags.add(toins);
						}
					}
				}
			}
			localflags.clear();
			return( myflags.size() );
		}
	}
}
