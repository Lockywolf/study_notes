import java.math.BigInteger;
import java.util.*;


public class Flagsy
{
	
	public static void main(String[] args) throws NoSuchMethodError
	{
		new Flagsy();
		//return(0);
	}
	
	Flagsy()
	{
		Scanner in = new Scanner(System.in);
		int input = in.nextInt();
		BigInteger result = fib(input).multiply(new BigInteger("2"));
		
		
		System.out.println(result);
	}	
	
	private static ArrayList<BigInteger> fibCache = new ArrayList<BigInteger>();
	static {
	      fibCache.add(BigInteger.ZERO);
	      fibCache.add(BigInteger.ONE);
	}
	
	public static BigInteger fib(int n) {
        if (n >= fibCache.size()) {
            fibCache.add(n, fib(n-1).add(fib(n-2)));
        }
        return fibCache.get(n);
 }
}
