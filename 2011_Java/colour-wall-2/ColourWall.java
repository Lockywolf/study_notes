package net.lockywolf.study.vision.colorwall;

import java.applet.Applet;
import java.awt.FlowLayout;
import java.awt.GraphicsConfiguration;

import javax.media.j3d.*;

import javax.vecmath.Color3f;
import javax.vecmath.Point3d;
import javax.vecmath.Point3f;

import com.sun.j3d.utils.applet.MainFrame;
import com.sun.j3d.utils.behaviors.keyboard.KeyNavigatorBehavior;
import com.sun.j3d.utils.behaviors.mouse.MouseRotate;
import com.sun.j3d.utils.behaviors.mouse.MouseTranslate;
import com.sun.j3d.utils.geometry.Box;
import com.sun.j3d.utils.geometry.ColorCube;
import com.sun.j3d.utils.universe.SimpleUniverse;

public class ColourWall extends Applet {
	
	//Locky's additions
	public class MyAdapter extends java.awt.event.KeyAdapter
	{
		@Override public void keyPressed(java.awt.event.KeyEvent e)
		{
			//black magic
			char character = e.getKeyChar();
			//if(character == 's')
		//{
				//do black magic
				System.out.println("key pressed");
			//}
		}
	} 
	//public static MyAdapter MyAdapterObject; 
	
	public class Wall extends Shape3D{

		////////////////////////////////////////////
		//
		// create Shape3D with geometry and appearance
	        // the geometry is created in method yoyoGeometry
	        // the appearance is created in method yoyoAppearance
		//
		public Wall() {

	                this.setGeometry(WallGeometry());
		    
		} 
		
		private Geometry WallGeometry() {
			TriangleFanArray tsa;
			int N=6;
			Point3f coords[] = new Point3f[N];
			int stripCounts[] = {N};
			coords[0] = new Point3f(0,0,-0.5f);
			coords[1] = new Point3f(-10.0f,10.0f,-0.5f);
			coords[2] = new Point3f(-10f,-10f,-0.5f);
			coords[3] = new Point3f(10f,-10f,-0.5f);
			coords[4] = new Point3f(10f,10f,-0.5f);
			coords[5] = new Point3f(-10,10,-0.5f);
			
			tsa = new TriangleFanArray(N, TriangleFanArray.COORDINATES, stripCounts);
	        tsa.setCoordinates(0, coords);
	        return tsa;

		} // end of method yoyoGeometry in class Yoyo

	    }
		
	
	public class FrontWall extends Shape3D{

		////////////////////////////////////////////
		//
		// create Shape3D with geometry and appearance
	        // the geometry is created in method yoyoGeometry
	        // the appearance is created in method yoyoAppearance
		//
		public FrontWall() {

	                this.setGeometry(WallGeometry());
		    
		} 
		private Geometry WallGeometry() {
			TriangleStripArray tsa;
			int N=18;
			Point3f coords[] = new Point3f[N];
			int stripCounts[] = { N };
			coords[0] = new Point3f(0.5f,0.5f,0);
			coords[1] = new Point3f(10.0f,10.0f,0);
			coords[2] = new Point3f(-10,10, 0 );
			coords[3] = new Point3f(0.5f,0.5f, 0 );
			coords[4] = new Point3f(-0.5f,0.5f, 0f);
			coords[5] = new Point3f(-10f,10f,0f);
			coords[6] = new Point3f(-10,-10, 0f);
			coords[7] = new Point3f(-0.5f,0.5f, 0f);
			coords[8] = new Point3f(-0.5f,-0.5f, 0f);
			coords[9] = new Point3f(-10f,-10f, 0f);
			coords[10] = new Point3f(10f,-10f, 0f);
			coords[11] = new Point3f(-0.5f,-0.5f, 0f);
			coords[12] = new Point3f(0.5f,-0.5f, 0f);
			coords[13] = new Point3f(10f,-10f, 0f);
			coords[14] = new Point3f(0.5f,-0.5f, 0f);
			coords[15] = new Point3f(0.5f,0.5f, 0f);
			coords[16] = new Point3f(10f,10f, 0f);
			coords[17] = new Point3f(10f,-10f, 0f);
			
			
			Color3f coords2[] = new Color3f[N];
			coords2[0] = new Color3f(0.5f,0.5f,0);
			coords2[1] = new Color3f(10.0f,10.0f,0);
			coords2[2] = new Color3f(-10,10, 0 );
			coords2[3] = new Color3f(0.5f,0.5f, 0 );
			coords2[4] = new Color3f(-0.5f,0.5f, 0f);
			coords2[5] = new Color3f(-10f,10f,0f);
			coords2[6] = new Color3f(-10,-10, 0f);
			coords2[7] = new Color3f(-0.5f,0.5f, 0f);
			coords2[8] = new Color3f(-0.5f,-0.5f, 0f);
			coords2[9] = new Color3f(-10f,-10f, 0f);
			coords2[10] = new Color3f(10f,-10f, 0f);
			coords2[11] = new Color3f(-0.5f,-0.5f, 0f);
			coords2[12] = new Color3f(0.5f,-0.5f, 0f);
			coords2[13] = new Color3f(10f,-10f, 0f);
			coords2[14] = new Color3f(0.5f,-0.5f, 0f);
			coords2[15] = new Color3f(0.5f,0.5f, 0f);
			coords2[16] = new Color3f(10f,10f, 0f);
			coords2[17] = new Color3f(10f,-10f, 0f);
			
			tsa = new TriangleStripArray(N, TriangleStripArray.COORDINATES|TriangleStripArray.COLOR_3, stripCounts);
	        tsa.setCoordinates(0, coords);
	        tsa.setColors(0, coords2);
	        return tsa;

		}

	    }
    Canvas3D c1 = new Canvas3D(SimpleUniverse.getPreferredConfiguration());
    Canvas3D c2 = new Canvas3D(SimpleUniverse.getPreferredConfiguration());
    static MainFrame mf;
    private SimpleUniverse u = null;
    private BranchGroup scene = null;
    public void init() {
        setLayout(new FlowLayout());
        GraphicsConfiguration config =
           SimpleUniverse.getPreferredConfiguration();
        c1.setSize(360, 360);
        c1.setMonoscopicViewPolicy(View.RIGHT_EYE_VIEW);
        add(c1);
        c2.setSize(360, 360);
        c2.setMonoscopicViewPolicy(View.LEFT_EYE_VIEW);
        add(c2);

        // Create a simple scene and attach it to the virtual universe
        scene = createSceneGraph(0);
        //scene = createSceneGraph2();
        u = new SimpleUniverse(c1);

        View view0 = u.getViewer().getView();
        View view = new View();
        PhysicalBody myBod = view0.getPhysicalBody();
        //myBod.setLeftEyePosition(new Point3d(-.006,0.0, 0.0)); // default is(-0.033, 0.0, 0.0)
        //myBod.setRightEyePosition(new Point3d(+.006,0.0, 0.0));
        myBod.setLeftEyePosition(new Point3d(-.033,0.0, 0.0)); // default is(-0.033, 0.0, 0.0)
        myBod.setRightEyePosition(new Point3d(+.033,0.0, 0.0));
        
        
        view.setPhysicalBody(myBod);
        view.setPhysicalEnvironment(view0.getPhysicalEnvironment());
        view.attachViewPlatform(u.getViewingPlatform().getViewPlatform());
        view.addCanvas3D(c2);


        // This will move the ViewPlatform back a bit so the
        // objects in the scene can be viewed.
        u.getViewingPlatform().setNominalViewingTransform();
        //scene.compile();
        u.addBranchGraph(scene);

    }
    
    public BranchGroup createSceneGraph2() 
    {
    	// Create the root of the branch graph
    	BranchGroup objRoot = new BranchGroup();
    	// Create a simple shape leaf node, add it to the scene graph.
    	// ColorCube is a Convenience Utility class
    	//objRoot.addChild(new ColorCube(0.4));
    	objRoot.addChild(new Wall());
    	return objRoot;
    }
    
    void addPoints(TransformGroup tg)
    {
    	//dirty hack
    	
    	Color3f colour[] = new Color3f[6];
    	colour[0] = new Color3f(1f,0,0);
    	colour[1] = new Color3f(1.0f,0f,0);
    	colour[2] = new Color3f(1.0f,0f,0);
    	colour[3] = new Color3f(1.0f,0f,0);
    	colour[4] = new Color3f(1.0f,0f,0);
    	colour[5] = new Color3f(1.0f,0f,0);
    	
    	for (int i=0; i<40; i++)
    	{
    		Point3d pointcenter = new Point3d((Math.random()-0.5)*2, (Math.random()-0.5)*2, -0.49 );
    		int stripCounts[] = {6};
    		TriangleFanArray point = new TriangleFanArray(6, TriangleFanArray.COORDINATES | TriangleFanArray.COLOR_3, stripCounts);
    		Point3d coords[] = new Point3d[6];
    		coords[0] = pointcenter;
    		coords[1] = new Point3d(pointcenter.x+0.05, pointcenter.y+0.05,-0.49);
    		coords[2] = new Point3d(pointcenter.x-0.05, pointcenter.y+0.05,-0.49);
    		coords[3] = new Point3d(pointcenter.x-0.05, pointcenter.y-0.05,-0.49);
    		coords[4] = new Point3d(pointcenter.x+0.05, pointcenter.y-0.05,-0.49);
    		coords[5] = new Point3d(pointcenter.x+0.05, pointcenter.y+0.05,-0.49);
    		point.setCoordinates(0, coords);
    		point.setColors(0, colour);
    		Geometry helper = point;
    		Shape3D helper2 = new Shape3D();
    		helper2.setGeometry(helper);
    		tg.addChild(helper2);
    	}
    	
    }
    
    public BranchGroup createSceneGraph(int i) {
        // Create the root of the branch graph
        BranchGroup objRoot = new BranchGroup();
        TransformGroup objTrans = new TransformGroup();
        objTrans.setCapability(TransformGroup.ALLOW_TRANSFORM_WRITE);
        Transform3D t = new Transform3D();
      TransformGroup tg = new TransformGroup(t);
      tg.setCapability(TransformGroup.ALLOW_TRANSFORM_READ);
      tg.setCapability(TransformGroup.ALLOW_TRANSFORM_WRITE);
      objTrans.addChild(tg);
      
      //begin hacking
      	
      	//tg.addChild(new ColorCube(0.5));
      	tg.addChild(new Wall());
      	addPoints(tg);
      	tg.addChild(new FrontWall());
        //end hacking
        MouseRotate behavior = new MouseRotate();
      BoundingSphere bounds =
          new BoundingSphere(new Point3d(0.0,0.0,0.0), 100.0);
      behavior.setTransformGroup(tg);
      objTrans.addChild(behavior);
        // Create the translate behavior node
      MouseTranslate behavior3 = new MouseTranslate();
      behavior3.setTransformGroup(tg);
      objTrans.addChild(behavior3);
      behavior3.setSchedulingBounds(bounds);
      KeyNavigatorBehavior keyNavBeh = new KeyNavigatorBehavior(tg);
    keyNavBeh.setSchedulingBounds(new BoundingSphere(
      new Point3d(),1000.0));
    objTrans.addChild(keyNavBeh);
      behavior.setSchedulingBounds(bounds);
        objRoot.addChild(objTrans);
        return objRoot;
    }     
    public ColourWall() {
    }
    public void destroy() {
        u.removeAllLocales();
    }
    public void setSize(int width, int height) {
        System.out.println("setsize " + width +"," +height);
        super.setSize(width, height);
        int minDimension = Math.min(width/2, height);
        c1.setSize((minDimension - 20),(minDimension - 20)); 
        c2.setSize((minDimension - 20),(minDimension - 20)); 
        if (mf != null) {
            mf.appletResize(width, height);
        }
        validate();
    }
    public static void main(String[] args) 
    {
    	ColourWall ff = new ColourWall();
        mf = new MainFrame(ff, 800, 400);
        mf.addKeyListener(ff.new MyAdapter());
    }
}
