import java.util.*;


public class Flagsy
{
	class Flag
	{
		public Vector<Integer> colours = new Vector<Integer>();
		
		
		public String toString()
		{
			String result = new String();
			for(Integer dig : colours)
			{
				result = result + dig;
			}
			return(result);
		}
		
		public int hashCode()
	    {   
			
			int result=1;
			int i=1;
			for(Integer dig : colours)
			{
				result = result + ((int)dig+1)*(i+1);
			}
			//System.out.println("Im in hashcode " + result);
			return(result);
	    }
		
		public boolean equals(Object other)
		{
			if (this == other)
				return true;

			if ( !(other instanceof Flag) )
				return false;
			Flag comp = (Flag) other;
			//System.out.println("I am in equals");
			if( colours.size() != comp.colours.size())
				return false;
			else
			{
				boolean flag=true;
				for( int i=0; i<colours.size(); i++ )
				{
					if( (int)colours.get(i) != (int)comp.colours.get(i) )
						flag=false;
				}
				return(flag);
			}
		}
	}
	
	HashSet<Flag> myflags = new HashSet<Flag>();
	
	public static void main(String[] args) throws NoSuchMethodError
	{
		new Flagsy();
		//return(0);
	}
	
	Flagsy()
	{
		Scanner in = new Scanner(System.in);
		int input = in.nextInt();
		int result = calculate_result( input );
		//Debug:printing hashset
		/*for(Flag elem : myflags)
		{
			System.out.println(" " + elem);
		}
		System.out.println("");*/
		System.out.println(result);
	}	
	
	int calculate_result( int num_cols )
	{
		if( num_cols == 1 )
		{
			Flag a = new Flag();
			a.colours.insertElementAt(0,0);
			Flag b = new Flag();
			b.colours.insertElementAt(2,0);
			myflags.add(a);
			myflags.add(b);
			
			return(2);
		}
		else
		{
			calculate_result( num_cols-1 );
			HashSet<Flag> localflags = myflags;
			myflags=new HashSet<Flag>();
			for( Flag prev : localflags )
			{
				//сгенерить флаг и вставить его в myflags
				for( int i=0; i<prev.colours.size(); i++ )
				{
					if( i==0)
					{
						//Vector<Integer> colours = (Vector<Integer>) prev.colours.clone();
						Vector<Integer> colours = new Vector<Integer>();
						//Collections.copy(colours, prev.colours);
						colours.addAll(prev.colours);
						if( colours.get( i ) == 0 )
						{
							colours.insertElementAt( 2, i );
						}
						else
						{
							colours.insertElementAt( 0, i );
						}
						Flag toins = new Flag();
						toins.colours = colours;
						myflags.add(toins);
					}
					if ( i == (prev.colours.size() - 1)  )
					{
						//Vector<Integer> colours = (Vector<Integer>) prev.colours.clone();
						Vector<Integer> colours = new Vector<Integer>();
						//Collections.copy(colours, prev.colours);
						colours.addAll(prev.colours);
						if( colours.get( i ) == 0 )
						{
							colours.insertElementAt( 2, i+1 );
						}
						else
						{
							colours.insertElementAt( 0, i+1 );
						}
						Flag toins = new Flag();
						toins.colours = colours;
						myflags.add(toins);
					}
					else
					{
						//Vector<Integer> colours = (Vector<Integer>) prev.colours.clone();
						Vector<Integer> colours = new Vector<Integer>();
						//Collections.copy(colours, prev.colours);
						colours.addAll(prev.colours);
						if( (colours.get( i ) == 0 && colours.get( i+1 ) == 2)  || (colours.get( i ) == 2 && colours.get( i+1 ) == 0)  )
						{
							colours.insertElementAt( 1, i+1);
							Flag toins = new Flag();
							toins.colours = colours;
							myflags.add(toins);
						}
					}
				}
			}
			localflags.clear();
			return( myflags.size() );
		}
	}
}
