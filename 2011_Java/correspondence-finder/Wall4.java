
import java.awt.*;
import java.awt.event.*;

public class Wall4
{
	void m(Object o)
	{
		System.out.printf("Object\n");
	}
	
	void m(Integer... o)
	{
		System.out.printf("Integer\n");
	}
	
	public static void multiArrayCopy(int[][] source,int[][] destination)
	{
	for (int a=0;a<source.length;a++)
		{
		System.arraycopy(source[a],0,destination[a],0,source[a].length);
		}
	}
	
	public static void main( String args[] )
	{
		new Wall4();
		
	}
	public class MyListener extends WindowAdapter
	{
		public void windowClosing(WindowEvent e) 
		{
			Window ToKill = e.getWindow();
			ToKill.dispose();
		}
	}
	
	
	//int cam_wid = 280;
	//int cam_focus = 300;
	int cam_wid = 200;
	int cam_focus = 200;
	
	
	int base = 10;
	//int base =0;
	//сейчас я меряю все в пикселах фокусной плоскости.
	int nearwall = 600;
	int farwall = 1200;
	int holx = 450;
	int holy = 450;
	int holwx = 100;
	int holwy = 100;
	
	public int[][] frontfield = new int[1000][1000];
	public int[][] backfield = new int[1000][1000];
	
	/* Так, что это такое?
	 * По сути, на самом деле, я применил чит имени Дмитрия Петровича
	 * чит в следующем:
	 * 1)Объекты на дальней плоскости больше, чем на передней, 
	 * 	пропорционально расстояниям. Поэтому дальняя плоскость на деле такая же, как и 
	 * 	передняя. (по масштабу)
	 * 2)Обе плоскости моделируются уже в виде проекции на фокальную плоскость камер.
	 *  Вообще-это это очень сильный чит. Но нам помогает то, что оптические оси параллельны.
	 *  поэтому а)Масштаб точек одинаков, и база просто задает сдвиг.
	 */
	
	public class Camera extends Canvas 
		{
			
			private static final long	serialVersionUID	= 1L;
			public int[][] field = new int[cam_wid][cam_wid];
			
			boolean isLeft = true;
			
			
			
			Camera( boolean isLeft)
			{
				this.isLeft = isLeft;
				this.setSize(cam_wid, cam_wid );
				this.setBackground( new Color(255, 255, 255) );
				this.setVisible(true);
				this.generate_bitmap();
			}
			public void generate_bitmap()
			{
			    for( int k=0; k<cam_wid; k++)
			    {
			    	for( int i=0; i<cam_wid; i++ )
			    	{
			    		
			    		if(isLeft)
					    {
			    			int xcoor = 500 - base - (cam_wid/2) + i;
				    		int ycoor = 500-(cam_wid/2) + k;
			    			if( frontfield[xcoor][ycoor] == 1 )
			    			{
			    				//g.fillRect(i, k, 1, 1);
			    				field[k][i] = 1;
			    				if(xcoor > holx && xcoor < (holx+holwx) && ycoor > holy && ycoor < ( holy+holwy) )
			    				{
			    					field[k][i] = 0;
			    				}
			    			}
			    			if(xcoor > holx && xcoor < (holx+holwx) && ycoor > holy && ycoor < ( holy+holwy) )
		    				{
			    				float temp = 500 - base + (xcoor - (500 - base))*((nearwall+farwall+cam_focus)/(nearwall+cam_focus));
			    				//float temp = 500 + (xcoor - (500 - base))*((nearwall+farwall+cam_focus)/(nearwall+cam_focus));
			    				int tau = (int)temp;
		    					field[k][i] = 0;
		    					if( backfield[tau][ycoor] == 1 )
		    					{
		    						//g.fillRect(i, k, 1, 1);
		    						field[k][i] = 1;
		    					}
		    				}
					    }
			    		else
			    		{
			    			int xcoor = 500 + base - (cam_wid/2) + i;
				    		int ycoor = 500-(cam_wid/2) + k;
			    			if( frontfield[xcoor][ycoor] == 1 )
				    		{
			    				//g.fillRect(i, k, 1, 1);
			    				field[k][i] = 1;
			    				if(xcoor > holx && xcoor < (holx+holwx) && ycoor > holy && ycoor < ( holy+holwy) )
			    				{
			    					field[k][i] = 0;
			    				}
				    			
				    		}
			    			if(xcoor > holx && xcoor < (holx+holwx) && ycoor > holy && ycoor < ( holy+holwy) )
		    				{
			    				float temp = 500 + base + (xcoor - (500 + base))*((nearwall+farwall+cam_focus)/(nearwall+cam_focus));
			    				//float temp = 500  + (xcoor - (500 + base))*((nearwall+farwall+cam_focus)/(nearwall+cam_focus));
			    				int tau = (int)temp;
			    				field[k][i] = 0;
		    					if( backfield[tau][ycoor] == 1 )
		    					{
		    						//g.fillRect(i, k, 1, 1);
		    						field[k][i] = 1;
		    					}
		    				}
			    		}
			    	}	
			    }
			    
			}
			
			public void paint(Graphics g)
			{
				 g.setColor(Color.black);  
				    
				    for( int k=0; k<cam_wid; k++)
				    {
				    	for( int i=0; i<cam_wid; i++ )
				    	{
				    		if(field[k][i] == 1)
				    		{
				    			g.fillRect(i, k, 1, 1);
				    		}
				    		else if( field[k][i] == 10 )
				    		{
				    			g.setColor(new Color(255,150,150));
				    			g.fillRect(i, k, 1, 1);
				    			g.setColor(Color.BLACK);
				    		}
				    	}
				    }
			}
			
			public void paint0(Graphics g) 
			{
				//System.out.println( field[2][0] );
			    g.setColor(Color.black);  
			    
			    for( int k=0; k<cam_wid; k++)
			    {
			    	for( int i=0; i<cam_wid; i++ )
			    	{
			    		
			    		if(isLeft)
					    {
			    			int xcoor = 500 - base - (cam_wid/2) + i;
				    		int ycoor = 500-(cam_wid/2) + k;
			    			if( frontfield[xcoor][ycoor] == 1 )
			    			{
			    				g.fillRect(i, k, 1, 1);
			    				if(xcoor > holx && xcoor < (holx+holwx) && ycoor > holy && ycoor < ( holy+holwy) )
			    				{
			    					g.setColor(Color.white);
			    					g.fillRect(i, k, 1, 1);
			    					g.setColor(Color.black);
			    				}
			    			}
			    			if(xcoor > holx && xcoor < (holx+holwx) && ycoor > holy && ycoor < ( holy+holwy) )
		    				{
			    				float temp = 500 - base + (xcoor - (500 - base))*((nearwall+farwall+cam_focus)/(nearwall+cam_focus));
			    				int tau = (int)temp;
		    					g.setColor(Color.white);
		    					g.fillRect(i, k, 1, 1);
		    					g.setColor(Color.black);
		    					if( backfield[tau][ycoor] == 1 )
		    					{
		    						g.fillRect(i, k, 1, 1);
		    					}
		    				}
					    }
			    		else
			    		{
			    			int xcoor = 500 + base - (cam_wid/2) + i;
				    		int ycoor = 500-(cam_wid/2) + k;
			    			if( frontfield[xcoor][ycoor] == 1 )
				    		{
				    			g.fillRect(i, k, 1, 1);
				    			if(xcoor > holx && xcoor < (holx+holwx) && ycoor > holy && ycoor < ( holy+holwy) )
			    				{
			    					g.setColor(Color.white);
			    					g.fillRect(i, k, 1, 1);
			    					g.setColor(Color.black);
			    				}
				    			
				    		}
			    			if(xcoor > holx && xcoor < (holx+holwx) && ycoor > holy && ycoor < ( holy+holwy) )
		    				{
			    				float temp = 500 + base + (xcoor - (500 + base))*((nearwall+farwall+cam_focus)/(nearwall+cam_focus)); 
			    				int tau = (int)temp;
		    					if( backfield[tau][ycoor] == 1 )
		    					{
		    						g.fillRect(i, k, 1, 1);
		    					}
		    				}
			    		}
			    	}	
			    }
			    
			}
			
		}
	
	
	Wall4()
	{
		Frame w = new Frame("Birds Notes");
		w.setLocation( 0 , 0 );
		w.setSize( 600, 590 );
		MyListener MyListen = new MyListener();
		w.addWindowStateListener( MyListen );
		w.addWindowFocusListener( MyListen );
		w.addWindowListener( MyListen );

		w.setVisible(true);
		w.validate();
		
		w.setLayout(new FlowLayout());
		java.util.Random generator = new java.util.Random();
		for( int k=0; k<1000; k++)
	    {
	    	for( int i=0; i<1000; i++ )
	    	{
	    		if( true )
	    		{
	    			frontfield[k][i] = generator.nextInt(4);
	    			backfield[k][i] =  generator.nextInt(4);
	    		}
	    	}
	    }
		
		
		
		Camera lefteye = new Camera( true );
		w.add( lefteye );
		Camera righteye = new Camera( false );
		w.add( righteye );
		w.validate();
		//So, we plotted an RDS. And even validated it with LiveJournal.
		//Good.
		//Time to find corresponding images.
		//Well, Basically, nous avons lefteye.field et righteye.field
		//База у нас дана. Первое, что мы сделаем - это найдем полностью корреспондирующие строки.
		//это такие строки, которые сдвинуты на две базы пикселей
		//В принципе, база могла бы и не быть данной
		//тогда мы бы сравнивали строки варьируя базу от 0 до N
		//И рано или поздно ее бы нашли
		//Доделка по требованию Дмитрия Петровича
		//Найдем диспарити до первой плоскости.
		//Будем считать, что уж самая-то верхняя строчка пикселей
		//уж точно дыры не содержит.
		//Посчитаем, какое расстояние в пикселях
		//
		int new_dis1=100500;
		for(int k=0; k< cam_wid/2 ; k++ )
		{
			int flag=1;
			for( int t=0; t< cam_wid-k ; t++ )
			{
				if(lefteye.field[1][t+k] != righteye.field[1][t] )
				{
					flag=0;
					break;
				}
			}
			if(flag==1)
			{
				new_dis1 = k;
				break;
			}
		}
		System.out.println("dispon "+new_dis1+" base"+base);
		//конец доделки
		int prevflag = 1;
		int myholy=-2;
		int myholwy=0;
		for(int i=0; i<cam_wid; i++ )
		{
			int flag = 1;
			//for( int k=0; k<cam_wid-2*base; k++ )
			for( int k=0; k<cam_wid-new_dis1; k++ )
			{
				//if( lefteye.field[i][k+2*base] != righteye.field[i][k])
				if( lefteye.field[i][k+new_dis1] != righteye.field[i][k])
				{
					flag = 0;
					//Некую толерантность к соль|перцу можно получить
					//если здесь сделать не бинарный флаг, а скажем, допускать пару ошибок
					//при этом мы перестанем ловить дыры размера 2х2, зато будем чуть толерантне к ошибкам
				}
			}
			if( flag == 0 && prevflag==1) 
			{
				System.out.println("Line number "+i+" is upper border");
				prevflag=0;
				myholy = i;
			}
			if( flag == 1 && prevflag==0) 
			{
				System.out.println("Line number "+i+" is lower border");
				prevflag=1;
				myholwy = i-myholy;
			}
		}
		System.out.println("myholy:"+myholy+"myholw"+myholwy);
		//Проделаем то же самое с у
		prevflag = 1;
		int myrholx=-2;
		int myholwx=0;
		//for( int i=0; i<cam_wid-(2*base); i++ )
		for( int i=0; i<cam_wid-new_dis1; i++ )
		{
			int flag=1;
			for( int k=0; k<cam_wid; k++ )
			{
				//if( lefteye.field[k][i+(2*base)] != righteye.field[k][i])
				if( lefteye.field[k][i+new_dis1] != righteye.field[k][i])
				{
					flag=0;
				}
			}
			if( flag == 1)
			{
				//System.out.println("Column "+i+" matches");
			}
			if( flag == 0 && prevflag==1) 
			{
				System.out.println("Line number "+i+" is left border");
				prevflag=0;
				myrholx = i;
			}
			if( flag == 1 && prevflag==0) 
			{
				System.out.println("Line number "+i+" is right border");
				prevflag=1;
				myholwx = i-myrholx;
			}
			
		}
		System.out.println("myrholх:"+myrholx+"myholwх"+myholwx);
		int mylholx = myrholx+2*base;
		
		/*
		 * Вот таким нехитрым способом мы нашли координаты 
		 * дыры
		 * Ура, товарищи.
		 * Теперь надо найти корреспонеденцию внутри дыры
		 * Вертикальной диспаратности у нас нет совсем.
		 * Значит, на самом деле, внутри дыры у нас тоже сдвиг.
		 * Только не на базу, а на базу умножить на отношение расстояний
		 * Ка это искать?
		 * ну, попробуем
		 */
		int dispar = 0;
		for( dispar = 0; dispar < myholwx; dispar++ )
		{
			int flag=1;
			for( int i=0; i<myholwx-dispar; i++)
			{
				if(lefteye.field[myholy][mylholx+i] != righteye.field[myholy][myrholx+i+dispar])
				{
					flag=0;
				}
			}
			if(flag==1)
			{
				System.out.println("Found:Dispar= "+dispar);
				break;
			}
		}
		
		/*
		 * покажем некореспондирующие области
		 * в дырах
		 */
		Camera leftmod = new Camera( true );
		int[][] tempfield = new int[cam_wid][cam_wid];
		multiArrayCopy(lefteye.field, tempfield);
		leftmod.field = tempfield;
		w.add( leftmod );
		tempfield = new int[cam_wid][cam_wid];
		Camera rightmod = new Camera( false );
		multiArrayCopy(righteye.field, tempfield);
		rightmod.field = tempfield;
		w.add( rightmod );
		w.validate();
		for(int i=0; i<myholwy; i++)
		{
			for( int k=0; k<dispar; k++)
			{
				leftmod.field[i + myholy][k + mylholx+myholwx-dispar] = 10;
				rightmod.field[i + myholy][k + myrholx] = 10;
				
			} 
		}
		
		/*
		 * Теперь расстояние до плоскости
		 * ТАк, вспомним магическую формулу:
		 * H=(B/D)F
		 * 
		 * H - Расстояние
		 * Ну, для первой плоскости у нас просто 
		 * 
		 */
		int length_nearwall = (((2*base)/(base))*cam_focus - cam_focus)*2;
		int length_farwall  = ((2*base)/dispar)*cam_focus*2;
		System.out.println("Nearwall: "+ length_nearwall+" Farwall: "+ length_farwall);
		System.out.println("end");
		//m(new Object());
	}
}

