
import java.awt.*;
import java.awt.event.*;



public class Wall
{
	
	
	public static void main( String args[] )
	{
		new Wall();
		
	}
	public class MyListener extends WindowAdapter
	{
		public void windowClosing(WindowEvent e) 
		{
			//System.out.println("windowClosing called");
			Window ToKill = e.getWindow();
			ToKill.dispose();
		}
	}
	
	final int BASE = 2;
	public int[][] field = {
			{0,0,1,0,0,1,0,0,0,0},
			{0,0,0,0,0,0,0,0,0,0},
			{0,0,0,0,0,0,0,0,0,0},
			{0,0,0,0,0,0,0,0,0,0},
			{0,0,0,0,1,0,0,0,0,0},
			{0,0,0,0,0,0,0,0,0,0},
			{0,0,0,0,0,0,1,0,0,0},
			{0,0,0,0,0,0,0,0,0,0},
			{0,0,1,0,0,0,0,0,0,0},
			{0,0,0,0,0,0,0,0,0,0}
	};
	public int[][] field1 = new int[10][10];
	public int[][] field2 = new int[10][10];
	
	
	public class BirdsField extends Canvas 
		{
			int[][] field;
			
			
			BirdsField()
			{
				this.setSize(200, 200 );
				this.setBackground( new Color(200, 200, 200) );
				this.setVisible(true);
			}
			
			BirdsField(int[][] field)
			{
				this.setSize(200, 200 );
				this.setBackground( new Color(200, 200, 200) );
				this.setVisible(true);
				this.field = field;
				//this.bias = bias;
			}
			
			public void paint(Graphics g) 
			{
				//System.out.println( field[2][0] );
			    g.setColor(Color.black);  
			    for( int k=0; k<10; k++)
			    {
			    	for( int i=0; i<10; i++ )
			    	{
			    		if( field[i][k] == 1 )
			    		{
			    			g.fillRect(k*20 , i*20  , 20, 20);
			    		}
			    	}
			    }
			}
			
		}
	
	
	Wall()
	{
		Frame w = new Frame("Birds Notes");
		w.setLocation( 10 , 10 );
		w.setSize( 450, 450 );
		MyListener MyListen = new MyListener();
		w.addWindowStateListener( MyListen );
		w.addWindowFocusListener( MyListen );
		w.addWindowListener( MyListen );

		w.setVisible(true);
		w.validate();
		
		w.setLayout(new FlowLayout());
		
		for( int i=0; i<10; i++)
		{
			for( int k=0; k<10; k++)
			{
				if(k == 0)
				{
					field1[i][k]=0;
					continue;
				}
				else
				{
					field1[i][k] = field[i][k-1];
				}
			}
		}
		/*Это крэп. Копировать массивы таким образом - за это стоит убивать*/
		
		for( int i=0; i<10; i++)
		{
			for( int k=0; k<10; k++)
			{
				if(k == 9)
				{
					field2[i][k]=0;
					continue;
				}
				else
				{
					field2[i][k] = field[i][k+1];
				}
			}
		}
		
		BirdsField lefteye = new BirdsField( field1 );
		w.add( lefteye );
		BirdsField righteye = new BirdsField( field2 );
		w.add( righteye );
		BirdsField cyclop = new BirdsField( field );
		w.add( cyclop );
		w.validate();
		/*это мы выполнили задание 1.
		 * стереопару показали - кубики видны
		 * теперь установим корреспонденцию
		 * как это сделать? Ну, не мудрствуя лукаво.
		 * Будем считать все это в клеточках.
		 * То есть, по сути - пройдем по строкам и поищем паттерн
		 * Мы применим 2 чита.
		 * первый - будем сравнивать просто по строкам
		 * второй - будем верить, что диспартность вообще есть.
		 */
		 int disparity=0;
		 while( true )
		 {
			 int flag = 0;
			 for(int i=disparity; i<10-disparity; i++)
			 {
				 if( field1[0][i+disparity] != field2[0][i-disparity] )
				 {
					 /*
					  * НУ, come on, можно, конечно и по всем строкам пройти.
					  * Но зачем усложнять себе жизнь.
					  */
					 flag = 1;
				 }
			 }
			 if( flag==1 )
			 {
				 disparity++;
				 continue;
			 }
			 break;
		 }
		 disparity=2*disparity;
		 System.out.println("Disparity="+disparity);
		 /*
		  * НУ, ну тестовых данных все работает
		  * у меня.
		  * диспарити - 2 клетки
		  * Посчитаем расстояние до экрана в фокусных расстояниях
		  * (формула H=(L/D)F) где L - база, а D - диспаратность
		  * База у нас 2 клетки(камеру влево и вправо на 1 клетку я двигал)
		  */
		 double z_buffer = BASE/disparity;  
		System.out.println("Глубина плоскости = "+z_buffer);
	}
}

