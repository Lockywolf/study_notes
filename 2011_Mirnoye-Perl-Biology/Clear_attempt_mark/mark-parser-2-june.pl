#!/usr/bin/perl
#This script for processing biological data was written by Lockywolf (lockywolf@gmail.com) 07.2011
#Version 0.1
#The script is public domain, or, if it is not permitted in your country, distributed according to WTFPL.
use strict;
use warnings;
sub date_to_day
{
	my $text = shift;
	my $value;
	my ($day, $month) = ( $text =~ /(\d+).(\d+)/);
	if( int($month) == 5 )
	{
		$value = int($day);
		return $value;
	}
	elsif( int($month) == 6 )
	{
		$value = int($day)+31;
		return $value;
	}
	elsif( int($month) == 7 )
	{
		$value = int($day)+61;
		return $value;
	}
	elsif( int($month) == 8 )
	{
		$value = int($day)+92;
		return $value;
	}
}
#Блок 1. данный блок парсит входной файл
my $data_file="Nests_v9.1.csv";
open(my $in , "<", $data_file) || die("Could not open file!");
open(my $temp_csv, ">", "Splitted observations.csv" ) || die("Could not open file!");
my $result_file = "autoprocess.2checks.may.output.csv";
open(my $out, ">", $result_file) || die("Could not open file!");

my @lines = <$in>;
my @tokens;
my $i=0;
foreach my $line ( @lines )
{
	#print $line;
	#$/ = "";
	chop ( $line ) ;
	chop ( $line ) ;
	#$line =~ / *(*)/;
	#$line = $1;
	my @tmp = split '@', $line;
	push @tokens, [@tmp];
	#@tokens[$i]=
}
#конец блока 1. tokens - это таблица

$i=0;
#foreach my $lain ( @tokens )
#{
#	if($i == 1) 
#	{
#		#last;
#	}
#	$i++;	
#	foreach (@$lain)
#	{
#		print $_;
#		print "\%";
#	}
#	print "\n";
#}
#print '=====================================================' ."\n";
#my $k=0;
#foreach ($k=0; $k < 2 ;$k++ )


#Блок 2. генерация таблицы первого вида(разбивка по точкам с запятой)
my $ind=0;
my @tokens2;
foreach my $input_line ( @tokens )
{
	#if($ind == 1487)#debug
	#{
	#	#last;
	#	#print "debug:".@{$input_line}[8]."\n";
	#}
	my $month_num=6;
	my @tmp;
	#print "Debug:ind=".$ind.@$input_line[8]."\n";
	my $templine = substr( @{$input_line}[8] , 1, -1 );
	my @checks = split ';',  $templine;
	foreach my $check (@checks)
	{
		my $header="";
		my $bad_data = 0;
		my $obs=$check;
		my $destiny = 2;
		my $counter = 0;
		my $ce=0;
		my $we=0;
		my $pull=0;
		my $age = "notfound";
		my $date;
		my $date_num;
		$header = @{$input_line}[0].@{$input_line}[2];
		if( $check =~ /^ *([0-3]{0,1}\d)(\.|ю)(\d{1,2})/ )
		{
			$date = $1.".".$3;
			$month_num = $3;
			if( $' =~ /^ *([0-3]{0,1}\d)(\.|ю)(\d{1,2})/)
			{
				$bad_data=1;
				$date="-1";
				print "Nested date:@{$input_line}[0]\n";
			}
		}
		elsif ( $check =~ /^ *(\d{1,2})-([0-3]{0,1}\d)(\.|ю)(\d{1,2})/ )
		{
			
			$bad_data = 1;
			$date="-1";
			print "hyphen separated data:@{$input_line}[0]\n";
		}
		elsif ( $check =~ /^ *(\d{1,2})/ )
		{
			$date = $1.".".$month_num;
			if( $' =~ /^ *([0-3]{0,1}\d)(\.|ю)(\d{1,2})/)
			{
				$date="-1";
				$bad_data=1;
				print "Nested date:@{$input_line}[0]\n";
			}
		}
		else
		{
			$date = "-1";
			$bad_data = 1;
			print "Bad input data:@{$input_line}[0]\n";
		}
		if( $date ne "-1" )
		{
			$date_num = date_to_day( $date );
		}
		if( $check =~/(\d+) *(хя|х\.я\.|cold eggs)/ )
		{
			$ce = $1;
			if( $1 > 8)
			{
				print "Bad input data: @{$input_line}[0]\n";
				$bad_data=1;
			}
		}
			
		if( $check =~/(\d+) *(я|тя|т\.я\.|eggs)/ )
		{
			$we = $1;
			if( $1 > 8)
			{
				print "Bad input data: @{$input_line}[0]\n";
				$bad_data = 1;
			}
		}
		if( $check =~/(\d+) *(п[^оа]|пт|pull|alive|p|j)/ )
		{
			$pull = $1;
			if( $1 > 8)
			{
				print "Bad input data: @{$input_line}[0]\n";
				$bad_data=1;
			}
		}
		elsif( $check =~/(брош|разор)/ )
		{
			$pull = 0;
		}
		if( $check =~/(брош)/ )
		{
			$destiny = -1;
		}
		if( $check =~/(разо)/ )
		{
			$destiny = 1;
		}
		if( $check =~/(вылет)/ )
		{
			$destiny = 0;
		}
		
		if( $check =~/(\d+) *(дн|д|days old$)/ )
		{
			if( $1 > 16 )
			{
				print "Bad input data: @{$input_line}[0]\n";
				$bad_data=1;
			}
			$age=$1;
			
		}
		elsif( $check =~/(вылуп|вылупл|я вылуп)/ )
		{
			$age = "0";
		}
		else
		{
			#$need_killing++;
			$age = "notfound";
		}
		if( $bad_data !=1 )
		{
		@{$tokens2[$ind]}= ( $header,$obs, $date_num, $ce, $we, $pull, $age, $destiny, $bad_data );
		$ind++;
		}
	}
	
}
#конец блока 2. tokens2 - это таблица

#печать таблицы
$i=0;
foreach my $lain ( @tokens2 )
{
	#if($i == 0) 
	#{
	#	last;
	#	
	#}
	$i++;	
	foreach (@$lain)
	{
		print $temp_csv $_;
		print $temp_csv "@";
	}
	print $temp_csv "\n";
}
#print "===========================\n";
$i=0;

#Попробуем сгенерить еще одну  табличку - для марка

my $k1 = 0;
my $k2 = 0;
my $id = $tokens2[0][0];
my @nest;
my $temp;
$i = 0;
my $unprocessibles = 0;
my $processible = 0;
foreach my $lain ( @tokens2 )
{
	# 0-id, 2 - ввод, 3 - дата_число, 4 - хя, 5 - тя, 6 - пт, 7 - возраст, 8 - судьба, 9 - есть_ли_лажа
	#14 - кольца
	if( @$lain[0] eq $id )
	{
		$k2++;
	}
	elsif( @$lain[0] ne $id )
	{
		 
		
			$i++;
			@nest = @tokens2[($k1)..($k2-1)];
			$k1=$k2;
			$k2++;
			$id = @$lain[0];
			
			my $bad_data=0;
			my $destiny=2;
			my $maxeggs=0;
			my $workable=0;
			my $beginning;
			my $j=0;
			foreach my $check ( @nest )
			{
				#print @{$check};
				#print "\n";
				if( @{$check}[8] == 1)
				{
					
					$bad_data=1;
					last;
				}
				if( @{$check}[7] != 2 )
				{
					$destiny = @{$check}[7];
				}
				if( @{$check}[3] + @{$check}[4] + @{$check}[5] > $maxeggs )
				{
					$maxeggs = @{$check}[3] + @{$check}[4] + @{$check}[5];
				}
				#if( @{$check}[6] ne "notfound" )
				#{
				#	$workable = 1;
				#	$beginning = ( @{$check}[2] - int( @{$check}[6] ) - $maxeggs - 11) if (@{$check}[0] =~ /Tur.pil|Tur.ruf/ );
				#	$beginning = ( @{$check}[2] - int( @{$check}[6] ) - $maxeggs - 10) if (@{$check}[0] !~ /Tur.pil|Tur.ruf/ );
				#}
				$j++;
			}
			if( $bad_data == 1 || $destiny == 2 || $maxeggs == 0 || $j<2 )
			{
				$unprocessibles++;
				#print "Bad nest:$nest[0][0],$bad_data,$workable,$destiny,$maxeggs\n";
				next;
			}
			my $firstdate = -100;;
			my $alivedate;
			my $lastdate;
			$j=0;
			foreach my $check ( @nest )
			{
				if(  @{$check}[3] + @{$check}[4] + @{$check}[5]  > 0 && $firstdate == -100 )
				{
					$firstdate = int(@{$check}[2]);#days of 20may+
				}
				if(  @{$check}[3] + @{$check}[4] + @{$check}[5]  > 0 )
				{
					$alivedate = int( @{$check}[2] );#days of 20may+
				}
				$j++;
			}
			$lastdate = int( $nest[$j-1][2] ) ;
			if( $firstdate > $alivedate || $alivedate > $lastdate )
			{
				$unprocessibles++;
				print "Bad nest input data:$nest[0][0]\n";
				next;
			} 
			print $out $nest[0][0]."@".$firstdate."@".$alivedate."@".$lastdate."@".$destiny."@\n" ; 
			
	}
	else
	{	
		print "WTF???\n";
	}
	
	
}

print "checks:".$k1."\ntotal number of processible nests:".($i-$unprocessibles)."\nTotal nests:".$i."\n";

