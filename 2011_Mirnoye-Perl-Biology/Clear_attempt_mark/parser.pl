#!/usr/bin/perl
#This script for processing biological data was written by Lockywolf (lockywolf@gmail.com) 07.2011
#Version 0.1
#The script is public domain, or, if it is not permitted in your country, distributed according to WTFPL.
use strict;
use warnings;
sub date_to_day
{
	my $text = shift;
	my $value;
	my ($day, $month) = ( $text =~ /(\d+).(\d+)/);
	if( int($month) == 5 )
	{
		$value = int($day)-19;
		return $value;
	}
	elsif( int($month) == 6 )
	{
		$value = int($day)+12;
		return $value;
	}
	elsif( int($month) == 7 )
	{
		$value = int($day)+42;
		return $value;
	}
	elsif( int($month) == 8 )
	{
		$value = int($day)+73;
		return $value;
	}
}
#Блок 1. данный блок парсит входной файл
my $data_file="Nests_v8.csv";
open(my $in , "<", $data_file) || die("Could not open file!");
open(my $temp_csv, ">", "Splitted observations.csv" ) || die("Could not open file!");
my $result_file = "autoprocess.output.csv";
open(my $out, ">", $result_file) || die("Could not open file!");

my @lines = <$in>;
my @tokens;
my $i=0;
foreach my $line ( @lines )
{
	#print $line;
	#$/ = "";
	chop ( $line ) ;
	chop ( $line ) ;
	#$line =~ / *(*)/;
	#$line = $1;
	my @tmp = split '@', $line;
	push @tokens, [@tmp];
	#@tokens[$i]=
}
#конец блока 1. tokens - это таблица

$i=0;
#foreach my $lain ( @tokens )
#{
#	if($i == 1) 
#	{
#		#last;
#	}
#	$i++;	
#	foreach (@$lain)
#	{
#		print $_;
#		print "\%";
#	}
#	print "\n";
#}
#print '=====================================================' ."\n";
#my $k=0;
#foreach ($k=0; $k < 2 ;$k++ )


#Блок 2. генерация таблицы первого вида(разбивка по точкам с запятой)
my $ind=0;
my @tokens2;
foreach my $input_line ( @tokens )
{
	#if($ind == 1487)#debug
	#{
	#	#last;
	#	#print "debug:".@{$input_line}[8]."\n";
	#}
	my $month_num=6;
	my @tmp;
	#print "Debug:ind=".$ind.@$input_line[8]."\n";
	my $templine = substr( @{$input_line}[8] , 1, -1 );
	my @checks = split ';',  $templine;
	my $processible=0;
	my $preprocessible = 1;
	foreach my $check (@checks)
	{
		
		my $counter = 0;
		my $indexer = 0;
		my $need_killing = 0;
		foreach my $token( @{$input_line} )
		{
		if( $counter == 8)
		{
			
			#print $check."@";
			#push @tmp, [$check];
			#$tokens2[$ind][$counter] = $check."@";
			$tokens2[$ind][$indexer] = $check;
			$indexer++;
			#if( $check =~/^ *([0-9]{1,2}(\.|ю)[0-9]{1,2})/ )
			if( $check =~ /^ *([0-3]{0,1}\d)(\.|ю)(\d{1,2})/ )
			{
				$tokens2[$ind][$indexer] = $1.".".$3;
				
				$month_num = $3;
			}
			elsif ( $check =~ /^ *(\d{1,2})/ )
			{
				$tokens2[$ind][$indexer] = $1.".".$month_num;
			}
			else
			{
				$need_killing++;
				#$tokens2[$ind][$indexer] = "notfound";
				#nextline - danger
				$tokens2[$ind][$indexer] = "0";
				$preprocessible = 0;
				print "Bad input data:$tokens2[$ind][0]\n";
				$ind--;
				last;
				#$ind--;#говнокод, выкидывает эту строчку из таблицы
			}
			$indexer++;
			$tokens2[$ind][$indexer] = date_to_day($tokens2[$ind][$indexer-1]."@" );
			$indexer++;
			if( $check =~/(\d+) *(хя|х\.я\.|cold eggs)/ )
			{
				$tokens2[$ind][$indexer] = $1;
				if( $1 > 8)
				{
					print "Bad input data: $tokens2[$ind][0]\n";
					$ind--;
					last;
					$preprocessible = 0;
				}
			}
			else
			{
				$need_killing++;
				#$tokens2[$ind][$indexer] = "notfound";
				$tokens2[$ind][$indexer] = "0";
			}
			$indexer++;
			
			if( $check =~/(\d+) *(я|тя|т\.я\.|eggs)/ )
			{
				$tokens2[$ind][$indexer] = $1;
				if( $1 > 8)
				{
					print "Bad input data: $tokens2[$ind][0]\n";
					$ind--;
					last;
					$preprocessible = 0;
				}
			}
			else
			{
				$need_killing++;
				#$tokens2[$ind][$indexer] = "notfound";
				$tokens2[$ind][$indexer] = "0";
			}
			$indexer++;
			if( $check =~/(\d+) *(п[^о]|пт|pull|alive|p)/ )
			{
				$tokens2[$ind][$indexer] = $1;
				if( $1 > 8)
				{
					print "Bad input data: $tokens2[$ind][0]\n";
					
					$ind--;
					last;
					$preprocessible = 0;
				}
			}
			elsif( $check =~/(брош|разор)/ )
			{
				$tokens2[$ind][$indexer] = 0;
			}
			else
			{
				$need_killing++;
				#$tokens2[$ind][$indexer] = "notfound";
				$tokens2[$ind][$indexer] = "0";
				
			}
			$indexer++;
			
			
			if( $check =~/(\d+) *(дн|д|days old$)/ )
			{
				if( $1 > 16 )
				{
					print "Bad input data: $1\n";
					
					$ind--;
					last;
					$preprocessible = 0;
				}
				$tokens2[$ind][$indexer] = $1;
				if( $preprocessible == 1 )
				{
					$processible = 1;
				}
				
			}
			elsif( $check =~/(вылуп|вылупл|я вылуп)/ )
			{
				$tokens2[$ind][$indexer] = "0";
				if( $preprocessible == 1 )
				{
					$processible = 1;
				}
			}
			#elsif( $check =~/(вылет)/ )#need fixing with Tur.Pil
			#{
			#	$tokens2[$ind][$indexer] = "12";
			#	$processible = 1;
			#}
			else
			{
				#$need_killing++;
				$tokens2[$ind][$indexer] = "notfound";
			}
			$indexer++;
			$tokens2[$ind][$indexer] = $processible;
			$indexer++;
		}
		if (  $counter == 0 or  $counter == 1 or  $counter == 2 or  $counter == 3 or  $counter == 4 or  $counter == 5 or $counter == 15)
		{
			$tokens2[$ind][$indexer] = $token;
			$indexer++;
		}
			$counter++;
			
		}
		#push @tokens2, [@tmp];
		$ind++;
		#print "\n";
		#кусок говнокода, выкидывает все строки без даты или намека на дату
		#вернее, пытается
		#if( $check !~/^ *([0-9]{1,2})/ )
		#{
		#	$ind--;
		#}
		#if( $need_killing == 3 )
		#{
		#	$ind--;
		#}
		#конец говнокода
	}
}
#конец блока 2. tokens2 - это таблица

#печать таблицы
$i=0;
foreach my $lain ( @tokens2 )
{
	#if($i == 0) 
	#{
	#	last;
	#	
	#}
	$i++;	
	foreach (@$lain)
	{
		print $temp_csv $_;
		print $temp_csv "@";
	}
	print $temp_csv "\n";
}
#print "===========================\n";
$i=0;
#foreach my $table_line ( @tokens2 )
#{
#	my $counter = 0;
#	if($i == 6)
#	{
#		last;
#	}
#	foreach my $token2 (@$table_line )
#	{
#		if( $counter == 8)
#		{
#			print $token2;
#			if($token2=~/(^[0-9]{1,2}\.[0-9]{1,2})/)
#			{
#				print " regex:".$1;
#				
#			}
#			print "\n";
#			last;
#		}
#		$counter++;
#	}
#	$i++;
	#print "\n";
#}

#Попробуем сгенерить еще одну  табличку - с обработкой

my $k1 = 0;
my $k2 = 0;
my $id = $tokens2[0][0];
my @nest;
my $temp;
$i = 0;
my $unprocessibles = 0;
my $processible = 0;
foreach my $lain ( @tokens2 )
{
	# 0-id, 1 год, 2 - вид, 3 - площадка, 4 - место, 5 - дерево, 6 - ввод
	#7 - дата, 8 - дата_число, 9 - хя, 10 - тя, 11 - пт, 12 - возраст, 13 - процессабилити
	#14 - кольца
	if( @$lain[0] eq $id )
	{
		$k2++;
	}
	elsif( @$lain[0] ne $id )
	{
		 
		
			$i++;
			@nest = @tokens2[($k1)..($k2-1)];
			$k1=$k2;
			$k2++;
			$id = @$lain[0];
			#print "Nest successfully sliced\n";
			#if ( $nest[0][0]==1038 )
			#{
			#	foreach my $tempp ( @nest )
			#	{
			#	foreach (@$tempp)
			#	{
			#		print $_;
			#		print "@";
			#	}
			#	print "\n";
			#	}
			#}
			#мы нашли индексы для одного гнезда
			#Имеет смысл попытаться его обработать, если процессабилити есть
			$processible = 0;
			foreach (@nest )
			{
				if ( @$_[13] eq 1 )
				{
					$processible = 1;
					last;
				}
				if ( int(@$_[11]) > 8 )
				{
					$processible = 0;
					print "Buggy input many children:@$_[0]\n";
					last;
				}
				if ( (@$_[12] ne "notfound")  && (int(@$_[12]) > 16)  )
				{
					$processible = 0;
					print "Buggy input many days:@$_[0],int(@$_[12]) \n";
					last;
				}
			}
			if ( $processible ==0 )
			{
				$unprocessibles++;
				#print "Unprocessible nest:".($nest[0][0])."\n";
			}
			else
			{
				#обработка гнезда
				#надо переписать
				my $maxeggs=0;
				my $starting_date=0;
				foreach ( @nest )
				{
					if( (int(@$_[9])+int(@$_[10])+int(@$_[11])) > $maxeggs )
					{
						$maxeggs = int(@$_[9])+int(@$_[10])+int(@$_[11]);
					}
					if( @$_[12] ne "notfound" )
					{
						if( @$_[2] =~ "Tur.pil" || @$_[2] =~ "Tur.ruf" )
						{
							$starting_date = date_to_day( @$_[7] ) - @$_[12] - 11 - $maxeggs;
						}
						else
						{
							$starting_date = date_to_day( @$_[7] ) - @$_[12] - 10 - $maxeggs;
						}
						last;
					}
				}
				my @resline;
				for(my $p=0; $p<6; $p++)
				{
					$resline[$p] = $nest[0][$p]."@";
				}
				for(my $p=6; $p<120; $p++)
				{
					$resline[$p] = "@";
				}
				$resline[6] = $starting_date."@";
				
				foreach (@nest )
				{
					if( date_to_day( @$_[7] ) - $starting_date >120)
					{
						printf("Totally bad date:$nest[0][0]\n");
						$unprocessibles++;
						last;
					}
					if( (int(@$_[10]) + int(@$_[11]) + int(@$_[9]) ) == 0 &&  ( date_to_day( @$_[7] ) - $starting_date < 0 || date_to_day( @$_[7] ) - $starting_date > 45)   )
					{
						1;
						#день до начала откладки. Видимо, строят гнездо.
						#либо день заметно после окончания цикла - орнитологи протормозили
					}
					elsif( (int(@$_[10]) + int(@$_[11])  ) == 0  )
					{
						
						if( date_to_day( @$_[7] ) - $starting_date >= 0 )
						{
							$resline[ 7+ date_to_day( @$_[7] ) - $starting_date ] = int(@$_[9])."@" ;
						}
						
						else
						{
							print "Buggy input date1:".@$_[0]."date:".date_to_day( @$_[7] )."start:".$starting_date."\n";
							$unprocessibles++;
							#my $tdate = date_to_day( "@$_[7]" );
							#print "$tdate - $starting_date \n";
							#print "@$_[7] to ".$tdate."\n";
						}						
					}
					else
					{
						if( date_to_day( @$_[7] ) - $starting_date >= 0 )
						{
						$resline[ 7+ date_to_day( @$_[7] ) - $starting_date ] = (int(@$_[10]) + int(@$_[11]))."@";
						}
						else
						{
							print "Buggy input date2:".@$_[0]."\n";
							$unprocessibles++;
							#my $tdate = date_to_day( "@$_[7]" );
							#print "$tdate - $starting_date \n";
							#print "@$_[7] to ".$tdate."\n";
						}
					}
				}
				#Ну, попробуем вывести обработку в отдельный файл
				print $out @resline;
				print $out "\n";
				#print "ID:$nest[0][0]\n";
			}
	}
	else
	{	
		print "WTF???\n";
	}
	
	#if($i == 0) 
	#{
	#	last;
	#	
	#}
}

print "checks:".$k1."\ntotal number of processible nests:".($i-$unprocessibles)."\nTotal nests:".$i."\n";

