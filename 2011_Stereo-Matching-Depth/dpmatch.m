function disp = dpmatch(iml, imr)
  [h, w, c] = size(iml);
  disp = zeros(h, w, 'uint8');
  for y = 1 : h
    lline = [];
    rline = [];
    for yy = -2 : 2
      yyy = min(max(1, y + yy), h);
      line(:, :) = iml(yyy, :, :);
      lline = [lline, line];
      line(:, :) = imr(yyy, :, :);
      rline = [rline, line];
    end
    disp(y, :) = dpline(lline, rline);
  end
end

function dline = dpline(lline, rline)
  infinity = 1e10;
  miss_penalty = 20;
  w = size(lline, 1);
  weights = zeros(w, w);
  moves = zeros(w, w);
  for i = 1 : w
    v = abs(repmat(lline(i, :), [w, 1]) - rline);
    weights(i, :) = sum(v, 2) / size(v, 2);
  end
  weights(2 : w, 1) = infinity;
  saved = weights;
  saved(2 : w, 1) = 0;
  var = zeros(3, 1);
  for i = 2 : w
    for j = 2 : w
      var(1) = weights(i, j - 1) + miss_penalty - saved(i, j - 1);
      var(2) = weights(i - 1, j - 1);
      var(3) = weights(i - 1, j) + miss_penalty - saved(i - 1, j);
      [weights(i, j), moves(i, j)] = min(var + weights(i, j));
    end
  end
  i = w;
  [~, j] = min(weights(i, :));
  dline = zeros(w, 1);
  while i > 0 && j > 0
    dline(i) = i - j;
    switch moves(i, j)
      case 1
        i = i + 1;
      case 3
        j = j + 1;
    end
    i = i - 1;
    j = j - 1;
  end
  %figure(2);
  %plot(dline);
end
