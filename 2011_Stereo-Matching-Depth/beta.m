TsukubaL = imread( 'datasets/tsukuba1.png');
TsukubaR = imread( 'datasets/tsukuba2.png');

TsukubaT = imread( 'datasets/tsukuba_groundTruth_1_2.png' );
TsukubaD = uint8(disparity( rgb2gray(TsukubaL), rgb2gray(TsukubaR) ) * 8);

LeftL = imread( 'datasets/left2.png');
LeftR = imread( 'datasets/right2.png');

LeftD = uint8(disparity( rgb2gray(LeftL), rgb2gray(LeftR) ) * 8);

TsukubaM = uint8(stereomatch( rgb2gray(TsukubaL), rgb2gray(TsukubaR) , 3, 30, 0) * 12) ;
LeftM = uint8(stereomatch( rgb2gray(LeftL), rgb2gray(LeftR), 3, 30, 0 ) * 12);



subplot( 2,2,1 ), imshow( TsukubaD );

subplot( 2,2,2 ), imshow( LeftD );

subplot( 2,2,3 ), imshow( TsukubaM );

subplot( 2,2,4 ), imshow( LeftM );
