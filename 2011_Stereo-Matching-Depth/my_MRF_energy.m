function [ output_args ] = my_MRF_energy( field, left, right, lambda=1 )
%MY_MRF_ENERGY This function calculates the energy of MRF of disparity
%between 2 images
%   E = \Sigma \phi(x_{i,j}) + \Sigma \phi(x_{i,j}, x_{\ro(i,j)})
%   E = Eucl( val(n,m), val(n + dis, m)) + "�� �� �����, ��� 4 �������"
%����� � ���, ��� ���� �������� ����������� �� ������, �� ��� ����
%������������ �� �������� ��������.

maxm = size( field, 2 );
maxn = size( field, 1);

totalenergy = 0;
parfor n = 2:size( field, 1 )-1
    for m = 2:size( field, 2)-1
        %%Unary potentials
        if( m + field( n, m ) < 1 || m + field( n, m ) > maxm )
            eloc = 1;
            
            eside = 4;
            
        else
            debug = abs( left( n, m,:) - right( n, m + field( n, m ), : ) );
            debug1 = debug(:);
            eloc = norm(single(debug1), 2);
            
%             eside1 = abs( left( n-1, m,:) - right( n-1, m + field( n, m ), : ) );
%             eside1 = eside1(:);
%             eside1 = norm(single(eside1(:)));
%             
%             
%             eside2 = abs( left( n+1, m,:) - right( n+1, m + field( n, m ), : ) );
%             eside2 = eside2(:);
%              eside2 = norm(single(eside2(:)));
%             
%             eside3 = abs( left( n, m+1,:) - right( n, m + 1 + field( n, m ), : ) );
%             eside3 = eside3(:);
%              eside3 = norm(single(eside3(:)));
%             
%             eside4 = abs( left( n, m-1,:) - right( n, m - 1 + field( n, m ), : ) );
%             eside4 = eside4(:);
%              eside4 = norm(single(eside4(:)));
%             
%             eside = eside1 + eside2 + eside3 + eside4;
              eside = lambda*(...
                                (field(n,m) - field(n-1,m))^2+...
                                (field(n,m) - field(n+1,m))^2+...
                                (field(n,m) - field(n,m-1))^2+...
                                (field(n,m) - field(n,m+1))^2)
                          
        end;
       
        totalenergy = totalenergy + eloc + eside;
        
        %%Binary potentials
        %upper
%         if( n == 1 || n == maxn || m == 1 || m == maxm )
%             eside = 0;
%         else
%             eside = 
%         end;
        
        
    end
end

output_args = totalenergy;

end
