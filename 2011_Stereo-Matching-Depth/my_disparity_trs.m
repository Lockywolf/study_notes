function [ disp_field ] = my_disparity_trs( Left, Right )

% Left = int16(Left);
% Right= int16(Right);
%We have a non-convex energy field.
%We divide it into trees - equal to horizontal and vertical lines.

%We minimize energy on each tree independently, but with reweight.
%As this is a prototype, we do everything stupidly.

% hWaitBar = waitbar(0, 'Performing unary energy counting...');

n = size( Left, 1 );
m = size( Left, 2 );
%d = m;%d can be different. Here I do not limit disparities, but they can be limited.
d = 20;

unary_omegas = ones( n, m, 2d+1 )*1000;

idtic = tic();

for i = 1:n
    for j = 1:m
        mind = max( 1, j-d);
        maxd = min( m, j+d);
        for k = mind:maxd
            l = Left(i, j, :);
            r = Right(i, k, :);
            sl = squeeze( l );
            sr = squeeze( r );
            diff = int16(sl) - int16(sr);
            ad = single( abs( diff ) );
            nad = norm( ad );
            unary_omegas( i, j, k-j+d+1 ) = nad;
        end
    end
    %t=t+1;
    waitbar(i/n,hWaitBar);
    %fprintf( 'Processed %d lines.\n', i);
    toc(idtic)
end

close(hWaitBar); 
field = unary_omegas;



%������ ��������� �������� ����������

%E(p,q) = abs( p - q )*alpha;

%E(graph) = \sum E_i
conf = zeros( n, m);
for z = 1:n
ePrev = zeros(2*d+1,1);


for i = 1:m-1
    for j = 1:2*d+1%��������� �������� ������ �������������
        Eint = ones( 2*d+1,1)*1000;
        for k = 1:2*d+1 %��������� �������� ����� �������������
            Eint( k ) = ePrev(k) + field(z,i,k) + pair_energy( k, j);
        end
        Eloc(j) = min( Eint );%�������� ����������� ������� ����� ������� ��� �������� ������ �������������
        vall = find( Eint == Eloc(j));%�������� ����� ������������� � ����������� �������� ��� �������� ������ �������������
        E_val(i, j) = vall(1);
        
    end
    ePrev = Eloc(:);
end



Eglob = min( ePrev );
conf(z,m) = find( ePrev == Eglob );

for i= m-1:-1:1
    conf(z,i) = E_val( i, conf(z,i+1));
end
    
fprintf('line number %d\n', z);
end


%field = energy;
disp_field = conf;

end

function [ energy ] = pair_energy( a, b )
    A = 10;
    energy = A * abs( a - b);
    
end
