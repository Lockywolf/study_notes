fprintf('This is the file prepared for the credit course CV, 6 year MIPT student Vladimir Nikishkin.\n');

% Preparing Data

TsukubaL = imread( 'datasets/tsukuba1.png');
TsukubaR = imread( 'datasets/tsukuba2.png');

TsukubaD = uint8(disparity( rgb2gray(TsukubaL), rgb2gray(TsukubaR) ) * 8);
TsukubaT = imread( 'datasets/tsukuba_groundTruth_1_2.png' );

TsukubaDP = dpmatch(TsukubaL, TsukubaR);

% TsukubaLWFBlock = uint8(my_disparity_block( TsukubaL, TsukubaR ) * 6.2);
% TsukubaLWFTRW = my_disparity_trs(TsukubaL, TsukubaR);
TsukubaLWFBlock = 255 - TsukubaDP * 5;

% TsukubaLWFBlock = load( 'BLOK-LWF.mat' );
TsukubaLWFTRW = load( 'TRW-LWF.mat' );

TsukubaM = uint8(stereomatch( rgb2gray(TsukubaL), rgb2gray(TsukubaR) , 3, 30, 0) * 12) ;

figure( 1 );
% 
 subplot( 2, 3, 1), imshow( TsukubaL );
% subplot( 3, 2, 2), imshow( TsukubaR );
% subplot( 3, 2, 3), imshow( TsukubaD );
% subplot( 3, 2, 4), imshow( TsukubaT );
% subplot( 3, 2, 5), imshow( TsukubaLWF );

subplot( 2, 3, 3), imshow( TsukubaD );
subplot( 2, 3, 2), imshow( TsukubaT );



subplot( 2, 3, 4), imshow( 255 - TsukubaLWFBlock );
subplot( 2, 3, 5), imshow( TsukubaLWFTRW.work );

subplot( 2, 3, 6), imshow( TsukubaM );



% 
% ConesL = imread( 'datasets/cones1.png');
% ConesR = imread( 'datasets/cones2.png');
% 
% ConesD = uint8(disparity( rgb2gray(ConesL), rgb2gray(ConesR) )) * 4;
% ConesT = imread( 'datasets/cones_groundTruth_1_2.png' );
% 
% figure( 2 );
% 
% subplot( 2, 2, 1), imshow( ConesL );
% subplot( 2, 2, 2), imshow( ConesR );
% subplot( 2, 2, 3), imshow( ConesD );
% subplot( 2, 2, 4), imshow( ConesT );

