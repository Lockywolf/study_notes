
function [spdmap, dcost, pcost, wcost] = stereomatch(imgleft, imgright, windowsize, disparity, spacc)

% Set Parameters
WS  = uint16(windowsize);               % Size of thw window
WS2 = uint16( ( WS - 1 ) / 2 );         % Half 
D   = uint16(disparity)+1;              % Max disparity

% image sizes
heightL = uint16( size( imgleft, 1 ) );    heightR = uint16( size( imgright, 1 ) );
widthL  = uint16( size( imgleft, 2 ) );    widthR  = uint16( size( imgright, 2 ) );
if ( heightL ~= heightR  ||  widthL ~= widthR )
    error('Height and width of left and right image must be equal');
end

% Init
pcost = zeros( heightL, widthL, D, 'uint8' );
wcost = zeros( heightL, widthL, D, 'single' );
dmap  = zeros( heightL, widthL, 'uint8' );
dcost = zeros( heightL, widthL, 'single' );
h = zeros(WS,WS,'double'); h(1,1) = 1; h(1,WS) = -1; h(WS,1) = -1; h(WS,WS) = 1;

% pixel cost
for Dc = 1 : D
   maxL = widthL + 1 - Dc; 
   pcost(:, Dc : widthL, Dc ) = imabsdiff( imgright( :, 1 : maxL), imgleft( :, Dc : widthL) );
end

% cumulative cost over image
icost = single(pcost);
icost = cumsum( cumsum( icost ), 2 );

% window cost
wcost = imfilter(icost,h,'same','symmetric');

% Search disparity value
[ dcost(:,D+WS2:widthL), dmap(:,D+WS2:widthL)] = min( wcost(:,D+WS2:widthL,:),[], 3 );
for j=WS2+1:D+WS2
    [ dcost(:,j), dmap(:,j)] = min( wcost(:, j, 1 : (j - WS2) ),[], 3 );
end


warning off;
spdmap = single(dmap-1);


if spacc==1
for j=D+1:widthL
for i=1:heightL
if dmap(i,j)>1 && dmap(i,j)<D
p = polyfit2((single(dmap(i,j)-2:dmap(i,j))),shiftdim(single(wcost(i,j,dmap(i,j)-1:dmap(i,j)+1)),1),2);
temp=roots(p);
spdmap(i,j)=real(temp(1));
end
end
end
end
warning on;

