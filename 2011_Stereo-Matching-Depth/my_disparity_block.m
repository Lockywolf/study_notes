function [result] = my_dsparity_block( Left, Right )

Left = rgb2gray( Left );
Right = rgb2gray( Right );

Dbasic = zeros(size(Left), 'single');

disparityRange = 30;

halfBlockSize = 3;
blockSize = 2*halfBlockSize+1;

tmats = cell(blockSize);


hWaitBar = waitbar(0, 'Performing basic block matching...');
nRowsLeft = size(Left, 1);


for m=1:nRowsLeft
    minr = max(1,m-halfBlockSize);
    maxr = min(nRowsLeft,m+halfBlockSize);
    
    for n=1:size(Left,2)
        minc = max(1,n-halfBlockSize);
        maxc = min( size( Left, 2 ), n+halfBlockSize );
        % Compute disparity bounds.
        mind = max( -disparityRange, 1-minc );
%         mind = max( 0, 1-minc );
        
        maxd = min( disparityRange, size(Left,2)-maxc );
        
        % Construct template and region of interest.
        template = Left(minr:maxr,minc:maxc);
        templateCenter = floor((size(template)+1)/2);
        roi = [minc+templateCenter(2)+mind-1 ...
               minr+templateCenter(1)-1 ...
               maxd-mind+1 1];
        % Lookup proper TemplateMatcher object; create if empty.
        if isempty(tmats{size(template,1),size(template,2)})
            tmats{size(template,1),size(template,2)} = ...
                vision.TemplateMatcher('ROIInputPort',true);
        end
        thisTemplateMatcher = tmats{size(template,1),size(template,2)};

        % Run TemplateMatcher object.
        loc = step(thisTemplateMatcher, Right, template, roi);
%         Dbasic(m,n) = uint8(loc(1) - roi(1) + mind);
          Dbasic(m,n) = uint8(loc(1) - roi(1));
            
    end
    waitbar(m/nRowsLeft,hWaitBar);
end

close(hWaitBar);   
        
        

result = Dbasic;
end
