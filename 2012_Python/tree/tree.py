class Tree( object ):
	children = set()
	parent = None
	value = None
	def __get_parent__(self):
		return self.parent
	def __get_value__(self):
		return self.value
	def __put_value__(self, value):
		self.value = value
		return self.value
	def __get_children__(self):
		return self.children
	def __add_child__( self, child ):
		self.children.add( child )
		child.parent = self
	def __remove_child__( self, child ):
		if self.children.__contains__( child ):
			self.children.remove( child )
			child.parent = None
	def __parent__( self, parent ):
		if self.parent != None:
			return None
		else:
			parent._add_child( self )
	def count_children( self ):
		size = 0
		for i in children:
			if type(i) == Tree:
				size += i.count_children()
			else:
				size +=1
		return size
	def find_by_value( self, value ):
		if self.value == value:
			return self
		toret = None
		for i in self.childern:
			p = i.find_by_value()
			if p != None:
				return p
		return None

a = Tree()
b = Tree()
c = Tree()

a.__add_child__( b );
a.__add_child__( c );
