import decorator
import math

@decorator.cached
def my_sin( arg ) :
	return math.sin(  arg )

@decorator.cached
def my_cos( arg ) :
	return math.cos(  arg )

print my_sin( float(3.0) )
print my_sin( float(3.0) )

	
print my_cos( float(3.0) )
print my_cos( float(3.0) )
