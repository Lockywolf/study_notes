class SortedDict(dict):
#	def __repr__(self):
#		sorted3 = sorted(self.iteritems() )
#		return str(sorted3)
	def __getattr__(self, name):
		return self[name]
	def __setattr__(self, name, value):
		self[name] = value
	def __delattr__(self, name):
		del self[name]

#		return "Point(%d, %d)" % (self.a, self.b)
