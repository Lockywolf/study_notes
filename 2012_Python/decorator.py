def cached( myfunction ):
	T = dict()
	def f( *args ):		
		if T.has_key(args):
			print "debug"
			return T[args]
		else: 
			T[args]=myfunction( args[0] )
			return T[args]
	return f

