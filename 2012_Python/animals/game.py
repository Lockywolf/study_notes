#!python
import random
from sys import *

dimension = 10;

class switch(object):
    def __init__(self, value):
        self.value = value
        self.fall = False

    def __iter__(self):
        """Return the match method once, then stop"""
        yield self.match
        raise StopIteration
    
    def match(self, *args):
        """Indicate whether or not to enter a case suite"""
        if self.fall or not args:
            return True
        elif self.value in args: # changed for v1.5, see below
            self.fall = True
            return True
        else:
            return False



class Cell( object ):
	sex = int()#0 - empty, 1-stone, 2 - food, 3 - carniv
	hunger = 5;
	performed = False;
	viewed = False;
	def __init__( self, sex, x, y):
		self.sex = sex
		
		
		
	
class Game( object ):
	field = list()
	
	def __init__( self ):
		for i in xrange(0,dimension):
			self.field.append([])
			for k in xrange(0,dimension):
				self.field[i].append( Cell( random.randint(0,3), i, k ) )
	def __repr__( self ):
		return_string = ''
		for i in xrange(0,dimension):
			for k in xrange(0,dimension):
				return_string += str(self.field[i][k].sex)
			return_string += '\n'
		return return_string
	def make_list(self, i, k):
		if (i, k) == (0, 0):
			return ( self.field[0][1], self.field[1][0] )
		if (i, k) == (0, dimension-1):
			return ( self.field[0][dimension-2], self.field[1][dimension-1] )
		if (i, k) == (dimension-1, 0):
			return ( self.field[dimension-2][0], self.field[dimension-1][1] )
		if (i, k) == (dimension-1, dimension-1):
			return ( self.field[dimension-1][dimension-2], self.field[dimension-2][dimension-1] )
		if i == 0:
			return ( self.field[i][k-1], self.field[i+1][k], self.field[i][k+1] )
		if i == 9:
			return ( self.field[i][k-1], self.field[i-1][k], self.field[i][k+1] )
		if k == 0:
			return ( self.field[i-1][k], self.field[i+1][k], self.field[i][k+1] )
		if k == 9:
			return ( self.field[i-1][k], self.field[i-1][k], self.field[i][k-1] )
		return ( self.field[i-1][k], self.field[i+1][k], self.field[i][k+1], self.field[i][k-1] )
		
	def step(self):
		for i in xrange(0,dimension):
			for k in xrange(0,dimension):
				self.field[i][k].performed = False
	
		for i in xrange(0,dimension):
			for k in xrange(0,dimension):
				if self.field[i][k].performed == False:
					self.field[i][k].performed = True
					for case in switch( self.field[i][k].sex ):
						if case( 0 ):#empty
							break
						if case( 1 ):#stone
							break
						if case( 2 ):#food
							todo = random.randint(0, 2)
							if todo == 0:
								break
							if todo == 1: #move somewhere
								#make list of neighbors, rand, label
								#move
								mylist = list( self.make_list(i, k ) )
								for t in xrange( 0, len(mylist)-1):
									choose = mylist[ random.randint(0, len( mylist )-1)]
									if choose.sex == 0:
										choose.sex = self.field[i][k].sex
										choose.performed = True
										choose.hunger = self.field[i][k].hunger
										self.field[i][k].sex = 0
										self.field[i][k].hunger = 10
										break
									mylist.remove( choose )
									
										
								break
							if todo == 2:
								#make list of neighbors, rand, label
								#breed
								mylist = list( self.make_list(i, k ) )
								for t in xrange( 0, len(mylist)-1):
									choose = mylist[ random.randint(0, len( mylist )-1)]
									if choose.sex == 0:
										choose.sex = self.field[i][k].sex
										choose.performed = True
										choose.hunger = 5
										break
									mylist.remove( choose )
									
								
								break
						if case( 3 ):#carniv
							if self.field[i][k].hunger == 0:#die of hunger
								self.field[i][k].hunger = 9
								self.field[i][k].sex = 0
								break
							self.field[i][k].hunger -= 1 
							todo = random.randint(0, 2)
							if todo == 0:
								break
							if todo == 1: #move somewhere
								#make list of neighbors, rand, label
								#move if move to food - hunger++%10
								mylist = list( self.make_list(i, k ) )
								for t in xrange( 0, len(mylist)-1):
									choose = mylist[ random.randint(0, len( mylist )-1)]
									if choose.sex == 0 or choose.sex == 2:
										if choose.sex == 2:
											self.field[i][k].hunger = 10;
											print "eaten"
										choose.sex = self.field[i][k].sex
										choose.performed = True
										choose.hunger = self.field[i][k].hunger
										self.field[i][k].sex = 0
										self.field[i][k].hunger = 10;
										break
									mylist.remove( choose )
									
								
								break
							if todo == 2:
								#make list of neighbors, rand, label
								#breed
								mylist = list( self.make_list(i, k ) )
								for t in xrange( 0, len(mylist)-1):
									choose = mylist[ random.randint(0, len( mylist )-1)]
									if choose.sex == 0:
										choose.sex = self.field[i][k].sex
										choose.performed = True
										choose.hunger = 5
										break
									mylist.remove( choose )
								break
mygame = Game()
#for i in xrange(0, 100):
	#a = stdin.read(1)
#	mygame.step()
#	print mygame

print mygame

from Tkinter import *

root = Tk()

# w = Label(root, text="Hello, world!")
# w.pack()
line = list()
window = list() 
for i in xrange(0, dimension ):
	print "test"
	line = list()
	for k in xrange(0, dimension ):
		w = Label(root, text = str(mygame.field[ i][ k ].sex) );
		#aux = str(w.linktocell.sex) + "." + str(w.linktocell.hunger)
		w.linktocell = mygame.field[ i][ k ]
		w.config( text = ( str(w.linktocell.sex) + "." + str(w.linktocell.hunger) ))
		if w.linktocell.sex == 0:
			w.config( bg='white')
		if w.linktocell.sex == 1:
			w.config( bg='gray')
		if w.linktocell.sex == 2:
			w.config( bg='green')
		if w.linktocell.sex == 3:
			w.config( bg='red')
		
		w.ypos = i
		w.xpos = k
		w.grid( row = i, column = k )
		line.append( w )
	window.append( line )
	
def callback():
	mygame.step()
	for ml in window:
		for w in ml:
			w.config( text = ( str(w.linktocell.sex) + "." + str(w.linktocell.hunger) ))
			if w.linktocell.sex == 0:
				w.config( bg='white')
			if w.linktocell.sex == 1:
				w.config( bg='gray')
			if w.linktocell.sex == 2:
				w.config( bg='green')
			if w.linktocell.sex == 3:
				w.config( bg='red')
#	print mygame
#	print "click!"

b = Button(root, text="OK", command=callback )
b.grid(row = dimension)

root.mainloop()