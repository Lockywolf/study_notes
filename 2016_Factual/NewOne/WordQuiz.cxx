/*- 
 * WordQuiz.cxx This file is written as a part of the test assignment for the Factual interview in 2016.
 * Written by Vladimir Nikishkin.
 * This code belongs to public domain.
 *
 */

/*! \file WordQuiz.cxx
 *  \brief The main and the only file of the project.
 */
 
/*! \mainpage FactualInterview documentation.
 *  
 *
 *
 * This program finds solves a WordQuiz puzzle often found in newspapers.\n
 * It accepts a file name as its first argument and prints the starting and the ending index in the form (startY, startX)(endY, endX) for every word.
 * 
 * Build by \n
 * Windows:\n
 * cl /EHsc WordQuiz.cxx \n
 */
 
 
 
#include <iostream>
#include <fstream>
#include <vector>
#include <string>
#include <sstream>
#include <memory>

//errors
#define E_NOERR 0
#define E_BADFILE -1
#define E_BADFMT  -2
#define E_BADTYPE -3
#define E_OOM -4
#define E_UNK -5
#define E_BADARG -6
#define E_BADWRAP -7

void SA_IS(unsigned char *s, int *SA, int n, int K, int cs);
void calc_lcp(int n, int* rank, int* sa, unsigned char *s , int* lcp );

namespace FactualInterview2016
{
/*! \namespace FactualInterview2016
    \brief The namespace for the exam code.

    This namespace contains all the code for the exam program.
	
	
*/
	
	

/*! \class ErrorException 
    \brief This class indicates general error in the program. All the other errors are inherited from it.

*/
class ErrorException: public std::exception {};

/*! \class BadArgException
    \brief This exception is thrown if there is some problem with the amount of input arguments.

*/
class BadArgException : public ErrorException {};

/*! \class BadFileException
    \brief This exception is thrown if there is some problem with reading the file.

*/
class BadFileException : public ErrorException {};

/*! \class BadWrapException
    \brief This exception is thrown if the wrapping mode is incorrect.

*/
class BadWrapException : public ErrorException {};



typedef std::pair<int,int> vec2d;
vec2d operator+( const vec2d , const vec2d );
vec2d operator*( const vec2d , const int );


class myResponse
{
	private:
		vec2d start;
		vec2d end;
		bool found;
	public:
		myResponse( int A, int B, int C, int D)
		{
			start = std::make_pair(A, B);
			end = std::make_pair(C,D);
			found = true;
		};
		
		myResponse( vec2d first, vec2d second)
		{
			start = first;
			end = second;
			found = true;
		};
		
		
		myResponse( bool fnd )
		{
			found = false;
		}
		friend std::ostream& operator<<( std::ostream&, const myResponse& );
	
};

struct myCoordinate
{
	vec2d coordinate;
	vec2d direction;
};

struct myInputData
{
	int M;
	int N;
	std::vector< std::vector<unsigned char> > matrix;
	std::string wrapping_mode;
	int P;
	std::vector<std::string> words;
};

class Parser
{
	public:
		static myInputData parseFactualInput( std::string);
};

class AbstractWordFinder 
{
	public:
		virtual ~AbstractWordFinder() {};
		virtual myResponse find_word( std::string ) = 0;
		
};

class WordFinder : public AbstractWordFinder
{	
	protected:
		std::vector< std::vector<unsigned char> > matrix;
		int nrows = matrix.size();
		int ncols = matrix[1].size();
		vec2d end_index;
		
		std::vector< myCoordinate > expanded_matrix_coordinates;
		std::vector<unsigned char> expanded_matrix;
		int* sa;
		int* lcp;
		int* rank;
		unsigned char *s;
		int n;
		
		std::vector< vec2d > shifts = {
												std::make_pair(1,0),//down
												std::make_pair(-1,0),//up
												std::make_pair(0,1),//right
												std::make_pair(0,-1),//left
												std::make_pair(1,1),//down-right
												std::make_pair(-1,1),//up-right
												std::make_pair(1,-1),//down-left
												std::make_pair(-1,-1)//up-left
												};


		
	public: 

		WordFinder( std::vector< std::vector<unsigned char> > m) : matrix(m) {};
		~WordFinder() {};
		virtual myResponse find_word( std::string ) = 0;
		
	private:

		
		virtual bool compare_to_shift( unsigned char letter, vec2d begin, vec2d direction, int step) = 0;
		
	
};



class SimpleWordFinder : public WordFinder
{
	public:
		SimpleWordFinder( std::vector< std::vector<unsigned char> > m ) : WordFinder( m )
		{
			//WordFinder::WordFinder( m ) ;
			build_expanded_matrix();
			n = expanded_matrix.size()-1;
			sa = new int[n];
			lcp = new int[n];
			rank = new int[n];
			s = &expanded_matrix[0];
			
			//std::cout << "Debug:flattened matrix of size "<< n << ":";
			//for( int i=0; i<n; i++)
			//{
				//std::cout << expanded_matrix[i];
			//	std::cout << s[i];
				
			//}
			//std::cout << std::endl;
			//std::cout << std::endl;
			
			//std::cout << "Running suffix array magic \n";
			
			//SA_IS(s, sa, n + 1, 256, 1);
			
			SA_IS(s, sa, n +1 , 256, 1);
			calc_lcp(n, &rank[0], &sa[0], s, &lcp[0]);
			
			//std::cout << "Suffix array magic ended \n";
			
			std::string str ( expanded_matrix.begin(), expanded_matrix.end() );
			/*for (int i = 0; i < n; i++) 
			{
				//std::cout << " Debug: sa: " << sa[i+1] << " ";
				
				std::cout << str.substr(sa[i + 1]);
				
				auto p = str.substr(sa[i + 1]);
				
				if (i < n - 1)
					std::cout << " " << lcp[i + 1];
				std::cout << std::endl;
			}*/
			
		};
		
		~SimpleWordFinder()
		{
			delete sa;
			delete lcp;
			delete rank;
		};
		
		
		
		
		/*virtual myResponse find_word( std::string word )
		{
			std::string str ( expanded_matrix.begin(), expanded_matrix.end() );
			//std::string rword( word );
			//std::reverse( rword.begin(), rword.end());
			//printf( "debug: %s \n", myword_c);
			int lb = 1;
			int rb = n-1;
			while( true)
			{
				if( lb == rb-1 )
				{
					if( str.substr( sa[lb] ).substr(0, word.size()) == word )
					{
						//found todo
						std::cout << "found!" << std::endl;
						return myResponse( false );
						
					}
					else break;
				}
				
				std::string totest = str.substr( sa[(lb+rb)/2]);
				std::cout << "debug-totest: " << totest << std::endl;
				
				
				if( totest < word)
				{
					lb = (lb+rb)/2;
				}
				else
				{
					rb = (lb+rb)/2;
				}
			}
			
			
			
			
			
			return myResponse( false );
			
		};*/
		virtual myResponse find_word( std::string word )
		{
			
			for( int numrow = 0; numrow < nrows; numrow++)
			{
				for(int numcol = 0; numcol < ncols; numcol++)//start from every cell in a matrix
				{
					//std::cout << matrix[numrow][numcol];
					auto startpoint = std::make_pair(numrow, numcol);
					
					for( int i=0; i<shifts.size(); i++)//looking in all permitted directions
					{
						auto working_shift = shifts[i];//now we go to the direction of the shift and look for the pattern
						//int counter = 0;//
						int letnum=0;
						for( ; letnum<word.size(); letnum++)
						{
							if( compare_to_shift( word[letnum], startpoint, working_shift, letnum ) )//sets end_index inside
								continue;
							else
								break;
						}
						if( letnum == word.size())
						{
							return myResponse( startpoint, end_index);
						}
					}
					
					
				}

			}
			return myResponse( false );
		}
		
	private:
	
		
	
		virtual bool compare_to_shift( unsigned char letter, vec2d begin, vec2d direction, int step)
		{
			auto index = begin + direction*step;
			if( index.first < 0 || index.first >= ncols || index.second < 0 || index.second >= nrows)
				return false;
			if( letter == matrix[index.first][index.second])
			{
				end_index = index;
				return true;
			}
			return false;
		};
		
		void build_expanded_matrix( void )
		{
			
			for( int i=0; i<nrows; i++)//horizontal flattening
			{
				for( int j=0; j<ncols; j++)
				{
					expanded_matrix.push_back( matrix[i][j] );
					expanded_matrix_coordinates.push_back( myCoordinate{ std::make_pair(i,j), std::make_pair( 0,1)});
					//std::cout << matrix[i][j] ;
				}
				expanded_matrix.push_back( '$' );
				expanded_matrix_coordinates.push_back( myCoordinate{ std::make_pair(-1,-1), std::make_pair( 0,0)});
			}
			
			for( int i=0; i<ncols; i++)//vertical flattening
			{
				for( int j=0; j<nrows; j++)
				{
					expanded_matrix.push_back( matrix[j][i] );
					expanded_matrix_coordinates.push_back( myCoordinate{ std::make_pair(j,i), std::make_pair( 1, 0)});
					//std::cout << matrix[i][j] ;
				}
				expanded_matrix.push_back( '$' );
				expanded_matrix_coordinates.push_back( myCoordinate{ std::make_pair(-1,-1), std::make_pair( 0,0)});
			}
			
			//std::cout << std::endl;
			{//diagonal up-right flattening
				for( int i=0; i<( nrows); i++)//upper triangle, vector up-right
				{
					int j=0;
					while( i-j>=0 && j<nrows)
					{
						//std::cout << "i, j: " << i << " " << j << std::endl;
						expanded_matrix.push_back( matrix[i-j][j] );
						expanded_matrix_coordinates.push_back( myCoordinate{ std::make_pair(i-j,j), std::make_pair( -1,1)});
						//std::cout << matrix[i-j][j];
						j++;
						//getchar();
					}
					expanded_matrix.push_back( '$' );
					expanded_matrix_coordinates.push_back( myCoordinate{ std::make_pair(-1,-1), std::make_pair( 0,0)});
				}
				//std::cout << std::endl;
				for( int i=1; i<( ncols); i++)//lower triangle, vector up-right
				{
					int j=0;
					while( nrows-j>=0 && i+j<ncols)
					{
						//std::cout << "i, j: " << i << " " << j << std::endl;
						expanded_matrix.push_back( matrix[nrows-j-1][i+j] );
						expanded_matrix_coordinates.push_back( myCoordinate{ std::make_pair(nrows-j-1,i+j), std::make_pair( -1, 1 )});
						//std::cout << matrix[nrows-j-1][i+j] ;
						j++;
					}
					expanded_matrix.push_back( '$' );
					expanded_matrix_coordinates.push_back( myCoordinate{ std::make_pair( -1, -1 ), std::make_pair( -1, 1)});
				}
			}
			//std::cout << std::endl;
			
			{//diagonal down-right flattening. starting point left bottom
				for( int i=nrows-1; i>=0; i--)//lower triangle, vector down-right
				{
					int j=0;
					while( i+j<nrows && j<ncols)
					{
						//std::cout << "i, j: " << i << " " << j << std::endl;
						expanded_matrix.push_back( matrix[i+j][j] );
						expanded_matrix_coordinates.push_back( myCoordinate{ std::make_pair(i+j, j), std::make_pair( 1,1)});
						//std::cout << matrix[i+j][j];
						j++;
						//getchar();
					}
					expanded_matrix.push_back( '$' );
					expanded_matrix_coordinates.push_back( myCoordinate{ std::make_pair(-1,-1), std::make_pair( 0,0)});
				}
				//std::cout << std::endl;
				for( int i=1; i<( ncols); i++)//upper triangle, vector down-right
				{
					int j=0;
					while( i+j < nrows && i+j<ncols)
					{
						//std::cout << "i, j: " << i << " " << j << std::endl;
						expanded_matrix.push_back( matrix[j][i+j] );
						expanded_matrix_coordinates.push_back( myCoordinate{ std::make_pair(j,i+j), std::make_pair( 1,1)});
						//std::cout << matrix[j][i+j] ;
						j++;
					}
					expanded_matrix.push_back( '$' );
					expanded_matrix_coordinates.push_back( myCoordinate{ std::make_pair(-1, -1), std::make_pair( 0, 0)});
				}
				expanded_matrix.push_back( 0 );
				expanded_matrix_coordinates.push_back( myCoordinate{ std::make_pair(-1, -1), std::make_pair( 0, 0)});
			}
			
			
			
		};
};

class WrappingWordFinder : public WordFinder
{	
	public:
		WrappingWordFinder( std::vector< std::vector<unsigned char> > m ) : WordFinder( m ) 
		{
			used = std::vector< std::vector<bool> >(nrows, std::vector<bool>(ncols));
			
			for( int i = 0; i<nrows; i++)
				for( int j = 0; j<ncols; j++)
					used[i][j] = false;
		};
		
		virtual myResponse find_word( std::string word )
		{
			
			for( int numrow = 0; numrow < nrows; numrow++)
			{
				for(int numcol = 0; numcol < ncols; numcol++)//start from every cell in a matrix
				{
					//std::cout << matrix[numrow][numcol];
					auto startpoint = std::make_pair(numrow, numcol);
					
					for( int i=0; i<shifts.size(); i++)//looking in all permitted directions
					{
						auto working_shift = shifts[i];//now we go to the direction of the shift and look for the pattern
						//int counter = 0;//
						int letnum=0;
						for( ; letnum<word.size(); letnum++)
						{
							if( compare_to_shift( word[letnum], startpoint, working_shift, letnum ) )//sets end_index inside
								continue;
							else
								break;
						}
						if( letnum == word.size())
						{
							return myResponse( startpoint, end_index);
						}
					}
					
					
				}

			}
			return myResponse( false );
		}
	
	private:
		std::vector< std::vector<bool> > used;
		
		virtual bool compare_to_shift( unsigned char letter, vec2d begin, vec2d direction, int step)
		{
			
			if( step == 0)
			{
				for( int i = 0; i<nrows; i++)
					for( int j = 0; j<ncols; j++)
						used[i][j] = false;
			}
			auto index = begin + direction*step;
			index.first = index.first % nrows;
			if( index.first < 0) index.first+=nrows;
			index.second = index.second % ncols;
			if( index.second < 0) index.second+=ncols;
			
			//std::cout << index.first << " " << index.second << std::endl;
			
			if( used[index.first][index.second] == true )
				return false;
			if( letter == matrix[index.first][index.second])
			{
				end_index = index;
				used[index.first][index.second] = true;
				return true;
			}
			return false;
		}
};

class WordFinderFactory
{
	public:
		static AbstractWordFinder* getWordFinder( std::vector< std::vector< unsigned char > > matrix, std::string wrapping_mode)
		{
			if( wrapping_mode == "NO_WRAP")
				return new SimpleWordFinder( matrix );
			else if( wrapping_mode == "WRAP")
				return new WrappingWordFinder( matrix );
			else
				throw BadWrapException();
		}
};



int main( int argc, char** argv)
{
	
	try
	{
		if(argc !=2 )
		{
			std::cerr << "Usage: " << argv[0] << " filename " <<std::endl;
			//return( E_BADARG );
			throw BadArgException();
		}
		myInputData  InputData = Parser::parseFactualInput( argv[1] );
		
		std::unique_ptr<AbstractWordFinder> myWordFinder ( WordFinderFactory::getWordFinder( InputData.matrix, InputData.wrapping_mode ) );
			
		for( int i=0; i<InputData.P; i++)
		{
			//call a function to get coordinates
			//myResponse startend = find_word( InputData.words[i], InputData.matrix, InputData.wrap_flag);
			//std::cout << startend << std::endl;
			auto myres = myWordFinder->find_word( InputData.words[i]);
			std::cout << myres << std::endl;
			
		}

	//std::cout << "entering sarray \n";
		
	/*const int maxn = 200000;
	int* sa = new int[maxn];
	int* lcp = new int[maxn];
	int* rank = new int[maxn];
	
	unsigned char *s;
	int n;
	
	
	
    std::string str = "abcab";
    n = str.size();
    s = (unsigned char*) str.c_str();
    SA_IS(s, sa, n + 1, 256, 1);
    calc_lcp(n, &rank[0], &sa[0], s, &lcp[0]);

    for (int i = 0; i < n; i++) {
        std::cout << str.substr(sa[i + 1]);
        if (i < n - 1)
            std::cout << " " << lcp[i + 1];
        std::cout << std::endl;
    }
	
	delete sa, lcp, rank;*/
	
	return 0;
	
	}
	catch(std::bad_alloc ex)
	{
		printf( "bad alloc\n");
		return E_OOM;
	}
	catch(BadArgException ex)
	{
		
		printf( "bad arg\n");
		return E_BADARG;
	}
	catch(BadFileException ex)
	{
		printf( "bad file\n");
		return E_BADFILE;
	}
	catch( BadWrapException ex)
	{
		printf( "bad wrap\n");
		return E_BADWRAP;
	}
	catch(...)
	{
		printf( "Unknown Error\n");
		return E_UNK;
	}
}



std::ostream& operator<<( std::ostream& out, const myResponse& response)
{
	if( response.found == false )
	{
		out << "NOT FOUND";
		return out;
	}
	
	out << "(";
	out << response.start.first;
	out << ",";
	out << response.start.second;
	out << ")(";
	out << response.end.first;
	out << ",";
	out << response.end.second;
	out << ")";
    return out;
}

vec2d operator+( const vec2d A, const vec2d B)
{
	return std::make_pair( A.first+B.first, A.second+B.second);
}

vec2d operator*( const vec2d A, const int scalar)
{
	return std::make_pair( A.first*scalar, A.second*scalar);
}



myInputData Parser::parseFactualInput( std::string filename )
{
	try
	{
		std::string dummy;
		std::string inputline;
		std::ifstream myfile( filename );
		int M=0;
		int N=0;
		std::getline( myfile, inputline);
		std::stringstream mystream(inputline);
		mystream >> N >> M;
		//std::cout << N << " " << M << std::endl;
		std::vector< std::vector< unsigned char> > matrix (N, std::vector<unsigned char>(M));
		

		
		for( int i=0; i<N; i++ )
		{
			std::getline( myfile, dummy );//empty line
			std::getline( myfile, inputline );
			std::stringstream myMatStream(inputline);
			for( int j=0; j<M; j++ )
			{
				myMatStream >> matrix[i][j];
			//	std::cout << matrix[i][j];
			}
			//std::cout << std::endl;
		}
		std::getline( myfile, dummy);//empty line
		std::getline( myfile, inputline);
		//bool wrap_flag = false;
		std::string wrapping_mode = inputline;
		inputline.erase(inputline.find_last_not_of(" \n\r\t")+1);//trim a string crlf
		//wrap_flag = (bool)inputline.compare( "NO_WRAP");
		//std::cout << "    " << inputline << std::endl;
		
		int P = 0;
		std::getline( myfile, dummy);//empty line
		std::getline( myfile, inputline);//P
		std::stringstream mystream2(inputline);
		mystream2 >> P;
		
		//std::cout << "    numlines:" << P << std::endl;
		
		std::vector<std::string> words(P);
		for ( int i=0; i<P; i++)
		{
			myfile >> words[i];
			//std::cout << words[i] << std::endl;
		}
			//std::cout << "\n\n\n\n";
			
		return myInputData{ M, N, matrix, wrapping_mode, P, words};	
	}
	catch(...)
	{
		throw BadFileException();
	}
	
}

};


#ifndef TEST
int main( int argc, char** argv)
{

	return FactualInterview2016::main( argc, argv);

	
}
#endif


