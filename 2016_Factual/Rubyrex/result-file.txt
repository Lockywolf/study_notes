List of Restaurants' URLs

Salt and Straw  https://www.tripadvisor.co.uk/Restaurant_Review-g52024-d2372085-Reviews-Salt_Straw-Portland_Oregon.html

Smitty’s Famous Fish and Chicken   https://www.tripadvisor.co.uk/Restaurant_Review-g32272-d1010414-Reviews-Smitty_s_Famous_Fish_Chicken-Culver_City_California.html

Zankou Chicken    https://www.tripadvisor.co.uk/Restaurant_Review-g32655-d1133480-Reviews-Zankou_Chicken-Los_Angeles_California.html

Chego    https://www.tripadvisor.co.uk/Restaurant_Review-g32655-d2530827-Reviews-Chego-Los_Angeles_California.html

Ambala Dhaba (in Los Angeles)    https://www.tripadvisor.co.uk/Restaurant_Review-g32655-d379076-Reviews-Ambala_Dhaba-Los_Angeles_California.html

Colony Cafe    https://www.tripadvisor.co.uk/Restaurant_Review-g32655-d5046417-Reviews-The_Colony_Cafe-Los_Angeles_California.html

One website (tripadvisor)

Attributes:


name  /<div class="heading_height"><\/div>\n(.*?)\n<\/h1>/im 

streetAddress  /<span class="street-address" property="streetAddress">(.*?)<\/span>/im 

houseNum  /<span class="street-address" property="streetAddress">([0-9]+) .*?<\/span>/im

streetName  /<span class="street-address" property="streetAddress">[0-9]+ (.*?)<\/span>/im

addressLocality  /<span property=\"addressLocality\">(.*?)<\/span>/im

addressRegion  /<span property=\"addressRegion\">(.*?)<\/span>/im

postalCode  /<span property=\"postalCode\">(.*?)<\/span>/im

telephone  /<div class="fl phoneNumber">(.*?)<\/div>/im

servesCuisine  /<div class="title">\s*Cuisine\s*<\/div>\s*<div class="content">\s*(.*?)\s*<\/div>/im 

hoursToday  /<div class="today">Today<\/div> <div class="times">\s*<div class="time">(.*?)<\/div>/im 

servingType  /<div class="title">\s*Meals\s*<\/div>\s*<div class="content">\s*(.*?)\s*<\/div>/im 

#Hours of every day

hoursMonday  /<div class="detail">\s*<span class="day">\s*Monday\s*<\/span>\s*<span class="hours">\s*<div class='hoursRange'>\s*(.*?)\s*<\/div>\s*<\/span>\s*<\/div>/im 
hoursTuesday  /<div class="detail">\s*<span class="day">Tuesday<\/span>\n<span class="hours"><div class='hoursRange'>(.*?)<\/div><\/span>\n<\/div>/im 
hoursWednesday  /<div class="detail">\n<span class="day">Wednesday<\/span>\n<span class="hours"><div class='hoursRange'>(.*?)<\/div><\/span>\n<\/div>/im
hoursThursday  /<div class="detail">\n<span class="day">Thursday<\/span>\n<span class="hours"><div class='hoursRange'>(.*?)<\/div><\/span>\n<\/div>/im
hoursFriday  /<div class="detail">\n<span class="day">Friday<\/span>\n<span class="hours"><div class='hoursRange'>(.*?)<\/div><\/span>\n<\/div>/im
hoursSaturday  /<div class="detail">\n<span class="day">Saturday<\/span>\n<span class="hours"><div class='hoursRange'>(.*?)<\/div><\/span>\n<\/div>/im
hoursSunday  /<div class="detail">\n<span class="day">Sunday<\/span>\n<span class="hours"><div class='hoursRange'>(.*?)<\/div><\/span>\n<\/div>/im
