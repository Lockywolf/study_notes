
#ifndef __FIRST_H__
#define __FIRST_H__

#include <glib.h>
#include <glib-object.h>

G_BEGIN_DECLS


#define TYPE_SAMPLE (sample_get_type ())
#define SAMPLE(obj) (G_TYPE_CHECK_INSTANCE_CAST ((obj), TYPE_SAMPLE, Sample))
#define SAMPLE_CLASS(klass) (G_TYPE_CHECK_CLASS_CAST ((klass), TYPE_SAMPLE, SampleClass))
#define IS_SAMPLE(obj) (G_TYPE_CHECK_INSTANCE_TYPE ((obj), TYPE_SAMPLE))
#define IS_SAMPLE_CLASS(klass) (G_TYPE_CHECK_CLASS_TYPE ((klass), TYPE_SAMPLE))
#define SAMPLE_GET_CLASS(obj) (G_TYPE_INSTANCE_GET_CLASS ((obj), TYPE_SAMPLE, SampleClass))

typedef struct _Sample Sample;
typedef struct _SampleClass SampleClass;
typedef struct _SamplePrivate SamplePrivate;

struct _Sample {
	GObject parent_instance;
	SamplePrivate * priv;
};

struct _SampleClass {
	GObjectClass parent_class;
};


Sample* sample_construct (GType object_type);
Sample* sample_new (void);
void sample_run (Sample* self);
GType sample_get_type (void);


G_END_DECLS

#endif
