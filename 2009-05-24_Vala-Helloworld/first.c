
#include "first.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>




enum  {
	SAMPLE_DUMMY_PROPERTY
};
static gint sample_main (char** args, int args_length1);
static gpointer sample_parent_class = NULL;



Sample* sample_construct (GType object_type) {
	Sample * self;
	self = g_object_newv (object_type, 0, NULL);
	return self;
}


Sample* sample_new (void) {
	return sample_construct (TYPE_SAMPLE);
}


void sample_run (Sample* self) {
	g_return_if_fail (self != NULL);
	fprintf (stdout, "Hello, world!\n");
}


static gint sample_main (char** args, int args_length1) {
	Sample* sample;
	gint _tmp0;
	sample = sample_new ();
	sample_run (sample);
	return (_tmp0 = 0, (sample == NULL) ? NULL : (sample = (g_object_unref (sample), NULL)), _tmp0);
}


int main (int argc, char ** argv) {
	g_type_init ();
	return sample_main (argv, argc);
}


static void sample_class_init (SampleClass * klass) {
	sample_parent_class = g_type_class_peek_parent (klass);
}


static void sample_instance_init (Sample * self) {
}


GType sample_get_type (void) {
	static GType sample_type_id = 0;
	if (sample_type_id == 0) {
		static const GTypeInfo g_define_type_info = { sizeof (SampleClass), (GBaseInitFunc) NULL, (GBaseFinalizeFunc) NULL, (GClassInitFunc) sample_class_init, (GClassFinalizeFunc) NULL, NULL, sizeof (Sample), 0, (GInstanceInitFunc) sample_instance_init, NULL };
		sample_type_id = g_type_register_static (G_TYPE_OBJECT, "Sample", &g_define_type_info, 0);
	}
	return sample_type_id;
}




