#include<stdio.h>
#include<stdlib.h>
#include<string.h>
#define MU 1
#define SG 1
#define ADD 1

typedef struct entry
{
    int weight;
    char from;
} entry;

int main()
{
    char* word1 = "atgcatgcatgccgtacgta";
    char* word2 = "tgctgctgctgca";
    int wo1 = strlen(word1);
    int wo2 = strlen(word2);
    printf("%s\n%s\n",word1,word2);
    entry matrix[wo1][wo2];
    entry temp={0,'s'};
    matrix[0][0] = temp;
    int i = 0;
    temp.weight = 0;
    temp.from = 'l';
    for(i=1 ; i<wo1 ; i++)
    {
        matrix[i][0] = temp;
    }
    temp.weight=0;
    temp.from='u';
    for(i=1 ; i<wo2 ; i++)
    {
        matrix[0][i] = temp;
    }
    int k=0;
    for(i=1; i<wo1 ; i++)
    {
        for(k=1 ; k<wo2 ; k++)
        {
            int sdi=0;
            if( word1[i] == word2[k] )
                sdi = matrix[i-1][k-1].weight+2;
            else
                sdi = matrix[i-1][k-1].weight-MU;
            int slf = matrix[i-1][k].weight - SG;
            if( matrix[i-1][k].from == 'l') 
                slf = matrix[i-1][k].weight - ADD;
            int sup = matrix[i][k-1].weight - SG;
            if( matrix[i][k-1].from == 'u') 
                slf = matrix[i][k-1].weight - ADD;
            if( sup >= sdi && sup >= slf)
            {
                matrix[i][k].weight = sdi;
                matrix[i][k].from   = 'u';
            }
            else if ( slf >= sup && slf >= sdi )
                {
                    matrix[i][k].weight = slf;
                    matrix[i][k].from   = 'l';
                }
                else if( sdi >= slf && sdi >= sup )
                    {
                        matrix[i][k].weight = sdi;
                        matrix[i][k].from   = 'd';
                    }
        }
    }
    for(k=0; k<wo2 ; k++)
    {
        for(i=0; i<wo1; i++)
        {
            printf("%2d%c ", matrix[i][k].weight, matrix[i][k].from);
        }
        printf("\n");
    }
}
