#include<stdio.h>
#include<stdlib.h>
#define MU 1
#define SG 1

typedef struct entry
{
    int weight;
    char from;
} entry;

int main()
{
    char* word1 = "atgcatgcatgccgtacgta";
    char* word2 = "tgctgctgctgca";
    printf("%s\n%s\n",word1,word2);
    entry matrix[21][14];
    entry temp={0,'s'};
    matrix[0][0] = temp;
    int i = 0;
    temp.weight = 0;
    temp.from = 'l';
    for(i=1 ; i<21 ; i++)
    {
        matrix[i][0] = temp;
    }
    temp.weight=0;
    temp.from='u';
    for(i=1 ; i<14 ; i++)
    {
        matrix[0][i] = temp;
    }
    int k=0;
    for(i=1; i<21 ; i++)
    {
        for(k=1 ; k<14 ; k++)
        {
            int sdi=0;
            if( word1[i] == word2[k] )
                sdi = matrix[i-1][k-1].weight+2;
            else
                sdi = matrix[i-1][k-1].weight-MU;
            int slf = matrix[i-1][k].weight - SG;
            int sup = matrix[i][k-1].weight - SG;
            if( sup >= sdi && sup >= slf)
            {
                matrix[i][k].weight = sdi;
                matrix[i][k].from   = 'u';
            }
            else if ( slf >= sup && slf >= sdi )
                {
                    matrix[i][k].weight = slf;
                    matrix[i][k].from   = 'l';
                }
                else if( sdi >= slf && sdi >= sup )
                    {
                        matrix[i][k].weight = sdi;
                        matrix[i][k].from   = 'd';
                    }
        }
    }
    for(k=0; k<14 ; k++)
    {
        for(i=0; i<21; i++)
        {
            printf("%2d%c ", matrix[i][k].weight, matrix[i][k].from);
        }
        printf("\n");
    }
}
