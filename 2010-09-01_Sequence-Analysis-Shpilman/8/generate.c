#include<stdio.h>
#include<stdlib.h>

char* gen_generate(int gen_length);

int main(int argc, char** argv)
{
    if( argc != 2) 
    {
    //    printf("Usage..\n");
        exit(0);
    }
    char* gen_array = NULL;
    int gen_length = 0;
    gen_length = strtoll(argv[1],NULL,10);
    //gen_length = gen_length%10000;//not want overflow
    //printf("Length accepted = %d\n", gen_length);
    gen_array=gen_generate( gen_length );
    printf("Sequence:%s\n\n",gen_array);
    free(gen_array);
    return(0);
}

char* gen_generate(int gen_length)
{
	int i=0;
    int temp=0;
    char t='a';
    char* gen_array;
    gen_array=(char*)malloc(gen_length*sizeof(int));
    srand(time());
    for(i=0;i<gen_length;i++)
    {
        temp=rand()%4;
        switch( temp )
        {
            case 0:
                t='A';
                break;
            case 1:
                t='T';
                break;
            case 2:
                t='C';
                break;
            case 3:
                t='G';
                break;
        }
        gen_array[i]=t;
    }
    return(gen_array);
}