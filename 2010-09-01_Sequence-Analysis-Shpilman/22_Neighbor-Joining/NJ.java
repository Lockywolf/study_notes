
import java.util.Hashtable;
import java.util.Enumeration;
import java.util.Random;

public class NJ
{
	
 //       Hashtable<String, Hashtable<String, Double> > matro = new Hashtable<String, Hashtable<String, Double> >();
        public class  BiTree
        {
                BiTree left = null;
                BiTree right = null;
                Double rightlen = 0.0;
                Double leftlen = 0.0;
                String value = null;
                boolean leaf = true;
                
                public BiTree getright()
                {
                        return(right);
                }
                
                public BiTree getleft()
                {
                        return(left);
                }
                public Double getrightlen()
                {
                        return(rightlen);
                }
                public Double getleftlen()
                {
                        return(leftlen);
                }
                public void setrightlen(Double leng)
                {
                        rightlen = leng;
                }
                public void setleftlen(Double leng)
                {
                        leftlen = leng;
                }
                public void setvalue(String val)
                {
                        value = val;
                }
                public boolean getleaf()
                {
                        return(leaf);
                }
                public BiTree()
                {
                        ;
                }
                
                public BiTree(String name)
                {
                        value = name;
                }
                
                
                public BiTree(BiTree one, BiTree two)
                {
                        left = one;
                        right = two;
                        //value = (String)((String)one.getvalue() + (String)two.getvalue());
                        value = "node";
                        leaf = false;
                }
                
                public String getvalue()
                {
                        return(value);//TODO!!!
                }
                
                public void put(BiTree what, boolean where)
                {
                        if(where == false)
                                left = what;
                        if(where == true)
                                right = what;
                        leaf = false;
                }
                /*
                public void get(boolean where)
                {
                        if(where == false)
                                return(left);
                        if(where == true)
                                return(right);
                        if(right == null && left == null)
                        leaf = true;
                }*/
        }
        
        
        //BiTree Phylo_tree = new BiTree();
        
        public NJ()
        {
                Hashtable<BiTree, Hashtable<BiTree, Double> > matro = new Hashtable<BiTree, Hashtable<BiTree, Double> >();
                //okay, we build a matrix
                int size = 4;
                for(int i=0; i<size; i++)
                {
                        matro = add_random_vertex(matro);
                        //System.out.printf("%d\n", i);
                }
                print_matrix(matro);
                for(int i=2; i<size; i++)
                {
                        find_and_process(matro);
                        //System.out.printf("\n");
                        //print_matrix(matro);
                }
                System.out.println("Matrix processed. Result:");
                print_matrix(matro);
                System.out.println("Tree:");
                Enumeration e = matro.keys();
                while( e.hasMoreElements() )
                {
                System.out.println( print_tree( (BiTree)e.nextElement() ) );
                }
        }
        
        public int util = 0;
        
	public static void main(String[] args) 
	{
	//	String[] states = new String[] {MINUS, PLUS};
 
	//	String[] observations = new String[] {EAGLE, EAGLE,NUMBER,EAGLE,EAGLE,NUMBER,NUMBER,NUMBER,NUMBER};
		new NJ();
                System.out.println("END");
	}
        
        Hashtable<BiTree, Hashtable<BiTree, Double> > add_vertex(
                                                       BiTree name,
                                                       Hashtable<BiTree, Double> lens,
                                                       Hashtable<BiTree, Hashtable<BiTree, Double> > matro)
        {
                
                Enumeration e = matro.keys();
                while( e. hasMoreElements() )
                        {        
                                BiTree current =(BiTree) e.nextElement();
                                Hashtable<BiTree, Double> dist = matro.get(current);
                                dist.put(name, lens.get(current));
                                matro.remove(current);
                                matro.put(current,dist);
                        }
                matro.put(name, lens);
                return(matro);
        }
        Hashtable<BiTree, Hashtable<BiTree, Double> > find_and_process(
                                                       Hashtable<BiTree, Hashtable<BiTree, Double> > matro)
        {
                BiTree one = null;
                BiTree two = null;
                Enumeration e = matro.keys();
                Double temp = 120.0;
                while( e.hasMoreElements() )
                {
                        BiTree current =(BiTree) e.nextElement();
                        Hashtable<BiTree, Double> dist = matro.get(current);
                        Enumeration k = dist.keys();
                        while( k.hasMoreElements() )
                        {
                                BiTree lower = (BiTree) k.nextElement();
                                Double ro = (Double)(dist.get(lower) - sum(current,lower,matro) - sum(lower,current,matro));
                                if( ro <= temp)
                                {
                                        one = current;
                                        two = lower;
                                        //one.setleftlen(dist.get(lower) + sum(current, lower, matro) - sum(lower, current, matro));
                                        
                                        //two.setrightlen(dist.get(lower) - sum(current, lower, matro) + sum(lower, current, matro) );
                                        temp = ro;
                                }
                        }
                }
                matro = join_2_vertex( one, two, matro);
                return(matro);
        }
        Double sum( BiTree A, BiTree B,
                                            Hashtable<BiTree, Hashtable<BiTree, Double> > matro)
        {
                        Hashtable<BiTree, Double> Adists = matro.get(A);
                        //Hashtable<BiTree, Double> Bdists = matro.get(B);
                        double result = 0;
                        double counter = 0;
                        Enumeration k = Adists.keys();
                        while( k.hasMoreElements() )
                        {
                                result+=(Adists.get( k.nextElement() ));
                                counter++;
                        }
                        result-=Adists.get(B);
                        counter--;
                        result/=counter;
                        return((Double)result);
        }
                
        Hashtable<BiTree, Hashtable<BiTree, Double> > join_2_vertex(
                                                       BiTree name1,
                                                       BiTree name2,
                                                       Hashtable<BiTree, Hashtable<BiTree, Double> > matro)
        {
                BiTree join = new BiTree(name1, name2);
                Hashtable<BiTree, Double> dist1 = matro.get(name1);
                Hashtable<BiTree, Double> dist2 = matro.get(name2);
                join.setleftlen( (Double)((dist1.get(name2) + sum(name1, name2, matro) - sum(name2, name1, matro))/2) );
                join.setrightlen((Double)((dist1.get(name2) - sum(name1, name2, matro) + sum(name1, name2, matro))/2) );
                
                //join.setvalue(Integer.toString( (dist1.get(name2)/2) ) );
                matro.remove(name1);
                matro.remove(name2);
                Enumeration e = matro.keys();
                while( e. hasMoreElements() )
                        {        
                                BiTree current =(BiTree) e.nextElement();
                                Hashtable<BiTree, Double> dist = matro.get(current);
                                dist.remove(name1);
                                dist.remove(name2);
                                matro.remove(current);
                                matro.put(current,dist);
                        }
                Double ro = dist1.get( name2);
                dist1.remove(name2);
                dist2.remove(name1);
                Hashtable<BiTree, Double> lens = new Hashtable<BiTree, Double>();
                e = dist1.keys();
                 while( e. hasMoreElements() )
                        {
                                BiTree current =(BiTree) e.nextElement();
                                Double t1 = (Double)(dist1.get(current));
                                Double t2 = (Double)(dist2.get(current));
                                Double res = (Double)(( t1 + t2 - ro )/((Double)2.0));
                                lens.put(current,  res );
                        }
                matro = add_vertex(join, lens, matro);
                return(matro);
        }
        
         Hashtable<BiTree, Hashtable<BiTree, Double> > add_random_vertex(
                                                       Hashtable<BiTree, Hashtable<BiTree, Double> > matro)
        {
                Random r = new Random();
                //String name = Long.toString(Math.abs(r.nextInt(25)), 12);
                String name = Integer.toString(util);
                util++;
                BiTree ptt = new BiTree(name);
                Hashtable<BiTree, Double> lens = new Hashtable<BiTree, Double>();
                Enumeration e = matro.keys();
                while( e.hasMoreElements() )
                        {        
                                Double temp = (Double)(r.nextInt(50)*2.0);
                                BiTree current =(BiTree) e.nextElement();
                                lens.put(current, temp);
                        }
                add_vertex( ptt, lens, matro);
                return(matro);
        }
        
        void print_matrix( Hashtable<BiTree, Hashtable<BiTree, Double> > matro )
        {
                Enumeration e = matro.keys();
                Double temp = 101.0;
                while( e.hasMoreElements() )
                {
                        BiTree current =(BiTree) e.nextElement();
                        System.out.printf("%s:", current.getvalue());
                        Hashtable<BiTree, Double> dist = matro.get(current);
                        Enumeration k = dist.keys();
                        while( k.hasMoreElements() )
                        {
                                BiTree lower = (BiTree) k.nextElement();
                                Double ro = dist.get(lower);
                                System.out.printf(" (%s,%f)", lower.getvalue(),ro);
                        }
                        System.out.printf("\n");
                }
                System.out.printf("\n");
        }
        
        String print_tree(BiTree tree)
        {
                if( tree.getleaf() == true)
                {
                        return("\""+tree.getvalue()+"\"");
                }
                else
                {
                        return(tree.getvalue() + "("+ tree.getleftlen()+":" + print_tree(tree.getleft()) +","+ tree.getrightlen()+":"+ print_tree(tree.getright()) + ")" );
                }
        }
        
        
        }



