#define _POSIX_SOURCE
#define _POSIX_C_SOURCE 200112L
#define _XOPEN_SOURCE 600
#define _XOPEN_SOURCE_EXTENDED 1
//#include "main.h"
#define PORN_STATES 5
#define PORN_MAX_OBJECTS 10
#include<string.h>
#include<unistd.h>
#include<signal.h>
#include<stdarg.h>
#include<SDL/SDL.h>
#include<SDL/SDL_image.h>
#define GAMEFILE "scenario.game"
typedef enum porn_widget_type {PORN_BUTTON=0,PORN_IMAGE,PORN_GENERIC,PORN_SLIDER,PORN_RADIOBUTTON,PORN_CHECKBOX, PORN_CONTAINER} PORN_WIDGET_TYPE;
typedef struct
{
    char img_addr[20];
    char snd_addr[20];
    char log_addr[20];
} day_t;
extern day_t plan[20];
extern int current_day;
typedef struct
{
    PORN_WIDGET_TYPE porn_type;
    SDL_Surface* image[PORN_STATES];
} porn_generic_widget;
typedef struct
{
    PORN_WIDGET_TYPE porn_type;
    SDL_Surface* image[PORN_STATES];
    void (*button_function)(int agrument);
} porn_button;
typedef struct
{
    PORN_WIDGET_TYPE porn_type;
    SDL_Surface* image[PORN_STATES];
} porn_image;
typedef union
{
    PORN_WIDGET_TYPE porn_type;
    porn_generic_widget generic;
    porn_button button;
    porn_image image;
} porn_widget;

typedef struct
{
    PORN_WIDGET_TYPE porn_type;
    SDL_Surface* image[PORN_STATES];
    porn_widget objects[PORN_MAX_OBJECTS];
    SDL_Rect locations[PORN_MAX_OBJECTS];
    int visibility[PORN_MAX_OBJECTS];
    int amount;
} porn_container;

extern porn_container porn_topmost_container;
void porn_do_nothing(int);
porn_widget porn_create_widget(PORN_WIDGET_TYPE, ...);
porn_container porn_create_container(void);
void porn_draw_widget(porn_widget*, SDL_Surface*, int, int);
int porn_process_gamefile(char name[15]);
SDL_Surface* porn_initialize_video(SDL_Surface*);
void PrintKeyInfo( SDL_KeyboardEvent *key );
void PrintModifiers( SDLMod mod );
void porn_set_background(int, SDL_Surface*);
void porn_draw_widgets(porn_container*);
void porn_add_widget(porn_container*,porn_widget*,int , int);
void porn_refresh_container(porn_container*);
void porn_handle_events(SDL_Surface*);
void PrintMouseButtonInfo(SDL_MouseButtonEvent* button);
