
#include <stdio.h>
#include <string.h>
#include <stdlib.h>

#include<errno.h>
#include<unistd.h>
#include <time.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <dirent.h>
#include <sys/wait.h>


#define MAXDIRLEN 256
#define MAXDIRSIZE 1024

int P ( void );
int  I ( void );
int ZZ ( void );
int A (void );


int main( void )
{

	mkfifo( "i" , 0777 );
	mkfifo( "zz" , 0777 );
	mkfifo( "a" , 0777 );
	mkfifo( "ri" , 0777 );
	mkfifo( "rzz" , 0777 );
	mkfifo( "ra" , 0777 );
	
	
	
	int (*p[4]) (void);
	p[0] = P;
	p[1] = I;
	p[2] = ZZ;
	p[3] = A;
	
	int pids[4];
	int i = 0;
	for( i = 0; i < 5 ; i++ )
	{
		int temp = 0;
		errno = 0;
		temp = fork();
		if( temp == 0 )
		{
			(*p[i])();
			return(0);
		}
		else 
		{
			pids[i] = temp;
			temp = 0;
		}
	}
	
	i = 0;
	printf("start waiting\n");
	for( i=0; i<5; i++)
	{
		waitpid( pids[i] , NULL, 0);
	}
	printf("\n");
		
	
	
	return(0);
}


int P( void )
{
	int i = 0;
	char result;
	FILE* I =  fopen( "i", "a");
	FILE* ZZ =  fopen( "zz", "a");
	FILE* A =  fopen( "a", "a");
	FILE* RI =  fopen( "ri", "r");
	FILE* RZZ =  fopen( "rzz", "r");
	FILE* RA =  fopen( "ra", "r");
	printf("entering p\n");
	//FILE* I =  fopen( "p", "r+");
	for( i=0 ; i< 2 ; i++ )
	{
		printf("P");
		putc( 'R', I);
		result = getc(RI);
		//result = 'o';
		if( result != 'o')
		{
			printf("P:(ACHTUNG!:%c)",result);
			return(-1);
		}
		//putc('R' , ZZ);
		//fscanf(ZZ, "%c", &result );
		//result = getc( RZZ ); 
		//if( result != 'o')
		//{
		//	return(-1);
	//	}
		//putc('R', A);
		//fscanf(A, "%c", &result );
		//result = getc( RA );
		//if( result != 'o')
		//{
		//	return(-1);
		//}
	}
	fprintf(I, "T");
	fprintf(ZZ, "T");
	fprintf(A, "T");
	fclose( I );
	fclose( ZZ );
	fclose( A );
	fclose( RI );
	fclose( RZZ );
	fclose( RA );
	
	//printf("P");
	return(0);
}

int I( void )
{
	printf("entering i\n");
	FILE* fifa = fopen( "i", "r");
	FILE* rfifa = fopen( "ri", "a");
	while(1)
	{
		char temp = 0;
		printf("I:getc\n");
		temp = getc( fifa );
		printf("I:getc passed\n");
		if(temp == 'R')
		{
			printf("I");
			//fprintf(fifa, "o");
			putc( 'o', rfifa);
		}
		else if( temp == 'T' )
		{
			fclose(fifa);
			fclose(rfifa);
			
			return(0);
		}
		else
		{
			printf("error:%c\n", temp);
		}
	}
	return(0);
}

int ZZ( void )
{
	printf("entering zz\n");
	FILE* fifa = fopen( "zz", "r");
	FILE* rfifa = fopen( "rzz", "a");
	while(1)
	{
		char temp = 0;
		temp = getc( fifa );
		printf("I: %c\n", temp);
		if(temp == 'R')
		{
			printf("ZZ");
			putc('o', rfifa);
		}
		else if( temp == 'T' )
		{
			fclose(fifa);
			fclose(rfifa);
			return(0);
		}
		else
		{
			printf("error\n");
		}
	}
	//printf("A");
	return(0);
//return(0);
}

int A( void )
{
	printf("entering a\n");
	FILE* fifa = fopen( "a", "r");
	FILE* rfifa = fopen( "ra", "a");
	
	while(1)
	{
		char temp = 0;
		temp = getc( fifa );
		if(temp == 'R')
		{
			printf("A");
			putc('o', rfifa );
		}
		else if( temp == 'T' )
		{
			fclose(fifa);
			fclose(rfifa);
			
			return(0);
		}
		else
		{
			printf("error\n");
		}
	}
	//printf("A");
	return(0);
}


