
#include <stdio.h>
#include <string.h>
#include <stdlib.h>

#include<errno.h>
#include<unistd.h>
#include <time.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <dirent.h>

#define MAXDIRLEN 256
#define MAXDIRSIZE 1024

int main(int argc, char** argv)
{
	int mainlen=0;
	int i=0;
	
	char dir1[MAXDIRLEN];
	char dir2[MAXDIRLEN];
	struct dirent*  list1[MAXDIRSIZE];
	int len1=0;
	struct dirent* list2[MAXDIRSIZE];
	int len2=0;
	
	if( argc != 3 )
	{
		printf("Error\n");
		return(-1);
	}
	{
		
		struct stat sb;
		if (stat(argv[1], &sb) == 0 && S_ISDIR(sb.st_mode))
		{
			printf("Okay, dir1 exists\n");
			strcpy( dir1, argv[1] );
		}
		else
		{
			printf("Dir1 not exists\n");
			exit(EXIT_FAILURE);
		}
		if (stat(argv[2], &sb) == 0 && S_ISDIR(sb.st_mode))
		{
			printf("Okay, dir2 exists\n");
			strcpy( dir2, argv[2] );
		}
		else
		{
			printf("Dir2 not exists\n");
			exit(EXIT_FAILURE);
		}
	}
	

	
	errno = 0;
	DIR* dir = opendir(dir1);
	if(errno !=0)
	{perror("Opendir"); return(-1);}
        while ( (list1[i] = readdir(dir)) != NULL)
        {
		i++;
	}
	i++;
	len1=i;
	i=0;
	
	i = 0;
	errno = 0;
	dir = opendir(dir2);
	if(errno !=0)
	{perror("Opendir"); return(-1);}
        errno = 0;
        while ((list2[i] = readdir(dir)) != NULL)
        {
		if( errno != 0 )
		{
			perror("readdir");
			return(-1);
		}
		i++;
	}
	i++;
	len2=i;
	i=0;
	
	(len1 > len2) ? (mainlen=len1) : (mainlen=len2);
	
	for( i=0; i< len1 ; i++)
	{
		int k=0;
		for( k=0; k< len2; k++)
		{
			if( list1[i] != NULL && list2[k] != NULL)
			{
				if( 	(strcmp( (list1[i])->d_name, (list2[k])->d_name) == 0) 
					&&
					((list1[i])->d_type==(list2[k])->d_type)
				  )
				{
				//	printf("found similar entries\n");
					list1[i]=NULL;
					list2[k]=NULL;
				}
			}
		}
	}
	//we cleared lists
	printf("printing different entries:\n");
	i=0;
	for( i=0; i< len1 ; i++)
	{
		if( list1[i] != NULL )
			printf("%s\n", list1[i]->d_name );
	}
	for( i=0; i< len1 ; i++)
	{
		if( list2[i] != NULL )
			printf("%s\n", list2[i]->d_name );
	}
	printf("DONE\n");
	
	return(0);
}
