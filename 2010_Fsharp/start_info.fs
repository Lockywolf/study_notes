﻿module SomeNamespace.SubNamespace

open System

type student = {name:string; surname:string; sex:char; day:int; month:int; year:int; group:int; iq:int}
let students = [{name="Максим"; surname="Чуркин"; sex='m'; day=13; month=12; year=1991; group=331; iq=100};
                 {name="Вера"; surname="Аминова"; sex='f'; day=1; month=6; year=1991; group=331; iq=115};
                 {name="Елизавета"; surname="Анохина"; sex='f'; day=27; month=2; year=1992; group=332; iq=105};
                 {name="Дарья"; surname="Грекова"; sex='f'; day=18; month=3; year=1992; group=333; iq=106};
                 {name="Иван"; surname="Демкович"; sex='m'; day=3; month=1; year=1993; group=331; iq=107};
                 {name="Денис"; surname="Дубленых"; sex='m'; day=6; month=4; year=1991; group=334; iq=106};
                 {name="Алексей"; surname="Королев"; sex='m'; day=24; month=5; year=1993; group=333; iq=114};
                 {name="Вячеслав"; surname="Шапрынский"; sex='m'; day=28; month=11; year=1992; group=334; iq=103};
                 {name="Владислав"; surname="Исенбаев"; sex='m'; day=11; month=10; year=1991; group=331; iq=103};
                 {name="Дмитрий"; surname="Зонин"; sex='m'; day=10; month=4; year=1991; group=332; iq=108};
                 {name="Дмитрий"; surname="Карпов"; sex='m'; day=9; month=8; year=1991; group=331; iq=108};
                 {name="Александр"; surname="Голубев"; sex='m'; day=19; month=9; year=1992; group=332; iq=106};
                 {name="Вадим"; surname="Шакуро"; sex='m'; day=5; month=1; year=1992; group=333; iq=108};
                 {name="Сергей"; surname="Ларюшкин"; sex='m'; day=9; month=3; year=1993; group=333; iq=107};
                 {name="Александр"; surname="Сухман"; sex='m'; day=20; month=2; year=1993; group=331; iq=115};
                 {name="Кирилл"; surname="Корнюхин"; sex='m'; day=30; month=12; year=1992; group=332; iq=106};
                 {name="Марина"; surname="Масленникова"; sex='f'; day=29; month=3; year=1990; group=332; iq=114};
                 {name="Антон"; surname="Путрик"; sex='m'; day=2; month=11; year=1991; group=331; iq=107};
                 {name="Максим"; surname="Путрик"; sex='m'; day=16; month=10; year=1991; group=332; iq=107};
                 {name="Александр"; surname="Чегодаев"; sex='m'; day=12; month=9; year=1993; group=332; iq=105};
                 {name="Виктор"; surname="Самунь"; sex='m'; day=19; month=7; year=1992; group=331; iq=108};
                 {name="Дмитрий"; surname="Королев"; sex='m'; day=22; month=6; year=1991; group=333; iq=106};
                 {name="Павел"; surname="Скрипниченко"; sex='m'; day=11; month=5; year=1991; group=331; iq=115};
                 {name="Александр"; surname="Топоров"; sex='m'; day=4; month=11; year=1991; group=334; iq=102};
                 {name="Елена"; surname="Шалашугина"; sex='f'; day=5; month=12; year=1992; group=331; iq=105};
                 {name="Денис"; surname="Ядаринкин"; sex='m'; day=17; month=8; year=1992; group=331; iq=103};
                 {name="Даниил"; surname="Айзенштейн"; sex='m'; day=15; month=5; year=1993; group=331; iq=105};
                 {name="Вера"; surname="Аминова"; sex='f'; day=26; month=4; year=1993; group=332; iq=106};
                 {name="Ксения"; surname="Афоненко"; sex='f'; day=18; month=3; year=1993; group=332; iq=103};
                 {name="Илья"; surname="Дегтярев"; sex='m'; day=28; month=2; year=1992; group=333; iq=106};
                 {name="Дмитрий"; surname="Зонин"; sex='m'; day=22; month=1; year=1992; group=331; iq=106};
                 {name="Мария"; surname="Колмогорова"; sex='f'; day=21; month=9; year=1991; group=333; iq=103};
                 {name="Виктор"; surname="Самунь"; sex='m'; day=7; month=7; year=1992; group=331; iq=109};
                 {name="Олег"; surname="Михайлов"; sex='m'; day=6; month=11; year=1992; group=331; iq=107};
                 {name="Григорий"; surname="Семенов"; sex='m'; day=14; month=10; year=1989; group=334; iq=102};
                 {name="Иван"; surname="Кадочников"; sex='m'; day=22; month=7; year=1993; group=334; iq=103};
                 {name="Алексей"; surname="Королев"; sex='m'; day=15; month=6; year=1991; group=331; iq=102};
                 {name="Алексей"; surname="Кутырев"; sex='m'; day=1; month=5; year=1991; group=331; iq=111};
                 {name="Елена"; surname="Лысенко"; sex='f'; day=28; month=1; year=1990; group=333; iq=107};
                 {name="Антон"; surname="Путрик"; sex='m'; day=23; month=11; year=1992; group=333; iq=107};
                 {name="Илья"; surname="Отрадников"; sex='m'; day=15; month=3; year=1992; group=331; iq=115};
                 {name="Анна"; surname="Уставщикова"; sex='f'; day=10; month=2; year=1991; group=334; iq=100};
                 {name="Борис"; surname="Сафронов"; sex='m'; day=20; month=8; year=1993; group=334; iq=105};
                 {name="Надежда"; surname="Скачкова"; sex='f'; day=1; month=12; year=1991; group=331; iq=111};
                 {name="Владимир"; surname="Теличкин"; sex='m'; day=8; month=7; year=1991; group=331; iq=114};
                 {name="Иван"; surname="Турмышев"; sex='m'; day=7; month=5; year=1992; group=332; iq=107};
                 {name="Евгений"; surname="Кузнецов"; sex='m'; day=4; month=11; year=1992; group=331; iq=108};
                 {name="Сергей"; surname="Зайков"; sex='m'; day=3; month=8; year=1991; group=331; iq=108};
                 {name="Юлия"; surname="Буркова"; sex='f'; day=13; month=3; year=1991; group=331; iq=109};
                 {name="Юрий"; surname="Старовойтов"; sex='m'; day=12; month=4; year=1993; group=334; iq=102};
                 {name="Яков"; surname="Мозгоев"; sex='m'; day=10; month=9; year=1991; group=332; iq=107};
                 {name="Елена"; surname="Михайлова"; sex='f'; day=27; month=12; year=1993; group=334; iq=111};
                 {name="Егор"; surname="Игнатьев"; sex='m'; day=26; month=9; year=1992; group=333; iq=107};
                 {name="Сергей"; surname="Торопкин"; sex='m'; day=21; month=4; year=1991; group=331; iq=101};
                 {name="Татьяна"; surname="Елисеева"; sex='f'; day=25; month=6; year=1991; group=333; iq=100};
                 {name="Адель"; surname="Аухатова"; sex='f'; day=12; month=3; year=1991; group=332; iq=105};
                 {name="Алексей"; surname="Ефремов"; sex='m'; day=19; month=2; year=1993; group=334; iq=105};
                 {name="Диана"; surname="Таратухина"; sex='f'; day=17; month=1; year=1992; group=334; iq=102};
                 {name="Ростислав"; surname="Девятов"; sex='m'; day=5; month=8; year=1992; group=334; iq=108};
                 {name="Антон"; surname="Васильев"; sex='m'; day=2; month=5; year=1991; group=332; iq=112};
                 {name="Антон"; surname="Махлин"; sex='m'; day=8; month=4; year=1990; group=332; iq=109};
                 {name="Елена"; surname="Третьяк"; sex='f'; day=16; month=3; year=1993; group=333; iq=105};
                 {name="Игорь"; surname="Гуламов"; sex='m'; day=25; month=6; year=1991; group=333; iq=105};
                 {name="Илья"; surname="Корнаков"; sex='m'; day=22; month=12; year=1992; group=333; iq=101};
                 {name="Владимир"; surname="Батаев"; sex='m'; day=10; month=10; year=1991; group=334; iq=102};
                 {name="Арсений"; surname="Климовский"; sex='m'; day=4; month=5; year=1991; group=331; iq=102};
                 {name="Федор"; surname="Пахомов"; sex='m'; day=17; month=3; year=1993; group=334; iq=107};
                 {name="Виктор"; surname="Алюшин"; sex='m'; day=10; month=12; year=1993; group=334; iq=101};
                 {name="Игорь"; surname="Махлин"; sex='m'; day=28; month=1; year=1992; group=332; iq=108};
                 {name="Иван"; surname="Козлов"; sex='m'; day=21; month=8; year=1991; group=332; iq=111};
                 {name="Вадим"; surname="Авдеев"; sex='m'; day=11; month=3; year=1992; group=333; iq=111};
                 {name="Антон"; surname="Фонарев"; sex='m'; day=3; month=9; year=1991; group=332; iq=107};
                 {name="Андрей"; surname="Колосов"; sex='m'; day=1; month=7; year=1991; group=332; iq=107};
                 {name="Марина"; surname="Степанова"; sex='f'; day=9; month=11; year=1992; group=334; iq=104};
                 {name="Матвей"; surname="Корнилов"; sex='m'; day=7; month=10; year=1993; group=334; iq=105};
                 {name="Яков"; surname="Длугач"; sex='m'; day=5; month=5; year=1991; group=333; iq=109};
                 {name="Игорь"; surname="Туркин"; sex='m'; day=4; month=5; year=1991; group=333; iq=109};
                 {name="Олег"; surname="Самойленко"; sex='m'; day=1; month=8; year=1992; group=332; iq=107};
                 {name="Екатерина"; surname="Самбукова"; sex='f'; day=15; month=3; year=1993; group=334; iq=104};
                 {name="Дмитрий"; surname="Алексеев"; sex='m'; day=25; month=2; year=1992; group=334; iq=104};
                 {name="Павел"; surname="Ющенко"; sex='m'; day=21; month=10; year=1990; group=334; iq=108};
                 {name="Иван"; surname="Дудинов"; sex='m'; day=22; month=12; year=1991; group=331; iq=108};
                 {name="Виктор"; surname="Чернышов"; sex='m'; day=16; month=4; year=1991; group=331; iq=99};
                 {name="Антон"; surname="Лупанов"; sex='m'; day=11; month=12; year=1993; group=333; iq=108};
                 {name="Иван"; surname="Пузыревский"; sex='m'; day=6; month=1; year=1991; group=333; iq=99};
                 {name="Александр"; surname="Петров"; sex='m'; day=20; month=1; year=1992; group=332; iq=107};
                 {name="Леонид"; surname="Андриевский"; sex='m'; day=17; month=7; year=1990; group=334; iq=107};
                 {name="Александр"; surname="Рощупкин"; sex='m'; day=1; month=3; year=1991; group=333; iq=98};
                 {name="Александр"; surname="Корчагин"; sex='m'; day=15; month=7; year=1991; group=334; iq=112};
                 {name="Кирилл"; surname="Жалнин"; sex='m'; day=10; month=8; year=1992; group=332; iq=104};
                 {name="Дмитрий"; surname="Горбик"; sex='m'; day=13; month=6; year=1991; group=332; iq=104};
                 {name="Антон"; surname="Бобков"; sex='m'; day=27; month=2; year=1991; group=333; iq=104};
                 {name="Константин"; surname="Новиков"; sex='m'; day=22; month=10; year=1993; group=332; iq=101};
                 {name="Владимир"; surname="Ткачев"; sex='m'; day=21; month=1; year=1992; group=334; iq=94};
                 {name="Иван"; surname="Кондрашов"; sex='m'; day=6; month=5; year=1994; group=331; iq=104};
                 {name="Данила"; surname="Потапов"; sex='m'; day=23; month=6; year=1991; group=332; iq=104};
                 {name="Борис"; surname="Агафонцев"; sex='m'; day=13; month=4; year=1993; group=334; iq=135};
                 {name="Алексей"; surname="Давыдов"; sex='m'; day=7; month=7; year=1993; group=333; iq=103};
                 {name="Денис"; surname="Лисов"; sex='m'; day=4; month=12; year=1990; group=333; iq=108};
                 {name="Иван"; surname="Богатый"; sex='m'; day=14; month=4; year=1991; group=333; iq=107};
                 {name="Мария"; surname="Фелицина"; sex='f'; day=18; month=2; year=1990; group=333; iq=104};
                 {name="Андрей"; surname="Викулов"; sex='m'; day=2; month=2; year=1993; group=332; iq=107};
                 {name="Алексей"; surname="Крицин"; sex='m'; day=22; month=12; year=1991; group=332; iq=107};
                 {name="Артем"; surname="Кухаренко"; sex='m'; day=26; month=7; year=1990; group=332; iq=103};
                 {name="Сергей"; surname="Аноховский"; sex='m'; day=30; month=4; year=1993; group=331; iq=103};
                 {name="Александр"; surname="Захаров"; sex='m'; day=29; month=12; year=1991; group=332; iq=103};
                 {name="Андрей"; surname="Котов"; sex='m'; day=28; month=10; year=1991; group=333; iq=111};
                 {name="Кирилл"; surname="Знаменский"; sex='m'; day=30; month=9; year=1993; group=334; iq=103};
                 {name="Владимир"; surname="Лесниченко"; sex='m'; day=31; month=8; year=1993; group=334; iq=103};
                 {name="Илья"; surname="Смирнов"; sex='m'; day=31; month=7; year=1991; group=331; iq=108};
                 {name="Александр"; surname="Колесников"; sex='m'; day=31; month=3; year=1991; group=334; iq=101};
                 {name="Николай"; surname="Печенкин"; sex='m'; day=13; month=11; year=1991; group=333; iq=115};
                 {name="Игнат"; surname="Колесниченко"; sex='m'; day=29; month=7; year=1990; group=332; iq=101};
                 {name="Алена"; surname="Шевцова"; sex='f'; day=30; month=2; year=1993; group=334; iq=106};
                 {name="Федор"; surname="Волков"; sex='m'; day=30; month=3; year=1993; group=333; iq=107};
                 {name="Константин"; surname="Яблочкин"; sex='m'; day=9; month=1; year=1990; group=334; iq=109};
                 {name="Алексей"; surname="Окунев"; sex='m'; day=31; month=12; year=1991; group=334; iq=107};
                 {name="Татьяна"; surname="Поиленкова"; sex='f'; day=31; month=7; year=1991; group=334; iq=101};
                 {name="Ольга"; surname="Пакуляк"; sex='f'; day=31; month=3; year=1991; group=333; iq=108};
                 {name="Элина"; surname="Имашева"; sex='f'; day=31; month=5; year=1993; group=333; iq=108};
                 {name="Алексей"; surname="Гронский"; sex='m'; day=30; month=7; year=1989; group=332; iq=109};
                 {name="Мария"; surname="Гребенкина"; sex='f'; day=31; month=12; year=1991; group=332; iq=106}
                 ]
let french = [("Максим", "Чуркин"); ("Вера", "Аминова"); ("Дарья", "Грекова");
              ("Иван", "Демкович"); ("Денис", "Дубленых"); ("Алексей", "Королев");
              ("Дмитрий", "Зонин"); ("Дмитрий", "Карпов"); ("Вадим", "Шакуро");
              ("Кирилл", "Корнюхин"); ("Александр", "Чегодаев"); ("Павел", "Скрипниченко");
              ("Денис", "Ядаринкин"); ("Вера", "Аминова"); ("Ксения", "Афоненко");
              ("Мария", "Колмогорова"); ("Олег", "Михайлов"); ("Иван", "Кадочников");
              ("Елена", "Лысенко"); ("Антон", "Путрик"); ("Илья", "Отрадников");
              ("Анна", "Уставщикова"); ("Сергей", "Зайков"); ("Юлия", "Буркова");
              ("Елена", "Михайлова"); ("Адель", "Аухатова"); ("Диана", "Таратухина");
              ("Ростислав", "Девятов"); ("Антон", "Васильев"); ("Елена", "Третьяк");
              ("Владимир", "Батаев"); ("Федор", "Пахомов"); ("Игорь", "Махлин");
              ("Иван", "Козлов"); ("Антон", "Фонарев"); ("Яков", "Длугач");
              ("Игорь", "Туркин"); ("Дмитрий", "Алексеев"); ("Павел", "Ющенко");
              ("Иван", "Дудинов"); ("Антон", "Лупанов"); ("Иван", "Пузыревский");
              ("Леонид", "Андриевский"); ("Александр", "Корчагин"); ("Кирилл", "Жалнин");
              ("Антон", "Бобков"); ("Владимир", "Ткачев"); ("Данила", "Потапов");
              ("Денис", "Лисов"); ("Иван", "Богатый"); ("Алексей", "Крицин");
              ("Андрей", "Котов"); ("Кирилл", "Знаменский"); ("Владимир", "Лесниченко");
              ("Илья", "Смирнов"); ("Александр", "Колесников"); ("Николай", "Печенкин");
              ("Алена", "Шевцова"); ("Константин", "Яблочкин"); ("Татьяна", "Поиленкова");
              ("Мария", "Гребенкина")]
              
let discrete_mathematics = [("Елизавета", "Анохина"); ("Денис", "Дубленых"); ("Алексей", "Королев");
                            ("Вячеслав", "Шапрынский"); ("Дмитрий", "Карпов"); ("Сергей", "Ларюшкин");
                            ("Александр", "Топоров"); ("Даниил", "Айзенштейн"); ("Вера", "Аминова");
                            ("Ксения", "Афоненко"); ("Дмитрий", "Зонин"); ("Мария", "Колмогорова");
                            ("Алексей", "Королев"); ("Алексей", "Кутырев"); ("Илья", "Отрадников");
                            ("Анна", "Уставщикова"); ("Надежда", "Скачкова"); ("Владимир", "Теличкин");
                            ("Иван", "Турмышев"); ("Евгений", "Кузнецов"); ("Сергей", "Зайков");
                            ("Юлия", "Буркова"); ("Юрий", "Старовойтов"); ("Яков", "Мозгоев");
                            ("Елена", "Михайлова"); ("Сергей", "Торопкин"); ("Игорь", "Гуламов");
                            ("Владимир", "Батаев"); ("Игорь", "Махлин"); ("Иван", "Козлов");
                            ("Андрей", "Колосов"); ("Марина", "Степанова"); ("Матвей", "Корнилов");
                            ("Яков", "Длугач"); ("Игорь", "Туркин"); ("Олег", "Самойленко");
                            ("Виктор", "Чернышов"); ("Антон", "Лупанов"); ("Александр", "Рощупкин");
                            ("Александр", "Корчагин"); ("Кирилл", "Жалнин"); ("Антон", "Бобков");
                            ("Константин", "Новиков"); ("Иван", "Кондрашов"); ("Данила", "Потапов");
                            ("Денис", "Лисов"); ("Мария", "Фелицина"); ("Андрей", "Викулов");
                            ("Артем", "Кухаренко"); ("Сергей", "Аноховский"); ("Александр", "Захаров");
                            ("Андрей", "Котов"); ("Владимир", "Лесниченко"); ("Илья", "Смирнов");
                            ("Александр", "Колесников"); ("Николай", "Печенкин"); ("Алена", "Шевцова");
                            ("Татьяна", "Поиленкова"); ("Ольга", "Пакуляк"); ("Элина", "Имашева");
                            ("Алексей", "Гронский"); ("Мария", "Гребенкина")]

let programming = [("Дарья", "Грекова"); ("Иван", "Демкович"); ("Денис", "Дубленых");
                   ("Алексей", "Королев"); ("Владислав", "Исенбаев"); ("Дмитрий", "Зонин");
                   ("Александр", "Голубев"); ("Вадим", "Шакуро"); ("Кирилл", "Корнюхин");
                   ("Антон", "Путрик"); ("Максим", "Путрик"); ("Александр", "Чегодаев");
                   ("Вера", "Аминова"); ("Ксения", "Афоненко"); ("Мария", "Колмогорова");
                   ("Виктор", "Самунь"); ("Олег", "Михайлов"); ("Алексей", "Королев");
                   ("Илья", "Отрадников"); ("Анна", "Уставщикова"); ("Иван", "Турмышев");
                   ("Евгений", "Кузнецов"); ("Юлия", "Буркова"); ("Яков", "Мозгоев");
                   ("Егор", "Игнатьев"); ("Татьяна", "Елисеева"); ("Адель", "Аухатова");
                   ("Ростислав", "Девятов"); ("Антон", "Васильев"); ("Елена", "Третьяк");
                   ("Федор", "Пахомов"); ("Виктор", "Алюшин"); ("Иван", "Козлов");
                   ("Вадим", "Авдеев"); ("Антон", "Фонарев"); ("Екатерина", "Самбукова");
                   ("Дмитрий", "Алексеев"); ("Виктор", "Чернышов"); ("Иван", "Пузыревский");
                   ("Александр", "Петров"); ("Александр", "Рощупкин"); ("Александр", "Корчагин");
                   ("Антон", "Бобков"); ("Константин", "Новиков"); ("Владимир", "Ткачев");
                   ("Иван", "Кондрашов"); ("Данила", "Потапов"); ("Алексей", "Давыдов");
                   ("Денис", "Лисов"); ("Мария", "Фелицина"); ("Андрей", "Викулов");
                   ("Алексей", "Крицин"); ("Артем", "Кухаренко"); ("Александр", "Захаров");
                   ("Илья", "Смирнов"); ("Игнат", "Колесниченко"); ("Татьяна", "Поиленкова");
                   ("Ольга", "Пакуляк"); ("Алексей", "Гронский"); ("Мария", "Гребенкина")]

// печатаем список студентов
//let i = ref 0
//List.iteri (fun i y -> (printfn "%d). %s %s" i y.surname y.name)) students

// придумываем новый предмет
//let r = Random()
//printf "%A" (List.map (fun x -> (x.name,x.surname)) (List.filter (fun x -> r.Next(2)=1) students))

// группируем имена и фамилии в кортежи(туплы)
//printf "%A" (List.zip (List.map (fun x->x.name) students) (List.map (fun x->x.surname) students))
//(b.name b.surname b.sex b.day b.month  b.year (b.group + 100) b.iq)

let rec yid b = {name=b.name; surname=b.surname; sex=b.sex; day=b.day; month=b.month;  year=b.year; group=(b.group + 100); iq=b.iq};;

let rec process_list a = match a with 
                                                     | b::yb -> (yid b)::(process_list yb)
                                                     | [] ->[] ;;


let rec mystudents = process_list students;;

let i = ref 0
List.iteri (fun i y -> (printfn "%d). %s %s %d" i y.surname y.name y.group )) mystudents
//let rec myprint a = match a with  | t::rest -> (printf "%A" t); myprint rest | [] ->[];;
// myprint mystudents;;

//Console.ReadKey()