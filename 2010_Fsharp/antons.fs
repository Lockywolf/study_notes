module antons


type Tree = |Leaf of int |Node of int * (Tree list) ;;

let rnd = new System.Random()
let random () = rnd.Next(1,5)
let rec treeList list length tree k =

    match length with

    | 0 when list <> [] -> list

    | 0 -> treeList (list@[tree k]) length tree k

    | _ -> treeList (list@[tree k]) (length-1) tree k

    

let util x = (1, x)
let first (x,y) = x
let util2 (x,y) = (x+1,y)
let rec findmax x = match x with
                                                      | [] -> 0
			                              | (a,b)::ys -> a when (a > (findmax ys))
                                                      |  _ -> findmax ys
let rec process_list x max = match x with
                                                         |  (a,b)::ys -> (a,b) when ( a == max) 
                                                         |  _ -> process_list ys max

let rec randomTree k =

    if (k>1) then Node(random(), treeList ([]) (random()) randomTree (k-1))

    else Leaf (random())
    
let mytree = randomTree 5

let rec lofcn = function 
                                   | Leaf -> (0,0)
                                   | Node(x, y) -> (1,x) when ((findmax y) == 0)
                                   | Node(x, y) -> (util2 (process_list y (findmax y)))
