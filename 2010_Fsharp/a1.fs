(*
��������:   ������������ ������ �1 �� ����� "�������������� ����������������"
�����:     �������� �����, ������ 792
�������:    37
������� ������:
    37. ���������� �������, ����������� ��� ���� ���������, 
	�������������� �� ������ � ������� ������� ����������, ����������,��������������� � ���������, 
	�������� ���������� ������ ��������� �� �������.
*)
module Lab
//  ��� "log term" - ���� ���� (���������� �������), ���� ���������, ���������, ����������,���������������
//  ������ ���������
type log_term =
    | Atom of string
    | Not of log_term
    | And of log_term * log_term
    | Or of log_term * log_term
    |  Eqw of log_term * log_term
//  ����������� ����� �� ����� ���������
let rec printTerm e = 
    match e with
    | Atom a -> printf "%s" a
    | Not a -> printf "!"; printTerm a
    | And (a, b) -> printf "("; printTerm a; printf " & "; printTerm b; printf ")"
    | Or (a, b) -> printf "("; printTerm a; printf " | "; printTerm b; printf ")"
    | Eqw(a, b) -> printf "("; printTerm a; printf "<--> "; printTerm b; printf ")"


//  ����� �������� ���� ���������� � ������
let rec printAtoms l = 
    match l with
    | [] -> ()
    | (a,b)::t -> printfn "%s = %b" a b; printAtoms t

//  ������� �������� ��������� ���������� �� ������
let rec findVal l v = 
    match l with
    | [] -> false
    | (a,b)::t when (a=v) -> b
    | h::t -> findVal t v

//  ����������� ���������� �������� ���������
let rec calc e l =
    match e with
    | Atom a -> findVal l a
    | Not a -> not (calc a l)
    | And (a, b) -> (calc a l) && (calc b l)
    | Or (a, b) -> (calc a l) || (calc b l)
    | Eqw (a, b) -> (calc a l) = (calc b l)


let varif e1 l1 e2 l2 =
     not(calc e1 l1)||(calc e2 l2)     

//  ��������  ���������
let r1 = Or(And(Atom("c"), Atom("b")), And(Not(Atom("a")), Not(Or(Atom("b"), Atom("c")))))
let r2 = Eqw(And(Atom("d"), Atom("e")), And(Not(Atom("e")), Not(Or(Atom("f"), Atom("d")))))
printTerm r1
printf "\n"
printTerm r2
printf "\n"
//  ������ ���������� � �� ��������
let mutable v1= [("a",true);("b",true);("c",true)]
let mutable v2= [("d",true);("e",true);("f",true)]
printAtoms v1
printAtoms v2
let mutable r = varif r1 v1 r2 v2
printfn "%b" r

(* v1<-[("a",false);("b",true);("c",true)]
v2<-[("d",true);("e",true);("f",true)]
printAtoms v1
printAtoms v2
r<-varif r1 v1 r2 v2
printfn "%b" r

v1<-[("a",true);("b",false);("c",true)]
v2<-[("d",true);("e",true);("f",true)]
printAtoms v1
printAtoms v2
r<-varif r1 v1 r2 v2
printfn "%b" r

v1<-[("a",true);("b",true);("c",false)]
v2<-[("d",true);("e",true);("f",true)]
printAtoms v1
printAtoms v2
r<-varif r1 v1 r2 v2
printfn "%b" r


v1<-[("a",true);("b",true);("c",true)]
v2<-[("d",false);("e",true);("f",true)]
printAtoms v1
printAtoms v2
r<-varif r1 v1 r2 v2
printfn "%b" r

             
v1<-[("a",true);("b",true);("c",true)]
v2<-[("d",true);("e",false);("f",true)]
printAtoms v1
printAtoms v2
r<-varif r1 v1 r2 v2
printfn "%b" r

v1<-[("a",true);("b",true);("c",true)]
v2<-[("d",true);("e",true);("f",false)]
printAtoms v1
printAtoms v2
r<-varif r1 v1 r2 v2
printfn "%b" r

v1<-[("a",false);("b",false);("c",true)]
v2<-[("d",true);("e",true);("f",true)]
printAtoms v1
printAtoms v2
r<-varif r1 v1 r2 v2
printfn "%b" r

v1<-[("a",false);("b",true);("c",false)]
v2<-[("d",true);("e",true);("f",true)]
printAtoms v1
printAtoms v2
r<-varif r1 v1 r2 v2
printfn "%b" r

v1<-[("a",false);("b",true);("c",true)]
v2<-[("d",false);("e",true);("f",true)]
printAtoms v1
printAtoms v2
r<-varif r1 v1 r2 v2
printfn "%b" r

v1<-[("a",false);("b",true);("c",true)]
v2<-[("d",true);("e",false);("f",true)]
printAtoms v1
printAtoms v2
r<-varif r1 v1 r2 v2
printfn "%b" r

v1<-[("a",false);("b",true);("c",true)]
v2<-[("d",true);("e",true);("f",false)]
printAtoms v1
printAtoms v2
r<-varif r1 v1 r2 v2
printfn "%b" r

v1<-[("a",false);("b",true);("c",true)]
v2<-[("d",true);("e",true);("f",false)]
printAtoms v1
printAtoms v2
r<-varif r1 v1 r2 v2
printfn "%b" r

v1<-[("a",true);("b",false);("c",false)]
v2<-[("d",true);("e",true);("f",true)]
printAtoms v1
printAtoms v2
r<-varif r1 v1 r2 v2
printfn "%b" r

v1<-[("a",true);("b",false);("c",true)]
v2<-[("d",false);("e",true);("f",true)]
printAtoms v1
printAtoms v2
r<-varif r1 v1 r2 v2
printfn "%b" r

v1<-[("a",true);("b",false);("c",true)]
v2<-[("d",true);("e",false);("f",true)]
printAtoms v1
printAtoms v2
r<-varif r1 v1 r2 v2
printfn "%b" r


v1<-[("a",true);("b",false);("c",true)]
v2<-[("d",true);("e",true);("f",false)]
printAtoms v1
printAtoms v2
r<-varif r1 v1 r2 v2
printfn "%b" r

v1<-[("a",true);("b",true);("c",false)]
v2<-[("d",false);("e",true);("f",true)]
printAtoms v1
printAtoms v2
r<-varif r1 v1 r2 v2
printfn "%b" r

v1<-[("a",true);("b",true);("c",false)]
v2<-[("d",true);("e",false);("f",true)]
printAtoms v1
printAtoms v2
r<-varif r1 v1 r2 v2
printfn "%b" r

v1<-[("a",true);("b",true);("c",false)]
v2<-[("d",true);("e",true);("f",false)]
printAtoms v1
printAtoms v2
r<-varif r1 v1 r2 v2
printfn "%b" r

v1<-[("a",true);("b",true);("c",true)]
v2<-[("d",false);("e",false);("f",true)]
printAtoms v1
printAtoms v2
r<-varif r1 v1 r2 v2
printfn "%b" r

v1<-[("a",true);("b",true);("c",true)]
v2<-[("d",false);("e",true);("f",false)]
printAtoms v1
printAtoms v2
r<-varif r1 v1 r2 v2
printfn "%b" r

v1<-[("a",true);("b",true);("c",true)]
v2<-[("d",true);("e",false);("f",false)]
printAtoms v1
printAtoms v2
r<-varif r1 v1 r2 v2
printfn "%b" r


v1<-[("a",false);("b",false);("c",false)]
v2<-[("d",true);("e",true);("f",true)]
printAtoms v1
printAtoms v2
r<-varif r1 v1 r2 v2
printfn "%b" r

v1<-[("a",false);("b",false);("c",true)]
v2<-[("d",false);("e",true);("f",true)]
printAtoms v1
printAtoms v2
r<-varif r1 v1 r2 v2
printfn "%b" r

v1<-[("a",false);("b",false);("c",true)]
v2<-[("d",true);("e",false);("f",true)]
printAtoms v1
printAtoms v2
r<-varif r1 v1 r2 v2
printfn "%b" r

v1<-[("a",false);("b",false);("c",true)]
v2<-[("d",true);("e",true);("f",false)]
printAtoms v1
printAtoms v2
r<-varif r1 v1 r2 v2
printfn "%b" r

v1<-[("a",false);("b",true);("c",false)]
v2<-[("d",false);("e",true);("f",true)]
printAtoms v1
printAtoms v2
r<-varif r1 v1 r2 v2
printfn "%b" r

v1<-[("a",false);("b",true);("c",false)]
v2<-[("d",true);("e",false);("f",true)]
printAtoms v1
printAtoms v2
r<-varif r1 v1 r2 v2
printfn "%b" r

v1<-[("a",false);("b",true);("c",false)]
v2<-[("d",true);("e",true);("f",false)]
printAtoms v1
printAtoms v2
r<-varif r1 v1 r2 v2
printfn "%b" r

v1<-[("a",false);("b",true);("c",false)]
v2<-[("d",true);("e",true);("f",false)]
printAtoms v1
printAtoms v2
r<-varif r1 v1 r2 v2
printfn "%b" r
*)