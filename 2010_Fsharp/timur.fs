module MyModule
#light

let rec cheby (x:int) (num:int) = match num with
                                              | 0 -> 1
                                              | 1 -> x
                                              | _ -> (2*x*(cheby x (num-1)) - (cheby x (num-2)))
let rec pricheby (num:int) = match num with
                                              | 0 -> sprintf "1"
                                              | 1 -> sprintf "X"
                                              | _ -> sprintf "(2*X*%s)-(%s)" (pricheby (num-1)) (pricheby (num-2))
printf "%s\n" (pricheby 0)
printf "%s\n" (pricheby 1)
printf "%s\n" (pricheby 5)
