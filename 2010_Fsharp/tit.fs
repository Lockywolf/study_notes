#light

module antons.moddd


type Tree = |Leaf of int 
            |Node of int * (Tree list) 

let rnd = new System.Random()
let random () = rnd.Next(1,5)
let rec treeList list length tree k =
    match length with
    | 0 when list <> [] -> list
    | 0 -> treeList (list@[tree k]) length tree k
    | _ -> treeList (list@[tree k]) (length-1) tree k

    

let util x = (1, x)
let firsty (x,y) = x
let util2 (x,y) = (x+1,y)
let rec randomTree k =

    if (k>1) then Node(random(), treeList ([]) (random()) randomTree (k-1))

    else Leaf (random())
    
let mytree = randomTree 5
let rec process_list x maxt = match x with
                                                                   |  t::ys when ( (firsty t) = maxt)  -> t  
                                                                   |  t::ys              -> (process_list ys maxt)
                                                                   |  _ -> (0,0)


let rec findmax x = match x with
                        | t::ys when ((firsty t) > (findmax ys)) -> (firsty t)
                        | t::ys when ((firsty t) < (findmax ys)) -> findmax ys 
                        | [] -> 0


let rec lofcn = function
           | Leaf b -> (0,0)
           | Node(x, y) when ((child_ck y) = []) -> (1,x)
           | Node(x, y) -> process_list (child_ck y) (findmax (child_ck y))
and child_ck x = match x with 
                                      | t::ys when ((lofcn t) = (0,0)) -> (child_ck ys)
                                      | t::ys -> (lofcn t)::(child_ck ys)
                                      | [] -> []

