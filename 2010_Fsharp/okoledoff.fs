﻿
type 't btree = 
    | Node of 't  * ('t btree) * ('t btree)
    | Nil


let tr = Node(6,
            Node(3,
                Node(1,Nil,Nil),
                Node(4,Nil,Nil)),
            Node(7,Nil,Nil))


let cMax = 9  //  Максимальное генерируемое число 
let randGen = new System.Random() 
let random max = randGen.Next(max) 
let rand = random cMax  //  Функция, возвращающая новое случайное число



//Вставляет число в дерево

let rec Find num (tnode:int btree) = 
             match tnode with
                |Nil -> Nil
                |Node(n, left, right) ->
                    if num > n then
                        Find num right
                    else if num < n then
                            Find num left
                        else
                            tnode

let rec InsertNode num (tnode:int btree) = 
             match tnode with
                |Nil -> Node(num, Nil, Nil)
                |Node(n, left, right) ->
                    if num > n then
                        Node(n, left, InsertNode num right)
                    else if num < n then
                            Node(n, InsertNode num left, right)
                         else
                            tnode

let rec Generate = function
        |0 -> Nil
        |m -> InsertNode (random 100) (Generate (m-1))


let rec probel = function
            |0 -> ""
            |m -> " " + probel (m-1)
    


let rec print_tree (tnode:int btree) lvl = 
            match tnode with
                |Nil -> ()//printf "%sNil\n" (probel lvl)
                |Node(n, left, right) ->
                    printf "%s%d\n" (probel lvl) n;
                    print_tree left (lvl+1);
                    print_tree right (lvl+1)

   
        
let mutable root = Generate 30

let check num (tnode:int btree) =
            if (Find num tnode) = Nil then
                printf "%d Not Found\n" num
            else
                printf "%d - OK\n" num

print_tree root 0

check 52 root

