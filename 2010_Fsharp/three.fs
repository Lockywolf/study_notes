module MyModule

#light

type Elem = int

type Tree = E | T of Elem * (list Tree)

let empty = E

(* let rec mem = function
    | x, E -> false
    | x, T(a, y, b) when x < y -> mem(x, a)
    | x, T(a, y, b) when y < x -> mem(x, b)
    | _ -> true *)

let rec insert = function
    | x, E -> T(E, x, E)
    | x, T(a, y, b) when x < y -> T(insert(x, a), y, b)
    | x, T(a, y, b) when y < x -> T(a, y, insert(x, b))
    | _, s -> s

let rec count_empty  = function
                                                | E -> 0
                                                | T(a,y,b) -> ( 1 + (count_empty a )+ (count_empty b ))

let cMax = 90 
let randGen = new System.Random() 
let random max = randGen.Next(max) 
let rand = random cMax

let rec Generate = function
        |0 -> E
        |m -> insert  (random 10, (Generate (m-1)))

let mytree = Generate 10 
printf "%d" (count_empty mytree)