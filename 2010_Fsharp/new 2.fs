module Lab

type Tree = 
    |Leaf of int 
    |Node of int *(Tree list)


let rec unitel a = [a]

let rec makeForAll func list=
    match list with
    |[]->[]
    |h::t -> [func h]@ (makeForAll func t)
(*
let dep Tree =
    let rec depthes k  Tree =
        match Tree with
        |Leaf x -> Leaf 0
        |Node (a, child) -> Node(k, List.map (depthes (k+1) ) child)
    depthes 1 Tree  
*)
let rec d tr=
    match tr with
    |Leaf x->[]
    |Node (a, child) -> ((unitel a)@(makeForAll d child))
      
  
    
let b = Node (10, [Leaf 10; Leaf 5; Node (45,[Leaf 100])])


