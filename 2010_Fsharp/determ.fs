#light

let add (a:double) (b:double) = a + b;;

let sub (a:double) (b:double) = a - b;;
let mul (a:double) (b:double) = a * b;;
let div (a:double) (b:double) = a / b;;

let mat_string a b c = (a, b, c);;
//let matrix (a:mat_string) (b:mat_string) (c:mat_string) = (a, b, c);;

let str1 = mat_string 1 2 3;;
let str2 = mat_string 4 5 6;;
let str3 = mat_string 7 8 9;;
let my_mat = (str1, str2, str3);;

let listr a b c = [a;b;c];;

let li1 = listr 1 2 3;;
let li2 = listr 4 5 6;;
let li3 = listr 7 8 9;;
let myli = listr li1 li2 li3;;

let det2 ((a,b) , (c,d)) = (sub (mul a d) (mul b c));;

let det3 ((a,b,c),(d,e,f),(g,h,i)) = (add (sub (mul a (det2 ((e,f),(h,i))))   (mul b (det2 ((d,f),(g,i)))))  (mul c (det2 ((d,e),(g,h)))));;
