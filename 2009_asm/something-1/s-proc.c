#include<stdio.h>
#include<stdlib.h>
#include<sys/types.h>
#include<sys/stat.h>
#include<unistd.h>
#include<errno.h>

static void croak(const char* msg)
{
    fprintf(stderr, "%s\n", msg);
    fflush(stderr);
}

static void usage(const char* prgnam)
{
    fprintf(stderr, "\nExecute code : %s -e <file-containing-shellcode\n", prgnam);
    fprintf(stderr, "Convert code : %s -p <file-containing-shellcode>\n", prgnam);
    fflush(stderr);
    exit(-1);
}

static void barf(const char* msg)
{
    perror(msg);
    exit(1);
}

int main(int argc, char** argv)
{
    FILE* fp;
    struct stat sbuf;
    void *code,(*fptr)(void);
    int arg,i,l;
    int m=15;
    long flen;
    if(argc <3) usage(argv[0]);
    if(stat(argv[2], &sbuf)) barf("failed to stat file\n");
    flen=(long)sbuf.st_size;
    if(!(code=malloc(flen))) barf("failed to alloc memory\n");
    if(!(fp=fopen(argv[2],"rb"))) barf("failde to open file\n");
    if(fread(code,1,flen,fp)!=flen) barf("failed to read file\n");
    if(fclose(fp)) barf("failed to close file\n");
    while((arg=getopt(argc, argv, "e:p:"))!= -1)
    {
        switch(arg)
        {
            case 'e':
                croak("Calling code...");
                fptr=(void (*)(void))code;
                (*fptr)();
                break;
            case 'p':
                printf("\n/*the following shllcode in %d bytes long: */\n", flen);
                printf("\nchar shellcode[] =\n");
                l=m;
                for(i=0;i<flen;++i)
                {
                    if(l>=m)
                    {
                        if(i) printf("\"\n");
                        printf("\t\"");
                        l=0;
                    }
                    ++l;
                    printf("\\x%02x", ((unsigned char*)code)[i]);
                }
                printf("\";\n\n");
                break;
            default:
                usage(argv[0]);
        }
    }
    return(0);
}
