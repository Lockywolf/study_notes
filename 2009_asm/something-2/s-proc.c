#include<stdio.h>
#include<sys/mman.h>
#include<malloc.h>
#include<stdlib.h>
#include<sys/types.h>
#include<sys/stat.h>
#include<unistd.h>
#include<errno.h>

static void croak(const char* msg)
{
    fprintf(stderr, "%s\n", msg);
    fflush(stderr);
}

static void usage(const char* prgnam)
{
    fprintf(stderr, "\nExecute code : %s -e <file-containing-shellcode\n", prgnam);
    fprintf(stderr, "Convert code : %s -p <file-containing-shellcode>\n", prgnam);
    fflush(stderr);
    exit(-1);
}

static void barf(const char* msg)
{
    perror(msg);
    exit(1);
}

int main(int argc, char** argv)
{
    FILE* fp;
    struct stat sbuf;
    void *code,(*fptr)(void);
    int arg,i,l;
    int m=15;
    long flen;
    if(argc <3) usage(argv[0]);
    if(stat(argv[2], &sbuf)) barf("failed to stat file\n");
    flen=(long)sbuf.st_size;
    //if(!(code=malloc(flen))) barf("failed to alloc memory\n");
    //debug
    code=memalign(sysconf(_SC_PAGE_SIZE),flen);
    if(code==NULL) perror("Memalign");
    //debug
    if(!(fp=fopen(argv[2],"rb"))) barf("failde to open file\n");
    if(fread(code,1,flen,fp)!=flen) barf("failed to read file\n");
    if(fclose(fp)) barf("failed to close file\n");
    while((arg=getopt(argc, argv, "e:p:"))!= -1)
    {
        switch(arg)
        {
            case 'e':
                croak("Calling code...");
                //char shellcode[] ="\x66\x31\xc0\x66\x31\xdb\x66\x31\xc9\x66\x31\xd2\xe9\x10\x00\x66\x59\xb3\x01\xb2\x0d\xb0\x04\xcd\x80\xfe\xcb\xb0\x01\xcd\x80\xe8\xed\xff\x48\x65\x6c\x6c\x6f\x2c\x20\x77\x6f\x72\x6c\x64\x21";
                //printf("%s\n",shellcode);
                //return(0);
                
                fptr=(void (*)(void))code;
                //int pagesize=sysconf(_SC_PAGE_SIZE);
                int i;
                getchar();
                i=mprotect(fptr,sysconf(_SC_PAGE_SIZE),PROT_READ|PROT_WRITE|PROT_EXEC);
                if(i==-1)
                {
                if(errno==EACCES) perror("EACCESS");
                if(errno==EINVAL) perror("EINVAL");
                return(1);
                }
                getchar();
                (*fptr)();
                break;
            case 'p':
                printf("\n/*the following shllcode in %d bytes long: */\n", flen);
                printf("\nchar shellcode[] =\n");
                l=m;
                for(i=0;i<flen;++i)
                {
                    if(l>=m)
                    {
                        if(i) printf("\"\n");
                        printf("\t\"");
                        l=0;
                    }
                    ++l;
                    printf("\\x%02x", ((unsigned char*)code)[i]);
                }
                printf("\";\n\n");
                break;
            default:
                usage(argv[0]);
        }
    }
    return(0);
}
