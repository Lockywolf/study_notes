function [ distance ] = lwf_dist_tester_insopt_easy( distr1, distr2, epsilon )
%LWF_DIST_TESTER_INSOPT_EASY A simplified Valiant instance-optimal identity
%tester.
%   Does not include the lower tail part. The function probabilistically
%   tests if d1=d2 versus L1(d1, d2) > epsilon. For testing purposes, I
%   have a list of distributions, each is <0.2 close to a model one. Let's
%   see if they are > 0.1. 

n = numel(distr1);

sample_const = 10;%BUG. Need to try with 10000, SLOW!!!

sample_size = sample_const * sqrt( n )/ ( (epsilon/4) .^ 2 );

% sample_size_adj = sample_const * sqrt( n )/ ( (epsilon/10) .^ 2 );


[sort1, ind] = sort( distr1 );
sort2by1 = distr2( ind );

threshold = 4*sample_size*(n .^ (1/6));

empirical = zeros( n, 1 );

for i = 1:sample_size
	sample = lwf_draw_from_distr( sort2by1 );
	
	if( rem(i, 1000 ) == 0 )
		fprintf('DEBUG:i=%d out of %d or %f%%, sample=%d \n', i, sample_size, (i/sample_size)*100, sample );
	end;
	empirical( sample ) = empirical( sample ) +1;
end; 

tester_value = 0;

%epsilon/4

temp=0;
offset=0;
for i=1:n
	temp = temp + sort1(i);
	if( temp > (epsilon/4) )
		offset = i-1;
		break;
	end;
end;

for i=(offset+1):n
	temp = ((empirical(i) - sample_size*sort1(i)).^2 - empirical(i))/( (sort1(i)).^(2/3) );
	tester_value = tester_value + temp;
end;

if( tester_value > threshold )
	distance = 'FAR';
else
	distance = 'SAME';
end;

fprintf( 'offset = %d, tester_value=%f, threshold=%f \n', offset, tester_value, threshold );

debugdist = lwf_distr_l1( sort1, empirical/sample_size );

fprintf( 'debugdist = %f\n', debugdist);

%%Second part. Just in case.

tester2 = 0;
for i=1:offset
    tester2 = tester2 + empirical(i);
end;

thresh2 = (3/16)*(epsilon/4)*sample_size;

fprintf( 'Tester2=%d, thresh2=%f\n', tester2, thresh2 );
if( tester2 > thresh2 )
    fprintf( 'TEST2:FAR\n');
else
    fprintf( 'TEST2:SAME\n');
end;

end

