function [ distance ] = lwf_distr_l1( distr1, distr2 )
%LWF_DISTR_L1 Calculates L1 distance between vectors
%   Otherwise called the Manhattan metric.

distance = sum(abs(distr1 - distr2));

end

