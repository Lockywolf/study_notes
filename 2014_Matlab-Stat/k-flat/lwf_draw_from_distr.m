function [ value ] = lwf_draw_from_distr( distribution )
%LWF_DRAW_FROM_DISTR Draws a sample from a discrete distribution given as
%a vector summing to one.
%   A crude method.

mycdf = cumsum( distribution );


value = sum(rand > mycdf(1:end-1), 1) + 1;%do I really have to +1 ??



end

