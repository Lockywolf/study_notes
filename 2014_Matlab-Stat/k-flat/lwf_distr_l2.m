function [ distance ] = lwf_distr_l2( distr1, distr2 )
%LWF_DISTR_L2 Calculates the L2 distance.
%   Otherwise called Euclidian,
distance = norm(distr1 - distr2);

end

