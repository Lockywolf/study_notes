function [ distribution ] = lwf_distr_empirical( source, epsilon )
    %LWF_DISTR_EMPIRICAL Make an empirical distribution function
    %   Easy algorithm
    
    sample_const = 10;
    
    n = numel( source );
    
    sample_size = sample_const * sqrt( n )/ (epsilon .^ 2 );
    
    empirical = zeros( n, 1 );
    
    for i = 1:sample_size
        sample = lwf_draw_from_distr( source );
        
        if( rem(i, 1000 ) == 0 )
            fprintf('DEBUG:i=%d out of %d or %f \%, sample=%d \n', i, sample_size, (i/sample_size)*100, sample );
        end;
        empirical( sample ) = empirical( sample ) +1;
    end;

    empirical = empirical/sample_size;
    
end

