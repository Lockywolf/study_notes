function [ prob_function ] = lwf_gen_k_flat( varargin )
%LWF_GEN_K_FLAT Generates a discrete distribution function
%   Can have two optional parameters. The first one is the number of flat
%   intervals, the second one is the length of the domain.

% max_domain = intmax('int64');

max_domain = 1000;


domain_size = 0;
flatness = 0;


if( nargin == 0 )
	domain_size = randi( max_domain, 1 );
	flatness = randi( domain_size, 1 );
elseif( nargin == 1 )
	flatness = varargin{1};
	domain_size = randi( [flatness, max_domain], 1 );
else
	if( varargin{1} > varargin{2} )
		prob_function = -1;
		return;
	else
		domain_size = varargin{ 2 };
		flatness = varargin{ 1 };
	end;
end;

breakpoints = zeros( flatness, 1);
weights = double( zeros(flatness-1, 1) );
prob_function = zeros( domain_size, 1);


for i=1:flatness-1
	
	breakpoints(i) = randi( [ 1, domain_size] , 1);
% 	weights(i) = rand( 1 , 1);
end;

breakpoints( flatness ) = domain_size;
breakpoints = sort(unique(breakpoints) );
weights = double( zeros(numel(breakpoints), 1) );

for i=1:numel(breakpoints)
	
% 	breakpoints(i) = randi( [ 1, domain_size] , 1);
 	weights(i) = rand( 1 , 1);
end;


weights_sorted = [0, sort(weights)', 1];

for i=1:numel(breakpoints)
	weights(i) = weights_sorted(i+1) - weights_sorted(i);
	if( i~=1 )
		prob_function( breakpoints(i-1):breakpoints(i) ) = weights(i)/ (breakpoints(i)-breakpoints(i-1));
	else
		prob_function( 1:breakpoints(i) ) = weights(i)/breakpoints(i);
	end;
end;

%this seems to be done, but for some unknown reasons, the sum is not one.
%Maybe it's due to a rounding error?
%Let's fix it.

sum = 0;
for i=1:numel( prob_function )
	sum = sum+prob_function(i);
end;

prob_function = prob_function / sum;


end
