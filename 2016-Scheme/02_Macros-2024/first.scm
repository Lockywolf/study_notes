#!/usr/bin/scheme

(display "hello\n")

(let ()
  (define-syntax bind-to-zero
    (syntax-rules ()
      ((bind-to-zero id) (define id 0))))
  (define-syntax demo-1 (lambda (stx) (display stx)))
  (bind-to-zero x)
  x
  (demo-1 x))

(define-syntax used-as
  (make-variable-transformer
   (lambda (stx)
     (cond ((identifier? stx) (quote-syntax (quote reference)))
           ((free-identifier=? (car (unwrap-syntax stx)) #'set!)
            `(,(quote-syntax cons)
              ,(quote-syntax (quote assignment))
              (,(quote-syntax quote)
               ,(cdr (unwrap-syntax
                      (cdr (unwrap-syntax stx)))))))
           (else
            `(,(quote-syntax cons) ,(quote-syntax (quote combination))
              (,(quote-syntax quote)
               ,(cdr (unwrap-syntax stx)))))))))
