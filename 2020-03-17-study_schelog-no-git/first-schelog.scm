;#!/usr/bin/chibi-scheme
(load "my-schelog.scm")

(%which () %true)
(%which () %fail)
(%which () (%=:= 1 1))
(%which () (%< 1 2))

(define %knows
  (%rel ()
    (('Odysseus 'TeX))
    (('Odysseus 'Scheme))
    (('Odysseus 'Prolog))
    (('Odysseus 'Penelope))
    (('Penelope 'TeX))
    (('Penelope 'Prolog))
    (('Penelope 'Odysseus))
    (('Telemachus 'TeX))
    (('Telemachus 'calculus))))
(%which () (%knows 'Odysseus 'TeX))
(define %computer-literate
  (%rel (person)
    ((person)
      (%knows person 'TeX)
      (%knows person 'Scheme))
    ((person)
      (%knows person 'TeX)
      (%knows person 'Prolog))))

(%which () (%computer-literate 'Penelope))
(%which (person) (%computer-literate person))
(%which (what) (%knows 'Odysseus what))
(%more)
(%assert %knows ()
	 (('Odysseus 'archery)))

(define %parent %empty-rel)

(%assert %parent ()
  (('Laertes 'Odysseus)))

(%assert %parent ()
  (('Odysseus 'Telemachus))
  (('Penelope 'Telemachus)))
(%let (what)
      (%which ()
	      (%knows 'Odysseus what)))
(%more)
(define %member
  (%rel (x xs)
    ((x (cons x (_))))
    ((x (cons (_) xs))
      (%member x xs))))

(define %reverse
  (letrec
      ([revaux
	(%rel (x y z w)
	      [('() y y)]
	      [((cons x y) z w)
	       (revaux y
		       (cons x z) w)])])
    (%rel (x y)
	  [(x y) (revaux x '() y)])))
