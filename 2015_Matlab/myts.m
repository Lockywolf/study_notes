%% DOCUMENT TITLE
% INTRODUCTORY TEXT
%%
x = [-0.2 -0.3 13;
     -0.1 -0.4 15;
      NaN  2.8 17;
      0.5 0.3 NaN;
     -0.3 -0.1 15] %#ok<NOPTS>
 
 %%
 % $x^2+e^{\pi i}$ 
 
 
 %%
 % |MONOSPACED TEXT| 
 
 ts_pos = timeseries(x(:,1:2), 1:5, 'name', 'Position') %#ok<*NOPTS>
 %%
 
 getdatasamplesize(ts_pos)
 
 ts_vel = timeseries(x(:,3), 1:5, 'name', 'Velocity');
 
 getdatasamplesize(ts_vel)
 
 load count.dat
 count
count1 = timeseries(count(:,1), 1:24,'name', 'intersection1');
count2 = timeseries(count(:,2), 1:24,'name', 'intersection2');
count3 = timeseries(count(:,3), 1:24,'name', 'intersection3');

count_ts = timeseries(count, 1:24,'name','traffic_counts')

get(count1)
count1.DataInfo.Units = 'cars';
count1.DataInfo.Interpolation = tsdata.interpolation('zoh');
count1.DataInfo

count1.TimeInfo.Units = 'hours';
count2.TimeInfo.Units = 'hours';
count3.TimeInfo.Units = 'hours';

e1 = tsdata.event('AMCommute',8);
e1.Units = 'hours';            % Specify the units for time
count1 = addevent(count1,e1);  % Add the event to count1
count2 = addevent(count2,e1);  % Add the event to count2
count3 = addevent(count3,e1);  % Add the event to count3

e2 = tsdata.event('PMCommute',18);
e2.Units = 'hours';            % Specify the  units for time
count1 = addevent(count1,e2);  % Add the event to count1
count2 = addevent(count2,e2);  % Add the event to count2
count3 = addevent(count3,e2);  % Add the event to count3

% figure
% plot(count1)
% plot(count2)

tsc = tscollection({count1 count2},'name', 'count_coll')
tsc = addts(tsc, count3)


tsc1 = resample(tsc,1:2:24)
tsc1 = resample(tsc,1:0.5:24)

% hold off                % Allow axes to clear before plotting
% plot(tsc1.intersection1,'-xb','Displayname','Intersection 1')
% hold on
% plot(tsc1.intersection2,'-.xm','Displayname','Intersection 2')
% plot(tsc1.intersection3,':xr','Displayname','Intersection 3')
% legend('show','Location','NorthWest')

%%
% tsc1 = addsampletocollection(tsc1,'time',3.25,...
%        'intersection1',5);
   
tsc1.TimeInfo.Units = 'hours';
tsc1.TimeInfo.StartDate = '25-DEC-2009 00:00:00';
tsc1.intersection1.DataInfo.Units = 'car count';
tsc1.intersection2.DataInfo.Units = 'car count';

% hold off
% plot(tsc1.intersection1);

load count.dat
[n,p] = size(count);

% Define the x-values
t = 1:n;

% Plot the data and annotate the graph
% plot(t,count)
% legend('Station 1','Station 2','Station 3','Location','northwest')
% xlabel('Time')
% ylabel('Vehicle Count')

cov(count)
