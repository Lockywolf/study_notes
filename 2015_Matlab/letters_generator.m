images = uint8(ones(65,65,26)*255);
variants = uint8(ones(65, 65, 26, 2000 ))*255;


j=0;
for i=['a', 'b', 'c', 'd', 'e', 'f', 'g',...
       'h', 'i', 'j', 'k', 'l', 'm', 'n',...
       'o', 'p', 'q', 'r', 's', 't', 'u',...
       'v', 'w', 'x', 'y', 'z' ]
    j=j+1;
    images(:,:,j) = rgb2gray( insertText( images(:,:,j), [33 33], i, 'FontSize', 56, 'TextColor', [128 128 128], 'BoxOpacity', 1, 'BoxColor', 'white', 'AnchorPoint', 'Center') );
end;
    
% implay( images );

variants = uint8(ones(65, 65, 26, 2000 ))*255;


% imshow( images(:,:,1) );
J = imnoise( images(:,:, 1), 'gaussian');
imshow( J )
