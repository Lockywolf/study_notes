A = 0.9*gallery('integerdata',99,[3,4],1);
dlmwrite('ph.dat',A,',')

M = csvread('ph.dat')

N = csvread('ph.dat',0,2)

formatSpec = '%C%{MM/dd/yyyy HH:mm}D%f%f%{MM/dd/yyyy HH:mm}D%C';

T = readtable('outages.csv','Delimiter',',', ...
    'Format',formatSpec);

T(1:5,1:4)

ds = datastore('airlinesmall.csv',...
    'TreatAsMissing','NA')
preview(ds)

myData = gallery('uniformdata', [5000,1], 0);

fileID = fopen('records.dat','w');
fwrite(fileID, myData,'double');
fclose(fileID);


m = memmapfile('records.dat',        ...
      'Offset', 2048,                ...
      'Format', {                    ...
         'int16'  [2 2] 'model';     ...
         'uint32' [1 1] 'serialno';  ...
         'single' [1 3] 'expenses'});
     