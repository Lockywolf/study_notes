classdef spx_cell
    %CELL Class defining one cell in a supaplex/snake game.
    %   Detailed explanation goes here
    
    properties
        BackGround;
        ForeGround;
        Passable;
    end
    
    methods
        function obj = spx_cell()
%             'hello from cell constructor'
        obj.BackGround = ' ';
        obj.ForeGround = '#';
            
        end
        
        function console_print_value(obj)
            fprintf(1, '%c', obj.ForeGround);
        end;
            
    end
    
end

