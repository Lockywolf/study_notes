function  console_print_value( obj )
%CONSOLE_PRINT Prints a text representation of the field
%   Detailed explanation goes here

    fprintf(1, '|');
    for countery = 1:size( obj.GameField, 2)
            fprintf('=');
    end
    fprintf(1, '|\n');

    for counterx = 1:size(obj.GameField, 1)
        fprintf( 1, '|');
        for countery = 1:size( obj.GameField, 2)
            obj.GameField(counterx, countery).console_print_value();
        end
        fprintf(1, '|\n');
    end;
    
    fprintf(1, '|');
    for countery = 1:size( obj.GameField, 2)
            fprintf('=');
    end
    fprintf(1, '|\n');

    

end

