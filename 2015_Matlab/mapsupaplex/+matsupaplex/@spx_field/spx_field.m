classdef spx_field
    %SPX_FIELD Class for the game field
    %   Detailed explanation goes here
    
    properties
        
        GameField;
    end
    
    methods (Access = public)
        
        function obj = spx_field( dimx, dimy )
            obj.GameField = matsupaplex.spx_cell.empty(dimx, dimy,0);
            obj.GameField(1:dimx, 1:dimy,1) = matsupaplex.spx_cell();
        end
        
        console_print_value(obj)
        
    end
    
end

