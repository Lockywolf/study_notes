%%
load count.dat
x = count(:,1);
y = count(:,2);

p = polyfit(x,y,1)
yfit = polyval(p,x);
yresid = y - yfit;
SSresid = sum(yresid.^2);

SStotal = (length(y)-1) * var(y);

rsq = 1 - SSresid/SStotal

z = eig(randn(20))

% figure
% plot(z,'o')

x = fminbnd(@humps,0.3,1)
opts = optimset('Display','iter');
x = fminbnd(@humps,0.3,1,opts)


X = [3.5 8.2; 6.8 8.3; 1.3 6.5; 3.5 6.3; 5.8 6.2; 8.3 6.5;...
    1 4; 2.7 4.3; 5 4.5; 7 3.5; 8.7 4.2; 1.5 2.1; 4.1 1.1; ...
    7 1.5; 8.5 2.75];

% plot(X(:,1),X(:,2),'ob')
% hold on
% vxlabels = arrayfun(@(n) {sprintf('X%d', n)}, (1:15)');
% Hpl = text(X(:,1)+0.2, X(:,2)+0.2, vxlabels, 'FontWeight', ...
%   'bold', 'HorizontalAlignment','center', 'BackgroundColor', ...
%   'none');
% hold off
% 
% dt = delaunayTriangulation(X)

% figure, plot( dt.Points);


%% 
q = 14.641;
q



