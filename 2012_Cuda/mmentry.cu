#include <cuda.h>
#include <stdio.h>

/** this file contains the functions to be implemented by contest
		participants. A simple default implementation is provided matmul() is the
		function which participants must implement. matmulKernel() is part of the
		default implementation, and provides a simple kernel to be called on device.
		CHECK() is a simple macro to check validity of CUDA library calls */

#define CHECK(x) {\
		cudaError_t err = x;\
		if(err != cudaSuccess) {\
			fprintf(stderr, #x "\n");\
			fprintf(stderr, "error code %d: %s\n", err, cudaGetErrorString(err)); \
			exit(-3);\
		}\
	}

void matmul(double *c, const double *a, const double *b, int n);

__global__ void matmulKernel
(double *c, const double *a, const double *b, int n);

#define BLOCKDIM 32
#define SNBLOCKS 16

void matmul(double *c, const double *a, const double *b, int n) {
	int n2 = n * n, sz = n2 * sizeof(double);
	double *da[3], *db[3], *dc[3];

#pragma omp parallel for
        for(int i = 0; i < 3; ++i){
		// allocate
		CHECK(cudaSetDevice(i));
		CHECK(cudaMalloc((void**)&(da[i]), sz/3));
      		CHECK(cudaMalloc((void**)&(db[i]), sz));
		CHECK(cudaMalloc((void**)&(dc[i]), sz/3));
       

		// copy data
		CHECK(cudaMemcpy(db[i], b, sz, cudaMemcpyHostToDevice));
		if (n/BLOCKDIM/3<=SNBLOCKS)
		{
			CHECK(cudaMemcpy(da[i], &(a[n2/3*i]), sz/3, cudaMemcpyHostToDevice));
        
			// call matmul kernel
			dim3 blockDim(BLOCKDIM, BLOCKDIM);
			dim3 gridDim(n / BLOCKDIM, n / BLOCKDIM / 3);
			matmulKernel<<<gridDim, blockDim>>>(dc[i], da[i], db[i], n);
	
		        CHECK(cudaDeviceSynchronize());

			// copy data back
        
			CHECK(cudaMemcpy(&(c[n2/3*i]), dc[i], sz/3, cudaMemcpyDeviceToHost));
		} else {
			int numStr = n/BLOCKDIM/3/SNBLOCKS;
			cudaStream_t streams[numStr];
			for (int l = 0; l < numStr; l++) CHECK(cudaStreamCreate(&(streams[l])));
			for (int l = 0; l < numStr; l++)
			{
				CHECK(cudaMemcpyAsync
					(&(da[i][l*SNBLOCKS*BLOCKDIM*n]), 
					 &(a[n2/3*i+l*SNBLOCKS*BLOCKDIM*n]), 
					 sz/3/numStr,
					 cudaMemcpyHostToDevice, streams[l])
				     );
 				cudaStreamSynchronize(streams[i]);
				dim3 blockDim(BLOCKDIM, BLOCKDIM);
				dim3 gridDim(SNBLOCKS, SNBLOCKS);
				matmulKernel<<<gridDim, blockDim, 0, streams[l]>>>
					(&(dc[i][l*SNBLOCKS*BLOCKDIM*n]), &(da[i][l*SNBLOCKS*BLOCKDIM*n]), db[i], n);
 				cudaStreamSynchronize(streams[i]);
				CHECK(cudaMemcpyAsync
					(&(c[n2/3*i+l*SNBLOCKS*BLOCKDIM*n]),
					 &(dc[i][l*SNBLOCKS*BLOCKDIM*n]),
					 sz/3/numStr,
					 cudaMemcpyDeviceToHost, streams[l])
				);
	
 				cudaStreamSynchronize(streams[i]);
			}
			CHECK(cudaDeviceSynchronize());
			for (int l = 0; l < numStr; l++) CHECK(cudaStreamDestroy(streams[l]));
			
		}
        }        

}  // matmul

__global__ void matmulKernel
(double *c, const double *a, const double *b, int n) {
        int i = blockIdx.y * BLOCKDIM + threadIdx.y;
	int j = blockIdx.x * BLOCKDIM + threadIdx.x;
	double r = 0;
        
	int ty = threadIdx.y;
        int tx = threadIdx.x;
        
        // Index of the first sub-matrix of A processed by the block
        int aBegin = n * BLOCKDIM * blockIdx.y;
        int aEnd = aBegin + n - 1;

        // Step size used to iterate through the sub-matrices of A
        int aStep = BLOCKDIM;

        // Index of the first sub-matrix of B processed by the block
        int bBegin = BLOCKDIM * blockIdx.x;

        // Step size used to iterate through the sub-matrices of B
        int bStep = BLOCKDIM * n;

    for (int ia = aBegin, ib = bBegin; ia <= aEnd; ia += aStep, ib += bStep)
    {
        // Shared memory for the sub-matrix of A
        __shared__ float as [BLOCKDIM][BLOCKDIM+1];
        // Shared memory for the sub-matrix of B
        __shared__ float bs [BLOCKDIM][BLOCKDIM+1];

        // Load the matrices from global memory to shared memory
        as [ty][tx] = a [ia + n * ty + tx];
        bs [ty][tx] = b [ib + n * ty + tx];
    //    bs [tx][ty+BLOCKDIMY] = b [ib + n * tx + ty+BLOCKDIMY];
        // Synchronize to make sure the matrices are loaded
        __syncthreads();

        // Multiply the two matrices together;
        for (int k = 0; k < BLOCKDIM; k++)
            r += as [ty][k] * bs [k][tx];

        // Synchronize to make sure that the preceding
        // computation is done before loading two new
        // sub-matrices of A and B in the next iteration
        __syncthreads();
    }

    c [n * i + j] = r;

}  // matmulKernel
