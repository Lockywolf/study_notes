#!/bin/sh

echo Marking candidate: $1

echo "Compiling"

./setup-one ./$1

echo Marking part A...

echo "Reading the error log: cat ./$1/tour-for-marking.out"
cat ./$1/tour-for-marking.out
read -n 1

echo "Data 1: should be 0.000"
./$1/tour-for-marking < data1


echo "Data 2: should be 490.00000 and a line"
./$1/tour-for-marking < data2


echo "Data 3: should be 980.000 and a hook"

./$1/tour-for-marking < data3

echo "Data 101: should be 517.071068 and a hook and false"
./$1/tour-for-marking < data101

echo "Displaying code for part A"
read -n 1
#less ./$1/tour-for-marking.c
indent -st -bl -blf -bli0 -i4 ./$1/tour-for-marking.c | less


echo Marking parts BCD...

echo "Reading error log:cat ./$1/tour-for-marking2.out"
cat ./$1/tour-for-marking2.out
read -n 1

echo "Data 25: should be 6359.787621 and 4936.719500 and 1792.490761 and 2037.419704"

./$1/tour-for-marking2 < data25

echo "Displaying code for parts BCD"
read -n 1
indent -st -bl -blf -bli0 -i4 ./$1/tour-for-marking2.c | less



echo "Marking part E"

echo "Reading error log: cat ./$1/travel-for-marking.out"

cat ./$1/travel-for-marking.out
read -n 1

./$1/travel-for-marking

echo "Displaying code for part E"
read -n 1
indent -st -bl -blf -bli0 -i4 ./$1/travel-for-marking.c | less


#cat $1/tour.c | sed 's/int main/int old_main/g' > $1/tour-for-marking.c
#cat ./partAmain >> $1/tour-for-marking.c
#gcc -g -o $1/tour-for-marking -I./templates -Wall $1/tour-for-marking.c ./templates/fdescartes.o -lSDL -lm 2>&1 | tee $1/tour-for-marking.out
#echo Done.

#echo Preparing parts BCD...
#cat $1/tour.c | sed 's/int main/int old_main/g' > $1/tour-for-marking2.c
#cat ./partBCDmain >> $1/tour-for-marking2.c
#gcc -g -o $1/tour-for-marking2 -I./templates -Wall $1/tour-for-marking2.c ./templates/fdescartes.o -lSDL -lm 2>&1 | tee $1/tour-for-marking2.out
#echo Done.

#echo Preparing part E...
#cat $1/travel.c | sed 's/int main/int old_main/g' > $1/travel-for-marking.c
#cat ./partEmain >> $1/travel-for-marking.c
#gcc -g -o $1/travel-for-marking -I./templates -Wall $1/travel-for-marking.c 2>&1 | tee $1/travel-for-marking.out
#echo Done.
