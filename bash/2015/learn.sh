#!/bin/sh 
#Learner's script to manipulate files. 
#Written by Lockywolf 18.11.2015 on msys64

#script_parameters="-a -h -m -z"
#                  -a = all, -h = help, etc.
#if [ $# -ne $Number_of_expected_args ]
#then
#  echo "Usage: `basename $0` $script_parameters"
  # `basename $0` is the script's filename.
#  exit $E_WRONG_ARGS
#fi


echo "Starting a clever file"

DIRNAME=default

#Error codes:
E_NOFILE=501
E_NOMKDIR=502
E_BIGFILE=503
E_WRITE=504
E_WRONG_ARGS=85


if [ -d $DIRNAME ]; then 
	rm -rf $DIRNAME
fi

#unzip $DIRNAME.zip -d $DIRNAME > unzip.log
mkdir $DIRNAME
cd $DIRNAME/$DIRNAME

echo  My UID is $UID

echo "My own name is $0"

if [ -n "$1" ];
# Test whether command-line argument is present (non-empty).
then
	echo command line argument equals $1
else  
	echo No command line argument present # Default, if not specified on command-line.
fi  
echo "Basename \$0 is"
echo `basename $0`
ls
echo `ls`

LS_RESULT=`ls`
echo $LS_RESULT

echo hello; echo there


base64_charset=( {A..Z} {a..z} {0..9} + / = )

echo "Script finished"

exit 0

