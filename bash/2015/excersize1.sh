#!/bin/sh
#this script shows basic system info: date and time, logged-in users and system uptime. Then saves it to a logfile. Written by lwf as an excersise 18.11.2015

FILENAME="logfile.log"

LOGINNAME=`whoami`
echo $?
HOSTNAME=`hostname`
DATE=`date -R`
USERS=`users`
SESSIONS=`who`
UPTIME=`uptime`

echo -e $LOGINNAME'\n'$HOSTNAME'\n'$DATE'\n'$USERS'\n'$SESSIONS'\n'$UPTIME > $FILENAME


{
whoami
hostname
date -R
users
who
uptime
} > $FILENAME.2

#echo "$LOGINNAME\@ $HOSTNAME1 $DATE $USERS $SESSIONS $UPTIME" > $FILENAME

# whoami > "$FILENAME"
# hostname >> "$FILENAME"
# date -R >> "$FILENAME"
# users >> "$FILENAME"
# who >> "$FILENAME"
# uptime >> "$FILENAME"

