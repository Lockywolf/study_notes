# -*- mode: org; eval: (visual-line-mode) -*-
# Time-stamp: <2025-02-25 11:29:22 lockywolf>
#+title: Solving "awk primer".
#+author: lockywolf
#+date: 
#+created: <2021-12-06 Mon 14:42>
#+refiled:
#+language: en
#+category: study
#+tags: awk, study, languages, programming, software, diy
#+creator: Emacs 30.0.90/org-mode



* AWK Primer https://en.wikibooks.org/wiki/An_Awk_Primer

** A Guided Tour of Awk

***    Awk Overview [0/0]

#+name: s-input-test
#+begin_quote
TODO:
#+end_quote

#+header: :stdin s-input-test
#+begin_src shell :shebang "#!/usr/bin/gawk -f" :results output code :wrap example :exports both
  /.*/{print $1;}
#+end_src

#+RESULTS:
#+begin_example
TODO:
#+end_example


#+begin_src awk :stdin s-input-test :results output code :wrap example :exports both
  /.*/{print $1;}
#+end_src

#+RESULTS:
#+begin_example
TODO:
#+end_example


*** Awk Command-Line Examples [3/3]


**** Introduction

#+name: coins.txt
#+begin_example file: coins.txt
gold     1    1986  USA                 American Eagle
gold     1    1908  Austria-Hungary     Franz Josef 100 Korona
silver  10    1981  USA                 ingot
gold     1    1984  Switzerland         ingot
gold     1    1979  RSA                 Krugerrand
gold     0.5  1981  RSA                 Krugerrand
gold     0.1  1986  PRC                 Panda
silver   1    1986  USA                 Liberty dollar
gold     0.25 1986  USA                 Liberty 5-dollar piece
silver   0.5  1986  USA                 Liberty 50-cent piece
silver   1    1987  USA                 Constitution dollar
gold     0.25 1987  USA                 Constitution 5-dollar piece
gold     1    1988  Canada              Maple Leaf
#+end_example

#+begin_src awk :results output :wrap example :exports both :stdin coins.txt
/gold/
#+end_src

#+RESULTS:
#+begin_example
gold     1    1986  USA                 American Eagle
gold     1    1908  Austria-Hungary     Franz Josef 100 Korona
gold     1    1984  Switzerland         ingot
gold     1    1979  RSA                 Krugerrand
gold     0.5  1981  RSA                 Krugerrand
gold     0.1  1986  PRC                 Panda
gold     0.25 1986  USA                 Liberty 5-dollar piece
gold     0.25 1987  USA                 Constitution 5-dollar piece
gold     1    1988  Canada              Maple Leaf
#+end_example

**** Printing the Descriptions


#+begin_src awk :results output :wrap example :exports both :stdin coins.txt
/gold/ {print $5,$6,$7,$8}
#+end_src

#+RESULTS:
#+begin_example
American Eagle  
Franz Josef 100 Korona
ingot   
Krugerrand   
Krugerrand   
Panda   
Liberty 5-dollar piece 
Constitution 5-dollar piece 
Maple Leaf  
#+end_example

**** Conditionals

#+begin_src awk :results output :wrap example :exports both :stdin coins.txt
{if ($3 < 1980) print $3, "    ",$5,$6,$7,$8}
#+end_src

#+RESULTS:
#+begin_example
1908      Franz Josef 100 Korona
1979      Krugerrand   
#+end_example

Awk has {} blocks like C.

#+begin_src awk :results output :wrap example :exports both :stdin coins.txt
{if ($3 < 1980) {print $3, "    ",$5,$6,$7,$8};}
#+end_src

#+RESULTS:
#+begin_example
1908      Franz Josef 100 Korona
1979      Krugerrand   
#+end_example

#+begin_src awk :results output :wrap example :exports both :stdin coins.txt
END {print NR, "coins"}
#+end_src

#+RESULTS:
#+begin_example
13 coins
#+end_example

**** Counting Money

#+begin_src awk :results output :wrap example :exports both :stdin coins.txt
/gold/ {ounces += $2} END {print "value = $" 425*ounces}
#+end_src

#+RESULTS:
#+begin_example
value = $2592.5
#+end_example

**** Practice [3/3]
***** DONE Try modifying one of the above programs to calculate and display the total amount (in ounces) of gold and silver, separately but with one program. You will have to use two pairs of pattern/action.
CLOSED: [2025-02-11 Tue 13:27]

#+begin_src awk :results output :wrap example :exports both :stdin coins.txt
  /gold/ {gold_ounces += $2}
  /silver/ {silver_ounces += $2}
  END {print "gold value = $" 425*gold_ounces " silver value = $" 100*silver_ounces}
#+end_src

#+RESULTS:
#+begin_example
gold value = $2592.5 silver value = $1250
#+end_example

***** DONE Write an Awk program that finds the average weight of all coins minted in the USA.
CLOSED: [2025-02-11 Tue 13:30]

#+begin_src awk :results output :wrap example :exports both :stdin coins.txt
  /USA/ { ncoins+=1; weight+=$2}
  END {print "ncoins = " ncoins " weight=" weight " average=" weight/ncoins}
#+end_src

#+RESULTS:
#+begin_example
ncoins = 7 weight=14 average=2
#+end_example

***** DONE Write an Awk program that reprints its input with line numbers before each line of text.
CLOSED: [2025-02-11 Tue 13:32]

#+begin_src awk :results output :wrap example :exports both :stdin coins.txt
  BEGIN {lineno=0}
  {lineno+=1; print lineno " " $0}
#+end_src

#+RESULTS:
#+begin_example
1 gold     1    1986  USA                 American Eagle
2 gold     1    1908  Austria-Hungary     Franz Josef 100 Korona
3 silver  10    1981  USA                 ingot
4 gold     1    1984  Switzerland         ingot
5 gold     1    1979  RSA                 Krugerrand
6 gold     0.5  1981  RSA                 Krugerrand
7 gold     0.1  1986  PRC                 Panda
8 silver   1    1986  USA                 Liberty dollar
9 gold     0.25 1986  USA                 Liberty 5-dollar piece
10 silver   0.5  1986  USA                 Liberty 50-cent piece
11 silver   1    1987  USA                 Constitution dollar
12 gold     0.25 1987  USA                 Constitution 5-dollar piece
13 gold     1    1988  Canada              Maple Leaf
#+end_example

***    Awk Program Example [3/3]

**** The "Master" Program

#+begin_src awk :results output :wrap example :exports both :stdin coins.txt
# This is an awk program that summarizes a coin collection.
/gold/    { num_gold++; wt_gold += $2 }                           # Get weight of gold.
/silver/  { num_silver++; wt_silver += $2 }                       # Get weight of silver.
END {
    val_gold = 485 * wt_gold;                                     # Compute value of gold.
    val_silver = 16 * wt_silver;                                  # Compute value of silver. 
    total = val_gold + val_silver;

    print "Summary data for coin collection:";
    printf("\n");                                                 # Skips to the next line.
    printf("    Gold pieces:\t\t%4i\n", num_gold);
    printf("    Weight of gold pieces:\t%7.2f\n", wt_gold);
    printf("    Value of gold pieces:\t%7.2f\n", val_gold);
    printf("\n");
    printf("    Silver pieces:\t\t%4i\n", num_silver);
    printf("    Weight of silver pieces:\t%7.2f\n", wt_silver);
    printf("    Value of silver pieces:\t%7.2f\n", val_silver);
    printf("\n");
    printf("    Total number of pieces:\t%4i\n", NR);
    printf("    Value of collection:\t%7.2f\n", total);
}
#+end_src

#+RESULTS:
#+begin_example
Summary data for coin collection:

    Gold pieces:		   9
    Weight of gold pieces:	   6.10
    Value of gold pieces:	2958.50

    Silver pieces:		   4
    Weight of silver pieces:	  12.50
    Value of silver pieces:	 200.00

    Total number of pieces:	  13
    Value of collection:	3158.50
#+end_example

**** Practice [3/3]

***** DONE Modify the above program to tally and display the countries from "coins.txt".
CLOSED: [2025-02-11 Tue 13:43]

#+begin_src awk :results output :wrap example :exports both :stdin coins.txt
//{map[$4]=1}
END {
for (a in map) print a
}
#+end_src

#+RESULTS:
#+begin_example
USA
PRC
Austria-Hungary
Canada
Switzerland
RSA
#+end_example

***** DONE Write a program that counts and displays the number of blank and non-blank lines (use NF).
CLOSED: [2025-02-11 Tue 13:49]

#+name: blank-nonblank
#+begin_example
 aa 

bb test test ttt
cc


ddsdfasdfasdfasdfasdfsdfasdfasdf

e
#+end_example

#+begin_src awk :results output :wrap example :exports both :stdin blank-nonblank
  /^$/ {blank++}
  END {print "blank=" blank " non-blank=" NR-blank}
#+end_src

#+RESULTS:
#+begin_example
blank=4 non-blank=5
#+end_example

***** DONE Modify the program from #2 to count the average number of words per line.
CLOSED: [2025-02-11 Tue 13:49]

#+begin_src awk :results output :wrap example :exports both :stdin blank-nonblank
  /^$/ {blank++}
  // {totalfields+=NF}
  END {print "blank lines=" blank " nonblank=" NR-blank " total fields=" totalfields " avg fields=" totalfields/NR}
#+end_src

#+RESULTS:
#+begin_example
blank lines=4 nonblank=5 total fields=8 avg fields=0.888889
#+end_example

** Awk Syntax

***    Awk Invocation and Operation

**** Practice [3/3]

**** DONE If you haven't already, try running the program from "Field Separator" to list all users. See what happens without the -F:. (If you're not using Unix or Linux, sorry; it won't work.)
CLOSED: [2025-02-11 Tue 13:58]

#+name: catpasswd
#+begin_src shell :results output
cat /etc/passwd
#+end_src

#+begin_src awk :results output :wrap example :exports both :stdin catpasswd
  BEGIN{FS=":"}
  //{lineno++ ; print $1 ; if(lineno==10) exit}
#+end_src

#+RESULTS:
#+begin_example
root
bin
daemon
adm
lp
sync
shutdown
halt
mail
news
#+end_example

**** DONE Write an Awk program to convert "coins.txt" (from the previous chapters) into a tab-delimited file. This will require "piping", which varies depending on your system, but you should probably write something like > tabcoins.txt to send Awk's output to a new file instead of the screen.
CLOSED: [2025-02-11 Tue 14:03]

I do not want to change files.

#+begin_src awk :results output :wrap example :exports both :stdin coins.txt
  //{idx=1 ; while(idx<NF) { printf("%s",$idx); if(idx<(NF-1)) printf("\t") ; idx++}; printf("\n") }
#+end_src

#+RESULTS:
#+begin_example
gold	1	1986	USA	American
gold	1	1908	Austria-Hungary	Franz	Josef	100
silver	10	1981	USA
gold	1	1984	Switzerland
gold	1	1979	RSA
gold	0.5	1981	RSA
gold	0.1	1986	PRC
silver	1	1986	USA	Liberty
gold	0.25	1986	USA	Liberty	5-dollar
silver	0.5	1986	USA	Liberty	50-cent
silver	1	1987	USA	Constitution
gold	0.25	1987	USA	Constitution	5-dollar
gold	1	1988	Canada	Maple
#+end_example



**** DONE Write a program that acts as a simple calculator. It does not need an input file; let it receive input from the keyboard. The input should be two numbers with an operator (like + or -) in between, all separated by spaces. Match lines containing these patterns and output the result.
CLOSED: [2025-02-11 Tue 14:07]

I typed in a sort of an RPN calculator.

#+name: calc-input
#+begin_example
+ 1 2
 * 3 3
/ 4 2
#+end_example


#+begin_src awk :results output :wrap example :exports both :stdin calc-input
  #  //{print}
    $1=="+" { print ($2 + $3) }
    $1=="-" { print ($2 - $3) }
    $1=="*" { print ($2 * $3) }
    $1=="/" { print ($2 / $3) }
#+end_src

#+RESULTS:
#+begin_example
3
9
2
#+end_example

***    Search Patterns (1)

Basically POSIX regexps.

***    Search Patterns (2)

Okay, AWK can do more than regexps, it can do various stuff on the fields.

**** Practice [3/3]

***** DONE Write an Awk program that prints any line with less than 5 words, unless it starts with an asterisk.
CLOSED: [2025-02-11 Tue 14:20]

#+name: pat2-input1
#+begin_example
 a b c d e e e not printed
 * a a a a a a a a a a dfdfsdfsdf printed
 a a a a a a a a a a dfdfsdfsdf not printed
 a b c
#+end_example

#+begin_src awk :results output :wrap example :exports both :stdin pat2-input1
(NF<5) || ($1 == "*") {print $1}
#+end_src

#+RESULTS:
#+begin_example
,*
a
#+end_example

Wtf, why does a comma appear?

***** DONE Write an Awk program that prints every line beginning with a number.
CLOSED: [2025-02-11 Tue 14:22]

#+name: pat2-input2
#+begin_example
1 one
2 two
sss
test test
#+end_example

#+begin_src awk :results output :wrap example :exports both :stdin pat2-input2
$1 ~ /^[[:digit:]]/{print}
#+end_src

#+RESULTS:
#+begin_example
1 one
2 two
#+end_example

***** DONE Write an Awk program that scans a line-numbered text file for errors. It should print out any line that is missing a line number and any line that is numbered incorrectly, along with the actual line number.
CLOSED: [2025-02-11 Tue 14:28]

#+name: pat2-input3
#+begin_example
1 one
2 two
sss
test test
5 test
7 line 6
#+end_example

#+begin_src awk :results output :wrap example :exports both :stdin pat2-input3
$1 !~ /^[[:digit:]]+/ || $1 != NR {print $0 " actual line number is " NR}
#+end_src

#+RESULTS:
#+begin_example
sss actual line number is 3
test test actual line number is 4
7 line 6 actual line number is 6
#+end_example


***    Numbers and Strings

**** Practice

***** DONE Write the address book program. You'll need to set FS to a newline and RS to a blank line. Your program should read the multi-line input and output it in single-line format.
CLOSED: [2025-02-11 Tue 14:41]

#+name: addressbook
#+begin_example
Ivan
Ivanov
1231231231241

John
Smith
23424234234234

Brian
Kernighan
23424234234234

Dennis
Ritchine
23424234234234

Rob
Pike
23424324234

#+end_example

#+begin_src awk :results output :wrap example :exports both :stdin addressbook
  BEGIN{FS="\n" ; RS="\n\n" }
  {idx=1 ; printf("%d ", NR) ; while(idx<=NF) { printf("%s",$idx); if(idx<(NF)) printf("\t") ; idx++}; printf("\n")}
#+end_src

#+RESULTS:
#+begin_example
1 Ivan	Ivanov	1231231231241
2 John	Smith	23424234234234
3 Brian	Kernighan	23424234234234
4 Dennis	Ritchine	23424234234234
5 Rob	Pike	23424324234
#+end_example

***** DONE Write a program that reads a list of numbers and outputs them in a different format. Each input line should begin with a character such as a comma or hyphen, followed by a space and then up to five numbers (space-separated). Your program should output these numbers with the new separator (given at the beginning of the line) in between the numbers. You'll have to modify OFS for each line of input.
CLOSED: [2025-02-11 Tue 14:45]

#+name: numberformat
#+begin_example
- 1 22 232323 2
+ 2 2 34234234
, 2342432 2342423 2342423
#+end_example

#+begin_src awk :results output :wrap example :exports both :stdin numberformat
{OFS=$1 ; print $2,$3,$4,$5,$6}
#+end_src

#+RESULTS:
#+begin_example
1-22-232323-2-
2+2+34234234++
2342432,2342423,2342423,,
#+end_example

Weird problem.
***    Variables
***    Arrays
***    Operations
***    Standard Functions
***    User-defined Functions
***    Control Structures
***    Output with print and printf
***    A Digression: The sprintf Function
***    Output Redirection and Pipes

** Awk Examples, NAWK & Awk Quick Reference

***    Using Awk from the Command Line
***    Awk Program Files
***    A Note on Awk in Shell Scripts
***    Nawk
***    Awk Quick Reference Guide
***    Revision Histor

* Aho-Kernighan-Weinberger

Data can be downloaded from:
https://www.awk.dev/programs.tar


** Awk Tutorial

*** Getting Started

#+name: payroll
#+begin_quote
Beth  21    0
Dan   19    0
Kathy 15.50 10
Mark  25    20
Mary  22.50 22
Susie 17    18
#+end_quote

**** Print the name and pay (rate times hours) for everyone
who worked more than zero hours.

#+begin_src awk :results output :wrap example :exports both :stdin payroll
$3 > 0 {print $1, $2*$3}
#+end_src

#+RESULTS:
#+begin_example
Kathy 155
Mark 500
Mary 495
Susie 306
#+end_example

**** To print the names of those employees who did not work

#+begin_src awk :results output :wrap example :exports both :stdin payroll
$3==0 {print $1}
#+end_src

#+RESULTS:
#+begin_example
Beth
Dan
#+end_example

**** Print every line of input

#+begin_src awk :results output :wrap example :exports both :stdin payroll
{print}
#+end_src

#+RESULTS:
#+begin_example
Beth  21    0
Dan   19    0
Kathy 15.50 10
Mark  25    20
Mary  22.50 22
Susie 17    18
#+end_example

**** Print first and third fields

#+begin_src awk :results output :wrap example :exports both :stdin payroll
{print $1, $3}
#+end_src

#+RESULTS:
#+begin_example
Beth 0
Dan 0
Kathy 10
Mark 20
Mary 22
Susie 18
#+end_example

**** Print how many lines are there in a line for each line

#+name: lwf-count-fields
#+begin_quote
a a a
a
 a
	aaaa
a a a a a a a a a a a a a a a  a   a
#+end_quote

#+begin_src awk :results output :wrap example :exports both :stdin lwf-count-fields
{print $0, "has", NF, "fields"}
#+end_src

#+RESULTS:
#+begin_example
a a a has 3 fields
a has 1 fields
 a has 1 fields
	aaaa has 1 fields
a a a a a a a a a a a a a a a  a   a has 17 fields
#+end_example

**** Enumerate lines in a file (poor man's ~num~ command)

#+begin_src awk :results output :wrap example :exports both :stdin lwf-count-fields
{print NR, $0}
#+end_src

#+RESULTS:
#+begin_example
1 a a a
2 a
3  a
4 	aaaa
5 a a a a a a a a a a a a a a a  a   a
#+end_example


*** TODO Formatted output

**** TODO some boring tasks here

**** Print all the data for each employee, along with his or her pay, sorted in order of increasing pay

I am going to be a little tricky here.

#+begin_src awk :results output :wrap example :exports both :stdin payroll
  BEGIN {o="";}
  { pay= $2*$3 ; if ( NR==1) o = o pay " " $0  ;  else o = o "\n" pay " " $0 }
  END { print o | "sort" }
#+end_src 

#+RESULTS:
#+begin_example
0 Beth  21    0
0 Dan   19    0
155 Kathy 15.50 10
306 Susie 17    18
495 Mary  22.50 22
500 Mark  25    20
#+end_example

*** TODO Selection

**** TODO Print those who earn more than 20$ per hour

**** TODO Print employees whose total pay exceeds 200$

**** TODO Print an entry for Suzie

**** TODO Print entries for employees who earn at least 20$ per hour or work at least 20 hours

#+begin_src awk :results output :wrap example :exports both :stdin payroll
$2>20 || $3>20 {print}
#+end_src

#+RESULTS:
#+begin_example
Beth  21    0
Mark  25    20
Mary  22.50 22
#+end_example

**** Print database headers in the output, fields are NAME RATE HOURS


#+begin_src awk :results output :wrap example :exports both :stdin payroll
  BEGIN {print "NAME", "RATE", "HOURS"}
  {print}
#+end_src

#+RESULTS:
#+begin_example
NAME RATE HOURS
Beth  21    0
Dan   19    0
Kathy 15.50 10
Mark  25    20
Mary  22.50 22
Susie 17    18
#+end_example

*** Computing in AWK

**** Print the number of employees who work more than 15 hours

#+begin_src awk :results output :wrap example :exports both :stdin payroll
  BEGIN{ ne=0}
  $3 > 15 {ne++}
  END {print ne, "employees work more than 15 hours"}
#+end_src

#+RESULTS:
#+begin_example
3 employees work more than 15 hours
#+end_example

**** TODO Boring parts


*** Control-Flow Statements

**** TODO Boring exercises

*** Arrays

**** Print a file in reverse

#+begin_src awk :results output :wrap example :exports both :stdin payroll
    { a[NR]=$0}
    END{ for (i=NR; i>0 ; i--)
        print a[i]}
#+end_src

#+RESULTS:
#+begin_example
Susie 17    18
Mary  22.50 22
Mark  25    20
Kathy 15.50 10
Dan   19    0
Beth  21    0
#+end_example

*** Useful One-liners

**** TODO Some boring exercises

**** Print every line with the first field replaced by the line number:

#+begin_src awk :results output :wrap example :exports both :stdin payroll
{ $1 = NR ; print}
#+end_src

#+RESULTS:
#+begin_example
1 21 0
2 19 0
3 15.50 10
4 25 20
5 22.50 22
6 17 18
#+end_example

**** Print every line after erasing the second field

#+begin_src awk :results output :wrap example :exports both :stdin payroll
{ $2="" ; print}
#+end_src

#+RESULTS:
#+begin_example
Beth  0
Dan  0
Kathy  10
Mark  20
Mary  22
Susie  18
#+end_example

** Awk in Action

*** Personal Computation

**** Ask the user for his weight and height, and output BMI

#+name: bmi
#+begin_quote
190 74
#+end_quote

#+begin_src awk :results output :wrap example :exports both :stdin bmi
  BEGIN {print "Enter pounds inches" }
  {print ($1/2.2)/ ($2 * 2.54/100)^2}
#+end_src

#+RESULTS:
#+begin_example
Enter pounds inches
24.4456
#+end_example

**** TODO Take user's weight and height from command-line parameters and output the bmi


*** Selection

**** Duplicate the UNIX ~head~ command, print the first 10 lines of a file

#+name: lwf-long-file
#+begin_quote
line 1
line 2
line 3
line 4
line 5
line 6
line 7
line 8
line 9
line 10
line 11
line 12
line 13
#+end_quote

#+begin_src awk :results output :wrap example :exports both :stdin lwf-long-file
  NR <= 10 {print}
  NR == 10 {exit}
#+end_src

#+RESULTS:
#+begin_example
line 1
line 2
line 3
line 4
line 5
line 6
line 7
line 8
line 9
line 10
#+end_example


**** TODO Print the first three and the last three lines of the input

AWK is not very good for this.
You need to keep three lines in memory and rotate the index.
Annoying.

*** Transformation

**** Replace CRLF with LF

Hint: you can enter CR on Emacs with typing C-q C-m

#+name: lwf-widows-file
#+begin_quote
line 1
line 2 
some more text
#+end_quote

#+begin_src awk :results output :wrap example :exports both :stdin lwf-widows-file
{ sub( /\r/, "" ); print}
#+end_src

#+RESULTS:
#+begin_example
line 1
line 2 
some more text
#+end_example

**** Implement unix2dos

#+begin_src awk :results output :wrap example :exports both :stdin payroll
{ if( !/\r/ ) printf( "%s\r\n", $0 )}
#+end_src

#+RESULTS:
#+begin_example
Beth  21    0
Dan   19    0
Kathy 15.50 10
Mark  25    20
Mary  22.50 22
Susie 17    18
#+end_example

**** TODO Reflow a list of names into 80chars

This is not too hard, but tedious.

*** Summarization

**** Write ~addup~ script which adds up values in all columns and print sums in the end

#+name: lwf-column-data
1 2 3 4 5
1 2 3
4 5 6
1 2 3 4 5 6
1
#+begin_quote

#+end_quote

#+begin_src awk :results output :wrap example :exports both :stdin lwf-column-data
  {for( i=1 ; i<=NF ; i++ ) s[i]+=$i}
  END{ for( i in s ) printf("%d ", s[i]) }
#+end_src

#+RESULTS:
#+begin_example
8 11 15 8 10 6 
#+end_example

*** Personal Database

**** Integrate a function with a 365, 31, and 7 day windows.

#+name: lwf-random-numbers
#+begin_src awk :results output :wrap example :exports both
BEGIN { for (i=1; i<=1000 ; i++) print int(11000*rand()); }
#+end_src

#+begin_src awk :results output :wrap example :exports both :stdin lwf-random-numbers
      { fun[NR]=$1 ; integral[NR]=integral[NR-1]+$1 }
  #    NR > 970 {print}
    END{ print "integral=", integral[NR]
	print "integral[NR-30]=", integral[NR-30]
	lm=integral[NR]-integral[NR-30]
    print "walked last month:", lm}
#+end_src

#+RESULTS:
#+begin_example
integral= 5505046
integral[NR-30]= 5344382
walked last month: 160664
#+end_example

**** TODO Plot a histogram of a distribution using ASCII-art, for example of the same random distribution above

**** Write a simple website scraper, extracting data from a tag to a tag

#+begin_src awk :results output :wrap example :exports both :stdin 
    #  BEGIN { print "y" | "curl https://www.aijingu.com/" | output }
    BEGIN {
      idx=0
      # ("curl https://www.aijingu.com/ 2>/dev/null" | getline )
      while (("cat ./stock.html" | getline) > 0){
        lineno++
        if(lineno<100 || lineno>295) continue;
#        print "NR=" lineno, $0
        symbol[0]="\n"
        symbol[1]=" "
        symbol[2]=" "
        if ( /title=".*"/ ) {
          idx++
          match($0, /title="(.*)"/, ary)
          printf("%s%s", ary[1], symbol[idx%3])
        }

  #    print $0
    }}
     #END{print "DONE"}
#+end_src

#+RESULTS:
#+begin_example
【002681】奋达科技 奋达科技 10.05
【300244】迪安诊断 迪安诊断 9.26
【300657】弘信电子 弘信电子 13.07
【600602】云赛智联 云赛智联 10.00
#+end_example

*** A Personal Library

**** Write a function which returns the rest of the arguments after Nth

#+name: lwf-test-functions
#+begin_quote
a b c d e f
1 2 3 4 5

a
b

#+end_quote

#+begin_src awk :results output :wrap example :exports both :stdin lwf-test-functions
  function rest(n,  s) {
    while( n<=NF)
      { s = s $n " "; n++; }
    return s
  }
  {printf( "%s ", NR) ; print;
    for( i=0 ; i<=NF+1 ; i++)
      printf( "%3d [%s]\n", i, rest(i))
  }
#+end_src

#+RESULTS:
#+begin_example
1 a b c d e f
  0 [a b c d e f a b c d e f ]
  1 [a b c d e f ]
  2 [b c d e f ]
  3 [c d e f ]
  4 [d e f ]
  5 [e f ]
  6 [f ]
  7 []
2 1 2 3 4 5
  0 [1 2 3 4 5 1 2 3 4 5 ]
  1 [1 2 3 4 5 ]
  2 [2 3 4 5 ]
  3 [3 4 5 ]
  4 [4 5 ]
  5 [5 ]
  6 []
3 
  0 [ ]
  1 []
4 a
  0 [a a ]
  1 [a ]
  2 []
5 b
  0 [b b ]
  1 [b ]
  2 []
6 
  0 [ ]
  1 []
#+end_example

*** Date Formatter

**** Write a function to convert American dates (11/02/23) to ISO (2023-02-11)

#+name: steps
#+begin_quote
6/24/23 9342
6/25/23 4493
6/26/23 4924
6/27/23 16611
6/28/23 8762
6/29/23 15370
6/30/23 17897
7/1/23  6087
7/2/23  7595
7/3/23  14347
7/4/23  15762
7/5/23  20021
#+end_quote

#+begin_src awk :results output :wrap example :exports both :stdin steps
  function datefix(s, arr) {
    split( s, arr, "/")
    return sprintf( "20%02d-%02d-%02d", arr[3],arr[1],arr[2])
  }
  {print datefix($1)}
#+end_src

#+RESULTS:
#+begin_example
2023-06-24
2023-06-25
2023-06-26
2023-06-27
2023-06-28
2023-06-29
2023-06-30
2023-07-01
2023-07-02
2023-07-03
2023-07-04
2023-07-05
#+end_example

**** Write an inverted split, which makes split words keys, and positions values

#+begin_src awk :results output :wrap example :exports both
  function isplit( str, arr,   n,tmp,i ) {
    n = split(str, tmp)
    for( i in tmp ) arr[tmp[i]]=i
    return n
  }
  BEGIN{
    s = "Jan Feb Mar Apr May Jun Jul Aug Sep Oct Nov Dec"
    isplit(s, arr)
    for (idx in arr)
      print idx, "=", arr[idx]
  }
#+end_src

#+RESULTS:
#+begin_example
Feb = 2
Sep = 9
May = 5
Apr = 4
Jan = 1
Dec = 12
Nov = 11
Jul = 7
Mar = 3
Oct = 10
Aug = 8
Jun = 6
#+end_example

** Exploratory Data Analysis

*** The Sinking of the Titanic

A-ha, that's where the Kaggle's tutorial is coming from.

#+name: titanic.tsv
#+begin_quote
Type	Class	Total	Lived	Died
Male	First	175	57	118
Male	Second	168	14	154
Male	Third	462	75	387
Male	Crew	885	192	693
Female	First	144	140	4
Female	Second	93	80	13
Female	Third	165	76	89
Female	Crew	23	20	3
Child	First	6	5	1
Child	Second	24	24	0
Child	Third	79	27	52
#+end_quote

**** Print all lines where Lived+Died != Total

#+begin_src awk :results output :wrap example :exports both :stdin titanic.tsv
$4+$5 != $3 {print}
#+end_src

#+RESULTS:
#+begin_example
Type	Class	Total	Lived	Died
#+end_example

**** Print how many males, females, and children were there, as well as classes

#+begin_src awk :results output :wrap example :exports both :stdin titanic.tsv
    NR>1 {gender[$1]+=$3 ; class[$2]+=$3}
    END {
      for (i in gender) print i, gender[i]
      print ""
      for (i in class) print i, class[i]
    }
#+end_src

#+RESULTS:
#+begin_example
Female 425
Child 109
Male 1690

Third 706
First 325
Crew 908
Second 285
#+end_example

**** Sort the dataset titanic.tsv according to survival rate

#+begin_src awk :results output :wrap example :exports both :stdin titanic.tsv
   NR > 1{set[$4/$3]=$0}

  END{for(i in set) print set[i]
    print ""
  asorti(set, sorted)
  for(i in sorted) print set[sorted[i]] "\t" int(sorted[i]*100) "%"}
#+end_src

#+RESULTS:
#+begin_example
Female	Crew	23	20	3
Male	First	175	57	118
Male	Second	168	14	154
Female	First	144	140	4
Male	Crew	885	192	693
Child	Third	79	27	52
Female	Third	165	76	89
Male	Third	462	75	387
Child	Second	24	24	0
Female	Second	93	80	13
Child	First	6	5	1

Male	Second	168	14	154	8%
Male	Third	462	75	387	16%
Male	Crew	885	192	693	21%
Male	First	175	57	118	32%
Child	Third	79	27	52	34%
Female	Third	165	76	89	46%
Child	First	6	5	1	83%
Female	Second	93	80	13	86%
Female	Crew	23	20	3	86%
Female	First	144	140	4	97%
Child	Second	24	24	0	100%
#+end_example

My data is not the same file, because I didn't find the original file, but it might work.

#+begin_src shell :exports both :results output  :wrap example
  awk 'NR==11 {print}' ./passengers.csv
  awk --csv '{print NF}' ./passengers.csv | sort | uniq -c | sort -nr
#+end_src

#+RESULTS:
#+begin_example
11,1st,0,"Astor, Colonel John Jacob",47.0,Cherbourg,"New York, NY",,17754 L224 10s 6d,(124),male
   1313 11
#+end_example


**** Print the first four survivors as tab-separated file

#+begin_src shell :exports both :results output :wrap example
awk --csv 'NR < 6 && NR > 1 { OFS="\t"; print $2, $3, $4, $5, $11 }' ./passengers.csv
#+end_src

#+RESULTS:
#+begin_example
1st	0	Allison, Miss Helen Loraine	2.0	female
1st	0	Allison, Mr Hudson Joshua Creighton	30.0	male
1st	0	Allison, Mrs Hudson J.C. (Bessie Waldo Daniels)	25.0	female
1st	1	Allison, Master Hudson Trevor	0.9167	male
#+end_example

**** Find how many babies, aged <1 are in the dataset

#+begin_src shell :exports both :results output :wrap example
awk --csv '$5<1 && $5!="" { OFS="\t"; print }' ./passengers.csv
#+end_src

#+RESULTS:
#+begin_example
5,1st,1,"Allison, Master Hudson Trevor",0.9167,Southampton,"Montreal, PQ / Chesterville, ON",C22,,11,male
359,2nd,1,"Caldwell, Master Alden Gates",0.8333,Southampton,"Bangkok, Thailand / Roseville, IL",,,13,male
545,2nd,1,"Richards, Master George Sidney",0.8333,Southampton,"Cornwall / Akron, OH",,,4,male
617,3rd,1,"Aks, Master Philip",0.8333,Southampton,"London, England Norfolk, VA",,392091,11,male
752,3rd,0,"Danbom, Master Gilbert Sigvard Emanuel",0.3333,Southampton,"Stanton, IA",,,,male
764,3rd,1,"Dean, Miss Elizabeth Gladys (Millvena)",0.1667,Southampton,"Devon, England Wichita, KS",,,12,female
#+end_example

Okay, my results are slightly different from the book, because some data is different.

*** Beer Ratings

The dataset is huge and requires registration

https://www.kaggle.com/datasets/rdoume/beerreviews?resource=download


**** Compare word sizes and character sizes returned by gawk and wc

#+begin_src shell :exports both :results output :wrap example
  /usr/bin/time -p wc ./beer_reviews.csv 2>&1
  /usr/bin/time -p awk '{ nc += length($0) + 1; nw += NF }END { print NR, nw, nc, FILENAME }' ./beer_reviews.csv 2>&1
  head -n 4 ./beer_reviews.csv
#+end_src

#+RESULTS:
#+begin_example
  1586615  12170550 180174429 ./beer_reviews.csv
real 0.39
user 0.38
sys 0.01
1586615 12170527 179963813 ./beer_reviews.csv
real 0.85
user 0.82
sys 0.02
brewery_id,brewery_name,review_time,review_overall,review_aroma,review_appearance,review_profilename,beer_style,review_palate,review_taste,beer_name,beer_abv,beer_beerid
10325,Vecchio Birraio,1234817823,1.5,2,2.5,stcules,Hefeweizen,1.5,1.5,Sausa Weizen,5,47986
10325,Vecchio Birraio,1235915097,3,2.5,3,stcules,English Strong Ale,3,3,Red Moon,6.2,48213
10325,Vecchio Birraio,1235916604,3,2.5,3,stcules,Foreign / Export Stout,3,3,Black Horse Black Beer,6.5,48215
#+end_example

Okayish.

**** Print the beer with maximum ABV

#+begin_src awk :exports both :results output :wrap example :in-file rev.tsv
  BEGIN {FS="\t"}
  NR >1 && $5 > maxabv { maxabv = $5; brewery=$1 ; name=$4 }
  END { print "maxabv="maxabv , "brewery="brewery , "name=" name }
#+end_src

#+RESULTS:
#+begin_example
maxabv=57.7 brewery=Schorschbräu name=Schorschbräu Schorschbock 57%
#+end_example

**** TODO Do some more beer analysis

*** Grouping Data

Some stuff on arrays

*** Visual analysis

Generate gnuplot, etc...

** Data Processing

**** TODO Write a program which sums the columns

Boring and I have already done that

**** Adding commas to a number

Attention to the "&".

#+begin_src awk :results output :wrap example :exports both 
  function addcomma(x, num) {
    if (x < 0)
      return "-" addcomma(-x)
    num = sprintf("%.2f", x)
    # num is dddddd.dd
    while (num ~ /[0-9][0-9][0-9][0-9]/)
      sub(/[0-9][0-9][0-9][,.]/, ",&", num)
    return num
  }
  BEGIN{ number = "121334523453425.34"
    print addcomma(number)
  }
#+end_src

#+RESULTS:
#+begin_example
121,334,523,453,425.34
#+end_example

*** Fixed-Field Input

**** Write a program which parses ~ls~ output, printing the last file name

#+name: lwf-ls
#+begin_src shell :exports both :results output :wrap example
ls --time-style=full-iso -lh | tr '-' ' ' 
#+end_src

#+RESULTS: lwf-ls
#+begin_example
total 281M
 rw r  r   1 lockywolf users  32K 2025 02 18 14:57:48.137481224 +0800 2021 12 06_awk index.org
 rw r  r   1 lockywolf users 172M 2019 10 07 16:32:12.000000000 +0800 beer_reviews.csv
 rw r  r   1 lockywolf users  91K 2025 02 18 13:22:34.591810459 +0800 passengers.csv
 rw r  r   1 lockywolf users 108M 2022 06 24 02:17:33.000000000 +0800 rev.tsv
 rw r  r   1 lockywolf users  30K 2025 02 17 21:25:42.925579463 +0800 stock.html
 rw r  r   1 lockywolf users  29K 2025 02 18 12:04:57.377840488 +0800 titanic_test.csv
 rw r  r   1 lockywolf users  68K 2025 02 18 12:05:01.360867634 +0800 titanic_train.csv
#+end_example

I have crafted a specific output with spaces in file names.

#+begin_src awk :results output :wrap example :exports both :stdin lwf-ls
{print substr($0, index($0,$13))}
#+end_src

#+RESULTS:
#+begin_example
total 281M
2021 12 06_awk index.org
beer_reviews.csv
passengers.csv
rev.tsv
stock.html
titanic_test.csv
titanic_train.csv
#+end_example

**** TODO Analyse the output of ~nm~ with AWK

This exercise failed for me, because my ~nm\sim prints files in a different format.

**** TODO Check if file has valid paired delimiters

**** Print all multiline records, which mention New York

#+name: multiline-records
#+begin_quote
Adam Smith
1234 Wall St., Apt. 5C
New York, NY 10021
212 555-4321

Donald E. Knuth
The Art of Computer Programming
Volume 4B: Combinatorial Algorithms, Part 2
Addison-Wesley, Reading, Mass.
2022

Chateau Lafite Rothschild 1947
12 bottles at 12.95

David W. Copperfield
221 Dickens Lane
Monterey, CA 93940
408 555-0041
work phone 408 555-6532
birthday February 2

Canadian Consulate
466 Lexington Avenue, 20th Floor
New York, NY 10017
1-844-880-6519
#+end_quote

#+begin_src awk :results output :wrap example :exports both :stdin multiline-records
  BEGIN{RS="" ; ORS="\n\n" }
  /New York/ {print}
#+end_src

#+RESULTS:
#+begin_example
Adam Smith
1234 Wall St., Apt. 5C
New York, NY 10021
212 555-4321

Canadian Consulate
466 Lexington Avenue, 20th Floor
New York, NY 10017
1-844-880-6519

#+end_example

**** Print Smith's phone

#+begin_src awk :results output :wrap example :exports both :stdin multiline-records
  BEGIN{RS="" ; ORS="\n\n" ; FS="\n"}
  $1 ~ /Smith$/  {print $1, $4}
#+end_src

#+RESULTS:
#+begin_example
Adam Smith 212 555-4321

#+end_example

**** Learn range patterns, print Smith's record without resorting to RS

#+begin_src awk :results output :wrap example :exports both :stdin multiline-records
  /^Adam Smith/ {p=1}
  p==1
  /^$/ {p=0}
#+end_src

#+RESULTS:
#+begin_example
Adam Smith
1234 Wall St., Apt. 5C
New York, NY 10021
212 555-4321

#+end_example

** Reports and Databases

(This is why Larry Wall wrote Perl.)

#+name: countries
#+begin_quote
Russia	16376	145	Europe
China	9388	1411	Asia
USA	9147	331	North America
Brazil	8358	212	South America
India	2973	1380	Asia
Mexico	1943	128	North America
Indonesia	1811	273	Asia
Ethiopia	1100	114	Africa
Nigeria	910	206	Africa
Pakistan	770	220	Asia
Japan	364	126	Asia
Bangladesh	130	164	Asia
#+end_quote

**** TODO Some boring things

#+begin_src awk :results output :wrap example :exports both :stdin countries :eval no

#+end_src

**** Try and test the Q query language

#+begin_src awk :results output :wrap example :exports both

#+end_src


** Processing words

Okay, this chapter has a lot of easy examples of generating random sentences, and one fairly big example of parsing  grammar and generating a random text matching this grammar.

It is an interesting read, but not really teaching a lot about awk's own usage.
The "troff" re-formatter is fun though.

** Little Languages

Okay, I mostly just read through this paper.
It is nice, but it is not going to teach you a lot about AWK itself.

