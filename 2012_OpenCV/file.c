#include< stdlib.h >
#include< stdbool.h >

void write_file( bool inuse );
void* opencv_read_image();
bool viola_jones( void* );

int main( int argc, char** argv)
{
//process_file();
if( camera_in_use() )
{ 
	write_file( true );
	return(0);
}
else
{
	img = opencv_read_image();
	result = viola_jones( img );
	write_file( &result );
}

return(0);
}
