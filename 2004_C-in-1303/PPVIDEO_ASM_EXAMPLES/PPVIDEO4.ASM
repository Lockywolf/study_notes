; PPVideo4.asm

code	segment
	assume	cs:code, ds:code
	org	100h
start:
	jmp	begin

;==========================================================================
;			       SUBROUTINE
;==========================================================================
counter	db	0
direction db	0
rate	db	0

Int1C:
	inc	cs:rate
	cmp	cs:rate,72	; ��⨢����㥬�� �� ����� 72 ⨪ ⠩���
	jne	go_iret

	mov	cs:rate,0

	push	ax
        push	dx
        push	es

	mov	dx,3C4h
	mov	al,1
	out	dx,al

	inc	dx

	cmp	byte ptr cs:direction,0FFh
	je	decrement
; ---   㢥��稬 ���稪
	inc	byte ptr cs:counter
	cmp	byte ptr cs:counter,1h
check:
	jne	out_port
	not	byte ptr cs:direction
	jmp	short out_port
decrement:
	dec	byte ptr cs:counter
	
	cmp	byte ptr cs:counter,00h
	jmp	short check

out_port:
	mov	al,byte ptr cs:counter
	out	dx,al
        pop	es
        pop	dx
        pop	ax
go_iret:
	iret

begin:  
	mov	ah,9
	mov	dx,offset msg
	int	21h

	xor	ax,ax
	int	16h

	mov	ax,251Ch
	mov	dx, offset Int1C
	int	21h

	mov	dx, offset begin
	int	27h

msg	db	'���������� ���������⥩ �����������',13,10
	db	'����� ������⢮ �祪 �� ᨬ��� (8-9)',13,10
	db	'����: ����檨� ����਩  ���� �� 1993',13,10
	db	13,10,'������ ���� ������� ...',13,10,'$'

code	ends
	end	start