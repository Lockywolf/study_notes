#include <stdio.h>
#include <conio.h>
#include <stdlib.h>

int main(int argc, char *argv[])
{
  FILE *in, *out;
  int ncnt = 0, tcnt = 0;
  int i, j = 3;
  char c;

  if ( argc < 3 )
  {
    fprintf(stderr, "Incorrect number of arguments\n");
    puts("Press any key to exit");
    getch();
    return 1;
  }

  for(j = 2; argv[j][0] == '-'; j++)
  {
    if (argv[j][1] == 't')
    {
      for(i = 0; argv[j][i] != '\0'; i++)
      {
        tcnt = tcnt * 10 + argv[j][i] - '0';
      }
    } else
    if (argv[j][1] == 'n')
    {
        ncnt = ncnt * 10 + argv[j][i] - '0';
    }
  }

  if (ncnt == 0) ncnt = 1;
  if (tcnt == 0) tcnt = 1;

  in = fopen(argv[j], "r");
  if (in == NULL)
  {
    fprintf(stderr, "Coudn't open file for readnig\n");
    puts("Press any key to exit");
    return 2;
  }

  out = fopen(argv[j+1], "w");
  if (out == NULL)
  {
    fprintf(stderr, "Coudn't open file for writing\n");
    fclose(in);
    puts("Press any key to exit");
    getch();
    return 3;
  }

  while (!feof(in))
  {
    c = fgetc(in);
    if (c != '\\')
      fputc(c, out);
    else
    {
      c = fgetc(in);
      if (c == 'n') for(j = 0; j < ncnt; j++) fputc('\n', out); else
      if (c == 't') for(j = 0; j < tcnt; j++) fputc( ' ', out); else
      if (c == '\\')for(j = 0; j < ncnt; j++) fputc('\\', out); else
                              {fputc('\\', out); fputc(c, out); }
    }
  }                                                             

  fclose(in);
  fclose(out);
  puts("Executed successfully");
  puts("Press any key to exit");
  getch();

  return 0;
}
