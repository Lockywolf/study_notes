#include <stdio.h>
#include <string.h>
#include <stdlib.h>

int main (const char argc, const char** argv)
{
	int t=0,n=0,c=0;
	int a,b,scount,sym,p=0,foundstr=0,o=0,size=100;
	char *strs,*strch;
	FILE *file;

	strch = (char*)malloc(sizeof(char)*size);

	for (a=1; a<argc; a++)
	{
	   if (argv[a][0]=='-')
		switch (argv[a][1])
		{
		 case 't': {t=1; printf("t\n"); break;}
		 case 'n': {n=1; printf("n\n"); break;}
		 case 'c': {c=1; printf("c\n"); break;}
		 default: {printf("Error 1: wrong option (-%c)\n\n\n",argv[a][1]); return 1;}
		}
	   else
		{
		if(!(file=fopen(argv[a],"r"))) {printf("Error 2: Oshibka otkritiya faila %s\n\n\n",argv[a]); return 2;}
		else{printf("File name: %s\n",argv[a]);}
		a++;

		strs = (char*)malloc(sizeof(char)*strlen(argv[a]));
		for(b=0; argv[a][b]!='\0'; b++)
		   strs[b] = argv[a][b];
		strs[b] = '\0';
		printf("string:\n\t\"%s\"\n",strs);

		if ((a+1)!=argc) {printf("Error 3: Lishnie parametri (%s)\n\n\n",argv[a+1]); return 3;}
		}
	}

	sym=getc(file);
	for(scount=1; sym!=EOF; scount++)
		{

		o=0;
		while((sym!='\n') && (sym!=EOF))  /*zapominaem stroku*/
			{
			strch[o]=sym;
			o++;
			if(o==size)
			   {size=size*2; realloc(strch, size);}
			sym=getc(file);
			}
		strch[o]='\0';

		o=0;
		p=0;
		while(strch[o]!='\0')
			{
			while (strch[o]==strs[p])  /*ishem v stroke stroku strs*/
			   {
			   p++;
			   o++;
			   }

			if (p==strlen(strs)) /*esli sovpalo*/
			   {
			   if ((n==0) && (t==0) && (c==0)) {printf("Found\n\n\n"); return 0;}

			   if (n==1) printf("%d",scount);

			   if (t==1)
			      if (n==1) printf(": %s\n",strch);
			      else      printf("%s\n",strch);
			   else if (n==1) printf("\n");

			   foundstr++;
			   p=0;
			   break; /*kajdaya stroka uchitivaetsya tol'ko 1 raz*/
			   }
			else p=0;

			o++; /*obhod strch*/
			}

		sym=getc(file);
		}

	if (c==1) printf("#%d\n",foundstr);
	else if ((n==0) && (t==0)) printf("Not found\n"); /*esli net opcii, to syuda doidet, esli net iskomoi stroki*/

	printf("\n\n\n");
	fclose(file);
	return 0;
}