#include <iostream>
#include <fstream>
#include <memory.h>
#include <stdlib.h>
using namespace std;
#define DEBUGBUILD 1
#define DEBUG if (DEBUGBUILD) cout
#include "BigNum.h"
	BigNum::BigNum():osn(10000)
	{
		DEBUG<<"START:BigNum::BigNum()//default ctor"<<endl;
		ar[0]=1;
		for(int i=1;i<MAXDIG;i++)
			ar[i]=0;
		DEBUG<<"END:BigNum::BigNum()//default ctor"<<endl;
	}
	BigNum::~BigNum()
	{
		DEBUG<<"BigNum::~BigNum()//default dtor"<<endl;
	}

	BigNum::BigNum(long val)
	{
		DEBUG<<"START:BigNum::BigNum(long val)//long ctor"<<endl;
		osn=10000;
		int i;
		for(i=1;i<MAXDIG;i++)
			ar[i]=0;
		for (i=1;val!=0;val/=osn,i++)
			ar[i]=val%osn;
		ar[0]=i-1;
		DEBUG<<"END:BigNum::BigNum(long val)//long ctor"<<endl;
	}

	void BigNum::clear()
	{
		DEBUG<<"START:void BigNum::clear()"<<endl;
		int i;
		for(i=1;i<MAXDIG;i++)
			ar[i]=0;
		ar[0]=1;
		DEBUG<<"END:void BigNum::clear()"<<endl;
	}

	BigNum::BigNum(const BigNum &Bign)
	{
		DEBUG<<"START:BigNum::BigNum(const BigNum &Bign)// copy ctor"<<endl;
		osn=Bign.osn;
		memcpy(ar,Bign.ar,sizeof(int)*(MAXDIG));
		DEBUG<<"END:BigNum::BigNum(const BigNum &Bign)// copy ctor"<<endl;
	}


	void BigNum::read(istream &f)
	{
		DEBUG<<"START:void BigNum::read(istream &f)"<<endl;
		char ch;int i;
		do
		f.read(&ch,1);
		while (ch<'0'||ch>'9');
		while(ch>='0'&&ch<='9')
		{
			for(i=ar[0];i>=1;i--)//������������� ������� ����� �� ar[i] � ������� �� ar[i+1]
			{	
				ar[i+1]+=(long(ar[i]*10)/osn);
				ar[i]=long(ar[i]*10)%osn;
			}
			ar[1]+=ch-'0';
			if(ar[ar[0]+1]>0)
			{
				ar[0]++;
				ar[ar[0]+1]=0;
			}
			f.read(&ch,1);
		}
		DEBUG<<"END:void BigNum::read(istream &f)"<<endl;
	}


	void BigNum::print(ostream &f) const
	{
		DEBUG<<"START:void BigNum::print(ostream &f)"<<endl;
		char *str;
		int numdig,os=osn/10;
		for(numdig=0;os>0;numdig++)
			os/=10;
		str=new char [numdig+1];
		f<<ar[ar[0]];
		for(int i=ar[0]-1;i>=1;i--)
		{
			itoa(ar[i],str,10);

			f.width(numdig);
			f.fill('0');
			f<<str;
		}
		f<<endl;
		delete [] str;
		DEBUG<<"END:void BigNum::print(ostream &f)"<<endl;
	}

	BigNum  BigNum::operator=(const BigNum & Bign)
	{
		DEBUG<<"START:BigNum BigNum::operator=(const BigNum & Bign)"<<endl;
		osn=Bign.osn;
		memcpy(ar,Bign.ar,sizeof(int)*(MAXDIG));
		DEBUG<<"END:BigNum  BigNum::operator=(const BigNum & Bign)"<<endl;
		return *this;
	}

	BigNum  BigNum::operator!(void) const
	{
		DEBUG<<"START:BigNum BigNum::operator!(void) const"<<endl;
		BigNum i;
		BigNum d;
		d.ar[1]=1;
		i.ar[1]=1;
		for(;i<=*this;i=i+1)
		{
			d=d*i;
			;
		}
		DEBUG<<"END:BigNum BigNum::operator!(void) const"<<endl;
		return d;
	}

	BigNum BigNum::operator+(int val) const
	{
		DEBUG<<"START:BigNum BigNum::operator+(int val) const"<<endl;
		//if(val>=osn)
		//{
		//	DEBUG<<"END:BigNum BigNum::operator+(int val) const"<<endl;
		//	return (*this)+BigNum(val);
		//}
		BigNum C;
		C=*this;
		C.ar[1]+=val;
		C.ar[2]=C.ar[1]/osn;
		C.ar[1]%=osn;
		if(C.ar[C.ar[0]+1]!=0)
			C.ar[0]++;
		return C;
		DEBUG<<"END:BigNum BigNum::operator+(int val) const"<<endl;
	}

	BigNum BigNum::operator+(BigNum & bn)const
	{
		DEBUG<<"START:BigNum BigNum::operator+(BigNum & bn)const"<<endl;
		int i,k;
		BigNum C;
		k=(ar[0]>bn.ar[0])?ar[0]:bn.ar[0];
		for(i=1;i<=k;i++)
		{
			C.ar[i+1]=(C.ar[i]+ar[i]+bn.ar[i])/osn;
			C.ar[i]=(C.ar[i]+ar[i]+bn.ar[i])%osn;
		}
		if(C.ar[k+1]==0)
			C.ar[0]=k;
		else
			C.ar[0]=k+1;
		DEBUG<<"END:BigNum BigNum::operator+(BigNum & bn)const"<<endl;
		return C;
	}

	BigNum BigNum::operator-(const  BigNum &bn)const
	{
		DEBUG<<"BigNum BigNum::operator-(const  BigNum &bn)const"<<endl;
		return (*this).sub(bn,0);
	}

	BigNum BigNum::sub(const  BigNum &bn,int sp) const
	{
		DEBUG<<"START:BigNum BigNum::sub(const  BigNum &bn,int sp) const"<<endl;
		int i;
		BigNum c;
		c=(*this);
		for(i=1;i<=bn.ar[0];i++)
		{
			c.ar[i+sp]-=bn.ar[i];
			for(int j=i;c.ar[j+sp]<0&&j<=c.ar[0];j++)
			{
				c.ar[j+sp]+=osn;
				c.ar[j+sp+1]--;
			}
		}
		for(i=c.ar[0];c.ar[i]==0&&i>1;i--)
			;
		c.ar[0]=i;
		DEBUG<<"END:BigNum BigNum::sub(const  BigNum &bn,int sp) const"<<endl;
		return c;
	}

	BigNum BigNum::operator*(const BigNum &bn)const
	{
		DEBUG<<"START:BigNum BigNum::operator*(const BigNum &bn)const"<<endl;
		int i,k;
		long dv;
		BigNum C;
		for(i=1;i<=ar[0];i++)
			for(k=1;k<=bn.ar[0];k++)
			{
				dv=ar[i]*bn.ar[k]+C.ar[i+k-1];
				C.ar[i+k]+=dv/osn;
				C.ar[i+k-1]=dv%osn;
			}
		C.ar[0]=ar[0]+bn.ar[0];
		for(;C.ar[0]>1&&C.ar[C.ar[0]]==0;C.ar[0]--)
			;
		DEBUG<<"END:BigNum BigNum::operator*(const BigNum &bn)const"<<endl;
		return C;
	}

/*	int FindBin(BigNum & ost,const  BigNum & b,int sp)
	{
		int up,down;
		BigNum c;
		down=0;up=osn;
		while(up-1>down)
		{
			c=((up+down)/2)*b;
			switch(c.more(ost,sp))
			{
				case 0:{down=(up+down)/2;break;}
				case 1:{up=(up+down)/2;break;}
				case 0:{down=up=(up+down)/2;break;}
			}


	BigNum BigNum::del(const  BigNum &,BigNum &)const
	{
		
*/
	BigNum BigNum::operator*(const  int k)const
	{
		DEBUG<<"START:BigNum BigNum::operator*(const  int k)const"<<endl;
		BigNum c;
		if(k==0)
		{
			DEBUG<<"END:BigNum BigNum::operator*(const  int k)const"<<endl;
			return c;
		}
		if(k>=osn)
		{
			DEBUG<<"END:BigNum BigNum::operator*(const  int k)const"<<endl;
			return (*this)*(BigNum(k));
		}
		for(int i=1;i<=ar[0];i++)
		{
			c.ar[i+1]=(ar[i]*k+c.ar[i])/osn;
			c.ar[i]=(ar[i]*k+c.ar[i])%osn;
		}
		c.ar[0]=ar[0]+(c.ar[ar[0]+1]>0);
		DEBUG<<"END:BigNum BigNum::operator*(const  int k)const"<<endl;
		return c;
	}

	long BigNum::operator[](int ind) const
	{
		DEBUG<<"long BigNum::operator[](int ind) const"<<endl;
		return ar[ind];
	}

	bool BigNum::operator==(const BigNum & bn)const
	{
		DEBUG<<"START:bool BigNum::operator==(const BigNum & bn)const"<<endl;
		if(ar[0]!=bn.ar[0])
		{
			DEBUG<<"END:bool BigNum::operator==(const BigNum & bn)const"<<endl;
			return false;
		}
		int i;
		for(i=1;i<=ar[0]&&ar[i]==bn.ar[i];i++)
			;
		DEBUG<<"END:bool BigNum::operator==(const BigNum & bn)const"<<endl;
		return(i==ar[0]+1);
	}

	bool BigNum::operator>(const BigNum & bn)const
	{
		DEBUG<<"START:bool BigNum::operator>(const BigNum & bn)const"<<endl;
		if(ar[0]<bn.ar[0])
		{
			DEBUG<<"END:bool BigNum::operator>(const BigNum & bn)const"<<endl;
			return false;
		}
		int i;
		for(i=ar[0];i>0&&ar[i]==bn.ar[i];i--)
			;
		if (i==0)
		{
			DEBUG<<"END:bool BigNum::operator>(const BigNum & bn)const"<<endl;
			return false;
		}
		if (ar[i]>bn.ar[i])
		{
			DEBUG<<"END:bool BigNum::operator>(const BigNum & bn)const"<<endl;
			return true;
		}
		DEBUG<<"END:bool BigNum::operator>(const BigNum & bn)const"<<endl;
		return false;
	}

	bool BigNum::operator>=(const BigNum & bn)const
	{
		DEBUG<<"START:bool BigNum::operator>=(const BigNum & bn)const"<<endl;
		if(ar[0]<bn.ar[0])
		{
			DEBUG<<"END:bool BigNum::operator>=(const BigNum & bn)const"<<endl;
			return false;
		}
                int i;
		for(i=ar[0];i>0&&ar[i]==bn.ar[i];i--)
			;
		if (i==0)
		{
			DEBUG<<"END:bool BigNum::operator>=(const BigNum & bn)const"<<endl;
			return true;
		}
		if (ar[i]>bn.ar[i])
		{
			DEBUG<<"END:bool BigNum::operator>=(const BigNum & bn)const"<<endl;
			return true;
		}
		DEBUG<<"END:bool BigNum::operator>=(const BigNum & bn)const"<<endl;
		return false;
	}

	bool BigNum::operator<(const BigNum & bn)const
	{
		DEBUG<<"bool BigNum::operator<(const BigNum & bn)const"<<endl;
		return !(this->operator>=(bn));
	}



	bool BigNum::operator<=(const BigNum & bn)const
	{
		DEBUG<<"bool BigNum::operator<=(const BigNum & bn)const"<<endl;
		return !(this->operator>(bn));
	}
	
	bool BigNum::operator!=(const BigNum & bn)const
	{
		DEBUG<<"bool BigNum::operator!=(const BigNum & bn)const"<<endl;
		return !(this->operator==(bn));
	}
	
	short BigNum::more(const BigNum & bn,int sdvig) const
	{
		DEBUG<<"START:short BigNum::more(const BigNum & bn,int sdvig) const"<<endl;
		if (ar[0]>(bn.ar[0]+sdvig))
		{
			DEBUG<<"END:short BigNum::more(const BigNum & bn,int sdvig) const"<<endl;
			return 0;
		}
		if(ar[0]<(bn.ar[0]+sdvig))
		{
			DEBUG<<"END:short BigNum::more(const BigNum & bn,int sdvig) const"<<endl;
			return 1;
		}
                int i;
		for(i=ar[0];i>sdvig&&ar[i]==bn.ar[i-sdvig];i--)
			;
		if(i==sdvig)
		{
			DEBUG<<"END:short BigNum::more(const BigNum & bn,int sdvig) const"<<endl;
			for(i=1;i<=sdvig;i++)
				if(ar[i]>0)
					return 0;
			DEBUG<<"END:short BigNum::more(const BigNum & bn,int sdvig) const"<<endl;
			return 2;
		}
		else
			DEBUG<<"END:short BigNum::more(const BigNum & bn,int sdvig) const"<<endl;
			return (ar[i]<bn.ar[i-sdvig]);
	}



		