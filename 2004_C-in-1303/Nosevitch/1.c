#include <stdio.h>

void shift(int *i1,int *i2,int *i3)
{
	int temp;
	temp=*i1;
	*i1=*i3;
	*i3=*i2;
	*i2=temp;
}

int main()
{
	int i,ai[3];
	for(i=0;i<3;i++){
		printf("Vvedite %d chislo:",i+1);
		scanf("%d",&(ai[i]));
	}
	shift(&(ai[0]),&(ai[1]),&(ai[2]));
	for(i=0;i<3;i++)
		printf("%d\n",ai[i]);
	return 0;
}
	