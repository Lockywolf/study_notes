#include <io.h>
#include <fcntl.h>
#define SIZE 65536
int Buf_p=SIZE;
char Buffer[SIZE];
int Size=SIZE;
int read_f(int fd, void * buf, unsigned n)
{
	char *temp=(char*)buf;
        if(Buf_p==Size&&Size<SIZE)
                return -1;
	int count;
	unsigned int i=0;
	while(i!=n)
	{
		if(Size-Buf_p>0)
			temp[i++]=Buffer[Buf_p++];
		else
		{
			if(SIZE==Size)
			{
				if((count=read(fd,Buffer, SIZE))<0)
					return count;
				else
				{
					Buf_p=0;
                                        Size=count;
				}
			}
			else
				return Size;
		}
	}
	return 0;	
}

int get_c(int fd, void * buf)
{
	return read_f(fd,buf,1);
}

char WRBUF[SIZE];
int WRBuf_p=0;
int write_f(int fd,const void* buf, int n)
{
        char *temp=(char*)buf;
	int i=0;
        while(i<n)
        {
                if(WRBuf_p==SIZE)
                {
                     WRBuf_p=0;
                     write(fd, WRBUF, SIZE);
                }
                WRBUF[WRBuf_p++]=temp[i++];
        }
       return 0;
}

void put_c(int fd, char c)
{
        write_f(fd, &c, 1);
}

void f_flush(int fd)
{
        write(fd, WRBUF, WRBuf_p);
        WRBuf_p=0;
}
