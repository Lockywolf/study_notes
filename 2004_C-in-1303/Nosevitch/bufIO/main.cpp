#include <stdio.h>
#include <errno.h>
#include <time.h>
#include "BUF_IO.h"

char table[256];
void init()
{
	int c;
	for(c=0;c<256;c++)
	{
		if(c<='z'&&c>='a')
			table[c]=c+'A'-'a';
		else
			table[c]=c;
	}
}

int main(int argc, char** argv)
{
        init();
	int fdin=open(argv[1], O_RDONLY|O_BINARY);
        int fdout=open(argv[2], O_WRONLY|O_TRUNC|O_BINARY);
        perror("������ :");
	int c;
        clock_t end, beg=clock();
        while(get_c(fdin, (void*)(&c))!=EOF)
        {
                if(c<='z'&&c>='a')
                        c-='A'-'a';
                put_c(fdout,c);
        }
        f_flush(fdout);
        end=clock();
        printf("%d\n%lf", end-beg,(double(end-beg))/CLOCKS_PER_SEC);
        getchar();
}
