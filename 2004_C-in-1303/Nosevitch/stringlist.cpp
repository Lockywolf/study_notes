#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#define BUF_SIZE 1001

int size;
char * s;

typedef struct Item{
	char * str;
	int len;
	struct Item * next;
} Item;

typedef Item * List;

Item * item_init(char * str,Item* next,int len)
{
	Item * item;
	item=(List)malloc(sizeof(item));
	item->str=str;
	item->len=len;
	item->next=next;

	return item;
}

void append_front(List *list,char * str,int len)
{
	Item* item=item_init(str,*list, len);
	(*list)=item;
}

List append_after(List list,char * str,int len)
{
	Item* item=item_init(str,NULL, len);
	(list)->next=item;
	return item;
}

int getline(FILE * f) /* get line into s, return length */
{
	int c, i;
	for(i=0;(c=getc(f))!=EOF && c!='\n'; ++i)
	{
		if (i>size)
		{
			size*=2;
			s=(char *)realloc(s,sizeof(char)*(size+1));
		}
		s[i] = c;
	}
	s[i] = '\0';
	return i;
}
int input(FILE * f,List* l)
{

	char * str1;
	int max=0;
	List l1=*l;
	int len;
	size=BUF_SIZE;
	s=(char *)malloc(sizeof(char)*BUF_SIZE);
	while((len=getline(f))!=0)
	{
		if(len>max)
			max=len;
		str1=(char*)malloc(sizeof(char)*(len+1));
		strcpy(str1,s);

		if(l1==NULL)
		{
			append_front(l,str1,len);
			l1=*l;
		}
		else
			l1=append_after(l1,str1,len);
	}
	return max;
}


void print(FILE * f ,List l)
{
	List item=l;
	fprintf(f,"%s",item->str);
	for(item=item->next;(item->next)!=0;item=item->next)
		fprintf(f,"%s",item->str);
	fprintf(f,"%s",item->str);
}

int main(int argc, char **argv)
{
	List l=0;int max;
	if (argc!=4)
		return 1;
	char ch=*argv[3];
	FILE * in=fopen(argv[1],"r"),*out=fopen(argv[2],"w");

	max=input(in,&l);
	for(int i=0;i<max;i++)
	{
		List item=l;
		for(;item!=0;item=item->next)
			putc (((i<item->len)?item->str[i]:ch),out);
		putc('\n',out);
	}
	return 0;
}
