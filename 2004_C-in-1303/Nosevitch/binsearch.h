unsigned long BinSearch(int * array, int key, unsigned long n)
{
        if(array[n/2]<key)
                return BinSearch( (array+(n/2+1)), key,n-(n/2+1));
        if(array[n/2]>key)
                return BinSearch( (array), key,n/2);
        else
                return n/2;
}

