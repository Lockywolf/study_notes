//sort.cpp
#include <iostream>
#include <memory.h>
using namespace std;
#include "sorting.h"
#include <conio.h>

//1. BubbleSort - ���������� ���������.
//2. InsertionSort - ���������� ���������.
//3. InsertionSort with guarding element - ���������� ��������� �� ���������� ���������.
//4. SelectSort - ���������� �������.
//5. ShakerSort - ���������� ��������� ������������
//6. ShellSort  - ���������� �����.
int main(int argc, char ** argv)
{
	int n;
	long seed;
	float *ar[7];
	cin>>n;
	for(int i=0;i<7;i++)
		ar[i]=new float[n];
	if (argv[1][0]=='r')
	{
		cin>>seed;
		generate(ar[0],n,seed);
	}
	else if(argv[1][0]=='a')
	{
		asc(ar[0],n);
	}
	else if(argv[1][0]=='d')
	{
		desc(ar[0],n);
	}
	else
		return -1;
	for(int i=1;i<7;i++)
		memcpy(ar[i],ar[0],sizeof(float)*n);
	BubbleSort(ar[0],n);
	InsertionSort(ar[1],n);
	ShakerSort(ar[2],n);
	SelectSort(ar[3],n);
	ShellSort(ar[5],n);
	MergeSort(ar[4],n);
	QSort(ar[0],n);
        getch();
	return 0;
}
