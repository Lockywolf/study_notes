//shell.h
////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////
	
//�������� ���������� ���������� �������� - Sedgewick increment alogorithm
long increment(long inc[], long size) 
{
	long p1, p2, p3, s;
	p1=p2=p3=1;
	s=-1;
	do 
	{
		if(++s%2) 
		{
			inc[s]=8*p1-6*p2+1;
		} 
		else 
		{
			inc[s]=9*p1-9*p3+1;
			p2*=2;
			p3*=2;
		}
		p1*=2;
	} 
	while(3*inc[s]<size);  
	return s>0?--s:0;
}

//6.ShellSort  - ���������� �����.
void ShellSort(void * ar, size_t size,unsigned long n, int (*compare)(const void* ,const void*) )
{
	long inc, i, j, seq[40];
	char * array=(char*) ar;
	void * temp=malloc(size);
	DEB(ass=comp=loop=0);
	long s;
	// ���������� ������������������ ����������
	s = increment(seq, n);
	while (s >= 0) 
	{
		// ���������� ��������� � ������������ inc[] 
		inc = seq[s--];
		for (i = inc*size; i < n*size; i+=size) 
		{
			memcpy((void *)temp,(void *)(array+i),size);
			DEB(ass++);
			for (j = i-inc*size; (j >= 0) && (compare((void *) (array+j),temp)>0); j -= inc*size)
			{
				memcpy((void *)(array+(j+size*inc)),(void *)(array+j),size);
				DEB(comp++);
				DEB(ass++);
			}
			memcpy((void *)(array+(j+size*inc)),temp,size);
			DEB(ass++);
			DEB(loop++);
		}
	}
}

void ShellSortINT(int * ar,long n)
{
	ShellSort(ar,sizeof(int),n,compINT);
#ifdef DEBUGBUILD
	cout<<"      ShellSort        "<<endl
		<<"Number of assignments: "<<ass<<endl
		<<"Number of comparisons: "<<comp<<endl
		<<"Number of loops      : "<<loop<<endl;
#endif // #ifdef DEBUGBUILD
#ifdef PRINT
	printINT(ar,n);
#endif //#ifdef PRINT
}