long BinSearch(const void* ar, size_t size,unsigned long n, const void * key,int (*compare)(const void* ,const void*))
{
	long lb=0;
	long rb=n;
	char *a=(char *)ar;
	while(1)
	{
		long temp=(rb+lb)/2;
		if(     compare(key, (void *)(a+temp*size))>0)
			lb=temp+1;
		else if(compare(key, (void *)(a+temp*size))<0)
			rb=temp-1;
		else 
			return temp;
		if(lb > rb) 
			return -1;
	}
}

int compStrIntKey(const void * key, const void * elem)
{
	return strcmp((char*)(*(unsigned long *)key),((StrInt *)elem)->str);

}
long BinSearchINT(const int *ar,unsigned long n,int key)
{
	return BinSearch((void*)ar,sizeof(int),n,(void*) &key,compINT);
}

long BinSearchStrInt(const StrInt * a,unsigned long n, char * key)
{
	return BinSearch((void*)a,sizeof(StrInt),n,(void*) &key,compStrIntKey);
}