//select.h


////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////
//4. SelectSort - ���������� �������.
void SelectSort(void * ar, size_t size,unsigned long n, int (*compare)(const void* ,const void*))
{
	unsigned long i,k,j;
	DEB(ass=comp=loop=0);
	void * temp=malloc(size);
	char * array=(char*) ar;;
	for( i=0; i < n*size; i+=size) 
	{   	// i - ����� �������� ����
		k=i; 
		memcpy((void *)temp,(void *)(array+i),size);
		DEB(ass++);
		for( j=i+size; j < n*size; j+=size)	// ���� ������ ����������� ��������
		{
			DEB(comp++);
			if ( compare(array+j,temp)<0) 
			{
				k=j; 
				memcpy((void *)temp,(void *)(array+j),size);
				DEB(ass++);// k - ������ ����������� ��������
			}
		}
		memcpy((void *)(array+k),(void *)(array+i),size);; 
		memcpy((void *)(array+i),(void *)temp     ,size);   	// ������ ������� ���������� � array[i]
		DEB(ass+=2);
		DEB(loop++);
	}
}
void SelectSortINT(int * ar,long n)
{
	SelectSort(ar,sizeof(int),n,compINT);
#ifdef DEBUGBUILD
	cout<<"       SelectSort       "<<endl
		<<"Number of assignments: "<<ass<<endl
		<<"Number of comparisons: "<<comp<<endl
		<<"Number of loops      : "<<loop<<endl;
#endif // #ifdef DEBUGBUILD
#ifdef PRINT
	printINT(ar,n);
#endif //#ifdef PRINT
}
