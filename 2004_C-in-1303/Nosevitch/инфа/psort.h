//psort.h
int getli(FILE * f,char *s)
{
	int c,i=0;
	while((c=getc(f))!='\"'&&c!=EOF)
		;
	while((c=getc(f))!='\"'&&c!=EOF)
		s[i++]=c;
	s[i]='\0';
	return c;
}


struct StrInt
{
	char* str;
	int num;
};

int StrIntKey(const void * a)
{
	return ((StrInt*) a)->num;
}

int ReadStrInt(FILE * f,StrInt* * ar, int k,int * n)
{
	int i=k;
	int c=1;
	char *temp=new char[256];
	while(c!=EOF)
	{
		(*ar)[i].str=new char[256];
		c=getli(f,temp);
		if (c==EOF)
			continue;
		strcpy((*ar)[i].str,temp);
		fscanf(f,"%d",&((*ar)[i].num));
		i++;
		if(i>=*n)
		{
			(*n)*=2;
			StrInt  * temp=(StrInt *)malloc(sizeof(StrInt)*(*n));
			memcpy(temp,*ar,sizeof(StrInt)*(*n));
			free(*ar);
			*ar=temp;
		}
	}
	return i-1;
}


int PocketSort(void* array, size_t size,unsigned long n,int (*key)(const void*))
{
	char * ar=(char*)array;
        char *temp=(char*)malloc(size);
        char *temp1=(char*)malloc(size);
        memcpy(temp,ar+size*key((void*)ar),size);
        memcpy( (void*)(ar+size*key((void*)ar)),(void *)ar,size);
	for(;;)
        {
                int ke=key((void*)temp);
                if(ke!=key( (void*)(ar+size*ke) ))
                {
                        memcpy((void*)temp1,(void*)(ar+size*key((void*)temp)),size);
		        memcpy( (void*)(ar+size*key((void*)temp)),(void*)temp,size);
                        memcpy((void*)temp,(void*)temp1,size);
                }
                else
                        return 0;
	}
}

int PocketSortINTSTR(StrInt* ar,unsigned long n)
{
	return PocketSort((void*) ar, sizeof(StrInt),n,StrIntKey);
}