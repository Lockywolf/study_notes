//merge.h
//8.���������� ��������.
void merge(char  *a,size_t size, long lb, long split, long rb,int (*compare)(const void* ,const void*) ) 
{
// ������� ������������� ������ ������� � ����� temp
// � ���������� ��������� ����������� temp � a[lb]...a[rb]

	split*=size;
	// ������� ������� ������ �� ������ ������������������ a[lb]...a[split]
	long pos1=lb*size;
	// ������� ������� ������ �� ������ ������������������ a[split+1]...a[rb]
	long pos2=split+size;
	// ������� ������� ������ � temp
	long pos3=0;  
	char  *temp = (char*)malloc(size*(rb-lb+1));
	// ���� �������, ���� ���� ���� ���� ������� � ������ ������������������
	while (pos1<=split&&pos2<=rb*size) 
	{
		if (compare((void *) (a+pos1),(void *) (a+pos2))<0)
		{
			memcpy((void*)(temp+pos3),(void*)(a+pos1),size);
			pos3+=size;
			pos1+=size;
		}
		else
		{
			memcpy((void*)(temp+pos3),(void*)(a+pos2),size);
			pos3+=size;
			pos2+=size;
		}
		DEB(comp++);
		DEB(ass++);
		DEB(loop++);
	}
	// ���� ������������������ ����������� - 
	// ���������� ������� ������ � ����� ������ 
	while (pos2<=rb*size)   // ���� ������ ������������������ ������� 
	{
		memcpy((void*)(temp+pos3),(void*)(a+pos2),size);
		pos3+=size;
		pos2+=size;
		DEB(ass++);
		DEB(loop++);
	}
	while (pos1 <= split)  // ���� ������ ������������������ �������
	{
		memcpy((void*)(temp+pos3),(void*)(a+pos1),size);
		pos3+=size;
		pos1+=size;
		DEB(ass++);
		DEB(loop++);
	}
	 // ����������� ����� temp � a[lb]...a[rb]
	memcpy((void *)(a+lb*size),temp,size*(rb-lb+1));
	DEB(ass+=rb-lb+1);
	free(temp);
}


void mergeSortEx(char *a, size_t size, long lb, long rb,int (*compare)(const void* ,const void*)) 
{ 
	long split;                   // ������, �� �������� ����� ������
	if(lb<rb) 
	{                // ���� ���� ����� 1 ��������
		split=(lb+rb)/2;
		mergeSortEx(a,size,lb,split,compare);       // ����������� ����� �������� 
		mergeSortEx(a,size,split+1,rb,compare);     // ����������� ������ �������� 
		merge(a,size,lb,split,rb,compare);          // ����� ���������� � ����� ������
	}
}
void MergeSort(void *a,size_t size, long n,int (*compare)(const void* ,const void*))
{
	DEB(ass=comp=loop=0);
	char * array=(char*) a;
	mergeSortEx(array ,size, 0, n-1,compare);
}

void MergeSortINT(int  *a, long n)
{
	MergeSort(a,sizeof(int),n,compINT);
#ifdef DEBUGBUILD
	cout<<"      MergeSort        "<<endl
		<<"Number of assignments: "<<ass<<endl
		<<"Number of comparisons: "<<comp<<endl
		<<"Number of loops      : "<<loop<<endl;
#endif // #ifdef DEBUGBUILD
#ifdef PRINT
	printINT(a,n);
#endif //#ifdef PRINT
}