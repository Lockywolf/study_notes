//bubble.h
////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////
//1. Bubble sort - ���������� ���������.
void BubbleSort(void * ar, size_t size,unsigned long n, int (*compare)(const void* ,const void*) )
{
	long i=n-1;
	long k;
	char * array=(char*) ar;
	DEB(ass=comp=loop=0);
	void * temp=malloc(size);
	bool flag=true;
	while(flag)
	{
		flag=false;
		for(k=0;k<i*size;k+=size)
			if(compare((void *) (array+k),(void *) (array+(k+size)))>0)
			{
				DEB(ass+=3);
				DEB(comp++);
				memcpy((void *)temp            ,(void *)(array+(k+size)),size);
				memcpy((void *)(array+(k+size)),(void *)(array+k)       ,size);
				memcpy((void *)(array+k)       ,(void *)temp            ,size);
				flag=true;
			}
		i--;
		DEB(loop++);
	}
}


void BubbleSortINT(int * ar,long n)
{
	BubbleSort(ar,sizeof(int),n,compINT);
#ifdef DEBUGBUILD
	cout<<"      BubbleSort       "<<endl
		<<"Number of assignments: "<<ass<<endl
		<<"Number of comparisons: "<<comp<<endl
		<<"Number of loops      : "<<loop<<endl;
#endif // #ifdef DEBUGBUILD
#ifdef PRINT
	printINT(ar,n);
#endif //#ifdef PRINT
}

////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////
	
//5. ShakerSort - ���������� ��������� ������������
void ShakerSort(float array[], unsigned long n) 
{
	unsigned long j, k = n-1;
	DEB(ass=comp=loop=0);
	unsigned long lb=1, rb = n-1; // ������� ����������������� ����� �������
	float temp;
	do 
	{// ������ ����� ����� 
		for( j=rb; j>0; j-- ) 
		{
			if ( array[j-1] > array[j] ) 
			{
				temp=array[j-1]; 
				array[j-1]=array[j]; 
				array[j]=temp;
				k=j;
				DEB(ass+=3);
			}
			DEB(comp++);
	    }
		lb = k+1;
		// ������ ������ ���� 
		for (j=1; j<=rb; j++) 
		{
			if ( array[j-1] > array[j] ) 
			{
				temp=array[j-1]; 
				array[j-1]=array[j]; 
				array[j]=temp;
				k=j;
				DEB(ass+=3);
			}
			DEB(comp++);
		}
		rb = k-1;
		DEB(loop++);
	}
	while ( lb < rb );

#ifdef DEBUGBUILD
	cout<<"      ShakerSort       "<<endl
		<<"Number of assignments: "<<ass<<endl
		<<"Number of comparisons: "<<comp<<endl
		<<"Number of loops      : "<<loop<<endl;
#endif // #ifdef DEBUGBUILD
#ifdef PRINT
	cout<<endl<<"{ ";
	for(long i=0;i<n;i++)
	{
		cout<<array[i]<<" ";
	}
	cout<<"}"<<endl<<endl;
#endif //#ifdef PRINT

}
