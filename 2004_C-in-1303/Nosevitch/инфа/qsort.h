//qsort.h
////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////

// 7. ������� ����������.
void QSortEx(char * array, size_t size,unsigned long N, int (*compare)(const void* ,const void*) )
{
// �� ����� - ������ a[], a[N] - ��� ��������� �������.
	long i=0,j=N*size; 		// ��������� ��������� �� �������� �����
	void * temp=malloc(size);
	void * p=malloc(size);
	memcpy(p,(void *)(array+(N>>1)*size),size);		// ����������� �������
	// ��������� ����������
	while ( i<=j )
	{
		while(compare( (void *) (array+i), p )<0) 
		{
			i+=size;
			DEB(comp++);
			DEB(loop++);
		}
		while(compare( (void *) (array+j), p )>0) 
		{
			j-=size;
			DEB(comp++);
			DEB(loop++);
		}
		if(i<=j) 
		{
			memcpy((void *)temp     ,(void *)(array+i),size);
			memcpy((void *)(array+i),(void *)(array+j),size);
			memcpy((void *)(array+j),(void *)temp     ,size);
			DEB(ass+=3);
			i+=size; 
			j-=size;
		}
	} 
	// ����������� ������, ���� ����, ��� ����������� 
	if (j>0) 
		QSortEx(array,size,j/size,compare);
	if (N*size>i) 
		QSortEx(array+i,size,N-i/size,compare);
}

void QSort(void* a,size_t size,unsigned long n, int (*compare)(const void* ,const void*))
{
	char * array=(char*) a;
	DEB(ass=comp=loop=0);
	QSortEx(array,size,n-1,compare);
}

void QSortINT(int * ar,long n)
{
	QSort(ar,sizeof(int),n,compINT);
#ifdef DEBUGBUILD
	cout<<"      QSort       "<<endl
		<<"Number of assignments: "<<ass<<endl
		<<"Number of comparisons: "<<comp<<endl
		<<"Number of loops      : "<<loop<<endl;
#endif // #ifdef DEBUGBUILD
#ifdef PRINT
		printINT(ar,n);
#endif //#ifdef PRINT
}

int compStrInt(const void *f,const void * s)
{
	return strcmp(((StrInt *)f)->str,((StrInt *)s)->str);
}

void QSortStrInt(StrInt  * ar,long n)
{
	QSort((void*)ar,sizeof(StrInt),n,compStrInt);
#ifdef DEBUGBUILD
	cout<<"      QSort       "<<endl
		<<"Number of assignments: "<<ass<<endl
		<<"Number of comparisons: "<<comp<<endl
		<<"Number of loops      : "<<loop<<endl;
#endif // #ifdef DEBUGBUILD
}

