#include <stdio.h>
#include <string.h>

int pos(char c, char *str)
{
	int i;
	for (i=0;i<=strlen(str);i++)
		if(str[i]==c)
			return i;
	return -1;
}
void null(char *str)
{
	int i;
	for(i=0;i<strlen(str);i++,str[i]='\0')
		;
}

void copy(int nfrom,int num, char *from,char *to)
{
	int i,k;
	null(to);
	for (k=0,i=nfrom;i<=nfrom+num;i++,k++)
		to[k]=from[i];
}


void parse(char *source,char *dest)
{
	int len,pos1;
	char *after, *before;
	dest=source;
	while((pos1=pos('#',dest))!=-1)
	{
		copy(0,pos1-1,dest,before);
		copy(pos1,strlen(dest)-pos1,dest,after);
		strcat(before,after);
		null(dest);
		dest=before;
	}
}

int main()
{
	char *source;
	char *dest;
	scanf("%s",source);
	parse(source,dest);
	printf("%s",dest);
	return 0;
}

