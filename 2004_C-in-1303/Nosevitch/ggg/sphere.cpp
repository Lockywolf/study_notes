#include <stdio.h>
#include <stdlib.h>
#include <math.h>

double do_min(double *x,double *y,double*z,int n, int ans[2])
{
	double min=sqrt((x[0]-x[1])*(x[0]-x[1])+(y[0]-y[1])*(y[0]-y[1])+(z[0]-z[1])*(z[0]-z[1]));
	ans[0]=0;
	ans[1]=1;
	double ro;
	for(int i=0;i<n;i++)
		for(int j=i+1;j<=n;j++)
		{
			ro=sqrt((x[i]-x[j])*(x[i]-x[j])+(y[i]-y[j])*(y[i]-y[j])+(z[i]-z[j])*(z[i]-z[j]));
			if(ro<=  min)
			{
				min=ro;
				ans[1]=i;
				ans[0]=j;
			}
		}

	return ro;
}

int main(int argc, char** argv)
{
	double *x,*y,*z;
	double alpha=0, beta=0, ro=0;
	int pos=0, ans[2];
	int size=32;
	if(argc!=2)
	{
		printf("Usage: %s <filename>", argv[0]);
		return 0;
	}
	FILE* f=fopen(argv[1],"r");
	x=(double *)malloc(sizeof(double)*size);
	y=(double *)malloc(sizeof(double)*size);
	z=(double *)malloc(sizeof(double)*size);
	while(fscanf(f,"%lf%lf%lf",&alpha, &beta, &ro)!=EOF)
	{
		x[pos]=ro*cos(alpha)*cos(beta);
		y[pos]=ro*sin(alpha)*cos(beta);
		z[pos]=ro*sin(alpha)*sin(beta);
		pos++;
		if(pos>size)
		{
			size*=2;
			x=(double *)realloc(x,sizeof(double)*size);
			y=(double *)realloc(y,sizeof(double)*size);
			z=(double *)realloc(z,sizeof(double)*size);
		}
	}
	double rez=do_min(x,y,z,pos-1,ans);
	printf("ro(%dth,%dth)= %lf",ans[0]+1,ans[1]+1,rez);
        getchar();
	return 0;
}

