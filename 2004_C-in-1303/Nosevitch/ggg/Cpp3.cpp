#include <stdio.h>
#include "chismet.h"

int main()
{
	double E=0.000000000001;
	double a=-1.5,b=-0.7;
	int count;
	double rez=poldel(fu,E,a,b,&count);
	printf("POLDEL:\nx=%2.20e, f(x)=%2.20e loops:%d\n", rez,fu(rez),count);
	rez=hord(fu,E,a,b,&count);
	printf("HORDS:\nx=%2.20e, f(x)=%2.20e loops:%d\n", rez,fu(rez),count);
	rez=Newton(fu,fup,E,a,b,&count);
	printf("Newton:\nx=%2.20e, f(x)=%2.20e loops:%d\n", rez,fu(rez),count);
	return 0;

}