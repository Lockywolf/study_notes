#!/usr/bin/csi
;; You need to use 
(import ck-macros)

;; (define-record-type <foo> 
;;   (make-foo bar baz) 
;;   foo?
;;   (bar foo-bar)
;;   (baz foo-baz))

;; (define-syntax c-struct
;;   (syntax-rules ()
;;     ((_ s t f1 f2)
;;      (ck s `(define-record-type t
;;               ,((c-string->symbol (c->string-append "make-" (c-symbol-string t))) f1 f2)
;;               ,(c-string->symbol (c->string-append  (c-symbol-string t) "?"))
;;               (f1 ,(c-string->symbol (c->string-append (c-symbol-string t) "-" (c-symbol-string f1))))
;;               (f2 ,(c-string->symbol (c->string-append (c-symbol-string t) "-" (c-symbol-string f2))))
;;              )))))

;; (define-syntax struct
;;   (syntax-rules ()
;;     ((_ . args)
;;      (ck () (c-quote (c-struct 'args))))))

(define-syntax c-struct
  (syntax-rules ()
    ((_ type field)
     (ck () (c-list 'define (c-list (quote type) (quote arg1)) `(list (quote field) arg1))))))

(display "hello") (newline)
(c-struct foo bar)
(display (foo 'test-arg))(newline)
(display (expand '(c-struct foo bar)))(newline)
