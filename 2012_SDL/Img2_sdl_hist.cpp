// Img2_sdl_hist.cpp : Defines the entry point for the console application.
//
#include "StdAfx.h"
#include <stdio.h>
#include "SDL.h"
#include "SDL_image.h"

int main(int argc, char* argv[])
{
	 if((SDL_Init(SDL_INIT_EVERYTHING)==-1)) 
	 { 
        printf("Could not initialize SDL: %s.\n", SDL_GetError());
        exit(-1);
    }
	 SDL_Surface *screen;
	screen = SDL_SetVideoMode(640, 480, 32, SDL_SWSURFACE|SDL_ANYFORMAT);
    if ( screen == NULL ) 
	{
        fprintf(stderr, "Couldn't set 640x480x8 video mode: %s\n",
                        SDL_GetError());
        exit(1);
    }


	SDL_Surface* image = IMG_Load("ScanImage001.tif");
	if(!image)
	{
    printf("IMG_Load: %s\n", IMG_GetError());
    // handle error
	}
	SDL_Rect temp;
	temp.x=0;
	temp.y=0;
	temp.w=600;
	temp.h=400;
	if(SDL_BlitSurface(image, NULL, screen, NULL) < 0)
        fprintf(stderr, "BlitSurface error: %s\n", SDL_GetError());

    SDL_UpdateRect(screen, 0, 0, screen->w, screen->h);
	//SDL_Delay( 2000 );
	//printf("hello, world\n");
	SDL_WM_SetCaption("Simple Window", "Simple Window");
	SDL_Event event;
	bool done=false;
	while(!done)
	{
	while(SDL_PollEvent(&event)) 
	{
		if (event.type == SDL_QUIT) 
		{
			done=true;
		}
	}
	}
	SDL_FreeSurface( image );
	SDL_FreeSurface( screen );
	SDL_Quit();
	return 0;
}

