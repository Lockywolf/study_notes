//#include "../000_common-unp/unp.h"
/*
#+ Time-stamp: <2022-12-04 18:55:11 lockywolf>
#+created: <2022-12-04 Sun 14:58>
 */
/*! I decided to have an individually generated unp.h for each case, because
  there are just too many functions in the default one.
  I need to study it step by step, and in order to do this, I will bypass
  autoconf and do things manually.
*/
#include "unp_001.h"
#include <arpa/inet.h>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <netinet/in.h> /* sockaddr_in{} and other Internet defns */
#include <unistd.h>

int main(int argc, char ** argv) {
  if (argc != 2)
    err_quit("usage: a.out <IPaddress>");

  int sockfd;
  int n;
  char recvline[MAXLINE + 1];

  struct sockaddr_in servaddr;
  bzero(&servaddr, sizeof(servaddr));
  servaddr.sin_family = AF_INET;
  servaddr.sin_port = htons(13); /* daytime server */ // LWF: wtf???
  if (inet_pton(AF_INET, argv[1], &servaddr.sin_addr) <= 0)
    err_quit("inet_pton error for %s", argv[1]);

  if ((sockfd = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP)) < 0) // 0 also works
    err_sys("socket error");

  if (connect(sockfd, (SA *)&servaddr, sizeof(servaddr)) < 0)
    err_sys("connect error");

  while ((n = read(sockfd, recvline, MAXLINE)) > 0) {
    recvline[n] = 0;
    /* null terminate */
    if (fputs(recvline, stdout) == EOF)
      err_sys("fputs error");
  }
  if (n < 0)
    err_sys("read error");
  exit(0);
}
