#ifndef UNP_001_H
#define UNP_001_H

/* Miscellaneous constants */
#define MAXLINE 4096 /* max text line length */
#define SA struct sockaddr

// extern "C" {
extern void err_quit(const char *, ...);
extern void err_sys(const char *, ...);
//}
#endif
