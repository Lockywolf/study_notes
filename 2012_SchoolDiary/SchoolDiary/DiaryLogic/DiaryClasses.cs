using System.Collections.Generic;
using System.Linq;


namespace DiaryLogic
{
    partial class Class
    {
        public IEnumerable<Student> GetStudentsList()
        {
            return this.Students.OrderBy(x => x.Name).ToList();
        }
    }
}
