﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DiaryLogic;

namespace UsingLogic
{
    class Program
    {
        static void Main(string[] args)
        {
            var dataContext = new DiaryClassesDataContext(Properties.Settings.Default.diaryDBConnectionString);
            if (!dataContext.DatabaseExists())
                dataContext.CreateDatabase();

            dataContext.Lessons.InsertOnSubmit(new Lesson() { LessonID = Guid.NewGuid().ToString() });
            dataContext.SubmitChanges();

            var myLesson = from l in dataContext.Lessons where l.Marks.Any() select l ;
        }
    }
}
