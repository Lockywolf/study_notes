﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Site.Master" Inherits="System.Web.Mvc.ViewPage<dynamic>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
	Создаем новый класс
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">

    <h2>Создаем новый класс</h2>
    <% using(Html.BeginForm()) {%>
    <p>
        Название класса: <%:Html.TextBox("ClassName")%>
    </p>
    <input type="submit" value="Создать"/>
    <%}%>
</asp:Content>
