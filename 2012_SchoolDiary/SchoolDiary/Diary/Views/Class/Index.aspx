﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Site.Master" Inherits="System.Web.Mvc.ViewPage<DiaryLogic.Class>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
    Index
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <script type="text/javascript">
    function diplay_form_for_add_students() {
        var d = document.getElementById("add_students_form");
        if (d.style.display == 'none')
            d.style.display = 'block';
        else
            d.style.display = 'none';
    }
    </script>
    <h2>
        Класс <%: Model.Title%></h2>
    <p>
        <h3>
            Ученики</h3>
        <ul>
            <% foreach (var student in Model.GetStudentsList())
               { %>
            <li>
                <%: Html.ActionLink("[х]", "RemoveStudent", "Class", new { studentID = student.StudentID}, null)%>
                <%:student.Name %></li>
            <% } %>
        </ul>
        <h3>
            <span style="border-bottom: 1px dotted gray; cursor: pointer;" onclick="diplay_form_for_add_students();">
                Добавить учеников </span>
        </h3>
        <div style="display: none" id="add_students_form">
            <%using (Html.BeginForm("AddStudents", "Class"))
              {%>
            <%: Html.HiddenFor(x => x.ClassId)%>
            <%: Html.TextArea("AddStudents")%><br />
            <input type="submit" value="Добавить">
            <%} %>
        </div>
    </p>
</asp:Content>
