﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Site.Master" Inherits="System.Web.Mvc.ViewPage" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
	Create
</asp:Content>



<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">

    <h2>Создание урока</h2>

    <%: Html.ValidationSummary("Введите корректные данные!") %>

    <% using (Html.BeginForm()) {%>

        <fieldset>

            <p><label for="time">Время:</label></p>
            <p><%= Html.TextBox("time")%>
            <%= Html.ValidationMessage("time", "*") %></p>

            <p><label for="lesson">Предмет:</label></p>
            <p><%= Html.DropDownList("subject", ViewData["Subjects"] as SelectList)%>
            <%= Html.ValidationMessage("subject", "*") %></p>

            <p><label for="teacher">Учитель:</label></p>
            <p><%= Html.DropDownList("teacher", ViewData["Teachers"] as SelectList)%>
            <%= Html.ValidationMessage("teacher", "*")%></p>

            <p><label for="class">Класс:</label></p>
            <p><%= Html.DropDownList("class", ViewData["Classes"] as SelectList)%>
            <%= Html.ValidationMessage("class", "*")%></p>

            <p><input type="submit" value="Создать" /></p>
        </fieldset>
    <% } %>
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="MenuAdds" runat="server">
    <li>
    <%: Html.ActionLink("Расписание", "Index") %> </li>
</asp:Content>

