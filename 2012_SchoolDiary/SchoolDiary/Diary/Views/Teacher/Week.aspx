﻿
<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Site.Master" Inherits="System.Web.Mvc.ViewPage<Diary.Models.TeacherWeek>" %>
<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
	Week
</asp:Content>
<asp:Content ID="ADDMENU" ContentPlaceHolderID="MenuAdds" runat="server">
        <li>
            <%: Html.ActionLink("Создать урок", "CreateLesson") %></li>
        <li>
            <%:Html.ActionLink("Классы", "Classes")%></li>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">

    <h2>Расписание учителя на неделю</h2>
    <table style="border-style: none; width: 100%;">
        <tr>
            <td>
                <% Html.RenderPartial("ViewOneTeacherDay", Model.Monday);%>
            </td>
            <td>
                <% Html.RenderPartial("ViewOneTeacherDay", Model.Thursday);%>
            </td>
        </tr>
        <tr>
            <td>
                <% Html.RenderPartial("ViewOneTeacherDay", Model.Tuesday);%>
            </td>
            <td>
                <% Html.RenderPartial("ViewOneTeacherDay", Model.Friday);%>
            </td>
        </tr>
        <tr>
            <td>
                <% Html.RenderPartial("ViewOneTeacherDay", Model.Wednesday);%>
            </td>
            <td>
                <% Html.RenderPartial("ViewOneTeacherDay", Model.Saturday);%>
            </td>
        </tr>
    </table>

</asp:Content>
