﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Site.Master" Inherits="System.Web.Mvc.ViewPage<IEnumerable<DiaryLogic.Class>>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
    Classes
</asp:Content>
<asp:Content ID="ADDMENU" ContentPlaceHolderID="MenuAdds" runat="server">
        <li>
            <%:Html.ActionLink("Создать класс", "Create", "Class")%></li>
        <li>
            <%:Html.ActionLink("Расписание", "Index")%></li>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <h2>
        Classes</h2>
    Список классов с возможностью создания, изменения.
    <ul>
        <% foreach (var cls in Model)
       { %>
        <li>
            <%:Html.ActionLink(cls.Title.ToString(), "Index", "Class", new { classID = cls.ClassId }, null)%></li>
        <%} %>
    </ul>
</asp:Content>
