﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Site.Master" Inherits="System.Web.Mvc.ViewPage<Diary.Models.StudentQuarter>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
	Quarter
</asp:Content>

<asp:Content ID="ADDMENU" ContentPlaceHolderID="MenuAdds" runat="server">
        <li>
            <%:Html.ActionLink("Расписание на неделю", "Index")%></li>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">

    <h2><%:Model.QuarterName%></h2>
    <table style="border-color: SteelBlue; border-style: ridge; border-width: medium;
        margin-bottom: 10px; width: 48%">
        <tr style="text-align: center; background: SteelBlue; color: White; font-weight: bold;">
            
                        <td style="width: 1px">
                Номер
            </td>
            <td style="width: 120px">
                Предмет
            </td>
            <%foreach (var dt in Model.Dates) {%>
            <td style="width: 1px">
                <%:dt.Day %> <%:dt.ToString("MMM")%>
            </td>
            <% } %>

        </tr>    
        
        <tr>
            <td style="text-align: center; background: SteelBlue; color: White; " >
                1
            </td>
            <td>
                Английский язык
            </td>
            <td>
                ...
            </td>
            <td style="text-align: center">
                н
            </td>
            <td>
                ...
            </td>
            <td>
                ...
            </td>
            <td>
                ...
            </td>
        </tr>
        <tr>
            <td style="text-align: center; background: SteelBlue; color: White; " >
                2
            </td>
            <td>
                Геометрия
            </td>
            <td>
                ...
            </td>
            <td style="text-align: center; background: SteelBlue; color: White; ">
                о
            </td>
            <td>
                ...
            </td>
            <td>
                ...
            </td>
            <td>
                ...
            </td>


        </tr>

        <tr>
            <td style="text-align: center">
                3
            </td>
            <td>
                Физкультура</td>
            <td>
                ...
            </td>
            <td style="text-align: center">
                5
            </td>
            <td>
                ...
            </td>
            <td>
                ...
            </td>
            <td>
                ...
            </td>

        </tr>
        <tr>
            <td style="text-align: center">
                4
            </td>
            <td>
                Русский язык</td>
            <td>
                ...
            </td>
            <td style="text-align: center">
                &nbsp;</td>
                <td>
                ...
            </td>
            <td>
                ...
            </td>
            <td>
                ...
            </td>
        </tr>
        <tr>
            <td style="text-align: center">
                5
            </td>
            <td>
                География</td>
            <td>
                ...
            </td>
            <td style="text-align: center">
                4
            </td>
            <td>
                ...
            </td>
            <td>
                ...
            </td>
            <td>
                ...
            </td>
        </tr>
        <tr>
            <td style="text-align: center">
                6
            </td>
            <td>
                Физика</td>
            <td>
                &nbsp;</td>
            <td style="text-align: center">
                &nbsp;</td>
                <td style="text-align: center">
                &nbsp;</td>
                <td style="text-align: center">
                &nbsp;</td>
                <td style="text-align: center">
                &nbsp;</td>
        </tr>
        <tr>
            <td style="text-align: center">
                7
            </td>
            <td>
                ...
            </td>
            <td>
                ...
            </td>
            <td style="text-align: center">
                ...
            </td>
            <td style="text-align: center">
                &nbsp;</td>
                <td style="text-align: center">
                &nbsp;</td>
                <td style="text-align: center">
                &nbsp;</td>
        </tr>
        <tr>
            <td style="text-align: center">
                8
            </td>
            <td>
                ...
            </td>
            <td>
                ...
            </td>
            <td style="text-align: center">
                ...
            </td>
            <td style="text-align: center">
                &nbsp;</td>
                <td style="text-align: center">
                &nbsp;</td>
                <td style="text-align: center">
                &nbsp;</td>
        </tr>
    </table>
    <br />
   
    
</asp:Content>
