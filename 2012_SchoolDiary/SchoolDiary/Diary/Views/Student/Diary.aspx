﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Site.Master" Inherits="System.Web.Mvc.ViewPage<Diary.Models.StudentWeek>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
    Дневник
</asp:Content>

<asp:Content ID="ADDMENU" ContentPlaceHolderID="MenuAdds" runat="server">
        <li>
            <%:Html.ActionLink("Оценки за четверть", "Quarter")%></li>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <h2>
        Дневник</h2>
    <table style="border-style: none; width: 100%;">
        <tr>
            <td>
                <% Html.RenderPartial("ViewOneStudentDay", Model.Monday);%>
            </td>
            <td>
                <% Html.RenderPartial("ViewOneStudentDay", Model.Thursday);%>
            </td>
        </tr>
        <tr>
            <td>
                <% Html.RenderPartial("ViewOneStudentDay", Model.Tuesday);%>
            </td>
            <td>
                <% Html.RenderPartial("ViewOneStudentDay", Model.Friday);%>
            </td>
        </tr>
        <tr>
            <td>
                <% Html.RenderPartial("ViewOneStudentDay", Model.Wednesday);%>
            </td>
            <td>
                <% Html.RenderPartial("ViewOneStudentDay", Model.Saturday);%>
            </td>
        </tr>
    </table>
</asp:Content>
