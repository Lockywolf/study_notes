﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<Diary.Models.StudentDaySchedule>" %>

<table style="border-color: #5c87b2; border-style: ridge; border-width: medium;
    margin-bottom: 10px; width: 100%">
    <tr style="text-align: center; background: #5c87b2; color: White; font-weight: bold;">
        <td rowspan="9" style="width: 1px">
            <%:Model.DayTitle %>
        </td>
        <td style="width: 1px">
            Урок
        </td>
        <td style="width: 120px">
            Предмет
        </td>
        <td style="">
            Домашнее задание
        </td>
        <td style="width: 1px">
            Оценка
        </td>

	</tr>
        <% foreach (var lesson in Model.Lessons)
          {%>
	<tr>
        <td style="text-align: center"><%:lesson.Number %></td>
		<td><%:lesson.Name %></td>
        <td><%:lesson.Comments%></td>
        <td style="text-align: center"><%:lesson.Mark %></td>
	</tr>
        <%} %>
</table>
