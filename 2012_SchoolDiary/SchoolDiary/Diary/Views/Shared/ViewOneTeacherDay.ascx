﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<Diary.Models.TeacherDaySchedule>" %>
<table style="border-color: #5c87b2; border-style: ridge; border-width: medium;
    margin-bottom: 15px; width: 100%;">
    <tr style="text-align: center; background: #5c87b2; color: White; font-weight: bold;">
    <th  COLSPAN=6 align=center style=" text-align: center; background:#5c87b2;"><%:Model.Date.ToShortDateString()%></th>
    </tr>
    <tr style="text-align: center; background: #5c87b2; color: White; font-weight: bold;">
        <td rowspan="6" style="width: 1px">
            <%:Model.DayTitle %>
        </td>

        <td style="width: 1px">
            Урок
        </td>
        <td style="width: 50px">
            Предмет
        </td>
         <td style="width: 10px">
            Аудитория
        </td>
        <td style="width: 20px">
            Класс
        </td>
        <td style="width: auto">
            Тема урока
        </td>
	</tr>
        <% foreach (var lesson in Model.Lessons)
          {%>
	<tr>
        <td style="text-align: center"><%:lesson.NumberOfLesson %></td>
		<td><%:lesson.NameOfLesson %></td>
        <td style="text-align: center"><%:lesson.numberOfAudience%></td>
        <td><%:lesson.Class %></td>
        <td><%:lesson.Comments %></td>
	</tr>
        <%} %>
</table>