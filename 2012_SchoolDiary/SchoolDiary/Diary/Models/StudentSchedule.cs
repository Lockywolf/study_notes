﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Diary.Models
{
    public class StudentWeek
    {
        public StudentDaySchedule Monday { get; set;}
        public StudentDaySchedule Tuesday { get; set; }
        public StudentDaySchedule Wednesday { get; set; }
        public StudentDaySchedule Thursday { get; set; }
        public StudentDaySchedule Friday { get; set; }
        public StudentDaySchedule Saturday { get; set; }
    }

    public class StudentDaySchedule
    {
        public string DayTitle { get; set; }

        public StudentDaySchedule()
        {
            Lessons = new List<StudentLesson>();
        }
        public DateTime Date {get;set;}

        public List<StudentLesson> Lessons { get; private set; }
    }

    public class StudentLesson
    {
        public string Number { get; set; }
        public string Name { get; set; }
        public string Comments { get; set; }
        public string Mark { get; set; }
    }
}