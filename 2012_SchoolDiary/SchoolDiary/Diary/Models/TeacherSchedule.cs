﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Diary.Models
{
    public class TeacherWeek
    {
        public TeacherDaySchedule Monday { get; set; }
        public TeacherDaySchedule Tuesday { get; set; }
        public TeacherDaySchedule Wednesday { get; set; }
        public TeacherDaySchedule Thursday { get; set; }
        public TeacherDaySchedule Friday { get; set; }
        public TeacherDaySchedule Saturday { get; set; }
    }

    public class TeacherDaySchedule
    {
        public string DayTitle { get; set; }

        public TeacherDaySchedule()
        {
            Lessons = new List<LessonForTeacher>();
        }
        public DateTime Date { get; set; }

        public List<LessonForTeacher> Lessons { get; private set; }
    }

    public class LessonForTeacher
    {
        public string NumberOfLesson { get; set; }
        public string NameOfLesson { get; set; }
        public string numberOfAudience { get; set; }
        public string Class { get; set; }
        public string Comments{ get; set; }
    }
}