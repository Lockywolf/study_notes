﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Diary.Models
{
    /// <summary>
    /// квартальные оценки по всем предметам
    /// </summary>
    public class StudentQuarter
    {
        public StudentQuarter()
        {
            Subjects = new List<SubjectMarks>();
            Dates = new List<DateTime>();
        }

        public string QuarterName { get; set; }

        public List<SubjectMarks> Subjects { get; private set; }

        public List<DateTime> Dates { get; private set; }
    }

    /// <summary>
    /// оценки по предмету
    /// </summary>
    public class SubjectMarks
    {
        Dictionary<DateTime, string> _marks = new Dictionary<DateTime, string>();
        /// <summary>
        /// название предмета
        /// </summary>
        public string SubjectName { get; set; }
        
        public void SetMarkAt(DateTime dt, string mark)
        {
            _marks[dt] = mark;
        }

        public string GetMarkAt(DateTime date)
        {
            if (_marks.ContainsKey(date))
                return _marks[date];

            return string.Empty;
        }
    }
}