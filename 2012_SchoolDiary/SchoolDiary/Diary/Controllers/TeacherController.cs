﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Diary.Models;
using DiaryLogic;

namespace Diary.Controllers
{
    public class TeacherController : Controller
    {
        //
        // GET: /Teacher/

        public ActionResult Index()
        {
            var week = FillTestWeekData();
            return View("Week", week);
            //return View("Week");
        }
        private static void FillLesson(ref TeacherWeek week, LessonForTeacher l)
        {
            week.Monday.Lessons.Add(l);
            week.Tuesday.Lessons.Add(l);
            week.Wednesday.Lessons.Add(l);
            week.Thursday.Lessons.Add(l);
            week.Friday.Lessons.Add(l);
            week.Saturday.Lessons.Add(l);
        }
        private static TeacherWeek FillTestWeekData()
        {
            var week = new TeacherWeek();
            week.Monday = new TeacherDaySchedule()
            {
                Date = DateTime.Today.AddDays(-2),
            };
            week.Tuesday = new TeacherDaySchedule()
            {
                Date = DateTime.Today.AddDays(-1),
            };
            week.Wednesday = new TeacherDaySchedule()
            {
                Date = DateTime.Today,
            };
            week.Thursday = new TeacherDaySchedule()
            {
                Date = DateTime.Today.AddDays(1),
            };
            week.Friday = new TeacherDaySchedule()
            {
                Date = DateTime.Today.AddDays(2),
            };
            week.Saturday = new TeacherDaySchedule()
            {
                Date = DateTime.Today.AddDays(3),
            };
            var subject1 = new LessonForTeacher()
            {
                NumberOfLesson = "1",
                NameOfLesson = "Алгебра",
                numberOfAudience = "каб. 112",
                Class = "4А",
                Comments = "Перемножение дробей"
            };
            var subject2 = new LessonForTeacher()
            {
                NumberOfLesson = "2",
                NameOfLesson = "Геометрия",
                numberOfAudience = "каб. 115",
                Class = "6Б",
                Comments = "Треугольники,теорема Пифагора"
            };
            var subject4 = new LessonForTeacher()
            {
                NumberOfLesson = "4",
                NameOfLesson = "Введение в матанализ",
                numberOfAudience = "каб. 128",
                Class = "11А",
                Comments = "Правила дифференцирования"
            };
            for(int i = 1; i <= 5; i++)
            {
                switch (i)
                {
                    case 1:
                        FillLesson(ref week, subject1);
                        break;
                    case 2:
                        FillLesson(ref week, subject2);
                        break;
                    case 4:
                        FillLesson(ref week, subject4);
                        break;
                    default:
                        FillLesson(ref week, new LessonForTeacher() { NumberOfLesson = i.ToString() });
                        break;
                }
               // week.Monday.Lessons.Add(new LessonForTeacher() { NameOfLesson = i.ToString() });
            }

           


            week.Monday.DayTitle = "П О Н Е Д Е Л Ь Н И К";
            week.Tuesday.DayTitle = "В Т О Р Н И К";
            week.Wednesday.DayTitle = "С Р Е Д А";
            week.Thursday.DayTitle = "Ч Е Т В Е Р Г";
            week.Friday.DayTitle = "П Я Т Н И Ц А";
            week.Saturday.DayTitle = "С У Б Б О Т А";

            return week;
        }

        public ActionResult Classes()
        {
            return View(DiaryDB.Get().Class);
        }

        // GET /Teacher/Create
        public ActionResult CreateLesson()
        {
            ViewData["Subjects"] = new SelectList(DiaryDB.Get().Subjects.Select(x => x.Title));
            ViewData["Teachers"] = new SelectList(DiaryDB.Get().Teachers.Select(x => x.Name));
            ViewData["Classes"] = new SelectList(DiaryDB.Get().Class.Select(x => x.Title));
            
            return View("CreateLesson");
        }

        // POST /Teacher/Create
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult CreateLesson(FormCollection formValues)
        {
            using (var db = DiaryDB.Get())
            {
                Subject sbj;
                Teacher tch;
                Class cls;
                try
                {
                    sbj = db.Subjects.Single(x => x.Title == formValues["subject"]);
                    tch = db.Teachers.Single(x => x.Name == formValues["teacher"]);
                    cls = db.Class.Single(x => x.ClassId == formValues["class"]);
                }
                catch (System.InvalidOperationException)
                {
                    return View("Week");
                }
                
                Lesson lesson = new Lesson()
                {
                    LessonID = Guid.NewGuid().ToString(),
                    DateTime = Convert.ToDateTime(formValues["time"]),
                    Subject = sbj,
                    Teacher = tch
                };

                db.Lessons.InsertOnSubmit(lesson);
                db.SubmitChanges();
            }
            return View("Week");
        }

        public ActionResult DeleteLesson(string lessonID)
        {
            using (var db = DiaryDB.Get())
            {
                var lesson = db.Lessons.Single(x => x.LessonID == lessonID);
                db.Lessons.DeleteOnSubmit(lesson);
                db.SubmitChanges();
            }
            return View("Week");
        }

        public ActionResult EditLesson(string lessonID, FormCollection formValues)
        {
            using (var db = DiaryDB.Get())
            {
                var lesson = db.Lessons.Single(x => x.LessonID == lessonID);
                
                lesson.DateTime = Convert.ToDateTime(formValues["time"]);
                lesson.Subject = db.Subjects.Single(x => x.Title == formValues["title"]);
                lesson.Teacher = db.Teachers.Single(x => x.Name == formValues["teacher"]);

                db.SubmitChanges();
            }
            return View("Week");
        }
    }
}
