﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Diary.Models;

namespace Diary.Controllers
{
    public class StudentController : Controller
    {
        //
        // GET: /Student/

        public ActionResult Index()
        {
            var week = FillTestWeekData();
            return View("Diary", week);
        }

        /// <summary>
        /// наполнение тестовой недели
        /// </summary>
        /// <returns></returns>
        private static StudentWeek FillTestWeekData()
        {
            var week = new StudentWeek();
            week.Monday = new StudentDaySchedule()
            {
                Date = DateTime.Today,
            };
            week.Monday.Lessons.Add(new StudentLesson()
            {   
                Number = "1",
                Name = "Математика",
                Comments = "Контрольная",
                Mark = "5"
            });

            week.Monday.Lessons.Add(new StudentLesson() { Number = "2" });
            week.Monday.Lessons.Add(new StudentLesson() { Number = "3" });
            week.Monday.Lessons.Add(new StudentLesson() { Number = "4" });
            week.Monday.Lessons.Add(new StudentLesson() { Number = "5" });
            week.Monday.Lessons.Add(new StudentLesson() { Number = "6" });
            week.Monday.Lessons.Add(new StudentLesson() { Number = "7" });
            week.Monday.Lessons.Add(new StudentLesson() { Number = "8" });

            week.Tuesday = new StudentDaySchedule()
            {
                Date = DateTime.Today.AddDays(1),
            };


            week.Tuesday.Lessons.Add(new StudentLesson() { Number = "1" });

            week.Tuesday.Lessons.Add(new StudentLesson()
            {
                Number = "2",
                Name = "Русский язык",
                Comments = "Домашнее задание",
                Mark = "3"
            });

            week.Tuesday.Lessons.Add(new StudentLesson() { Number = "3" });
            week.Tuesday.Lessons.Add(new StudentLesson() { Number = "4" });
            week.Tuesday.Lessons.Add(new StudentLesson() { Number = "5" });
            week.Tuesday.Lessons.Add(new StudentLesson() { Number = "6" });
            week.Tuesday.Lessons.Add(new StudentLesson() { Number = "7" });
            week.Tuesday.Lessons.Add(new StudentLesson() { Number = "8" });

            week.Wednesday = new StudentDaySchedule()
            {
                Date = DateTime.Today.AddDays(2),
            };

            week.Wednesday.Lessons.Add(new StudentLesson() { Number = "1" });
            week.Wednesday.Lessons.Add(new StudentLesson() { Number = "2" });

            week.Wednesday.Lessons.Add(new StudentLesson()
            {
                Number = "3",
                Name = "Физика",
                Comments = "",
                Mark = "н"
            });

            week.Wednesday.Lessons.Add(new StudentLesson() { Number = "4" });
            week.Wednesday.Lessons.Add(new StudentLesson() { Number = "5" });
            week.Wednesday.Lessons.Add(new StudentLesson() { Number = "6" });
            week.Wednesday.Lessons.Add(new StudentLesson() { Number = "7" });
            week.Wednesday.Lessons.Add(new StudentLesson() { Number = "8" });
            
            week.Thursday = new StudentDaySchedule()
            {
                Date = DateTime.Today.AddDays(3),
            };
            week.Thursday.Lessons.Add(new StudentLesson() { Number = "1" });
            week.Thursday.Lessons.Add(new StudentLesson() { Number = "2" });
            week.Thursday.Lessons.Add(new StudentLesson() { Number = "3" });
            week.Thursday.Lessons.Add(new StudentLesson() { Number = "4" });
            week.Thursday.Lessons.Add(new StudentLesson() { Number = "5" });
            week.Thursday.Lessons.Add(new StudentLesson() { Number = "6" });
            week.Thursday.Lessons.Add(new StudentLesson() { Number = "7" });
            week.Thursday.Lessons.Add(new StudentLesson() { Number = "8" });
            
            week.Friday = new StudentDaySchedule()
            {
                Date = DateTime.Today.AddDays(4),
            };
            week.Friday.Lessons.Add(new StudentLesson()
            {
                Number = "1",
                Name = "Химия",
                Comments = "Лабораторная работа",
                Mark = "4"
            });

            week.Friday.Lessons.Add(new StudentLesson() { Number = "2" });
            week.Friday.Lessons.Add(new StudentLesson() { Number = "3" });
            week.Friday.Lessons.Add(new StudentLesson() { Number = "4" });
            week.Friday.Lessons.Add(new StudentLesson() { Number = "5" });
            week.Friday.Lessons.Add(new StudentLesson() { Number = "6" });
            week.Friday.Lessons.Add(new StudentLesson() { Number = "7" });
            week.Friday.Lessons.Add(new StudentLesson() { Number = "8" });

            week.Saturday = new StudentDaySchedule()
            {
                Date = DateTime.Today.AddDays(5),
            };

            week.Saturday.Lessons.Add(new StudentLesson() { Number = "1" });
            week.Saturday.Lessons.Add(new StudentLesson() { Number = "2" });
            week.Saturday.Lessons.Add(new StudentLesson() { Number = "3" });
            week.Saturday.Lessons.Add(new StudentLesson() { Number = "4" });
            week.Saturday.Lessons.Add(new StudentLesson() { Number = "5" });
            week.Saturday.Lessons.Add(new StudentLesson() { Number = "6" });
            week.Saturday.Lessons.Add(new StudentLesson() { Number = "7" });
            week.Saturday.Lessons.Add(new StudentLesson() { Number = "8" });


            week.Monday.DayTitle = "П О Н Е Д Е Л Ь Н И К";
            week.Tuesday.DayTitle = "В Т О Р Н И К";
            week.Wednesday.DayTitle = "С Р Е Д А";
            week.Thursday.DayTitle = "Ч Е Т В Е Р Г";
            week.Friday.DayTitle = "П Я Т Н И Ц А";
            week.Saturday.DayTitle = "С У Б Б О Т А";

            return week;
        }

        public ActionResult Diary()
        {
            return View();
        }

        public ActionResult Quarter()
        {
            StudentQuarter q = CreateTestQuarter();
            return View(q);
        }

        private StudentQuarter CreateTestQuarter()
        {
            var q = new StudentQuarter() { QuarterName = "I четверть" };
            DateTime start = DateTime.Today.AddDays(-20);
            DateTime dt = start;
            while (dt < DateTime.Today)
            {
                q.Dates.Add(dt);
                dt = dt.AddDays(1);
            }

            var ruslang = new SubjectMarks() { SubjectName = "Русский язык" };
            var math = new SubjectMarks() { SubjectName = "Математика" };
            q.Subjects.Add(ruslang);
            q.Subjects.Add(math);

            ruslang.SetMarkAt(start, "4");
            math.SetMarkAt(start.AddDays(3), "5");
            return q;
        }

    }
}
