﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Diary.Models;
using DiaryLogic;

namespace Diary.Controllers
{
    public class ClassController : Controller
    {
        //
        // GET: /Class/

        public ActionResult Index(string classID)
        {
            var cls = DiaryDB.Get().Class.SingleOrDefault(x => x.ClassId == classID);
            return View(cls);
        }

        public ActionResult Create()
        {
            return View();
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult AddStudents(FormCollection formValues)
        {
            var students = formValues["AddStudents"]
                .Split(new []{'\r', '\n'}, 
                StringSplitOptions.RemoveEmptyEntries);

            var clsID = formValues["ClassId"];
            using (var db = DiaryDB.Get())
            {
                var newStudents = students.Select(x => new Student()
                {
                    StudentID = Guid.NewGuid().ToString(),
                    classID = clsID,
                    Name = x,
                });
                db.Student.InsertAllOnSubmit(newStudents);
                db.SubmitChanges();

            }
            return RedirectToAction("Index", new { classID = clsID });
        }

        public ActionResult RemoveStudent(string studentID)
        {
            string clsID = string.Empty;

            using (var db = DiaryDB.Get())
            {
                var std = db.Student.Single(x => x.StudentID == studentID);
                clsID = std.classID;

                db.Student.DeleteOnSubmit(std);
                db.SubmitChanges();
            }

            if (string.IsNullOrEmpty(clsID))
                return RedirectToAction("Index", "Teacher");
            return RedirectToAction("Index", new { classID = clsID });
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult Create(FormCollection formValues)
        {
            var newClassName = formValues["ClassName"];
            var newClassID = Guid.NewGuid().ToString();
            using (var db = DiaryDB.Get())
            {
                db.Class.InsertOnSubmit(new DiaryLogic.Class() 
                { 
                    ClassId = newClassID, 
                    Title = newClassName
                });

                db.SubmitChanges();
            }
            return RedirectToAction("Index", new { classID = newClassID });
        }
    }
}
