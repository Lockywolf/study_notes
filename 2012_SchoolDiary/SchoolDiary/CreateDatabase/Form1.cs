﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Data.SqlClient;

namespace CreateDatabase
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
            UpdateConString();
        }

        private void serverName_TextChanged(object sender, EventArgs e)
        {
            UpdateConString();
        }

        private void DBName_TextChanged(object sender, EventArgs e)
        {
            UpdateConString();
        }

        private void UpdateConString()
        {
            var builder = new SqlConnectionStringBuilder();
            builder.DataSource = serverName.Text;
            builder.InitialCatalog = DBName.Text;
            builder.IntegratedSecurity = true;
            conString.Text = builder.ToString();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            using (var db = new DiaryLogic.DiaryClassesDataContext(conString.Text))
            {
                try
                {
                    if (db.DatabaseExists())
                        db.DeleteDatabase();

                    db.CreateDatabase();

                    MessageBox.Show("Готово!");
                }
                catch (System.Exception ex)
                {
                    MessageBox.Show(ex.Message);
                }
            }
        }
    }
}
