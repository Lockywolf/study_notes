﻿namespace teacher_loader
{
    partial class Form1
    {
        /// <summary>
        /// Требуется переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Обязательный метод для поддержки конструктора - не изменяйте
        /// содержимое данного метода при помощи редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.path_to_file_teacher = new System.Windows.Forms.TextBox();
            this.browse_file_teacher = new System.Windows.Forms.Button();
            this.file_load = new System.Windows.Forms.Button();
            this.brouse_db = new System.Windows.Forms.Button();
            this.path_to_db = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.browse_file_parent = new System.Windows.Forms.Button();
            this.path_to_file_parent = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.browse_file_pupil = new System.Windows.Forms.Button();
            this.path_to_file_pupil = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(13, 19);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(126, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Путь к списку учителей";
            // 
            // path_to_file_teacher
            // 
            this.path_to_file_teacher.Location = new System.Drawing.Point(149, 16);
            this.path_to_file_teacher.Name = "path_to_file_teacher";
            this.path_to_file_teacher.Size = new System.Drawing.Size(281, 20);
            this.path_to_file_teacher.TabIndex = 1;
            // 
            // browse_file_teacher
            // 
            this.browse_file_teacher.Location = new System.Drawing.Point(445, 14);
            this.browse_file_teacher.Name = "browse_file_teacher";
            this.browse_file_teacher.Size = new System.Drawing.Size(48, 23);
            this.browse_file_teacher.TabIndex = 2;
            this.browse_file_teacher.Text = "...";
            this.browse_file_teacher.UseVisualStyleBackColor = true;
            this.browse_file_teacher.Click += new System.EventHandler(this.browse_file_teacher_Click);
            // 
            // file_load
            // 
            this.file_load.Location = new System.Drawing.Point(338, 198);
            this.file_load.Name = "file_load";
            this.file_load.Size = new System.Drawing.Size(115, 23);
            this.file_load.TabIndex = 3;
            this.file_load.Text = "Загрузка";
            this.file_load.UseVisualStyleBackColor = true;
            this.file_load.Click += new System.EventHandler(this.file_load_Click);
            // 
            // brouse_db
            // 
            this.brouse_db.Location = new System.Drawing.Point(444, 157);
            this.brouse_db.Name = "brouse_db";
            this.brouse_db.Size = new System.Drawing.Size(48, 23);
            this.brouse_db.TabIndex = 6;
            this.brouse_db.Text = "...";
            this.brouse_db.UseVisualStyleBackColor = true;
            this.brouse_db.Click += new System.EventHandler(this.brouse_db_Click);
            // 
            // path_to_db
            // 
            this.path_to_db.Location = new System.Drawing.Point(146, 159);
            this.path_to_db.Name = "path_to_db";
            this.path_to_db.Size = new System.Drawing.Size(281, 20);
            this.path_to_db.TabIndex = 5;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(13, 162);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(67, 13);
            this.label2.TabIndex = 4;
            this.label2.Text = "Путь к базе";
            // 
            // browse_file_parent
            // 
            this.browse_file_parent.Location = new System.Drawing.Point(445, 55);
            this.browse_file_parent.Name = "browse_file_parent";
            this.browse_file_parent.Size = new System.Drawing.Size(48, 23);
            this.browse_file_parent.TabIndex = 9;
            this.browse_file_parent.Text = "...";
            this.browse_file_parent.UseVisualStyleBackColor = true;
            this.browse_file_parent.Click += new System.EventHandler(this.browse_file_parent_Click);
            // 
            // path_to_file_parent
            // 
            this.path_to_file_parent.Location = new System.Drawing.Point(148, 57);
            this.path_to_file_parent.Name = "path_to_file_parent";
            this.path_to_file_parent.Size = new System.Drawing.Size(281, 20);
            this.path_to_file_parent.TabIndex = 8;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(13, 60);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(134, 13);
            this.label3.TabIndex = 7;
            this.label3.Text = "Путь к списку родителей";
            // 
            // browse_file_pupil
            // 
            this.browse_file_pupil.Location = new System.Drawing.Point(446, 102);
            this.browse_file_pupil.Name = "browse_file_pupil";
            this.browse_file_pupil.Size = new System.Drawing.Size(48, 23);
            this.browse_file_pupil.TabIndex = 12;
            this.browse_file_pupil.Text = "...";
            this.browse_file_pupil.UseVisualStyleBackColor = true;
            this.browse_file_pupil.Click += new System.EventHandler(this.browse_file_pupil_Click);
            // 
            // path_to_file_pupil
            // 
            this.path_to_file_pupil.Location = new System.Drawing.Point(148, 104);
            this.path_to_file_pupil.Name = "path_to_file_pupil";
            this.path_to_file_pupil.Size = new System.Drawing.Size(281, 20);
            this.path_to_file_pupil.TabIndex = 11;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(15, 107);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(127, 13);
            this.label4.TabIndex = 10;
            this.label4.Text = "Путь к списку учеников";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(560, 293);
            this.Controls.Add(this.browse_file_pupil);
            this.Controls.Add(this.path_to_file_pupil);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.browse_file_parent);
            this.Controls.Add(this.path_to_file_parent);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.brouse_db);
            this.Controls.Add(this.path_to_db);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.file_load);
            this.Controls.Add(this.browse_file_teacher);
            this.Controls.Add(this.path_to_file_teacher);
            this.Controls.Add(this.label1);
            this.Name = "Form1";
            this.Text = "Ввод учителей в базу";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox path_to_file_teacher;
        private System.Windows.Forms.Button browse_file_teacher;
        private System.Windows.Forms.Button file_load;
        private System.Windows.Forms.Button brouse_db;
        private System.Windows.Forms.TextBox path_to_db;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button browse_file_parent;
        private System.Windows.Forms.TextBox path_to_file_parent;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Button browse_file_pupil;
        private System.Windows.Forms.TextBox path_to_file_pupil;
        private System.Windows.Forms.Label label4;
    }
}

