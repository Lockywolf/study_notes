#include <stdio.h>


//extern "C" int GetCpuId(unsigned __int32* cpuinfo, char* vendorstr);

int main(int argc,char **argv)
{

   char vendor[13]={0};
   unsigned __int32 flags;

   if (!GetCpuId(&flags, vendor)){
      printf("CpuId values has been successfully retrieved from ASM file.\n");
      printf("Those CpuID values comes from an external ASM file assembled with ml64.exe\n");
      printf("linked and compiled with cl.exe within Visual Studio 2010 Ultimate Edition.\n");
      printf("Flags: %u, Vendor: %s\n", flags, vendor);
   }
   else
   {
      printf("CpuId values cannot be retrieved !!!\n");
   }

   getchar();

   return 0;
}
