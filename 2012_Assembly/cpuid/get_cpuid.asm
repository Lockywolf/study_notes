;GetCpuId on x64 Window 7 Ultimate
   .code
;void getcpuid(unsigned __int32* cpuinfo,char* vendorstr);

GetCpuId   proc
   push   rbp
   mov      rbp,rsp
   mov      r8,rcx     ; capture the first and second arguments into 1->r8 and 2->r9
   mov      r9,rdx  ; cause cpuid will obliterate the lower half of those regs
   ;retrive the vendor string in its 3 parts
   mov      eax,0
   cpuid
   mov      [r9+0],ebx
   mov      [r9+4],edx
   mov      [r9+8],ecx

   mov      eax,1
   cpuid
   mov      [r8],eax
   mov      eax,0
   leave
   ret
GetCpuId   endp

end
