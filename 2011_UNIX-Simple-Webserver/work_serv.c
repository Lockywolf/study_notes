//
// webserver.c
//
// Simple HTTP server by Timur, modified by lockywolf
//

#define _POSIX_SOURCE 1
#define _POSIX_C_SOURCE 200112L
#define _XOPEN_SOURCE 600

#define _XOPEN_SOURCE_EXTENDED 1

#include<stdio.h>
#include<string.h>
#include<stdlib.h>

#include<unistd.h>
#include <time.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <dirent.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>

#define SERVER "webserver/1.0"
#define PROTOCOL "HTTP/1.0"
#define RFC1123FMT "%a, %d %b %Y %H:%M:%S GMT"
#define PORT 2000
#define DIRECTORY "./"
 char directory[256] = DIRECTORY;



char *get_mime_type(char *name)
{
  char *ext = strrchr(name, '.');
  if (!ext) return NULL;
  if (strcmp(ext, ".html") == 0 || strcmp(ext, ".htm") == 0) return "text/html";
  if (strcmp(ext, ".jpg") == 0 || strcmp(ext, ".jpeg") == 0) return "image/jpeg";
  if (strcmp(ext, ".gif") == 0) return "image/gif";
  if (strcmp(ext, ".txt") == 0) return "text/plain";
  if (strcmp(ext, ".png") == 0) return "image/png";
  if (strcmp(ext, ".css") == 0) return "text/css";
  if (strcmp(ext, ".au") == 0) return "audio/basic";
  if (strcmp(ext, ".wav") == 0) return "audio/wav";
  if (strcmp(ext, ".avi") == 0) return "video/x-msvideo";
  if (strcmp(ext, ".mpeg") == 0 || strcmp(ext, ".mpg") == 0) return "video/mpeg";
  if (strcmp(ext, ".mp3") == 0) return "audio/mpeg";
  if (strcmp(ext, ".cgi") == 0) return "cgi";
	
	return ("octet/stream");
  //return NULL;
}

void send_headers(FILE *f, int status, char *title, char *extra, char *mime, 
                  int length, time_t date)
{
  time_t now;
  char timebuf[128];

  fprintf(f, "%s %d %s\r\n", PROTOCOL, status, title);
  fprintf(f, "Server: %s\r\n", SERVER);
  now = time(NULL);
  strftime(timebuf, sizeof(timebuf), RFC1123FMT, gmtime(&now));
  fprintf(f, "Date: %s\r\n", timebuf);
  if (extra) fprintf(f, "%s\r\n", extra);
  if (mime) fprintf(f, "Content-Type: %s\r\n", mime);
  if (length >= 0) fprintf(f, "Content-Length: %d\r\n", length);
  if (date != -1)
  {
    strftime(timebuf, sizeof(timebuf), RFC1123FMT, gmtime(&date));
    fprintf(f, "Last-Modified: %s\r\n", timebuf);
  }
  fprintf(f, "Connection: close\r\n");
  fprintf(f, "\r\n");
}

void send_error(FILE *f, int status, char *title, char *extra, char *text)
{
//	printf("%s\n", extra);
  send_headers(f, status, title, extra, "text/html", -1, -1);
  fprintf(f, "<HTML><HEAD><TITLE>%d %s</TITLE></HEAD>\r\n", status, title);
  fprintf(f, "<BODY><H4>%d %s</H4>\r\n", status, title);
  fprintf(f, "%s\r\n", text);
//	fprintf(f, "%s\r\n", extra);
  fprintf(f, "</BODY></HTML>\r\n");
}

void send_file(FILE *f, char *path, struct stat *statbuf)
{
  char data[4096];
  int n;

  FILE *file = fopen(path, "r");
  printf("%u", file);
  if (!file)
    send_error(f, 403, "Forbidden", NULL, "Access denied.");
  else
  {
    int length = S_ISREG(statbuf->st_mode) ? statbuf->st_size : -1;
    send_headers(f, 200, "OK", NULL, get_mime_type(path), length, statbuf->st_mtime);
    //printf("sending file %s st_size:%d\n", path,statbuf->st_size);
    while ((n = fread(data, 1, sizeof(data), file)) > 0) fwrite(data, 1, n, f);
    fclose(file);
  }
}

int process(FILE *f)
{
  char buf[4096];
  char buf2[4096];
  char *method;
  char *o_path;	
  char *query_string; 
  //char *path;
	char path[4096];;
  char *protocol;
  struct stat statbuf;
  char pathbuf[4096];
  int len;

  if (!fgets(buf, sizeof(buf), f)) return -1;
  //printf("URL: %s", buf);

strcpy(buf2,"1\n");

while(strcmp(buf2,"\r\n")!=0){
  if (!fgets(buf2, sizeof(buf2), f)) return -1;
  //printf("Header: %d %d %d %s", strlen(buf2) ,buf2[0],buf2[1],buf2);
}

  method = strtok(buf, " ");
  o_path = strtok(NULL, " ");
  //printf("suburl: %s\n", o_path);
  //query_string = strtok(NULL, " ");
  strcpy( path, directory );
  //path=0;
  strcat( path, o_path); 
  protocol = strtok(NULL, "\r");
  if (!method || !path || !protocol)
  {
	   send_error(f, 400, "Bad Request", NULL, "Syntax err");
	  return(-1);
  }

  fseek(f, 0, SEEK_CUR); // Force change of stream direction

  if (strcmp(method, "GET") != 0)
    send_error(f, 501, "Not supported", NULL, "Method is not supported.");
  else if (stat(path, &statbuf) < 0)
    //send_error(f, 404, "Not Found", NULL, "File not found.");
    send_error(f, 404, "Not Found", path , "File not found.");
  
  else if (S_ISDIR(statbuf.st_mode))
  {
    len = strlen(path);
    if (len == 0 || path[len - 1] != '/')
    {
      snprintf(pathbuf, sizeof(pathbuf), "Location: %s/", path);
      send_error(f, 302, "Found", pathbuf, "Directories must end with a slash.");
    }
    else
    {
      snprintf(pathbuf, sizeof(pathbuf), "%sindex.html", path);
      if (stat(pathbuf, &statbuf) >= 0)
        send_file(f, pathbuf, &statbuf);
      else
      {
        DIR *dir;
        struct dirent *de;

        send_headers(f, 200, "OK", NULL, "text/html", -1, statbuf.st_mtime);
        fprintf(f, "<HTML><HEAD><TITLE>Index of %s</TITLE></HEAD>\r\n<BODY>", path);
        fprintf(f, "<H4>Index of %s</H4>\r\n<PRE>\n", path);
        fprintf(f, "Name Last Modified Size\r\n");
        fprintf(f, "<HR>\r\n");
        if (len > 1) fprintf(f, "<A HREF=\"..\">..</A>\r\n");

        dir = opendir(path);
        while ((de = readdir(dir)) != NULL)
        {
          char timebuf[32];
          struct tm *tm;

          strcpy(pathbuf, path);
          strcat(pathbuf, de->d_name);

          stat(pathbuf, &statbuf);
          tm = gmtime(&statbuf.st_mtime);
          strftime(timebuf, sizeof(timebuf), "%d-%b-%Y %H:%M:%S", tm);

          fprintf(f, "<A HREF=\"%s%s\">", de->d_name, S_ISDIR(statbuf.st_mode) ? "/" : "");
          fprintf(f, "%s%s", de->d_name, S_ISDIR(statbuf.st_mode) ? "/</A>" : "</A> ");
          if (de->d_reclen < 32) fprintf(f, "%*s", 32 - de->d_reclen, "");

          if (S_ISDIR(statbuf.st_mode))
            fprintf(f, "%s\r\n", timebuf);
          else
            fprintf(f, "%s %10d\r\n", timebuf, statbuf.st_size);
        }
        closedir(dir);

        fprintf(f, "</PRE>\r\n<HR>\r\n<ADDRESS>%s</ADDRESS>\r\n</BODY></HTML>\r\n", SERVER);
      }
    }
  }
  else
  {
	  printf("entering CGI, path=%s\n", path);
	  char* mimetype = get_mime_type(path);
	  printf("mimetype=%s", mimetype );
	  int flag = strcmp( mimetype, "cgi");
	  if(  flag == 0 )
	  {
			  
	  printf("exec cgi %s\n",path );
	  //setenv("QUERY_STRING", query_string,1);
	int pipefd[2];
	pipe(pipefd);
	int temp=fork();
	if( temp ==0 )
	{
		dup2( pipefd[1], 1);
		//printf("aaa\n");
		execl( path, "", (char*)0);
		//printf("bbb\n");
	}
	else
	{
		close(pipefd[1]);
		int n=0;
		char data[4096];
		FILE* r = fdopen(pipefd[0], "r");
		//FILE* f = stdout;
		//printf("bbb\n");
		//n = fread(data, sizeof(char), sizeof(data), r);
		//printf("aaab\n");
		//fwrite(data, sizeof(char), n, f);
		while ((n = fread(data, 1, sizeof(data), r)) > 0) fwrite(data, 1, n, f);
		fclose( r );//stop	
		waitpid(temp, NULL, 0);
	}
	  
	  }
	  else 
	  {
	  //printf("debug\n");
	  send_file(f, path, &statbuf);
	  }
  }
  return 0;
}

int main(int argc, char *argv[])
{
	
  int portnumber = PORT;
  
  if( argc > 1 ){
	  struct stat sb;
	  if (stat(argv[1], &sb) == 0 && S_ISDIR(sb.st_mode))
	  {
            strcpy( directory, argv[1] );
	    strcat( directory, "/");
	  }
	  else
	  {
		  return EXIT_FAILURE;
	  }
  }
  if( argc > 2 ){
	  int prt = strtol( argv[2], NULL, 0);
	  if (( prt > 65535) || ( prt <=1000 ))
	  {
		  return EXIT_FAILURE;
	  }
	portnumber=prt ;
  }
  if( argc > 3 ) return EXIT_FAILURE;
	
  int sock;
  struct sockaddr_in sin;

  sock = socket(AF_INET, SOCK_STREAM, 0);

  int yes=1;
  if( setsockopt( sock, SOL_SOCKET, SO_REUSEADDR, &yes, sizeof(int))==-1) 
  {
	  perror("socket address");
	  return(0);
  }
  
  sin.sin_family = AF_INET;
  sin.sin_addr.s_addr = INADDR_ANY;
  sin.sin_port = htons(portnumber);
  printf("debug: port=%d htons = %d\n", portnumber, htons(portnumber)); 
  
  if( bind(sock, (struct sockaddr *) &sin, sizeof(sin)) == -1 ) 
  {
	  perror("Bind");
	  return 0;
  }

  if( listen(sock, 5)!=0 )
  {
	  perror("error");
	  return 0 ;
  }
  printf("HTTP server listening on port %d at %s\n", ntohs(sin.sin_port), inet_ntoa(sin.sin_addr));

  while (1)
  {
    int s;
    FILE *f;

    s = accept(sock, NULL, NULL);
    if (s < 0) break;

    f = fdopen(s, "r+");
    process(f);
    fclose(f);
  }

  close(sock);
  return 0;
}

//void d_cleanup()
//{
	
	
