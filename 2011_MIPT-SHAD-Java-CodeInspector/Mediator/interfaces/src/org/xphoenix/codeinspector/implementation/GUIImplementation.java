/**
 * Implementation of GUI interface.
 */
package org.xphoenix.codeinspector.implementation;

import java.io.IOException;
import java.util.Date;
import java.util.List;

import org.xphoenix.codeinspector.interfaces.GUI;
import org.xphoenix.codeinspector.models.NoSuchSvnAccountException;
import org.xphoenix.codeinspector.models.NoSuchUserException;
import org.xphoenix.codeinspector.models.SVNCommitDescription;
import org.xphoenix.codeinspector.models.SVNUser;
import org.xphoenix.codeinspector.models.User;
import org.xphoenix.codeinspector.models.UserAlreadyExistsException;

/**
 * @author V. Korchagin
 *
 */
public class GUIImplementation implements GUI {

	StorageBackendFilesystem storage;
	
	public GUIImplementation() throws IOException {
		storage = new StorageBackendFilesystem();
	}

	@Override
	public void createUser(User user) throws UserAlreadyExistsException {
		storage.addUser(user);
	}

	@Override
	public boolean isLoginFree(String login) {
		return storage.isLoginFree(login);
	}

	@Override
	public void AddSvnAccount(User user, SVNUser svnAccount)
			throws NoSuchUserException, NoSuchSvnAccountException {
		try {
			storage.associateSVNUserToUser(user, svnAccount);
		} catch (IOException e) {
			throw new NoSuchSvnAccountException();
		}
	}

	@Override
	public List<SVNUser> getSvnAccounts(User user) throws NoSuchUserException {
		try {
			return storage.getSvnAccounts(user);
		} catch (IOException e) {
			throw new NoSuchUserException();
		} 
	}

	@Override
	public List<SVNCommitDescription> getSvnCommits(SVNUser svnAccount,
			Date from, Date to) throws NoSuchSvnAccountException {
		return storage.getSvnCommits(svnAccount, from, to);
	}

	@Override
	public List<SVNCommitDescription> getSvnCommits(SVNUser SvnAccount,
			long fromRevision, long toRevision)
			throws NoSuchSvnAccountException {
		// Don't have such possibilities yet.		
		return null;
	}

}
