package org.xphoenix.codeinspector.models;

import java.io.Serializable;

public class SVNUser implements Serializable {
    private static final long serialVersionUID = 1L;
    private String name;
    private String password;
    private String repositoryPath;

    public SVNUser(String name, String password, String repositoryPath) {
        this.name = name;
        this.password = password;
        this.repositoryPath = repositoryPath;
    }

    public String getRepositoryPath() {
        return repositoryPath;
    }

    public void setRepositoryPath(String repositoryPath) {
        this.repositoryPath = repositoryPath;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
