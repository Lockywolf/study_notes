package org.xphoenix.codeinspector.interfaces;

import org.xphoenix.codeinspector.models.NoSuchSvnAccountException;
import org.xphoenix.codeinspector.models.SVNCommitDescription;
import org.xphoenix.codeinspector.models.SVNUser;

import java.util.Date;
import java.util.List;


public interface SVNCommitDescriptionProvider {

    public SVNUser getSVNUser(String userName) throws NoSuchSvnAccountException;

    public boolean checkIfUserExists(SVNUser user);

    public List<SVNCommitDescription> getCommitDescriptions(SVNUser author, long fromRevision, long toRevision);

    public List<SVNCommitDescription> getCommitDescriptions(SVNUser author, Date fromDate, Date toDate);

    public List<SVNCommitDescription> getCommitDescriptions(int num, int offset);

    public List<SVNCommitDescription> getAllCommitDescriptions();

}
        
     
