package org.xphoenix.codeinspector.interfaces;

import org.xphoenix.codeinspector.models.SVNCommitDescription;
import org.xphoenix.codeinspector.models.SVNCommitDiff;

import java.util.List;
import java.util.Map;


public interface SVNClient extends SVNCommitDescriptionProvider{
    SVNCommitDiff getDiff(SVNCommitDescription commit);
    Map<SVNCommitDescription,SVNCommitDiff> getDiff(List<SVNCommitDescription> commits);
}
