package org.xphoenix.codeinspector.mediator;


import static akka.actor.Actors.actorOf;
import static akka.actor.Actors.poisonPill;
import static java.util.Arrays.asList;

import akka.actor.Actor;
import akka.actor.ActorRef;
import akka.actor.UntypedActor;
import akka.actor.UntypedActorFactory;
import akka.routing.CyclicIterator;
import akka.routing.InfiniteIterator;
import akka.routing.Routing.Broadcast;
import akka.routing.UntypedLoadBalancer;

import java.util.concurrent.CountDownLatch;


public class Master extends UntypedActor {
    private final CountDownLatch latch;

    private double pi;
    private int nrOfResults;
    private long start;

    private ActorRef router;

    static class Router extends UntypedLoadBalancer {
        private final InfiniteIterator<ActorRef> workers;

        public Router(ActorRef[] workers) {
            this.workers = new CyclicIterator<ActorRef>(asList(workers));
        }

        public InfiniteIterator<ActorRef> seq() {
            return workers;
        }
    }

    public Master(int nrOfWorkers, Class<? extends AbstractWorker> WorkerClass, CountDownLatch latch) {
        this.latch = latch;

        // create the workers
        final ActorRef[] workers = new ActorRef[nrOfWorkers];
        for (int i = 0; i < nrOfWorkers; i++) {
            workers[i] = (ActorRef) actorOf((Class<? extends Actor>) WorkerClass).start();
        }

        // wrap them with a load-balancing router
        router = (ActorRef) actorOf(new UntypedActorFactory() {
            public UntypedActor create() {
                return new Router(workers);
            }
        }).start();
    }

    // message handler
    public void onReceive(Object message) {

        if (message instanceof AbstractMessage) {
            // schedule work

            router.tell(message, getContext());

        } else throw new IllegalArgumentException("Unknown message [" + message + "]");
    }

    public void stop() {
        // send a PoisonPill to all workers telling them to shut down themselves
        router.tell(new Broadcast(poisonPill()));

        // send a PoisonPill to the router, telling him to shut himself down
        router.tell(poisonPill());
    }

    @Override
    public void preStart() {
        start = System.currentTimeMillis();
    }

    @Override
    public void postStop() {
        // tell the world that the calculation is complete
        System.out.println(String.format(
                "\n\tWork time: \t%s millis",
                (System.currentTimeMillis() - start)));
        latch.countDown();
    }
}

abstract class AbstractWorker extends UntypedActor {

    // define the work

    // message handler
    public void onReceive(Object message) {
        if (message instanceof AbstractMessage) {
            AbstractMessage msg = (AbstractMessage) message;

            // perform the work
            System.out.println(String.format(
                    "\n\tMsg recieved by worker \t%s \n", this));

            // reply with the result
            getContext().reply(null);

        } else throw new IllegalArgumentException("Unknown message [" + message + "]");
    }
}

abstract class AbstractMessage {
}
