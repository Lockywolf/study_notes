package org.xphoenix.codeinspector.mediator;

import static akka.actor.Actors.actorOf;
import static akka.actor.Actors.poisonPill;
import static java.util.Arrays.asList;

import static akka.actor.Actors.*;


import akka.actor.ActorRef;
import akka.actor.UntypedActor;
import akka.actor.UntypedActorFactory;
import akka.routing.CyclicIterator;
import akka.routing.InfiniteIterator;
import akka.routing.Routing.Broadcast;
import akka.routing.UntypedLoadBalancer;

import java.util.concurrent.CountDownLatch;


/*Ну, будем считать, что это наши импорты*/
import org.xphoenix.codeinspector.mediator.*;

/*Кстати, а почему мы пишем в иерархии akka.* ?*/

/**
 * Hello world!
 */

/*
 * Что это за файл?
 * Зачем он нужен?
 * Ну, будем считать, что это хелловорлд, так?
 * 
 * Так, эта фигня стартует листенер на 2552 порту(не справшивайте меня, почему 2552)
 * и вроде как должна отдавать запросы Мастеру
 * Только я не понял, у нас Мастер c воркерами в DMZ сидит, или Диспетчер?
 * То есть, в каком месте у нас вся эта многопоточная штука крутится? 
 * Пока она отдает запросы Хелловорлду.
 * 
 */
public class App {
	
	

    public static void main(String[] args) {
    	remote().start("localhost", 2552).register(
    			  "hello-service",
    			   actorOf( HelloWorldActor.class ));
        System.out.println("Hello World!");
    }
}
