package shad.codeinspector.eclipseplugin.manager;

import java.io.File;
import java.util.Date;
import java.util.List;

import shad.codeinspector.eclipseplugin.domain.Feedback;
import shad.codeinspector.eclipseplugin.domain.Message;
import shad.codeinspector.eclipseplugin.domain.User;

public interface IMessageManager {
	List<Message> getMessagesByDate(Date dateBegin, Date dateEnd);

	List<Message> getMessageByAuthor(User author);

	List<Message> getMessageByFile(File file);

	Feedback messageAdd(Message message);

	Feedback messageDel(Message message);

	Feedback messageEdit(Message message);
}
