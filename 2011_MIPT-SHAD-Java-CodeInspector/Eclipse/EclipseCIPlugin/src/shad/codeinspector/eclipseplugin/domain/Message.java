package shad.codeinspector.eclipseplugin.domain;

import java.io.File;
import java.io.Serializable;
import java.util.Date;
import java.util.List;

public class Message implements Serializable {
	private static final long serialVersionUID = 1L;
	private int id;
	private int parentId;

	private File file;
	private int version;
	private List<Integer> lineList;

	private String title;
	private String messageType;
	private String descr;
	private User author;
	private User performer;
	private Date creationDate;
	private boolean isActual;

	public Message(int id, int parentId, File file, int version,
			List<Integer> lineList, String title, String messageType,
			String descr, User author, User performer, Date creationDate,
			boolean isActual) {
		super();
		this.id = id;
		this.parentId = parentId;
		this.file = file;
		this.version = version;
		this.lineList = lineList;
		this.title = title;
		this.messageType = messageType;
		this.descr = descr;
		this.author = author;
		this.performer = performer;
		this.creationDate = creationDate;
		this.isActual = isActual;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getParentId() {
		return parentId;
	}

	public void setParentId(int parentId) {
		this.parentId = parentId;
	}

	public File getFile() {
		return file;
	}

	public void setFile(File file) {
		this.file = file;
	}

	public int getVersion() {
		return version;
	}

	public void setVersion(int version) {
		this.version = version;
	}

	public List<Integer> getLineList() {
		return lineList;
	}

	public void setLineList(List<Integer> lineList) {
		this.lineList = lineList;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getMessageType() {
		return messageType;
	}

	public void setMessageType(String messageType) {
		this.messageType = messageType;
	}

	public String getDescr() {
		return descr;
	}

	public void setDescr(String descr) {
		this.descr = descr;
	}

	public User getAuthor() {
		return author;
	}

	public void setAuthor(User author) {
		this.author = author;
	}

	public User getPerformer() {
		return performer;
	}

	public void setPerformer(User performer) {
		this.performer = performer;
	}

	public Date getCreationDate() {
		return creationDate;
	}

	public void setCreationDate(Date creationDate) {
		this.creationDate = creationDate;
	}

	public boolean isActual() {
		return isActual;
	}

	public void setActual(boolean isActual) {
		this.isActual = isActual;
	}

}