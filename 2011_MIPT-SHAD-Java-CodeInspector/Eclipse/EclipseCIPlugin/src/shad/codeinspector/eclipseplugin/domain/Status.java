package shad.codeinspector.eclipseplugin.domain;

public enum Status {
	OK, WARNING, ERROR, FATAL;
}
