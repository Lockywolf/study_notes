package org.xphoenix.codeinspector.mediator;

import akka.actor.UntypedActor;

class HelloWorldActor extends UntypedActor {
		  public void onReceive(Object msg) {
		    getContext().tryReply(msg + " World");
		  }
		}