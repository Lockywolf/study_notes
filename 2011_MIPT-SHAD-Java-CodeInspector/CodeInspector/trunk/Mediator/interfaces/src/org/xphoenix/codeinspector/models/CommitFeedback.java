package org.xphoenix.codeinspector.models;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

/*		
 * 	id - id сообщения
 * 	parentId - сообщение родитель (если это сообщение - ответ на какое то сообщение
 * 		
 * 	file - объект-файл, к которому относится сообщение
 * 	  dark: предположение что файл будет всегда на локальной фс конечно сильное....
 * 	revision - версия файла
 * 	lineList - строки кода, к которым относится данное сообщение
 * 
 * 	title - заголовок сообщения
 * 	messageType - тип сообщения ()	
 * 	description - текст сообщения
 * 	author - автор сообщения
 *  
 *  creationDate - дата создания
 * 	isActual - актуально?
 * 	
 * 		
 */
public class CommitFeedback implements Serializable {
    private static final long serialVersionUID = 1L;
    private int id;
    private int parentId;

    private int revision;
    private List<Integer> lineList;
    private String fileName;
    private String title;
    //private String messageType;//В каком смысле "тип сообщения"? у нас они уже есть? и почему строка?
    private String description;
    private User author;
    private Date creationDate;
    private boolean isActual;

    public CommitFeedback(int id, int parentId, int revision, List<Integer> lineList,
                          String fileName, String title, String description, User author,
                          Date creationDate, boolean actual) {
        this.id = id;
        this.parentId = parentId;
        this.revision = revision;
        this.lineList = lineList;
        this.fileName = fileName;
        this.title = title;
        this.description = description;
        this.author = author;
        this.creationDate = creationDate;
        isActual = actual;
    }

    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getParentId() {
        return parentId;
    }

    public void setParentId(int parentId) {
        this.parentId = parentId;
    }

    public int getRevision() {
        return revision;
    }

    public void setRevision(int revision) {
        this.revision = revision;
    }

    public List<Integer> getLineList() {
        return lineList;
    }

    public void setLineList(List<Integer> lineList) {
        this.lineList = lineList;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

//	public String getMessageType() {
//		return messageType;
//	}
//
//	public void setMessageType(String messageType) {
//		this.messageType = messageType;
//	}

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public User getAuthor() {
        return author;
    }

    public void setAuthor(User author) {
        this.author = author;
    }

    public Date getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(Date creationDate) {
        this.creationDate = creationDate;
    }

    public boolean isActual() {
        return isActual;
    }

    public void setActual(boolean isActual) {
        this.isActual = isActual;
    }

}
