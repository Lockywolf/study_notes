package org.xphoenix.codeinspector.models;

import java.io.Serializable;
import java.util.Date;

public class SVNCommitDescription implements Serializable {
    private static final long serialVersionUID = 1L;
    private Date date;
    private String logMessage;
    private long revision;
    SVNUser author;


    public SVNCommitDescription(long revision, Date date, String log_message, SVNUser author) {
        this.date = date;
        this.logMessage = log_message;
        this.revision = revision;
        this.author = author;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public String getLogMessage() {
        return logMessage;
    }

    public void setLogMessage(String logMessage) {
        this.logMessage = logMessage;
    }

    public long getRevision() {
        return revision;
    }

    public void setRevision(long revision) {
        this.revision = revision;
    }

    public SVNUser getAuthor() {
        return author;
    }

    public void setAuthor(SVNUser author) {
        this.author = author;
    }

    public String toString() {
        String result = "Repository: " + author.getRepositoryPath() + "\n" +
                "Revision: " + revision + "\n" +
                "Author: " + author.getName() + "\n" +
                "Date: " + date + "\n" +
                "Log msg: " + logMessage + "\n";
        return result;
    }
}

