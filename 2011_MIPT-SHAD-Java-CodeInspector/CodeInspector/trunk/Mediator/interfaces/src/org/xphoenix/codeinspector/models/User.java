package org.xphoenix.codeinspector.models;

import java.io.Serializable;

public class User implements Serializable {
    private static final long serialVersionUID = 1L;
    private String login;
    private String password;
//	private String role;
    //TODO : dark: Should we prefer groups+gid over String roles?
    //TODO : dark: the question is to store or not to store SVNUsers in User

    public User(String login, String password)
    {
    	this.setLogin(login);
    	this.setPassword(password);
    }
    /*
     * added by lockywolf
     */
    
    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

//	public String getRole() {
//		return role;
//	}
//
//	public void setRole(String role) {
//		this.role = role;
//	}

}
