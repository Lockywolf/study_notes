package org.xphoenix.codeinspector.interfaces;

import java.util.Date;
import java.util.List;

import org.xphoenix.codeinspector.models.NoSuchSvnAccountException;
import org.xphoenix.codeinspector.models.NoSuchUserException;
import org.xphoenix.codeinspector.models.SVNCommitDescription;
import org.xphoenix.codeinspector.models.SVNUser;
import org.xphoenix.codeinspector.models.User;
import org.xphoenix.codeinspector.models.UserAlreadyExistsException;
/* dark:  откуда взялся этот файл и что из себя представляет? это вовсе не функции предоставляемые ГУИ, да и ГУИ у нас 
          вроде пока нет. Название не соответсвует содержанию, все функции предоставляются существующими интерфейсами,
          
          прошу до обсуждения этот интерфейс не использовать, так как ясно зачем он.
*/ 
@Deprecated public interface GUI {
	void createUser(User user) throws UserAlreadyExistsException;
	
	boolean isLoginFree(String login);
	
	void AddSvnAccount(User user, SVNUser svnAccount) throws
		NoSuchUserException,
		NoSuchSvnAccountException;

	List<SVNUser> getSvnAccounts(User user) throws NoSuchUserException;
	
	List<SVNCommitDescription> getSvnCommits(SVNUser svnAccount, Date from, Date to)
		throws NoSuchSvnAccountException;
	
	List<SVNCommitDescription> getSvnCommits(SVNUser SvnAccount, long fromRevision, long toRevision)
		throws NoSuchSvnAccountException;
}
