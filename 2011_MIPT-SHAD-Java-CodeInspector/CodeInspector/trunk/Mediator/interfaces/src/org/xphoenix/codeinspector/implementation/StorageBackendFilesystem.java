package org.xphoenix.codeinspector.implementation;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.PrintStream;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map.Entry;
import java.util.Scanner;
import java.util.Set;

import org.xphoenix.codeinspector.interfaces.*;
import org.xphoenix.codeinspector.models.*;


class StorageBackendFilesystem implements Storage
{
	Scanner insUsersFile = null;
	HashMap<String, String> insUsers = null;
	
	StorageBackendFilesystem() throws IOException
	{
		insUsers = new HashMap<String, String>();
		File f = new File("InspectorUsers");
		if( !f.exists() )
		{
			f.createNewFile();
		}
		insUsersFile = new Scanner( f );
		
	}
	
	HashSet<SVNUser> usrSVNAccounts = null;
	
	private void loadInsUsers()
	{
		 while (insUsersFile.hasNextLine()) {
	          String insUser = insUsersFile.nextLine();
	          String temp[] = new String[2];
	          temp = insUser.split(" ");
	          insUsers.put(temp[1], temp[2]);
	      }
	}
	
	public boolean isLoginFree(String login)
	{
		if( insUsers.containsKey(login) )
		{
			return( false );
		}
		return( true );
	}
	
	HashSet<SVNUser> extractSVNusers( String name ) throws IOException
	{
		usrSVNAccounts = new HashSet<SVNUser>();
		File f = new File( name );
		if( !f.exists())
		{
			f.createNewFile();
		}
		Scanner readwriter = new Scanner( f );
		while (readwriter.hasNextLine()) {
	          String insUser = insUsersFile.nextLine();
	          String[] temp = new String[3];
	          temp = insUser.split(" ");
	          SVNUser toadd = new SVNUser(temp[0], temp[1], temp[2] );
	          usrSVNAccounts.add(toadd);
	      }
		return usrSVNAccounts;
	}
	
	public void writeSVNusers( String name, Set<SVNUser> list) throws IOException
	{
		File f = new File( name );
		if( !f.exists() ) f.createNewFile();
		PrintStream out = new PrintStream( name );
		Iterator<SVNUser> myiter = list.iterator();
		while( !myiter.hasNext())
		{
			SVNUser temp = myiter.next();
			out.println(temp.getName() +" " + temp.getPassword() + " " + temp.getRepositoryPath() );
		}
	}
	
	public void associateSVNUserToUser(User user, SVNUser svnUser) throws IOException 
	{
		Set<SVNUser> accs4user = extractSVNusers( user.getLogin() );
		accs4user.add(svnUser);
		writeSVNusers(user.getLogin(), accs4user);
		
	}
	
	public List<SVNUser> getSvnAccounts(User user) throws IOException
	{
		Set<SVNUser> intlist = extractSVNusers( user.getLogin() );
		List<SVNUser> mylist = new LinkedList<SVNUser>();
		Iterator<SVNUser> intiter = intlist.iterator();
		while( intiter.hasNext() )
		{
			mylist.add( intiter.next() );
		}
		return( mylist );
	}
	
	public User loginUser(String login, String password) throws NoSuchUserException
	{
		if( !insUsers.containsKey(login)) throw new NoSuchUserException();
		if( insUsers.get( login ).equals(password))
			return( new User(login, password));
		else throw new Error();//а чего вы хотели? как у нас вообще аутентификация должна работать?
	}
	
	public void addUser(User user) throws UserAlreadyExistsException, FileNotFoundException
	{
		if( insUsers.containsKey(user.getLogin()))
		{
			throw new UserAlreadyExistsException();
		}
		else
		{
			insUsers.put(user.getLogin(), user.getPassword());
			PrintStream out = new PrintStream( "InspectorUsers" );
			Set<Entry<String, String>> towrite = insUsers.entrySet();
			Iterator<Entry<String, String>> myiter = towrite.iterator();
			while( !myiter.hasNext())
			{
				Entry<String,String> temp = myiter.next();
				out.println(temp.getKey() + " " + temp.getValue() );
			}
		}
	}
	
	HashSet<SVNCommitDescription> extractsvnusercommits( String name ) throws IOException, ClassNotFoundException
	{
		HashSet<SVNCommitDescription> usrsvncommits = new HashSet<SVNCommitDescription>();
		File f = new File( "svnuser"+name );
		if( !f.exists())
		{
			f.createNewFile();
		}
		FileInputStream fis = new FileInputStream( "svnuser"+name );
		ObjectInputStream ois = new ObjectInputStream(fis);
		
		usrsvncommits = (HashSet<SVNCommitDescription> ) ois.readObject();
		
		return usrsvncommits;
	}
	
	void writesvncommits( String name, HashSet<SVNCommitDescription> commits) throws IOException
	{
		File f = new File( "svnuser"+name );
		if( !f.exists())
		{
			f.createNewFile();
		}
		FileOutputStream fis = new FileOutputStream( "svnuser"+name );
		ObjectOutputStream ois = new ObjectOutputStream(fis);
		ois.writeObject(commits);
	}
	
	public void addUserCommit(SVNUser user, SVNCommitDescription commitDescription) throws IOException, ClassNotFoundException
	{
		HashSet<SVNCommitDescription> commits = extractsvnusercommits( user.getName() );
		commits.add(commitDescription);
		writesvncommits(user.getName(), commits);
	}
	
	public void addUserCommit(SVNUser user, SVNCommitDescription[] commitDescriptions) throws IOException, ClassNotFoundException
	{
		HashSet<SVNCommitDescription> commits = extractsvnusercommits( user.getName() );
		for( SVNCommitDescription x : commitDescriptions)
		{
			commits.add(x);
		}
	}
	
	public List<SVNCommitDescription> getSvnCommits(SVNUser svnAccount, Date from, Date to) throws IOException, ClassNotFoundException
	{
		HashSet<SVNCommitDescription> commits = extractsvnusercommits( svnAccount.getName());
		List<SVNCommitDescription> toreturn = new LinkedList<SVNCommitDescription>();
		for( SVNCommitDescription x : commits)
		{
			if( x.getDate().after(from) && x.getDate().before(to))
			{
				toreturn.add(x);
			}
		}
		return (toreturn);
	}
	
	
}