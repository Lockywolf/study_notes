package org.xphoenix.codeinspector.interfaces;

import org.xphoenix.codeinspector.models.*;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Date;
import java.util.List;

// TODO: Should it be renamed to SVNCommitDescriptionCache?

/*
 * Message from Lockywolf
 * я пока это писал, мне эклипс на какие-то там эксепшены ругался, ну мя их наверх пробросил.
 * Что с ними вообще делать надо?
 */
public interface Storage extends SVNCommitDescriptionProvider{
    void addUserCommit(SVNUser user, SVNCommitDescription commitDescription) throws IOException, ClassNotFoundException;
    /*
     * implemented
     */
    void addUserCommit(SVNUser user, SVNCommitDescription[] commitDescriptions) throws IOException, ClassNotFoundException;
    /*
     * implemented
     */
    
    void addUser(User user) throws UserAlreadyExistsException, FileNotFoundException;
    /*
     * implemented
     */
    
    
    User loginUser(String login, String password) throws NoSuchUserException;
    /*
     * это изврат какой-то
     * implemented
     */

    void associateSVNUserToUser(User user, SVNUser svnUser) throws IOException;
    /*
     * implemented
     */
    List<SVNUser> getSvnAccounts(User user) throws NoSuchUserException, IOException;
    /*
     * Тут есть мега-вопрос. А почему собственно, List, а не Set?
     * повторяющимхся то аккаунтов нет
     * implemented
     */

    List<SVNCommitDescription> getSvnCommits(SVNUser svnAccount, Date from, Date to)
            throws NoSuchSvnAccountException, IOException, ClassNotFoundException;

    boolean isLoginFree(String login);
    /*
     * implemented
     */
    //List<CommitFeedback> getFeedbacksForCommit(SVNCommitDescription commit);
    /*
     * закомментил. что это вообще такое?
     */

}
