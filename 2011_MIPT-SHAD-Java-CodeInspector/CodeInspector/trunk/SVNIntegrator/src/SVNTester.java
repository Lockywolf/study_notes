import java.util.Vector;

public class SVNTester {
	SVNUser user_ = new SVNUser("artusta", 
			                    "jingoo1e", 
			                    "http://svn.xphoenix.org/repos/CodeInspector/trunk/");
	
	SVNClient client_ = new SVNClient();
	
	public static void main(String[] argc) {
		SVNTester tester = new SVNTester();
		tester.TestPerson();
		tester.TestCommitList();
	}
	
	private void TestPerson() {
		System.out.println("User " + user_.name_ + ": " + client_.CheckUser(user_));
	}

	private void TestCommitList() {
		client_.SetUser(user_);
		Vector<SVNCommit> commits = client_.GetAllCommits();
		for(int i = 0; i < commits.size(); ++i) {                       
		        System.out.println(commits.get(i));
		}	
	}
}
