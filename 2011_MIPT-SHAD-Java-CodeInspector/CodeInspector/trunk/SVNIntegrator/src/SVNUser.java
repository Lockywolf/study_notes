
public class SVNUser {
	public String name_;
	public String password_;
	public String repo_path_;
	
	public SVNUser (String name, String password, String repo_path) {
		name_ = name;
		password_ = password;
		repo_path_ = repo_path;
	}
	
}
