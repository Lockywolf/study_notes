
import java.io.*;
import java.lang.Thread;

import org.tmatesoft.svn.core.SVNException;
import org.xphoenix.shad.svn.daemon.*;

public class Main {
	static String repoPath = "file:///home/saha/shad/jaba/svnrepo/test";
	public static void main(String[] args){
		System.out.println("main()");
		SVNDaemon test = new SVNDaemon(12345);
		Thread forListen = new Thread(test);
		forListen.start();
		HookReviewInfo review;
		while (true) {
			if (test.HasNextReview()) {
				try {
					review = test.getNextReview();
					System.out.println(review.getStartRevision());
					System.out.println(review.getEndRevision());
					
					SVNClient client = new SVNClient();
					client.SetUser((new SVNUser("user", "password", repoPath)));
					String diff = "";
					try {
						diff = GetRepositoryDiff.getRawDiff(client, review.getStartRevision() - 1, review.getEndRevision());
					} catch (SVNException svne) {
						svne.printStackTrace();
					}
					test.shutdown();
					try {
						forListen.join();
					} catch (InterruptedException e) {
						// okay
					}
					System.out.println(diff);
					System.out.println("(Correctly) Terminating...");
					return;
				} catch (NoReviewException e) {
					System.out.println("WTF. This shouldn't happen");
				}
			}
		}
	}

}