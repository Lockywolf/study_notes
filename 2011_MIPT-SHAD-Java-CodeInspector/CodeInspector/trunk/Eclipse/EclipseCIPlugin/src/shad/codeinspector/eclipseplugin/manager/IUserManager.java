package shad.codeinspector.eclipseplugin.manager;

import shad.codeinspector.eclipseplugin.domain.User;

public interface IUserManager {
	boolean login(User user);
}
