package shad.codeinspector.eclipseplugin.domain;

import java.io.Serializable;

public class Feedback implements Serializable {
	private static final long serialVersionUID = 1L;
	private Status state;
	private String descr;

	public Feedback(Status state, String descr) {
		super();
		this.state = state;
		this.descr = descr;
	}

	public Status getState() {
		return state;
	}

	public void setState(Status state) {
		this.state = state;
	}

	public String getDescr() {
		return descr;
	}

	public void setDescr(String descr) {
		this.descr = descr;
	}
}
