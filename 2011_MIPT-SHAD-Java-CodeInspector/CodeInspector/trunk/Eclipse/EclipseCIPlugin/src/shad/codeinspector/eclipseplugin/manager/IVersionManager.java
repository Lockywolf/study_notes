package shad.codeinspector.eclipseplugin.manager;

import java.io.File;
import java.util.List;

import shad.codeinspector.eclipseplugin.domain.Feedback;

public interface IVersionManager {
	Feedback commit(List<File> fileList);

	Feedback update(List<File> fileList);

	Feedback merge(List<File> fileList);

	Feedback revert(List<File> fileList);
}
