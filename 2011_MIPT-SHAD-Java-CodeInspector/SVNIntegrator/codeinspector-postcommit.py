#!/usr/bin/python

# this file should executed from hooks/post-commit

# usage: put to commit message either REVIEW_THIS create a review from only this commit
# or REVIEW_FROM commit_num to form a review from commit range [commit_num..current_commit]
# Examples of commit messages:
#   "This is example commit message
#    REVIEW_THIS"
# OR: "This is another example
#       REVIEW_FROM: 327"

import sys
import re
import subprocess
import time
import socket

SVNLOOK_PATH = "/usr/bin/svnlook"
TEST_PATH = "/home/saha/shad/jaba/"

CODEINSPECTOR_DAEMON_HOST="127.0.0.1"
CODEINSPECTOR_DAEMON_PORT=12345

def main():
    if len(sys.argv) != 3:
        print >> sys.stderr, 'Usage: %s <repos> <revnum>' % sys.argv[0]
        sys.exit(1)

    repos = sys.argv[1]
    revnum = sys.argv[2]

    print repos
    print revnum
    print SVNLOOK_PATH
    # get the log message
    svnlook = [SVNLOOK_PATH, 'log', '-r', revnum, repos]
    log = subprocess.Popen(svnlook, stdout=subprocess.PIPE).communicate()[0]

    start_revision = ""
    # check if review is being created only from this commit
    if log.find("REVIEW_THIS") != -1:
        start_revision = revnum
    else:
        start_revision_match = re.search(r'REVIEW_FROM: *([0-9]+)', log)
        if not start_revision_match:
            # no control string in log message
            sys.exit(0) 
        start_revision = start_revision_match.group(1)

    # get the author
    svnlook = [SVNLOOK_PATH, 'author', '-r', revnum, repos]
    author = subprocess.Popen(svnlook, stdout=subprocess.PIPE).communicate()[0]
    author = author.split('\n')[0]
    author = author.split('@')[0].replace('.','')


    # TODO: add fancy error reporting here
    s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    s.connect((CODEINSPECTOR_DAEMON_HOST, CODEINSPECTOR_DAEMON_PORT))
    s.send(str(start_revision) + '\n' + str(revnum) + '\n')
    s.close()
    
#    f = file(TEST_PATH + "somereview_" + str(time.time()), "w+")
#    print >>f, author
#    print >>f, log
#    print >>f, start_revision
#    print >>f, revnum

    sys.exit(0)

if __name__ == '__main__':
    sys.exit(main())
