package org.xphoenix.shad.svn.daemon;

import java.io.*;
import java.net.*;
import java.util.*; // for list

// listens and process external events for subversion repository
// like user creating a new commit or so
// TODO: shutdown() doensn't work properly because accept() is blocking
public class SVNDaemon implements Runnable {
    private ServerSocket socket;
    private Socket client;
    
    private boolean shutdownFlag = false;
    private int port = 0;
    private int limitReviews;
    
    private LinkedList<HookReviewInfo> reviews = new LinkedList<HookReviewInfo>();
    private Object reviewsMutex = new Object();

    public SVNDaemon(int portToListen) {
        port = portToListen;
    }
    
    // Maybe we should use asynchronous io here...
    public void run() {
        try {
            socket = new ServerSocket(port);
            socket.setSoTimeout(1000); 
            while (true) {
                boolean clientConnected = false;
                while (!clientConnected) {
                    try {
                        client = socket.accept();
                        clientConnected = true;
                    } catch (SocketTimeoutException e) {
                        
                    }
                    if (shutdownFlag) {
                        return;
                    }
                }
                BufferedReader in = new BufferedReader(
                                        new InputStreamReader(
                                        client.getInputStream()));
                int startRevision = 0;
                int endRevision = 0;
                try {
                    startRevision = Integer.parseInt(in.readLine());
                    endRevision = Integer.parseInt(in.readLine());
                    synchronized(reviewsMutex) {
                        if (reviews.size() > limitReviews) {
                            LogError("Too much review in buffer: ignoring another one");
                        } else {
                            reviews.add(new HookReviewInfo(startRevision, endRevision));
                        }
                    }
                } catch (NumberFormatException e) {
                    LogError("Exception occured while reading data from socket: " + e.toString());
                }
                
                client.close();
            }
        } catch (IOException e) {
            
        }
    }
    
    private void LogError(String message)
    {
        System.out.println(message);
    }
    
    public void shutdown() {
        shutdownFlag = true;
    }
    
    public boolean HasNextReview() {
        synchronized(reviewsMutex) {
            return !reviews.isEmpty();
        }
    }
    
    public HookReviewInfo getNextReview() throws NoReviewException {
        // TODO block when list is empty
        synchronized(reviewsMutex) {
            if (!reviews.isEmpty()) { 
                return reviews.poll();
            } else {
                throw new NoReviewException();
            }
        }
    }
    
}