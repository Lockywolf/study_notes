package org.xphoenix.shad.svn.daemon;

public class HookReviewInfo {
    HookReviewInfo(int startRevision, int endRevision) {
        startRev = startRevision;
        endRev = endRevision;
    }
    
    public int getStartRevision() {
        return startRev;
    }
    
    public int getEndRevision() {
        return endRev;
    }
    
    boolean Empty() {
        return startRev == 0 || endRev == 0;
    }
    
    private int startRev;
    private int endRev;
}
