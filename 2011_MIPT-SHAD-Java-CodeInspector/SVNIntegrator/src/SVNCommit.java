
import java.util.Date;

public class SVNCommit {
        public Date date_;
        public String log_msg_;
        public long revision_;
        
        public SVNCommit(long revision, Date date, String log_msg) {
                date_ = date;
                log_msg_ = log_msg;
                revision_ = revision;
        }
        
        public String toString() {
	        String result = "Date: " + date_ + "\n" +
	        			    "Revision: " + revision_ + "\n" +
	        			    "Log msg: " + log_msg_;
	        return result;
        }
}

