import java.util.Collection;

import java.util.Iterator;
import java.util.LinkedList;
import java.util.Set;
import java.util.Vector;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.OutputStream;
import java.io.StringWriter;
import java.nio.CharBuffer;
import java.util.Stack;
import java.util.Queue;


import org.tmatesoft.svn.core.auth.ISVNAuthenticationManager;
import org.tmatesoft.svn.core.internal.io.dav.DAVRepositoryFactory;
import org.tmatesoft.svn.core.io.SVNRepository;
import org.tmatesoft.svn.core.io.SVNRepositoryFactory;
import org.tmatesoft.svn.core.wc.SVNWCUtil;
import org.tmatesoft.svn.core.SVNDirEntry;
import org.tmatesoft.svn.core.SVNLogEntry;
import org.tmatesoft.svn.core.SVNLogEntryPath;
import org.tmatesoft.svn.core.SVNNodeKind;
import org.tmatesoft.svn.core.SVNRevisionProperty;
import org.tmatesoft.svn.core.SVNURL;
import org.tmatesoft.svn.core.SVNException;
import org.tmatesoft.svn.core.wc.SVNClientManager;
import org.tmatesoft.svn.core.wc.SVNDiffClient;
import org.tmatesoft.svn.core.wc.SVNRevision;
import org.tmatesoft.svn.core.wc.SVNWCClient;

//by Elshin Dennis
//First draft - it doesn't return anything, only shows the difference in two trees
//now recursion is off, Queue is being used

public class GetRepositoryDiff
{
	public static void listEntriesForSingleRevision(SVNRepository repository, String path, int revision,
		int status, LinkedList<ModifiedFile> list) throws SVNException
	{
		Queue<String> Paths = new LinkedList<String>();
		Paths.add(path);
		String cur_path;
		while (Paths.size() > 0)
		{
	        cur_path = Paths.remove();
	        
			Collection entries = repository.getDir(cur_path, revision, null, (Collection) null);
	        Iterator iterator = entries.iterator();
	        while (iterator.hasNext())
	        {
	            SVNDirEntry entry = (SVNDirEntry) iterator.next();
	            String filename = new String((cur_path.equals("") ? "" : cur_path + "/") + entry.getName());
	            //System.out.println("/" + (cur_path.equals("") ? "" : cur_path + "/") + entry.getName()
	            //	+ Marker);
	            if (entry.getKind() == SVNNodeKind.DIR)
	            {
	            	Paths.add(filename);
	            }
	            else
	            {
	            	list.add(new ModifiedFile(filename, status));
	            }
	        }
		}
    }

	
	public static void listEntries(SVNRepository repository, int startRevision, int endRevision,
		LinkedList<ModifiedFile> list) throws SVNException
	{
	    String path = new String("");
	    Queue<String> Paths = new LinkedList<String>();
		Paths.add(path);
		while (Paths.size() > 0)
		{
			path = Paths.remove();
			Collection entriesF = repository.getDir(path, startRevision - 1, null, (Collection) null);
		    Collection entriesL = repository.getDir(path, endRevision, null, (Collection) null);
		    Iterator iteratorF = entriesF.iterator();
		    SVNDirEntry entryF = null;
		    SVNDirEntry entryL = null;
		    boolean isCreated[] = new boolean[entriesL.size()];
		    int index;
		    for (index = 0; index < entriesL.size(); ++index)
		    {
		    	isCreated[index] = true;
		    }		            
		       
		    while (iteratorF.hasNext())
		    {
		       	entryF = (SVNDirEntry) iteratorF.next();
		       	Iterator iteratorL = entriesL.iterator();
		       	boolean wasDeleted = true;
		       	index = -1;
		       	while(iteratorL.hasNext())
		       	{
		       		entryL = (SVNDirEntry) iteratorL.next();
		       		++index;
		       		if (entryF.getName().compareTo(entryL.getName()) == 0)
		       		{
		       			String name = new String((path.equals("") ? "" : path + "/") + entryF.getName());
		       			//System.out.print(filename);
			        	if (entryL.getRevision() >= startRevision)
			       		{
			        		//System.out.println(" - MODIFIED");
			        		if (entryF.getKind() == SVNNodeKind.FILE)
			        		{
			        			list.add(new ModifiedFile(name, ModifiedFile.Modified));
			        		}
			        	}
				       	else
				       	{
				       		//System.out.println();
				       	}
				       	if (entryF.getKind() == SVNNodeKind.DIR)
				       	{
				       		Paths.add(name);
				       		//System.out.println();
					    }
				       	else
				       	{
				       		list.add(new ModifiedFile(name, ModifiedFile.Modified));
				       	}
					    wasDeleted = false;
					    isCreated[index] = false;
					    break;
			        }
			    }
			    if (wasDeleted)
			    {
			    	//System.out.println("/" + (path.equals("") ? "" : path + "/")
			    	//	+ entryF.getName() + " - DELETED");
			        String name = new String((path.equals("") ? "" : path + "/") + entryF.getName());
			        //prints everything in deleted directory
			        if (entryF.getKind() == SVNNodeKind.DIR)
			        {
					    listEntriesForSingleRevision(repository, name, startRevision - 1,
					    	ModifiedFile.Deleted, list);
					    //System.out.println();
			        }
			        else
			        {
			        	list.add(new ModifiedFile(name, ModifiedFile.Deleted));
			        }
			    }
			}
			            
			Iterator iteratorL = entriesL.iterator();
			for (index = 0; index < entriesL.size(); ++index)
			{
				entryL = (SVNDirEntry) iteratorL.next();
			    if (isCreated[index])
			    {
			    	//System.out.println("/" + (path.equals("") ? "" : path + "/")
			    	//	+ entryL.getName() + " - ADDED");
			    	String name = new String((path.equals("") ? "" : path + "/") + entryL.getName());
			    	
			    	//prints everything in added directory
				    if (entryL.getKind() == SVNNodeKind.DIR)
				    {
					    listEntriesForSingleRevision(repository, name, endRevision, ModifiedFile.Added, list);
					    //System.out.println();
				    }
				    else
				    {
				    	list.add(new ModifiedFile(name, ModifiedFile.Added));
				    }
			    }
			}
		}
		
		//return list;
	}

	public static LinkedList<ModifiedFile> getDiff(SVNClient client, int startRevision, int endRevision)
	{
		DAVRepositoryFactory.setup();
		LinkedList<ModifiedFile> ModifiedFiles = new LinkedList<ModifiedFile>();
		try
		{
            SVNRepository repository = SVNRepositoryFactory.create(SVNURL.parseURIEncoded(client.user_.repo_path_));
            ISVNAuthenticationManager authManager = SVNWCUtil.createDefaultAuthenticationManager(client.user_.name_,
            	client.user_.pass_);
            repository.setAuthenticationManager(authManager);
            System.out.println("Repository Root: " + repository.getRepositoryRoot(true));
            System.out.println( "Repository UUID: " + repository.getRepositoryUUID(true));
            SVNNodeKind nodeKind = repository.checkPath("",  -1);
            if (nodeKind == SVNNodeKind.NONE)
            {
            	System.err.println("There is no entry at '" + client.user_.repo_path_ + "'.");
            	System.exit(1);
            }
            else if (nodeKind == SVNNodeKind.FILE)
            {
            	System.err.println("The entry at '" + client.user_.repo_path_
            		+ "' is a file while a directory was expected.");
            	System.exit(1);
            }
            
            listEntries(repository, startRevision, endRevision, ModifiedFiles);
		}
        catch (SVNException svne)
        {
        	System.out.println(svne);
        }
		return ModifiedFiles;
	}

}
