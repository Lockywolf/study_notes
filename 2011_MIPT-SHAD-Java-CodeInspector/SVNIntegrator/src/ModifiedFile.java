public class ModifiedFile {
	private String name;
	private int status;
	public static int Deleted = 0;
	public static int Modified = 1;
	public static int Added = 2;
	
	ModifiedFile (String name, int status)
	{
		this.name = name;
		this.status = status; 
	}
	
	public String getFileName ()
	{
		return name;
	}
	
	public int getStatus ()
	{
		return status;
	}
	
}
