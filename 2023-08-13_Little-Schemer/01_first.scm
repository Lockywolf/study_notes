(display "hello")

(define atom?
  (lambda (x)
    (and (not (pair?
           x))
       (not (null?
           x)))))

(atom? 'atom)

(list? '(atom))
