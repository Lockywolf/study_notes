(define align
  (lambda (pora)
    (cond
     ( (atom? pora) pora)
     ( (a-pair? (first pora) )
       (align (shift pora)))
     (else ( build (first pora)
             ( align (second pora ) ) ) ) ) ) )

(define a-pair?
  (lambda (x)
    (cond
     ( (atom? x) #f )
     ( (null? x) #f )
     ( (null? (cdr x)) #f )
     ( (null? (cdr (cdr x))) #t )
     (else #f ) ) ))

(define atom?
  (lambda (x)
    (and (not (pair?
           x))
       (not (null?
           x)))))
(define first
  (lambda (p)
    ( cond
      (else ( car p)))))
(define second
  (lambda (p)
    (cond
     (else ( car ( cdr p ) ) ) ) ) )
(define build
  (lambda (sl s2 )
    (cond
     ( else ( cons sl
              ( cons s2 (quote ( ) ) ) ) ) ) ) )

(align 'a)
(align '(a (b c)))

(define weight*
  (lambda (pora)
    (cond
     ((atom? pora) 1)
     (else
      (+ (* (weight* (first pora)) 2)
         (weight* (second pora ) ) ) ) ) ) )

(define revpair
  (lambda (pair)
    (build (second pair) (first pair))))

(define weight*
  (lambda (pora)
    (cond
     ( (atom? pora) 1)
     (else
      (+ (*  ( weight* (first pora) ) 2)
        ( weight* (second pora ) ) ) ) ) ) )

(define shuffle
  (lambda (pora)
    (cond
     ( ( atom? pora) pora)
     ( ( a-pair? (first pora))
       ( shuffle ( revpair pora)))
     (else ( build (first pora)
             ( shuffle ( second pora) ) ) ) ) ) )
