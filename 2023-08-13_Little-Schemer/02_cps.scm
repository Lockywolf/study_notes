#!/usr/bin/chibi-scheme

(import (scheme small))

(define multirember&co
  (lambda ( a lat col)
    (display "run multirember&co:lat=") (display lat) (newline)
    (cond
     ((null? lat)
       (col (quote ()) (quote ())))
     ((eq? ( car lat) a)
       (multirember&co a
         (cdr lat)
         (lambda (newlat seen)
           (display "run lambda 1:")
           (display "")
           (newline)
           (col newlat
             (cons ( car lat) seen)))))
     (else
       (multirember&co a
         (cdr lat)
         (lambda (newlat seen)
           (display "run lambda 2:")
           (newline)
           (col (cons (car lat) newlat)
                seen)))))))

(define a-friend
  (lambda (a y)
    (display "run a-friend") (newline)
    (null? y)))

(multirember&co 'tuna '(tuna and fish and tuna venison) a-friend)
